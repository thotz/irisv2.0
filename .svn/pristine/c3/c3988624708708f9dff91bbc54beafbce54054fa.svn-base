@charset "utf-8";
/* CSS Document */

/* Fonts */
@font-face{font-family: OpenSansLight; src: url('../fonts/open-sans/OpenSans-Light.ttf');}              /* light */
@font-face{font-family: OpenSansRegular; src: url('../fonts/open-sans/OpenSans-Regular.ttf');} 		    /* regular */
@font-face{font-family: OpenSansSemibold; src: url('../fonts/open-sans/OpenSans-Semibold.ttf');} 		/* semibold */
@font-face{font-family: OpenSansBold; src: url('../fonts/open-sans/OpenSans-Bold.ttf');} 				/* bold */
@font-face{font-family: OpenSansExtraBold; src: url('../fonts/open-sans/OpenSans-ExtraBold.ttf');} 	    /* extra bold */

#page-wrapper {
	width: 100%;
	margin: 0;
	padding: 0;
}

.tooltip-inner {
    min-width: 100px;
    width: auto;
    max-width: auto;
}

/* Header */
#header {
	background-color: #ececec;
	width: 100%;
	border-top: 5px solid #003366;
}

/* Body */
body {
	background-color: #e4e4e4;
    font-family: OpenSansRegular;
    font-size: 13px;
    line-height: 16px;
}

/* Header */
.main-header{
    position: absolute;
    width:100%;
    border-bottom: 1px solid #f1f1f1;
}

.main-header .logo {
    width: 190px;
    background-color:#e0e0e0;
    height: 65px;
    border-top:5px solid #005f8d;
	/*border-bottom: 1px solid #f1f1f1;*/
}

.main-header .logo .logo-lg{
    margin: 5px 0 0 0;
}

.main-header .logo .logo-mini{
    margin: 15px 0 0 0;
}

.main-header .navbar {
	/*border-bottom: 1px solid #f1f1f1;*/
    z-index: 1000;
}

/* Navigation */
.main-header>.navbar {
    margin-left: 190px;
}

.main-header .navbar .page-title {
    font-family: OpenSansRegular;
    font-size: 16px;
    line-height: 18px;
    color: #005f8d;
    display: inline-block;
    float: left;
    width: 45%;
    margin-top: 20px;
}

.main-header .sidebar-toggle {
    font-size: 18px;
    line-height: 1.5em;
    color:#666666;
    padding:16px 20px;
    text-shadow: 0 1px 0 #ffffff;
}

.navbar-custom-menu .navbar-nav i.fa {
    font-size: 18px;
    line-height: 1.5em;
    color: #666666;
}

.navbar-nav>li>a:hover {
    background: #e0e0e0;
}

.navbar-nav>li>a:hover>.fa {
    color: #0099cc !important;
}

.navbar-nav>li.emergency-btn>a:hover>.fa {
    color: #f39c12 !important;
}

.navbar-nav>li.alert-btn>a:hover>.fa {
    color: #dd4b39 !important;
}

/* User Menu */

.navbar-custom-menu>.navbar-nav>li>.dropdown-menu {
    position: absolute;
    right: 0;
    left: auto;
    border: 1px solid #ffffff;
    border-radius: 0;
}

.navbar-nav>.user-menu .user-image {
    margin-top: -13px;
    width: 45px;
    height: 45px;
    border: 2px solid #ffffff;
    border-radius: 23px;
}

.navbar-nav>.user-menu>.dropdown-menu {
    width: 225px;
}

.navbar-nav>.user-menu>.dropdown-menu>li.user-header {
    padding-bottom: 10px;
    height: auto;
}

.navbar-nav>.user-menu>.dropdown-menu>li.user-header>img{
    border-color: #ffffff;
}

.navbar-nav>.user-menu>.dropdown-menu>li.user-header>p {
    font-family: OpenSansLight;
    font-size: 16px;
    line-height: 16px;
    color: #ffffff;
}

.navbar-nav>.user-menu>.dropdown-menu>li.user-header>p>small {
    font-size: 9px;
    color: #ffffff;
}

.navbar-nav>.user-menu>.dropdown-menu>.user-footer {
    padding: 0;
}

.navbar-nav>.user-menu>.dropdown-menu>.user-footer>.btn {
    margin: 0;
    font-size: 13px;
    font-family: OpenSansRegular;
    padding: 5px;
}

/* Sidebar */
.sidebar {
    padding-bottom: 20px;
}

.main-sidebar, .left-side {
    padding-top: 65px;
    width: 190px;
}

.sidebar-form, .sidebar-menu>li.header {
    background-color: #005f8d;
    color: #ffffff;
    font-family: OpenSansRegular;
    font-size: 11px;
    border: 0;
}

.sidebar-menu>li {
    background: #e1e1e1;
    border-top: 1px solid #e9e9e9;
    /*border-bottom: 1px solid #cfcfcf;*/
}

.sidebar-menu>li.active {
    background-image:url('../images/sidebar-active-bg.png');
    background-repeat: repeat-y;
    background-position: top left;
    color: #005f8d;
}

.sidebar-menu>li.active>a {
    color: #005f8d;
}

.sidebar-menu>li>a {
    color: #333333;
    font-family: OpenSansRegular;
    font-size: 13px;
    padding: 10px 5px 10px 15px;
    border-bottom: 1px solid #cfcfcf;
    text-shadow: 0 1px 0 #ececec;
}

.sidebar-menu>li>a>.fa{
    font-size: 14px;
}

.sidebar-menu .treeview-menu {
    padding:5px 0 5px 5px;
}

.sidebar-menu li>.treeview-menu {
    background-color: #dddddd;
}

.sidebar-menu .treeview-menu>li>a {
    font-family: OpenSansRegular;
    font-size: 13px;
    color: #666666;
    text-shadow: 0 1px 0 #ececec;
}

.sidebar-menu .treeview-menu>li>a:hover{
    color: #0099cc;
}

.sidebar-menu .treeview-menu .treeview-menu {
    margin-left:-10px;
    background-color: #d4d4d4;
}

/* Content */
.content-wrapper, .right-side, .main-footer {
    margin-left: 190px;
}

.content-wrapper{
    background-image: url('../images/background.jpg');
    background-position: center top;
    background-repeat: no-repeat;
    background-color: #eaeaea;
    margin-top: 66px;
}

.content-header{
    margin-top: 50px;
    margin-bottom: 35px;
}

.content-header>h1 {
    font-family: OpenSansLight;
    font-size: 48px;
    margin-top: 35px;
    color: #ffffff;
}

.content-header>h1>small {
    font-family: OpenSansRegular;
    font-size: 14px;
    color: #ffffff;
}

.box {
    background-color: #ffffff;
    border-radius: 0;
    border-top: 3px solid #0099cc;
}

.box-header>.fa,
.box-header>.glyphicon,
.box-header>.ion,
.box-header .box-title {
    font-family: OpenSansLight;
    font-size: 24px;
    color: #333333;
}

/* Footer */
.main-footer {
    background-color: #dcdcdc;
    font-size: 10px;
    line-height: 12px;
}

.main-footer img.ewpci-logo {
    float: left;
    vertical-align: middle;
    margin-right: 10px;
}

.main-footer img.iris-logo {
    float: right;
    vertical-align: middle;
    margin-left: 10px;
}

@media (min-width: 768px){
    
    /* Navigation */
    .navbar-nav>li>a {
        padding: 16px 15px;
        text-shadow: 0 1px 0 #ffffff;
    }
    
    .navbar-nav>li.user-menu>a {
        padding: 20px 15px;
        font-family: OpenSansRegular;
        font-size: 13px;
        color: #005f8d;
    }
    
    /* Sidebar */
    .sidebar {
        background-color: #e4e4e4;
    }
    
    .sidebar-mini.sidebar-collapse .main-header .logo {
        width: 45px;
        padding: 0;
    }
    
    .sidebar-mini.sidebar-collapse .main-header .navbar {
        margin-left: 45px;
    }
    
    .sidebar-mini.sidebar-collapse .main-sidebar {
        -webkit-transform: translate(0, 0);
        -ms-transform: translate(0, 0);
        -o-transform: translate(0, 0);
        transform: translate(0, 0);
        width: 45px!important;
        z-index: 850;
    }
    
    .sidebar-mini.sidebar-collapse .sidebar-menu>li:hover>a>span:not(.pull-right),
    .sidebar-mini.sidebar-collapse .sidebar-menu>li:hover>.treeview-menu {
        margin-left: -5px;
        height:auto;
    }
    
    .sidebar-mini.sidebar-collapse .sidebar-menu > li > .treeview-menu {
        padding-top: 5px;
        padding-bottom: 5px;
        border-radius: 0;
    }
    
    .sidebar-mini.sidebar-collapse .sidebar-menu > li > a > span{
        padding: 0; !important;
        background-color: #e1e1e1 !important;
        line-height: 16px !important;
        color: #333333 !important;
        font-family: OpenSansRegular !important;
        font-weight: normal !important;
        border-radius: 0 !important;
    }
    
    .sidebar-mini.sidebar-collapse .sidebar-menu > li > .treeview-menu{
        margin-top: -11px;
        border-top: 1px solid #cfcfcf;
    }
    
    .sidebar-mini.sidebar-collapse .sidebar-menu > li:hover > a > span:not(.pull-right),
    .sidebar-mini.sidebar-collapse .sidebar-menu > li:hover > .treeview-menu {
        display: block!important;
        position: absolute;
        width: 180px;
        left: 50px;
        float:left;
    }
    
    .sidebar-mini.sidebar-collapse .sidebar-menu>li:hover>a>span {
        padding:9px 5px 10px 20px;
    }
    
    .sidebar-mini.sidebar-collapse .content-wrapper,
    .sidebar-mini.sidebar-collapse .right-side,
    .sidebar-mini.sidebar-collapse .main-footer {
        margin-left:45px !important;    
    }
}












/* Large */
@media(min-width:1200px){
	
	
	
}

/* Medium */
@media(min-width:992px) and (max-width:1199px){
	
	
	
}

@media(max-width:1042px){
    .main-header .navbar .page-title {
        width: 25%;
        margin-top:10px;
    }
}

/* Small */
@media(min-width:768px) and (max-width:991px){
	
	
	
}

@media(max-width:889px){
    .main-header .navbar .page-title {
        display: none;
    }
}

/* Extra Small */
@media(max-width:767px){
	
    .main-header, .main-header .logo {
        width: 100%;
        height: 50px;
        font-family: OpenSansRegular;
        font-size: 18px;
        line-height: 36px;
    }
    
    .main-header, .main-header .logo img {
        vertical-align: top;
    }
    
    .main-header .navbar {
        width: 100%;
        border-top: 0 !important;
        margin-left: 0;
    }
    
    .main-header .sidebar-toggle {
        padding: 10px 20px;
    }
    
    .navbar-custom-menu .navbar-nav>li>a {
        padding-top: 9px;
        padding-bottom: 9px;
    }
    
    .navbar-nav>.user-menu .user-image {
        width: 30px;
        height: 30px;
        margin-top: 0;
    }
    
    .main-header>.navbar {
        min-height: auto !important;
        
    }
    
    .content-wrapper, .right-side, .main-footer {
        margin-left: 0;
    }
    
    .sidebar-open .content-wrapper, .sidebar-open .right-side, .sidebar-open .main-footer {
        -webkit-transform: translate(190px, 0);
        -ms-transform: translate(190px, 0);
        -o-transform: translate(190px, 0);
        transform: translate(190px, 0);
    }
    
    .sidebar-open .main-sidebar, .sidebar-open .left-side {
        padding-top: 98px;
    }
}

@media(max-width:481px){
    
    .main-header .logo {
        width: 100%;
        height: 50px;
        font-size: 0;
    }
    
}