
* personal table - Add id field auto increment

CREATE OR REPLACE VIEW view_personal AS
select p.id,p.status,p.applicant_id,p.lname,p.fname,p.mname
from personal as p
where 1
group by p.applicant_id
order by p.lname,p.fname,p.mname,p.applicant_id;


CREATE OR REPLACE VIEW view_initial_lineup as 
select l.line_up_id, l.applicant_id, mr.manpower_rid, mr.ref_no, mrpo.mr_pos_id, mrpo.mr_pos_rso, po.position_id, po.name, p.status, l.for_confirm_mode
from line_up as l
left join personal as p on p.applicant_id = l.applicant_id 
left join manpower_r as mr on mr.manpower_rid = l.manpower_rid
left join mr_position as mrpo on mrpo.mr_pos_id = l.mr_pos_id
left join positions as po on po.position_id = mrpo.position_id
where 1 
and p.status not in('DEADFILE','DEPLOYED','OPERATIONS','ON POOL')
and l.for_confirm_initial = 'yes'
and l.mob_result = 'AV'
and l.manpower_rid in (select manpower_rid from manpower_r where status = 'Active' and ref_no != '' and mrpriority = 1)
and l.mr_pos_id in (select mr_pos_id from mr_position where status_mr = 'Active')
group by l.line_up_id


CREATE OR REPLACE VIEW view_max_of_cvlocation as
select max(x.cv_location_id) as cv_location_id, x.applicant_id, p.status, max(x.status) as cv_status, x.user_id, x.user_id1, x.user_id2
FROM cv_location as x
join personal as p on p.applicant_id = x.applicant_id
where 1 group by x.applicant_id