<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


	function get_age($birthday) 
	{
		//for report_applicantlist
		//got code from phpmanual
		if (!@ereg ("([0-9]{4})-([0-9]{1,2})-([0-9]{1,2})", $birthday, $regs)) { return false;} // Geb-Datum nicht korrekt bergeben
		$age = date("Y") - $regs[1];
		if ($regs[2] > date("m")) {$age--;}
		elseif ($regs[2] == date("m")) {
			if ($regs[3] > date("d")) {$age--;}
		}
		if($age<100)
			return $age;
			else
				return;
	} // Ende function get_age
	
	/* check for DEADFILE, if morethan 360-days of last update/last reported/apply_date create new computer number */
	function is_deadfile_new_computer($applicant_id)
	{
		$return_data = get_nodays_lastupdate($applicant_id, "is_deadfile_new_computer");
		$nodays = $return_data['nodays'];
		$status = $return_data['status'];
	
		$result = false;
		//if ($status == 'DEADFILE' && $nodays > 360) {
		if (($status == 'DEADFILE' || $status == 'WORKERS IN ABROAD') && $nodays > 360) {
			$result = true;
		}
		return $result;
	}
	
	function check_medical_unfit($applicant_id)
	{
		$CI =& get_instance();
		
		/* check for UNFIT, please reffer to mam ARA / RSO. */
		$query = $CI->db->query("select m.status as med_status,p.*
				from personal as p
				left join medical as m ON m.applicant_id = p.applicant_id
				where p.applicant_id = '$applicant_id'");
		$result = $query->row_array();
	
		$information = false;
		if (is_array($result) && count($result) > 0) {
			if ($result['med_status'] && $result['status'] == 'DEADFILE' && strtolower($result['med_status']) == 'unfit') {
				$information = true;
			}
		}
		return $information;
	}
	
	function get_nodays_lastupdate($applicant_id, $what="") 
	{
		$CI =& get_instance();
		
		$sql_applicant_dates = "select x.*,p.apply_date,p.date_edit,p.date_reported,p.create_date,p.status
								from personal as p
								left join (select * from logs as l where l.applicant_id = '".$applicant_id."' order by l.time desc limit 1) as x on x.applicant_id = p.applicant_id
								where 1
								and p.applicant_id = '".$applicant_id."'";
		$query = $CI->db->query($sql_applicant_dates);
		$result = $query->row_array();
	
		$mostRecent= 0;
		if (is_array($result) && count($result) > 0) {
			$my_dates = array();
			if ($what == 'is_deadfile_new_computer') {
				$my_dates[] = $result['apply_date'];
				$my_dates[] = $result['date_reported'];
			} else {
				$my_dates[] = $result['time'];
				$my_dates[] = $result['apply_date'];
				$my_dates[] = $result['date_edit'];
				$my_dates[] = $result['date_reported'];
			}
	
			/* get recent date from [ ARRAY DATES ] */
			foreach($my_dates as $date){
				$curDate = strtotime($date);
				if ($curDate > $mostRecent) {
					$mostRecent = $curDate;
				}
			}
		}
	
		$return_data = array();
		if ($mostRecent != 0) {
			$date_recent = date("m-d-Y", $mostRecent); //"07-11-2003";
			$today = date ("m-d-Y"); //"09-04-2004";
			$nodays = dateDiff("-", $today, $date_recent);
			$return_data['nodays'] = $nodays;
			$return_data['status'] = $result['status'];
			$return_data['apply_date'] = $result['apply_date'];
			$return_data['date_reported'] = $result['date_reported'];
		} else {
			$nodays = false;
			$return_data['nodays'] = $nodays;
		}
	
		return $return_data;
	}
	
	function dateDiff($dformat, $endDate, $beginDate)
	{
		$date_parts1=explode($dformat, $beginDate);
		$date_parts2=explode($dformat, $endDate);
		$start_date=gregoriantojd($date_parts1[0], $date_parts1[1], $date_parts1[2]);
		$end_date=gregoriantojd($date_parts2[0], $date_parts2[1], $date_parts2[2]);
		return $end_date - $start_date;
	}
	
	function get_applicant_id($purpose_id) 
	{
		$CI =& get_instance();
		
		$query = $CI->db->query("select max(applicant_id) from personal where left(applicant_id,1) = '".makeletter($purpose_id)."' and right(applicant_id,2) = '".substr(date("Y"),-2)."'");
		$result = $query->row_array();

		if(count($result) && $result['max(applicant_id)']) {
			$comp = substr($result['max(applicant_id)'],2,-3) + 1;
			if(strlen($comp)=='1') { $comp = "000".$comp; }
			if(strlen($comp)=='2') { $comp = "00".$comp; }
			if(strlen($comp)=='3') { $comp = "0".$comp; }
			if(strlen($comp)=='4') { $comp = $comp; }
			$comp = makeletter($purpose_id)."-".$comp."-".substr(date("Y"),-2);
		} else {
			$comp = makeletter($purpose_id)."-0001-".substr(date("Y"),-2);
		}
		return $comp;
	}
	
	function makeletter($purpose_id='') 
	{
		$this_month = date("m");
		switch ($purpose_id) {
			case '1':
				if($this_month=='01') return 'M';
				if($this_month=='02') return 'N';
				if($this_month=='03') return 'O';
				if($this_month=='04') return 'P';
				if($this_month=='05') return 'Q';
				if($this_month=='06') return 'R';
				if($this_month=='07') return 'S';
				if($this_month=='08') return 'T';
				if($this_month=='09') return 'U';
				if($this_month=='10') return 'V';
				if($this_month=='11') return 'W';
				if($this_month=='12') return 'X';
					
				break;
					
			case '2':
				if($this_month=='01') return 'Z';
				if($this_month=='02') return 'Z';
				if($this_month=='03') return 'Z';
				if($this_month=='04') return 'Z';
				if($this_month=='05') return 'Z';
				if($this_month=='06') return 'Z';
				if($this_month=='07') return 'Z';
				if($this_month=='08') return 'Z';
				if($this_month=='09') return 'Z';
				if($this_month=='10') return 'Z';
				if($this_month=='11') return 'Z';
				if($this_month=='12') return 'Z';
					
				break;
					
			default:
				if($this_month=='01') return 'A';
				if($this_month=='02') return 'B';
				if($this_month=='03') return 'C';
				if($this_month=='04') return 'D';
				if($this_month=='05') return 'E';
				if($this_month=='06') return 'F';
				if($this_month=='07') return 'G';
				if($this_month=='08') return 'H';
				if($this_month=='09') return 'I';
				if($this_month=='10') return 'J';
				if($this_month=='11') return 'K';
				if($this_month=='12') return 'L';
				break;
					
		}
	
	}

	function prefix_cellphone($cellphone, $type = '')
	{
		if ($cellphone) {
			$CI =& get_instance();
			
			$query = $CI->db->query("select code from network_prefix where number='".substr(trim($cellphone),0,4)."'");
			$prefix_name = $query->row_array();
			switch ($type) {
				case 'input':
					$str_cellphone = ' ('. $prefix_name["code"] .')';
					break;
						
				case 'sort':
					$str_cellphone = $prefix_name["code"];
					break;
						
				default:
					$str_cellphone = $cellphone .' ('. $prefix_name["code"] .')';
			}
				
		}
		return $str_cellphone;
	}
	
	function prefix_office_phone($office_phone, $type = '')
	{
		if ($office_phone) {
			$CI =& get_instance();
			
			$office_phones = explode(',', $office_phone);
			$str_office_phones = '';
			foreach ($office_phones as $cellphone) {
				$query = $CI->db->query("select code from network_prefix where number='".substr(trim($cellphone),0,4)."'");
				$prefix_name = $query->row_array();
	
				switch ($type) {
					case 'input':
						if ($prefix_name["code"]) {
							$str_office_phones .= ' ('. $prefix_name["code"] . '), ';
						} else {
							$str_office_phones .= ', ';
						}
						break;
	
					default:
						if ($prefix_name["code"]) {
							$str_office_phones .= $cellphone .' ('. $prefix_name["code"] . '), ';
						} else {
							$str_office_phones .= $cellphone .', ';
						}
				}
			}
			$str_office_phones = substr($str_office_phones,0,-2);
		}
		return $str_office_phones;
	}
	
	function direct_online($applicant_id) 
	{
		$CI =& get_instance();
		
		$query = $CI->db->query("select p.applicant_id from personal as p
				join cv_location as cv ON cv.applicant_id = p.applicant_id
				where 1
				and p.applicant_id = '$applicant_id'
				and p.date_reported = CURDATE() and p.reporting_status = 4");
		$result = $query->row_array();
		
		if (is_array($result) && count($result)) {
			return true;
		} else {
			return false;
		}
	}
	
