<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	function debug($data=""){
		print "<pre>";
		if ($data) {
			print_r($data);
		} else {
			print_r($_GET);
			print_r($_POST);
		}
		exit;
	}
	
	function set_header_title($data){
		$CI =& get_instance();
		$CI->template->set('header_title', $data);
	}
	
	function enable_profiler($enabled = true){
		if ( !$enabled ) return false;
		get_instance()->output->enable_profiler(ENVIRONMENT == 'development');
	}
	
	function bm_start($item){
		get_instance()->benchmark->mark($item.'_start');
	}
	
	function bm_end($item){
		get_instance()->benchmark->mark($item.'_end');
	}
	

	function dbSEARCH(){
		$CI =& get_instance();
		return $CI->load->database('SEARCHDB', TRUE);
	}
	
	function dbREPORT(){
		$CI =& get_instance();
		return $CI->load->database('REPORTDB', TRUE);
	}

	function dbSMS(){
		$CI =& get_instance();
		return $CI->load->database('SMSDB', TRUE);
	}
	
	function dbEMAIL(){
		$CI =& get_instance();
		return $CI->load->database('EMAILDB', TRUE);
	}
	
	function is_login(){
		$CI =& get_instance();

		if (count($CI->session->all_userdata()) >= 4) {
			if ($CI->session->userdata['iris_user_id']) {
				return $CI->session->userdata;
			} else {
				return false;
			}
		}
		return false;
	}
	
	function send_email($data){
		$CI =& get_instance();
		$ci_email = $CI->load->library("email");
		
		//$ci_email
	}
	
	function send_sms($data)
	{
		
	}
	
	function getdata($sql_query){
		$CI =& get_instance();
		
		$query_result = $CI->db->query($sql_query);
		return $query_result->result_array();
	}
	
	function getdata_row($sql_query){
		$CI =& get_instance();
		
		$query_result = $CI->db->query($sql_query);
		return $query_result->row_array();
	}
	
	function getdata_count($table, $field, $value, $where=""){
		$CI =& get_instance();
		
		$sql_query = "select count(*) as cnt from $table where $field='$value' $where";
		$query_result = $CI->db->query($sql_query);
		$query_row = $query_result->row_array();
		return $query_row['cnt'];
	}
	
	function getdata_field($field, $table, $field, $value, $where=""){
		$CI =& get_instance();
		
		$sql_query = "select $field from $table where $field='$value' $where";
		$query_result = $CI->db->query($sql_query);
		$query_row = $query_result->row_array();
		return $query_row['cnt'];
	}
	
	function search_getdata($sql_query){
		$CI =& get_instance();
		
		$query_result = $CI->db->query($sql_query);
		return $query_result->result_array();
	}
	
	function search_getdata_row($sql_query){
		$CI =& get_instance();
		
		$query_result = $CI->db->query($sql_query);
		return $query_result->row_array();
	}

	/**
	 * create array to be used in dropdown menu
	 * 
	 * @param string $what
	 * @return array
	 */
	function get_options_from_cache($what)
	{
		$CI =& get_instance();
//  		$CI->cache->delete($what);
		if (! $CI->cache->get($what)) {
			$finalArray['']="Select ".ucwords($what); /* default selected */

			switch ($what){
				case 'access':
					$query = $CI->db->get('access');
					$field_key = "access_id";
					$field_val = "name";
					break;
					
				case 'branch':
					$query = $CI->db->get('branch');
					$field_key = "id";
					$field_val = "description";
					break;
					
				case 'city':
					$query = $CI->db->get('city');
					$field_key = "city_id";
					$field_val = "name";
					$field_val2 = "province";
					break;
					
				case 'standard category':
					$query = $CI->db->get('job_standard');
					$field_key = "job_standard_id";
					$field_val = "name";
					$field_val2 = "province";
					break;

				case 'positions':
					$query = $CI->db->get('positions');
					$field_key = "position_id";
					$field_val = "name";
					$field_val2 = "job_standard_id";
					break;

				case 'users':
					$query = $CI->db->get_where('users', array('status' => '1'));
					$field_key = "user_id";
					$field_val = "username";
					break;

				case 'country':
					$query = $CI->db->order_by('name', 'ASC')->get('country');
					$field_key = "country_id";
					$field_val = "name";
					break;

				case 'principals':
					$query = $CI->db->order_by('name', 'ASC')->get('principals');
					$field_key = "principal_id";
					$field_val = "name";
					break;
					
				default:
					return FALSE;
			}

			foreach($query->result_array() as $this_key => $this_data){
				if($what == 'city'){
					$value = $this_data[$field_val]." - ".$this_data[$field_val2];
				}else if($what == 'positions'){
					$value = array($field_val => $this_data[$field_val],
									$field_val2 => $this_data[$field_val2],
					);
				}else{
					$value = $this_data[$field_val];
				}

				$finalArray[$this_data[$field_key]] = $value;
			}
		
			$CI->cache->save($what, $finalArray, 0);
		}
		
		return $CI->cache->get($what);
	}
	
	function get_user_branch()
	{
		$CI =& get_instance();
		$query = $CI->db->query("select * from branch where id='".$CI->session->userdata['iris_user_branch_id']."'");
		return $query->row_array();
	}
	

	function applicant_nearest_branch($address_city="")
	{
		$CI =& get_instance();
	
		$sql_city = "";
		if ($address_city) $sql_city = " and city_id = '".$address_city."'";
	
		$query = $CI->db->query("select * from branch where 1 and id in (select city_branch from city where 1 {$sql_city})");
		return $query->result_array();
	
	}
	
	function applicant_method_application()
	{
		return array("", "Direct","Online","PRA/Headhunting/Jobs Fair","Referred","EWTC"); // remove  "Email",
	}

	/**
	 * Check the file On the File Server .72
	 * 
	 */
	function document_file_exists($type="", $document_link) {
		if ($type) {
			if (file_exists(FILE_SHARED.$type."/".$document_link) && $document_link) {
				return true;
			}
		}
		return false;
	}
	
	function show_log_ajax($module,$applicant_id){
		$CI =& get_instance();
		
		$str_logs = "";

		$query = $CI->db->query("select * from logs_copy where applicant_id = '".$applicant_id."' and module = '".$module."'");
		$result = $query->result_array();
		$num_data = count($result);
		if(!$num_data) return $str_logs;
	
		if($num_data > 3){
			$query = $CI->db->query("select * from (select time, action, username from logs_copy
					where applicant_id = '".$applicant_id."'
					and module = '".$module."'
					order by time asc
					limit 1) x
					union all
					select * from (select time, action, username from logs_copy
					where applicant_id = '".$applicant_id."'
					and module = '".$module."'
					order by time desc
					limit 0,3) y
					order by time desc");
			$result = $query->result_array();
		}else{
			$query = $CI->db->query("select * from logs_copy
					where applicant_id = '".$applicant_id."'
					and module = '".$module."'
					order by time desc limit 0,3");
			$result = $query->result_array();
		}
	
		
		$num_data = count($result);
		for($i=0;$i<$num_data;$i++){
			$spacer = " by ";
			if ($result[$i]['username'] == 'Through Apply Online') $spacer = " ";
			$str_logs .= "<li><small>";
			$str_logs .= dateformat($result[$i]['time'], 'M d, Y g:i a') ." - ". $result[$i]['action']. $spacer . $result[$i]['username'];
			$str_logs .= "</small></li>";
		}

		return $str_logs;
	}
	
	function dateformat($mydate, $format="F d, Y")
	{
		/* New */
		$date = date_create($mydate);
	
		return date_format($date, $format);
	
		/*	Description Below
		 *
		 *	Required. Specifies the format of the outputted date string. The following characters can be used:
		 	
		 d - The day of the month (from 01 to 31)
		 D - A textual representation of a day (three letters)
		 j - The day of the month without leading zeros (1 to 31)
		 l (lowercase 'L') - A full textual representation of a day
		 N - The ISO-8601 numeric representation of a day (1 for Monday, 7 for Sunday)
		 S - The English ordinal suffix for the day of the month (2 characters st, nd, rd or th. Works well with j)
		 w - A numeric representation of the day (0 for Sunday, 6 for Saturday)
		 z - The day of the year (from 0 through 365)
		 W - The ISO-8601 week number of year (weeks starting on Monday)
		 F - A full textual representation of a month (January through December)
		 m - A numeric representation of a month (from 01 to 12)
		 M - A short textual representation of a month (three letters)
		 n - A numeric representation of a month, without leading zeros (1 to 12)
		 t - The number of days in the given month
		 L - Whether it's a leap year (1 if it is a leap year, 0 otherwise)
		 o - The ISO-8601 year number
		 Y - A four digit representation of a year
		 y - A two digit representation of a year
		 a - Lowercase am or pm
		 A - Uppercase AM or PM
		 B - Swatch Internet time (000 to 999)
		 g - 12-hour format of an hour (1 to 12)
		 G - 24-hour format of an hour (0 to 23)
		 h - 12-hour format of an hour (01 to 12)
		 H - 24-hour format of an hour (00 to 23)
		 i - Minutes with leading zeros (00 to 59)
		 s - Seconds, with leading zeros (00 to 59)
		 u - Microseconds (added in PHP 5.2.2)
		 e - The timezone identifier (Examples: UTC, GMT, Atlantic/Azores)
		 I (capital i) - Whether the date is in daylights savings time (1 if Daylight Savings Time, 0 otherwise)
		 O - Difference to Greenwich time (GMT) in hours (Example: +0100)
		 P - Difference to Greenwich time (GMT) in hours:minutes (added in PHP 5.1.3)
		 T - Timezone abbreviations (Examples: EST, MDT)
		 Z - Timezone offset in seconds. The offset for timezones west of UTC is negative (-43200 to 50400)
		 c - The ISO-8601 date (e.g. 2013-05-05T16:34:42+00:00)
		 r - The RFC 2822 formatted date (e.g. Fri, 12 Apr 2013 12:01:05 +0200)
		 U - The seconds since the Unix Epoch (January 1 1970 00:00:00 GMT)
		 	
		 and the following predefined constants can also be used (available since PHP 5.1.0):
		 	
		 DATE_ATOM - Atom (example: 2013-04-12T15:52:01+00:00)
		 DATE_COOKIE - HTTP Cookies (example: Friday, 12-Apr-13 15:52:01 UTC)
		 DATE_ISO8601 - ISO-8601 (example: 2013-04-12T15:52:01+0000)
		 DATE_RFC822 - RFC 822 (example: Fri, 12 Apr 13 15:52:01 +0000)
		 DATE_RFC850 - RFC 850 (example: Friday, 12-Apr-13 15:52:01 UTC)
		 DATE_RFC1036 - RFC 1036 (example: Fri, 12 Apr 13 15:52:01 +0000)
		 DATE_RFC1123 - RFC 1123 (example: Fri, 12 Apr 2013 15:52:01 +0000)
		 DATE_RFC2822 - RFC 2822 (Fri, 12 Apr 2013 15:52:01 +0000)
		 DATE_RFC3339 - Same as DATE_ATOM (since PHP 5.1.3)
		 DATE_RSS - RSS (Fri, 12 Aug 2013 15:52:01 +0000)
		 DATE_W3C - World Wide Web Consortium (example: 2013-04-12T15:52:01+00:00)
	
		 */
	
	
		/*	Old function
		 *
		 *	list($year,$month,$day) = explode("-", $mysqldate);
	
		 settype($year, "integer");
		 settype($month, "integer");
		 settype($day, "integer");
	
		 if($month)  $return_date = date("F",mktime(0,0,0,$month,1,1));
		 if($day) $return_date .= " $day,";
		 if($year) $return_date .= " $year";
	
		 if($return_date)
		 	return $return_date;
		 	else return;
		 	*/
	
	}
	

	/**
	 *
	 * @param string $id applicant id/mr id/etc
	 * @param string $module
	 * @param string $action
	 * @param string $remarks
	 *
	 * @return bool
	 */
	function create_log($id, $module, $action, $remarks=NULL){
		$CI =& get_instance();
	
		$data = array(
				'time' => date("Y-m-d h:i:s a"),
				'applicant_id' => $id,
				'module' => $module,
				'action' => $action,
				'remarks' => $remarks,
				'username' => $CI->session->userdata('iris_user_name'),
		);
	
		return $CI->db->insert('logs', $data);
	}

	function filter_before_add($data = array(), $remove_data = array()){
		foreach ($data as $key => $val) {
			if (!in_array($key, $remove_data)) {
				if (is_numeric($val)) {
					$arr_data[$key] = $val;
				} else {
					$arr_data[$key] = addslashes($val);
				}
			}
		}
		return $arr_data;
	}
	
	function show_message_alert(){
		$msg = isset($_GET['msg']) ? $_GET['msg'] : '';
		if($msg) echo "<script>alert('$msg')</script>";
	}
?>