<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	function selectcode_single($array, $selected){
		$html = "";
		foreach($array as $value){
			if($value==$selected) {
				$html .= "<option value='$value' selected>$value";
			} else {
				$html .= "<option value='".addslashes($value)."'>".addslashes($value)."";
			}
			$html .= "</option>";
		}
		return $html;
	}

	function selectcode_multiple($array, $selected, $value="", $name=""){
		$html = "";
		for ($i=0; $i<count($array); $i++){
			$opt_value 	= $array[$i]["$value"];
			$opt_name 	= $array[$i]["$name"];
				
			if($opt_value=="") $opt_value=0;
			if($selected == $opt_value){
				$html .= "<option value='$opt_value' selected>$opt_name";
			} else {
				$html .= "<option value='$opt_value'>$opt_name";
			}
		}
		return $html;
	}
	
	function radiocode($array, $current_value, $name){
		foreach($array as $value){
			if($value==$current_value)
					echo "<input class=radio type=radio name='$name' value='$value' checked>$value&nbsp;&nbsp;";
				else
					echo "<input class=radio type=radio name='$name' value='$value'>$value&nbsp;&nbsp;";
		}
	}
	
	function radiocodeyesno($array,$realval,$name){
		$html = "";
		foreach($array as $value){
			if($value=='0') $subvalue = "No";
			if($value=='1') $subvalue = "Yes";
			if($value==$realval)
				$html .= "<input class=radio type=radio name='$name' value='$value' checked>$subvalue";
				else
					$html .= "<input class=radio type=radio name='$name' value='$value'>$subvalue";
		}
		return $html;
	
	}

	function getMonth($value, $type="") {
		switch ($type) {
			case 'whole':
				if($value=='') return 'Month';
				if($value=='01') return 'January';
				if($value=='02') return 'February';
				if($value=='03') return 'March';
				if($value=='04') return 'April';
				if($value=='05') return 'May';
				if($value=='06') return 'June';
				if($value=='07') return 'July';
				if($value=='08') return 'August';
				if($value=='09') return 'September';
				if($value=='10') return 'October';
				if($value=='11') return 'November';
				if($value=='12') return 'December';
	
				break;
	
			default:
				if($value=='') return 'MM';
				if($value=='01') return 'Jan';
				if($value=='02') return 'Feb';
				if($value=='03') return 'Mar';
				if($value=='04') return 'Apr';
				if($value=='05') return 'May';
				if($value=='06') return 'Jun';
				if($value=='07') return 'Jul';
				if($value=='08') return 'Aug';
				if($value=='09') return 'Sep';
				if($value=='10') return 'Oct';
				if($value=='11') return 'Nov';
				if($value=='12') return 'Dec';
					
		}
	}
	
	function dateselectmonth($name, $current_value, $style="", $type=""){
		if($current_value == '00') $current_value = '';
		$arr = array("01","02","03","04","05","06","07","08","09","10","11","12");
	
		$code = '<select name="'.$name.'" id="'.$name.'" ' . $style . '>';
		$code .= '<option value="">MM</option>';
		foreach ($arr as $value) {
			if($current_value==$value) { $selected = 'selected'; } else { $selected = ''; }
			$code .= '<option value="'.$value.'" '.$selected.'>'.getMonth($value, $type).'</option>';
		}
		$code .= '</select>';
	
		return $code;
	}
	
	function dateselectday($name, $current_value, $style=""){
		if($current_value == '00') $current_value = '';
		$arr = array("01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31");
	
		$code = '<select name="'.$name.'" id="'.$name.'" ' . $style . '>';
		$code .= '<option value="">DD</option>';
		foreach ($arr as $value) {
			if($current_value==$value) { $selected = 'selected'; } else { $selected = ''; }
			$code .= '<option value="'.$value.'" '.$selected.'>'.$value.'</option>';
		}
		$code .= '</select>';
	
		return $code;
	}
	
	function dateselectyear($name, $current_value, $style="", $yearfrom="60", $yearto="0"){
		if($current_value == '00') $current_value = '';
	
		$yearfrom = date("Y") - $yearfrom;
		$yearto = date("Y") + $yearto;
		$code = '<select name="'.$name.'" id="'.$name.'" ' . $style . '>';
		$code .= '<option value="">YYYY</option>';
		for ($count = $yearfrom; $count <= $yearto; ++$count) {
			if($current_value==$count) { $selected = 'selected'; } else { $selected = ''; }
			$code .= '<option value="'.$count.'" '.$selected.'>'.$count.'</option>';
		}
		$code .= '</select>';
	
		return $code;
	}
