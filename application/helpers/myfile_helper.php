<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	/**
	* create file directory for local and shared folder (cv, pic, jdq)
	* for shared add domain name value on $files_dir like "\\\\serv4\\"
	*/
	function create_directory($what = "", $applicant_id, $files_dir = FILE_LOCAL) {
			
		/*	START CREATE DIRECTORY  */
		//$files_dir = $_SERVER['DOCUMENT_ROOT']."files/";
		$curr_year = substr(date("Y"),0,2);

		//if ($files_dir != "c:\\") $files_dir = "\\\\serv4\\";
		
		switch ($what) {
			case 'iriscvwordpdf':
					$cv_dir = $files_dir.$what."/";
					
					if (!is_dir($cv_dir)) {		// FOR CV WORD/PDF
						$result = mkdir($cv_dir);
					}
					
					$cv_dir_year = $cv_dir.$curr_year.substr(trim($applicant_id),-2)."/";
					if (!is_dir($cv_dir_year)) {		// FOR CV WORD/PDF - YEAR
						$result = mkdir($cv_dir_year);
					}
					
					$cv_dir_year_month = $cv_dir_year.substr(trim($applicant_id),0,1)."/";
					if (!is_dir($cv_dir_year_month)) {		// FOR CV WORD/PDF - YEAR - MONTH
						$result = mkdir($cv_dir_year_month);
					}

				return array("directory1" => $cv_dir_year_month,
							 "directory2" => str_replace($cv_dir, "/", $cv_dir_year_month));
				break;
			case 'irispictures':
					$pic_dir = $files_dir.$what."/";
					
					if (!is_dir($pic_dir)) {		// FOR PICTURES
						$result = mkdir($pic_dir);
					}
					
					$pic_dir_year = $pic_dir.$curr_year.substr(trim($applicant_id),-2)."/";
					if (!is_dir($pic_dir_year)) {		// FOR PICTURES - YEAR
						$result = mkdir($pic_dir_year);
					}
		
					$pic_dir_year_month = $pic_dir_year.substr(trim($applicant_id),0,1)."/";
					if (!is_dir($pic_dir_year_month)) {		// FOR PICTURES - YEAR - MONTH
						$result = mkdir($pic_dir_year_month);
					}

				return array("directory1" => $pic_dir_year_month,
							 "directory2" => str_replace($pic_dir, "/", $pic_dir_year_month));
				break;
			case 'irisjdq':
					$curr_year = date("Y");
					$jdq_dir = $files_dir.$what."/";
					
					if (!is_dir($jdq_dir)) {		// FOR JDQ
						$result = mkdir($jdq_dir);
					}
					
					$jdq_dir_year = $jdq_dir.$curr_year."/";
					if (!is_dir($jdq_dir_year)) {		// FOR JDQ - YEAR
						$result = mkdir($jdq_dir_year);
					}
					
					$jdq_dir_year_month = $jdq_dir_year.$applicant_id."/";
					if (!is_dir($jdq_dir_year_month)) {		// FOR JDQ - YEAR - MONTH
						$result = mkdir($jdq_dir_year_month);
					}

				return array("directory1" => $jdq_dir_year_month,
							 "directory2" => str_replace($jdq_dir, "/", $jdq_dir_year_month));
				break;
			case 'recruitment_fee_guide':
					$curr_year = date("Y");
					$rfg_dir = $files_dir.$what."/";
					
					if (!is_dir($rfg_dir)) {		// FOR FEE GUIDE
						$result = mkdir($rfg_dir);
					}
					
					$rfg_dir_year = $rfg_dir.$curr_year."/";
					if (!is_dir($rfg_dir_year)) {		// FOR FEE GUIDE - YEAR
						$result = mkdir($rfg_dir_year);
					}
					
					$rfg_dir_year_month = $rfg_dir_year.$applicant_id."/";
					if (!is_dir($rfg_dir_year_month)) {		// FOR FEE GUIDE - YEAR - MONTH
						$result = mkdir($rfg_dir_year_month);
					}

				return array("directory1" => $rfg_dir_year_month,
							 "directory2" => str_replace($rfg_dir, "/", $rfg_dir_year_month));
				break;
				
			case 'documents':
					$cv_dir = $files_dir.$what."/";
					
					if (!is_dir($cv_dir)) {		// FOR documents
						$result = mkdir($cv_dir);
					}
					
					$cv_dir_year = $cv_dir.$curr_year.substr(trim($applicant_id),-2)."/";
					if (!is_dir($cv_dir_year)) {		// FOR documents - YEAR
						$result = mkdir($cv_dir_year);
					}
					
					$cv_dir_year_month = $cv_dir_year.substr(trim($applicant_id),0,1)."/";
					if (!is_dir($cv_dir_year_month)) {		// FOR documents - YEAR - MONTH
						$result = mkdir($cv_dir_year_month);
					}
					
					$cv_dir_year_month_comp = $cv_dir_year_month . $applicant_id."/";
					if (!is_dir($cv_dir_year_month_comp)) {		// FOR documents - YEAR - MONTH
						$result = mkdir($cv_dir_year_month_comp);
					}

				return array("directory1" => $cv_dir_year_month_comp,
							 "directory2" => str_replace($cv_dir, "/", $cv_dir_year_month_comp));
				break;

			case 'legal_archives':
					$cv_dir = $files_dir.$what."/";

					if (!is_dir($cv_dir)) {
						$result = mkdir($cv_dir);
					}

					return array("directory1" => $cv_dir,
								 "directory2" => str_replace($cv_dir, "/", $cv_dir));
				break;

			default:
				return false;
		}
	}
	
?>