<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class My_controller extends CI_Controller {

	/**
	 * Constructor
	 */
	public function __construct(){
		parent::__construct();
		
		/* All Required Login $except_controller */
		$except_controller = array('auth');
		if ( ! is_login() && ! in_array($this->router->fetch_class(), $except_controller)) {
			
			/* Check for Ajax Request */
			if ($this->input->is_ajax_request()) {
				exit('Re-Login Session has Expired.');
			} else {
				
				redirect('auth/login');
			}
		}
		
		/* Load Caching Driver */
		$this->load->driver('cache',
				array('adapter' => 'memcached', 'backup' => 'file', 'key_prefix' => 'my_')
				);
		
		/* Show Profiler in the Lower Page */
		if ( isset($_GET['enable_profiler']) && $_GET['enable_profiler'] ) {
			$enable_profiler = $_GET['enable_profiler'];
		} else {
			$enable_profiler = 0;
		}
		
		enable_profiler($enable_profiler);
		

		
/*
		$this->load->library('session');
		
		//save the previous controller and action name from session
		$this->previous_controller_name = $this->session->flashdata('previous_controller_name');
		$this->previous_action_name     = $this->session->flashdata('previous_action_name');
		
		print "previous_controller_name: ".$this->previous_controller_name."<br>";
		print "previous_action_name: ".$this->previous_action_name."<br>";
		
		//set the current controller and action name
		$this->controller_name = $this->router->fetch_directory() . $this->router->fetch_class();
		$this->action_name     = $this->router->fetch_method();
		
		print "controller_name: ".$this->controller_name."<br>";
		print "action_name: ".$this->action_name."<br>";
		$this->save_url();
		//$this->load_page();
		  
		 * 
		 */
	}
	
	public function load_page()
	{
		$this->template->view('reception/followup/index');
	}
	

	
	public function __destruct() {
		//save the controller and action names in session
		/*
		 * 
		 if ($this->save_previous_url) {
			$this->session->set_flashdata('previous_controller_name', $this->previous_controller_name);
			$this->session->set_flashdata('previous_action_name', $this->previous_action_name);
		}
		else {
			$this->session->set_flashdata('previous_controller_name', $this->controller_name);
			$this->session->set_flashdata('previous_action_name', $this->action_name);
			
			$this->save_url();
		}
		*/
	}
	
	protected function load_defaults() {
		$this->data['content'] = '';
		$this->data['css']     = '';
		//$this->add_title(); //add the title from the database
	}
	
	protected function render($template='main') {
		$view_path = $this->controller_name . '/' . $this->action_name . '.tpl.php'; //set the path off the view
		if (file_exists(APPPATH . 'views/' . $view_path)) {
			$this->data['content'] .= $this->load->view($view_path, $this->data, true);  //load the view
		}
			
		$this->load->view("layouts/$template.tpl.php", $this->data);  //load the template
	}
	
	protected function add_title() {
		$this->load->model('page_model');
		 
		//the default page title will be whats set in the controller
		//but if there is an entry in the db override the controller's title with the title from the db
		$page_title = $this->page_model->get_title($this->controller_name,$this->action_name);
		if ($page_title) {
			$this->data['title'] = $page_title;
		}
	}
	
	protected function save_url() {
		$this->save_previous_url = true;
	}

}
