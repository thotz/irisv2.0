<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
*	Source :	https://github.com/barinbritva/Sphinx-RT-Wrap-Codeigniter
*/

class SphinxRT {

	private $_ci;
	private $_connect;

	private $_data = array();
	private $_query = '';

	private $_trans_state = false;
	private $_trans_errors = 0;

	private $_error_message = '';
	private $_error_number = 0;

	private function _set_ci()
	{
		$this->_ci = &get_instance();
		return $this;
	}
	private function _get_ci()
	{
		return $this->_ci;
	}

	private function _set_connect()
	{
		//$config = $this->_get_config();

		try
		{
			$this->_connect = new PDO(
				SPHINXRT_DBDRIVER.':host='.SPHINXRT_HOST.';port='.SPHINXRT_PORT.';dbname=',
				SPHINXRT_USERNAME,
				''
			);
		}
		catch(PDOException $e)
		{
			show_error('Unable to communicate to the Sphinx Server: '.$e->getMessage());
		}

		return $this;
	}
	private function _get_connect()
	{
		return $this->_connect;
	}

	private function _set_data($data)
	{
		$this->_data = $data;
		return $this;
	}
	private function _get_data()
	{
		return $this->_data;
	}

	private function _set_query($query)
	{
		$this->_query = $query;
		return $this;
	}
	private function _get_query()
	{
		return $this->_query;
	}

	private function _set_trans_state($state)
	{
		if (!is_bool($state))
		{
			$state = false;
		}

		$this->_trans_state = $state;

		return $this;
	}
	private function _get_trans_state()
	{
		return $this->_trans_state;
	}

	private function _get_trans_errors()
	{
		return $this->_trans_errors;
	}
	private function _inc_trans_errors()
	{
		$this->_trans_errors++;
		return $this;
	}
	private function _reset_trans_errors()
	{
		$this->_trans_errors = 0;
		return $this;
	}

	private function _set_error_message($message)
	{
		if ($message===null)
		{
			$message = '';
		}

		$this->_error_message = $message;

		return $this;
	}
	public function _error_message()
	{
		return $this->_error_message;
	}
	private function _reset_error_message()
	{
		$this->_set_error_message('');
		return $this;
	}

	private function _set_error_number($number)
	{
		if ($number===null)
		{
			$number = 0;
		}

		$this->_error_number = $number;

		return $this;
	}
	public function _error_number()
	{
		return $this->_error_number;
	}
	private function _reset_error_number()
	{
		$this->_set_error_number(0);
		return $this;
	}

	private function _get_config()
	{
		$this->_get_ci()->config->load('sphinx_rt');
		return $this->_get_ci()->config->config['sphinx_rt'];
	}

	private function _reset_query_and_data()
	{
		$this
			->_set_data(array())
			->_set_query('');

		return $this;
	}

	private function _enable_trans()
	{
		$this->_set_trans_state(true);
		return $this;
	}

	private function _disable_trans()
	{
		$this
			->_set_trans_state(false)
			->_reset_trans_errors();

		return $this;
	}

	private function _set_error($error)
	{
		$this
			->_set_error_message($error[2])
			->_set_error_number($error[1]);

		return $this;
	}

	private function _reset_error()
	{
		$this
			->_reset_error_message()
			->_reset_error_number();

		return $this;
	}

	private function _register_transaction($error_report)
	{
		if (
			$this->_get_trans_state() &&
			$error_report!==0
		)
		{
			$this->_inc_trans_errors();
		}

		return $this;
	}


	function __construct()
	{
		$this
			->_set_ci()
			->_set_connect();
	}

	public function insert($index, $data)
	{
		$this
			->_set_data($data)
			->_filter_data($index);
		$fields = $this->_get_data_keys();

		$this->_prepare_data();
		$values = $this->_get_data_keys();
		
		return $this
			->_set_query("INSERT INTO ".$index." (".$fields.") VALUES (".$values.")")
			->_query();
	}

	public function replace($index, $data)
	{
		$this
			->_set_data($data)
			->_filter_data($index);
		$fields = $this->_get_data_keys();

		$this->_prepare_data();
		$values = $this->_get_data_keys();

		return $this
			->_set_query("REPLACE INTO ".$index." (".$fields.") VALUES (".$values.")")
			->_query();
	}

	public function update($index, $data, $where)
	{
		$data = $this
			->_set_data($data)
			->_filter_data_for_update($index)
			->_get_data();

		return $this
			->_prepare_data_for_update()
			->_set_query("UPDATE ".$index." SET ".$this->_get_data_keys().$this->_prepare_where($where))
			->_set_data($data)
			->_prepare_data()
			->_query();
	}

	private function _filter_data($index)
	{
		/**
		 Example : PDOStatement Object ([queryString] => DESC $index )
		 */
		$schema = $this
			->_set_query("DESC ".$index)
			->_query_without_parameters();

		/* Get All Fields of the Tables - Unique */
		$schema = array_unique($schema->fetchAll(PDO::FETCH_COLUMN, 0));

		/* Set and Arrange the New Data */
		$this->_data_is_multidimensional() ? $this->_filter_data_for_multi($schema) :$this->_filter_data_for_simple($schema);

		return $this;
	}

	private function _filter_data_for_simple($schema)
	{
		$data = $this->_get_data();

		foreach($data as $key=>$value)
		{
			if (!in_array($key, $schema))
			{
				unset($data[$key]);
			}
		}

		$this->_set_data($data);

		return $this;
	}

	private function _filter_data_for_multi($schema)
	{
		$data = $this->_get_data();

		foreach ($data as $data_item)
		{
			foreach($data_item as $key=>$value)
			{
				if (!in_array($key, $schema))
				{
					unset($data_item[$key]);
				}
			}
			$new_data[] = $data_item;
		}

		$this->_set_data($new_data);

		return $this;
	}

	private function _filter_data_for_update($index)
	{
		$data = $this->_get_data();
		$allowed_types = array('uint', 'bigint', 'float', 'multi', 'multi_64', 'timestamp');

		$full_schema = $this
			->_set_query("DESC ".$index)
			->_query_without_parameters()
			->fetchAll();

		foreach ($full_schema as $field)
		{
			if(
				in_array($field[1], $allowed_types) &&
				$field[0]!='id'
			)
			{
				$schema[] = $field[0];
			}
		}

		foreach($data as $key=>$value)
		{
			if (!in_array($key, $schema))
			{
				unset($data[$key]);
			}
		}

		$this->_set_data($data);

		return $this;
	}

	public function delete($index, $ids)
	{
		if (is_array($ids))
		{
			$where = array('id IN' => $ids);
		}
		else
		{
			$where = array('id' => $ids);
		}

		return $this
			->_set_query("DELETE FROM ".$index.$this->_prepare_where($where))
			->_query_without_parameters();
	}

	public function truncate($index)
	{
		return $this
			->_set_query("TRUNCATE RTINDEX ".$index)
			->_query_without_parameters();
	}

	public function delete_all($index)
	{
		$ids = $this
			->_set_query("SELECT id FROM ".$index."")
			->_query_without_parameters();
		$ids = implode(', ', $ids->fetchAll(PDO::FETCH_COLUMN, 0));

		return $this
			->_set_query("DELETE FROM ".$index." WHERE id IN (".$ids.")")
			->_query_without_parameters();
	}

	public function optimize($index)
	{
		return $this
			->_set_query("OPTIMIZE INDEX ".$index)
			->_query_without_parameters();
	}


	private function _get_data_keys()
	{
		$data = $this->_get_data();

		if ($this->_data_is_multidimensional())
		{
			$data = $data[key($data)];
		}

		return implode(',', array_keys($data));
	}

	private function _data_is_multidimensional()
	{
		return is_numeric(key($this->_get_data())) ? true : false;
	}

	private function _prepare_data()
	{
		$data = $this->_get_data();

		if ($this->_data_is_multidimensional())
		{
			foreach ($data as $item)
			{
				$new_data[] = $this->_prepare_item($item);
			}
		}
		else
		{
			$new_data = $this->_prepare_item($data);
		}

		$this->_set_data($new_data);

		return $this;
	}

	private function _prepare_item($data)
	{
		foreach ($data as $key=>$value)
		{
			$item[':'.$key] = $value;
		}

		return $item;
	}

	private function _prepare_data_for_update()
	{
		$data = $this->_get_data();

		foreach ($data as $key=>$value)
		{
			$new_data[$key.'=:'.$key] = $value;
		}

		$this->_set_data($new_data);

		return $this;
	}

	private function _prepare_where($where)
	{
		$where_string = empty($where) ? '' : ' WHERE ';
		$delimiter = '';

		foreach ($where as $key=>$value)
		{
			$operator = $this->_has_operator($key) ? ' ' : ' = ';
			$value = $this->_prepare_value($value);

			$where_string .= $delimiter.$key.$operator.$value;
			$delimiter = ' AND ';
		}

		return $where_string;
	}

	private function _has_operator($string)
	{
		$string = trim($string);
		if (preg_match("/(\s|<|>|!|=|in|not in|match)/i", $string))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	private function _prepare_value($value)
	{
		$db_connect = $this->_get_connect();

		$value_type = strtolower(gettype($value));

		switch($value_type)
		{
			case 'array':
				foreach ($value as $item)
				{
					$value_escaped[] = is_numeric($item) ? $item : $db_connect->quote($item);
				}
				$value = "(".implode(', ', $value_escaped).")";
				break;

			case 'null':
				$value = '';
				break;

			default:
				$value = is_numeric($value) ? $value : $db_connect->quote($value);
				break;
		}

		return $value;
	}

	private function _query()
	{
		return $this->_data_is_multidimensional() ? $this->_query_multi() : $this->_query_simple();
	}

	private function _query_without_parameters()
	{
		$this->_reset_error();

		$connect = $this->_get_connect();

		$query = $connect->prepare($this->_get_query());
		$query->execute();

		$this
			->_set_query('')
			->_set_error($query->errorInfo())
			->_register_transaction($this->_error_number());

		return $this->_error_number()===0 ? $query : false;
	}

	private function _query_simple()
	{
		$this->_reset_error();

		$connect = $this->_get_connect();
		$query_string = $this->_get_query();

		$query = $connect->prepare($query_string);
		$data = $this->_get_data();
		foreach ($data as $key=>$value)
		{
			$value = $this->_convert_type($value);
			$query->bindValue($key, $value, is_numeric($value) ? PDO::PARAM_INT : PDO::PARAM_STR);
		}
		$query->execute();

		$this
			->_reset_query_and_data()
			->_set_error($query->errorInfo())
			->_register_transaction($this->_error_number());

		return $this->_error_number()===0 ? $query : false;
	}

	private function _query_multi()
	{
		$this->_reset_error();

		$connect = $this->_get_connect();
		$query_string = $this->_get_query();

		$query = $connect->prepare($query_string);
		$data = $this->_get_data();
		foreach ($data as $item)
		{
			foreach ($item as $key=>$value)
			{
				$value = $this->_convert_type($value);
				$query->bindValue($key, $value, is_int($value) ? PDO::PARAM_INT : PDO::PARAM_STR);
			}
			$query->execute();

			$this
				->_set_error($query->errorInfo())
				->_register_transaction($this->_error_number());
		}

		$this->_reset_query_and_data();

		return $this->_error_number()===0 ? $query : false;
	}

	private function _convert_type($value)
	{
		if (gettype($value)=='string')
		{
			if (is_numeric($value))
			{
				if (substr_count($value, '.'))
				{
					$value = (float)$value;
				}
				else
				{
					$value = (int)$value;
				}
			}
		}

		return $value;
	}

	public function trans_begin()
	{
		$connect = $this->_get_connect();
		$query = $connect->prepare("BEGIN");
		$query->execute();

		$this->_enable_trans();

		return $this;
	}

	public function trans_start()
	{
		$this->trans_begin();
		return $this;
	}

	public function trans_commit()
	{
		$connect = $this->_get_connect();
		$query = $connect->prepare("COMMIT");
		$query->execute();

		$this->_disable_trans();

		return $this;
	}

	public function trans_rollback()
	{
		$connect = $this->_get_connect();
		$query = $connect->prepare("ROLLBACK");
		$query->execute();

		$this->_disable_trans();

		return $this;
	}

	public function trans_complete()
	{
		if ($this->trans_status())
		{
			$this->trans_commit();
			return true;
		}
		else
		{
			$this->trans_rollback();
			return false;
		}
	}

	public function trans_status()
	{
		if (
			$this->_get_trans_state() &&
			$this->_get_trans_errors()>0
		)
		{
			return false;
		}
		else
		{
			return true;
		}
	}

}