

    <!-- Sidebar -->
    <section class="sidebar">

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>

            <!-- Dashboard -->
            <li class="treeview">
                <a href="<?=MENU_LINKED?>mainpage">
                    <i class="fa fa-bar-chart"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            
            <?php 
            foreach ($formated_result as $values_high) {
            	$has_low_menu = count($values_high['menu_low'])
	            ?>
	            <li class="treeview">
	                <a href="#">
	                    <i class="fa <?=$values_high['menu_high_icon']?>"></i>
	                    <span><?=$values_high['high_name']?></span>
	                    <i class="fa <?=($has_low_menu) ? 'fa-angle-left' : ''?> pull-right"></i>
	                </a>
	                <ul class="treeview-menu">
	                <?php
	                	foreach ($values_high['menu_low'] as $value_low) {
	                		$has_sub_menu = count($value_low['menu_sub'])
		                	?>
		                    <li>
		                        <a href="<?=MENU_LINKED.$value_low['menu_low_filename']?>"><?=$value_low['menu_low_name']?> <i class="fa <?=($has_sub_menu) ? 'fa-angle-left' : ''?> pull-right"></i></a>
		                        <?php if ($has_sub_menu) { ?>
			                        <ul class="treeview-menu">
			                        <?php 
			                        	foreach ($value_low['menu_sub'] as $value_sub) { 
				                        	?>
				                            <li><a href="<?=MENU_LINKED.$value_sub['filename']?>"><?=$value_sub['name']?></a></li>
				                            <?php
			                        	}
			                            ?>
			                        </ul>
		                        <?php } ?>
		                    </li>
		                    <?php 
            			}
	                    ?>
	                </ul>
	            </li>
	        <?php 
	            }
	        ?>
        </ul>
    </section>
    <!-- /.sidebar -->
    
