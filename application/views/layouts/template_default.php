<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="description" content="IRIS: Integrated Recruitment Information System v2.0 of East West Placement Center, Inc.">
    <meta name="author" content="Karl Gerald Saul (Web Designer & Front-End Developer), Rodel G. Dimaculangan (Web Developer), Carlo Salonga (Programmer)">
  	<meta name="robots" content="noindex,nofollow">
    
    <title>IRIS: Interactive Recruitment Information System v2.0 | East West Placement Center, Inc.</title>
 
 	<link rel="shortcut icon" type="image/gif" href="<?=base_url();?>public/images/icons/iris-icon-114.gif">
 	<link rel="shortcut icon" href="<?=base_url();?>public/images/icons/iris-icon.png" type="image/x-icon" />
    <link rel="apple-touch-icon" href="<?=base_url();?>public/images/icons/iris-icon-57.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?=base_url();?>public/images/icons/iris-icon-72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?=base_url();?>public/images/icons/iris-icon-114.png">
    
    <!-- Reset CSS -->
	<link href="<?=base_url();?>public/css/reset.css" rel="stylesheet" />
    
	<!-- Bootstrap CSS -->  
	<link href="<?=base_url();?>public/css/bootstrap/bootstrap.min.css" rel="stylesheet" />
    
    <!-- Font Awesome -->
    <link href="<?=base_url();?>public/css/font-awesome/font-awesome.min.css" rel="stylesheet" />
    
    <!-- Ionicons -->
    <link href="<?=base_url();?>public/css/ionicons/ionicons.min.css" rel="stylesheet" />

	<!-- Load CSS from Controller -->
    <?php
	    $file_css = isset($file_css) ? $file_css : '';
    	if (is_array($file_css) && count($file_css) > 0) {
    		foreach ($file_css as $value) {
    			echo '<link href="'.base_url().'public/'.$value.'" rel="stylesheet" />';
    		}
    	}
    ?>
      
	<!-- CSS -->
    <link href="<?=base_url();?>public/css/adminlte/AdminLTE.min.css" rel="stylesheet" />
    
    <!-- Skin / Themes -->
    <link href="<?=base_url();?>public/css/skins/_all-skins.min.css" rel="stylesheet" />
    <link href="<?=base_url();?>public/css/skins/skin-main.css"  rel="stylesheet" />
    
        <link href="<?=base_url();?>public/js/plugins/facebox/facebox.css"  rel="stylesheet" />
    
    <!-- Global CSS -->
	<link href="<?=base_url();?>public/css/global.css" rel="stylesheet" />


	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <!-- FACEBOX -->
	<link href="<?=base_url();?>public/css/login.css" media="screen" rel="stylesheet" type="text/css" />

	<!-- Set base_url() in the Javascript -->	
	<script>
		function base_url_js() {
			return '<?=base_url();?>';
		}
	</script>
</head>

<body class="hold-transition main-skin sidebar-mini">

    <!-- loading -->
    <div id="loading-overlay" class="loading-overlay hide">
        <div class="spinner-md fa-spin"></div>
    </div>
    
    <!-- Site wrapper -->
    <div class="wrapper">
        
        <?php /**	Login user */ 
        	if ( isset($this->session->userdata['iris_user_id']) && $this->session->userdata['iris_user_id'] ) { ?>
		        <!-- Main Header-->
		        	<?php include APPPATH.'views/layouts/main-header.php';?>
		        
		        <!-- Main Sidebar -->
		        	<aside class="main-sidebar"><?php echo $this->global_model->get_side_menu(); ?></aside>

		        <!-- Content Wrapper. Contains page content -->
					<?php
					/* Add Dashboard Layout if Mainpage */
					$style_dashboard = "";
					if ($this->router->fetch_class() == 'mainpage') $style_dashboard = 'dashboard';
					?>
	
			        <div class="content-wrapper <?=$style_dashboard?>">
			            <?php echo $contents?>
			        </div>
		        
		        <!-- Main Footer -->
		        	<?php include APPPATH.'views/layouts/main-footer.php';?>
	    <?php } else { ?>
		        <?php echo $contents?>
	    <?php } ?>
    
	    <!-- Search Modal -->
	     	<?php include APPPATH.'views/modals/modal-search.php';?>
        
    </div>


    <!-- jQuery 2.1.4 -->
    <script src="<?=base_url();?>public/js/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    
    <!-- Bootstrap 3.3.5 -->
    <script src="<?=base_url();?>public/js/bootstrap/bootstrap.min.js"></script>
    
    <!-- DataTables -->
    <script src="<?=base_url();?>public/js/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?=base_url();?>public/js/plugins/datatables/dataTables.bootstrap.min.js"></script>
      
    <!-- SlimScroll -->
    <script src="<?=base_url();?>public/js/plugins/slimScroll/jquery.slimscroll.min.js"></script>
      
    <!-- FastClick -->
    <script src="<?=base_url();?>public/js/plugins/fastclick/fastclick.min.js"></script>
      
    <!-- Load Javascript from Controller -->
    <?php
	    $file_javascript = isset($file_javascript) ? $file_javascript : '';
    	if (is_array($file_javascript) && count($file_javascript) > 0) {
    		foreach ($file_javascript as $value) {
    			echo '<script src="'.base_url().'public/'.$value.'"></script>';
    		}
    	}
    ?>
    <!-- AdminLTE App -->
    <script src="<?=base_url();?>public/js/dist/app.min.js"></script>
      
    <!-- AdminLTE for demo purposes -->
    <script src="<?=base_url();?>public/js/dist/demo.js"></script>
    
    <!-- FACEBOX -->
    <script src="<?=base_url();?>public/js/plugins/facebox/facebox.js"></script>
    
    <script src="<?=base_url();?>public/js/global.js"></script>
    <script src="<?=base_url();?>public/js/modal/search.js"></script>

	
	<script>
	$(document).ready(function() {
		<!-- If Side Menu Does Not Show - Reload it. -->
		if ($('aside.main-sidebar').html() == '') {
			$.get('<?=base_url()?>ajax/side_menu', function(data) {
				$('.main-sidebar').html(data);
			});
		}

		
	});
	</script>

</body>
</html>
