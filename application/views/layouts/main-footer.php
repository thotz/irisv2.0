<footer class="main-footer">
    <img src="<?=base_url();?>public/images/iris-footer-logo.png" class="iris-logo pull-right" alt="EWPCI Logo" />
    <div class="pull-right hidden-xs text-right">
        Interactive Recruitment<br/>Information System.<br/>Version 2.
    </div>
    <img src="<?=base_url();?>public/images/ewpci-footer-logo.png" class="ewpci-logo" alt="EWPCI Logo" />
    Copyright &copy; 2016<br/>East West Placement Center, Inc.<br/>All rights reserved.
</footer>