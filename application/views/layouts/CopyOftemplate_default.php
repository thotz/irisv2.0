<html>
<head>
<title>Welcome IrisOnline</title>    
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="<?=base_url();?>public/css/plugins/jquery-ui.css" rel="stylesheet" type="text/css">
<link href="<?=base_url();?>public/css/plugins/menu.css" rel="stylesheet" type="text/css">
<link href="<?=base_url();?>public/css/plugins/calendar.css" rel="stylesheet" type="text/css">
<link href="<?=base_url();?>public/css/plugins/facebox.css" media="screen" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="<?=base_url();?>public/js/plugins/jquery/jquery1.4.2.js"></script>
<script type="text/javascript" src="<?=base_url();?>public/js/plugins/jquery/jquery-ui.js"></script>
<script type="text/javascript" src="<?=base_url();?>public/js/plugins/jquery/jquery.form.js"></script>
<script type="text/javascript" src="<?=base_url();?>public/js/plugins/tablednd.js"></script>
<script type="text/javascript" src="<?=base_url();?>public/js/plugins/facebox.js"></script>
<script type="text/javascript" src="<?=base_url();?>public/js/global.js"></script>
<script type="text/javascript" language="javascript">
function getBaseUrl() {
	return '<?=base_url();?>public/';
}
</script>
<script type="text/javascript" src="<?=base_url();?>public/js/plugins/menu/sniffer.js" language="javascript"></script>
<?php if (is_login()) require_once(APPPATH . "libraries/menu/custom.php"); ?>
<script type="text/javascript" src="<?=base_url();?>public/js/plugins/menu/style.js" language="javascript1.2"></script>	
<script type="text/javascript" src="<?=base_url();?>public/js/plugins/menu/menu.js" language="javascript1.2"></script>
<script type="text/javascript" src="<?=base_url();?>public/js/plugins/calendar.js"></script>
</head>	

<body marginheight="0" marginwidth="0" leftmargin="0" topmargin="0" background="<?=base_url();?>public/img/bodybg.jpg">
	
	
	<table border="0" cellspacing="0" width="100%" height="100%" cellpadding="0" bgcolor="#dce5f4">
		<!--	HEADER	-->
		<tr>
		  <td bgcolor="#9cb6e0" valign="top" height="41">
			<table border="0" width="100%" height="100%" cellspacing="0" cellpadding="0">
				<tr>
					<td valign="top"><img src="<?=base_url();?>public/img/logo.gif" border="0"><img src="<?=base_url();?>public/img/logotxt.gif" border="0"></td>
					<td align="right"><iframe src="popup.html" style="display:none;"></iframe>
					<td align="right">
						<table>
							<tr><td class="txtgray">Total No. of Applicants</td><td>:</td><td align="right" class="txtgray"> <font color="#a40101">107,364</font></td></tr>
							<tr><td class="txtgray">Total No. of Principals</td><td>:</td><td align="right" class="txtgray"><font color="#a40101">887</font></td></tr>
						</table>	
					</td>
		      </tr>
			</table>
		</td></tr>
		<!--	MENU	-->
		<tr><td colspan="2" valign="middle" height="25" background="<?=base_url();?>public/img/menutile.gif">		
			<table width="100%" cellspacing="0" cellpadding="0" border="0">
			<tr><td width="60%" background="<?=base_url();?>public/img/menutile.jpg"></td>
				<td width="20%" valign="top" align="right">
					<table width="100%" cellspacing="1" cellpadding="0" border="0">
						<tr><td width="100%" valign="middle" align="center" class="pagetitle">
							<div style="vertical-align:top"><?=$header_title?></div>
						</td></tr>
					</table>
				</td>
			</td></tr>
			</table>
		</td></tr>
		<!--	MAIN CONTENT	-->
		<tr>
			<td <?=$position_align?> height="100%" align="center"><?= $contents ?></td>
		</tr>
		<!--	FOOTER	-->
		<tr><td colspan="2">
			<table border=0 cellspacing=0 width="100%" cellpadding="0" bgcolor="#9cb6e0" height=100%>		
				<tr><td align=left>&nbsp;<font size="1" face="verdana">IRIS Online v16.1</td>
				<td align=right><a href="mailto:support@eastwest.com.ph"><font size="1" face="verdana" color="black">Property of East West Placement Center.</font></a>
				<a href="http://www.eastwest.com.ph" target="_blank"><!--<img src="<?=base_url();?>public/img/qxlogo-copyright.gif" border="0" align="absmiddle">--></a>
				</td></tr>					
			</table>	
		</td></tr>		
	</table>
</body>
</html>
