<?php 
$result = $this->db->query("select lname,position from users where user_id='".$this->session->userdata['iris_user_id']."'");

?>

<header class="main-header">

    <!-- Logo -->
    <a href="<?=base_url()?>mainpage" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini">
            <img src="<?=base_url();?>public/images/iris-header-logo-mini.png" alt="IRIS" />
        </span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg">
            <img class="hidden-xs" src="<?=base_url();?>public/images/iris-header-logo.png" alt="IRIS" />
            <img class="visible-xs-inline-block" src="<?=base_url();?>public/images/iris-header-logo-mobile.png" alt="IRIS" />
            <?//=$header_title?>
        </span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">

        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>
        
        <span class="page-title"><?=$header_title?></span>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- Search Button -->
                <li id="btn_modalsearch" class="search-btn" data-toggle="tooltip" data-placement="bottom" data-original-title="Search Button" >
                    <a href="#" data-toggle="modal" data-target="#modalSearch">
                        <i class="fa fa-search"></i>
                    </a>
                </li>
                <!-- Emergency Button -->
                <li class="emergency-btn">
                    <a href="<?=base_url();?>alert/emergency_cases/" data-toggle="tooltip" data-placement="bottom" data-original-title="Emergency Cases (10)">
                        <i class="fa fa-exclamation-circle"></i>
                        <span class="label label-warning">10</span>
                    </a>
                </li>
                <!-- Alert Button -->
                <li class="alert-btn" data-toggle="tooltip" data-placement="bottom" data-original-title="Show All Alert" >
                    <a href="#" data-toggle="modal" data-target="#modalAlert">
                        <i class="fa fa-exclamation-triangle"></i>
                    </a>
                </li>
                <!-- Message Button -->
                <li class="message-btn">
                    <a href="#" data-toggle="tooltip" data-placement="bottom" data-original-title="Show Messages (6)">
                        <i class="fa fa-envelope"></i>
                        <span class="label label-danger">6</span>
                    </a>
                </li>
                <!-- User Account -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?=base_url();?>public/images/user-thumbnail-160x160.png" class="user-image" alt="User Image">
                        <span class="hidden-xs">Welcome <strong><?php echo isset($this->session->userdata['iris_user_name']) ? $this->session->userdata['iris_user_name'] :'Iris' ?></strong></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="<?=base_url();?>public/images/user-thumbnail-160x160.png" class="img-circle" alt="User Image">
                            <!-- <p>Rodel "thotz"<br/>Dimaculangan<small>Web Developer (MIS Dept.)</small></p> -->
                            <p><?=$result->row()->lname?><small><?=$result->row()->position?></small></p>
                            
                            
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <a href="#" class="btn btn-default btn-flat btn-block">My Profile</a>
                            <a href="#" class="btn btn-default btn-flat btn-block">Change Password</a>
                            <a href="<?=base_url()?>auth/logout" class="btn btn-default btn-flat btn-block">Log-out</a>
                        </li>
                        <!-- Skin / Theme Settings Button
                        <li>
                            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                        </li> -->
                    </ul>
                </li>
            </ul>
        </div>

    </nav>

</header>