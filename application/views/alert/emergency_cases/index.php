            <!-- MAIN CONTENT -->
            <section class="content">
            
                <div class="row">
                    <div class="col-md-12">
            
                            
                            <!-- Add User Box -->
                            <div class="box">  
                                
                                <div class="box-header with-border">
                                    <h3 class="box-title">Emergency Cases <small>February 26, 2016 - Friday</small></h3>
                                </div>
                                
                                <div class="box-body">
                                    
                                    <h4>CONFIRMED FLIGHT BUT OEC/VISA NOT YET RELEASED</h4>
                                    
                                    <div class="row">
                                        <div class="col-md-12">
         							                      
                                            <table class="table table-bordered table-stripe">
                                                <thead>
                                                    <tr>
                                                        <th>Computer No.</th>
                                                        <th>Name</th>
                                                        <th>Ref No.</th>
                                                        <th>RSO</th>
                                                        <th>Booking Date</th>
                                                        <th>Status</th>
                                                        <th>OEC Release Date</th>
                                                        <th>Visa Release Date</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>I-1175-15</td>
                                                        <td>DIVINAGRACIA, DENNIS DORONILA</td>
                                                        <td>AGC-01L15-E1</td>
                                                        <td>carilyn</td>
                                                        <td>Feb 10, 2016</td>
                                                        <td>CONFIRMED</td>
                                                        <td>Feb 1, 2016</td>
                                                        <!-- [class="danger"] for "pink" | [class="expired"] for "red" -->
                                                        <td class="danger">sd</td>
                                                        <td><button type="button" class="btn btn-default btn-sm btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Open File"><i class="fa fa-folder-open"></i></button></td>
                                                    </tr>
                                                    <tr>
                                                        <td>F-1894-15</td>
                                                        <td>DE CASTRO, REYMOND ARLOS</td>
                                                        <td>AGC-01L15-E1</td>
                                                        <td>carilyn</td>
                                                        <td>Feb 10, 2016</td>
                                                        <td>CONFIRMED</td>
                                                        <td>Jan 29, 2016</td>
                                                        <!-- [class="danger"] for "pink" | [class="expired"] for "red" -->
                                                        <td class="expired"></td>
                                                        <td><button type="button" class="btn btn-default btn-sm btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Open File"><i class="fa fa-folder-open"></i></button></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            
                                        </div>
                                    </div>

                                    <hr />
                                    
                                    <h4>CONFIRMED FLIGHT BUT OEC IS EXPIRED</h4>
                                    
                                    <div class="row">
                                        <div class="col-md-12">
							
                                            <table class="table table-bordered table-stripe">
                                                <thead>
                                                    <tr>
                                                        <th>Computer No.</th>
                                                        <th>Name</th>
                                                        <th>Ref No.</th>
                                                        <th>RSO</th>
                                                        <th>Booking Date</th>
                                                        <th>Status</th>
                                                        <th>OEC Release Date</th>
                                                        <th>OEC Expiry Date</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>F-2643-15</td>
                                                        <td>JAMILLA, RICHARD MENDEZ</td>
                                                        <td>ES.RDB-01E15-E1</td>
                                                        <td>carilyn</td>
                                                        <td>Mar 3, 2016</td>
                                                        <td>CONFIRMED</td>
                                                        <td>Dec 14, 2015</td>
                                                        <!-- [class="danger"] for "pink" | [class="expired"] for "red" -->
                                                        <td class="expired">Feb 14, 2016</td>
                                                        <td><button type="button" class="btn btn-default btn-sm btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Open File"><i class="fa fa-folder-open"></i></button></td>
                                                    </tr>
                                                    <tr>
                                                        <td>C-1943-14</td>
                                                        <td>VILLANUEVA, FILTER ESCONDE</td>
                                                        <td>ES.RDB-01E15-E1</td>
                                                        <td>carilyn</td>
                                                        <td>Mar 3, 2016</td>
                                                        <td>CONFIRMED</td>
                                                        <td>Dec 16, 2015</td>
                                                        <!-- [class="danger"] for "pink" | [class="expired"] for "red" -->
                                                        <td class="expired">Feb 16, 2016</td>
                                                        <td><button type="button" class="btn btn-default btn-sm btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Open File"><i class="fa fa-folder-open"></i></button></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            
                                        </div>
                                    </div>

                                    <hr />
                                    
                                    <h4>NO CONFIRMED BOOKING BUT VISA IS EXPIRING</h4>
                                    
                                    <div class="row">
                                        <div class="col-md-12">
							
                                            <table class="table table-bordered table-stripe">
                                                <thead>
                                                    <tr>
                                                        <th>Computer No.</th>
                                                        <th>Name</th>
                                                        <th>Ref No.</th>
                                                        <th>RSO</th>
                                                        <th>VISA Release Date</th>
                                                        <th>VISA Expiry Date</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>H-0458-15</td>
                                                        <td>ELEAZAR, ARIEL ORTEZA</td>
                                                        <td>JPK.JPK-Plant-02G15-E1</td>
                                                        <td>vida</td>
                                                        <td>Dec 7, 2015</td>
                                                        <!-- [class="danger"] for "pink" | [class="expired"] for "red" -->
                                                        <td class="danger">Mar 6, 2016</td>
                                                        <td><button type="button" class="btn btn-default btn-sm btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Open File"><i class="fa fa-folder-open"></i></button></td>
                                                    </tr>
                                                    <tr>
                                                        <td>I-1764-15</td>
                                                        <td>GUTIERREZ, RUSSEL MATALOG</td>
                                                        <td>DE-01K15-E1</td>
                                                        <td>vida</td>
                                                        <td>Jan 4, 2016</td>
                                                        <!-- [class="danger"] for "pink" | [class="expired"] for "red" -->
                                                        <td class="danger">Mar 2, 2016</td>
                                                        <td><button type="button" class="btn btn-default btn-sm btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Open File"><i class="fa fa-folder-open"></i></button></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            
                                        </div>
                                    </div>

                                </div>

                            </div>
                            <!-- end of Add User Box -->

                    </div>
                </div>

            </section>
            <!-- end of MAIN CONTENT -->