<div class="modal fade" id="modalAddStatusInterview" tabindex="-1" role="dialog" aria-labelledby="modalAddStatusInterviewLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <!--<div class="modal-header">
                <h4 class="modal-title" id="modalAddWorkLabel"><i class="ion-university"></i> Education</h4>
            </div>-->
            <div class="modal-body">
                
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modalAddStatusInterviewLabel"><i class="fa fa-comments"></i> Status of Interview</h4>
                
                <form class="form-horizontal">
                <div class="box-body">
                    
                    <div class="form-group">
                        <label for="selectEvaluator" class="col-sm-4 control-label">Evaluator<span class="required">*</span>:</label>
                        <div class="col-sm-4">
                            <select class="form-control" id="selectEvaluator">
                                <option value=""></option>
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="selectMode" class="col-sm-4 control-label">Mode<span class="required">*</span>:</label>
                        <div class="col-sm-2">
                            <select class="form-control" id="selectMode">
                                <option value=""></option>
                                <option value="RC">RC</option>
                                <option value="PS">PS</option>
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="textInternalAdvisory" class="col-sm-4 control-label">Internal Advisory<span class="required">*</span>:</label>
                        <div class="col-sm-8">
                            <textarea class="form-control" id="textInternalAdvisory"></textarea>
                        </div>
                    </div>
                    
                </div>
                </form>
                
            </div>
            
            <div class="modal-footer">
                <div class="row">
                    <div class="col-sm-3 col-sm-offset-9">
                        <a href="#" type="button" class="btn btn-sm btn-flat btn-block btn-primary" data-dismiss="modal">Add</a>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>