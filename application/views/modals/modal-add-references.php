<div class="modal fade" id="modalAddReferences" tabindex="-1" role="dialog" aria-labelledby="modalAddReferencesLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <!--<div class="modal-header">
                <h4 class="modal-title" id="modalAddWorkLabel"><i class="ion-university"></i> Education</h4>
            </div>-->
            <div class="modal-body">
                
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modalAddReferencesLabel"><i class="fa fa-users-plus"></i> References</h4>
                
                <form class="form-horizontal">
                <div class="box-body">
                        
                    <div class="form-group">
                        <label for="checkboxStatus" class="col-sm-3 control-label"></label>
                        <div class="col-sm-8">
                            <label class="user-status"><input type="checkbox" id="checkboxStatus" class="flat-blue" name="not-applicable" value="not-applicable" /> <strong>Not Applicable</strong> (No Reference)</label>
                        </div>
                    </div>
                        
                    <hr />
                    
                    <div class="form-group">
                        <label for="inputLastName" class="col-sm-3 control-label">Name<span class="required">*</span>:</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="inputLastName" placeholder="Last Name" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-6 col-sm-offset-3">
                            <input type="text" class="form-control" id="inputFirstName" placeholder="First Name" />
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="inputMI" placeholder="M.I." />
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="inputPositionTitle" class="col-sm-3 control-label">Position Title<span class="required">*</span>:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="inputPositionTitle" />
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="inputCompanyName" class="col-sm-3 control-label">Company Name<span class="required">*</span>:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="inputCompanyName" />
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="inputTelNum" class="col-sm-3 control-label">Tel No.<span class="required">*</span>:</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="inputTelNum" />
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="inputEmail" class="col-sm-3 control-label">Email<span class="required">*</span>:</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="inputEmail" />
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="inputRelationship" class="col-sm-3 control-label">Relationship<span class="required">*</span>:</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="inputRelationship" />
                        </div>
                    </div>
                </div>
                </form>
                
            </div>
            
            <div class="modal-footer">
                <div class="row">
                    <div class="col-sm-3 col-sm-offset-9">
                        <a href="#" type="button" class="btn btn-sm btn-flat btn-block btn-primary" data-dismiss="modal">Add</a>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>