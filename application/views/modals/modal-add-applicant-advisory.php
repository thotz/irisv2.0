<div class="modal fade" id="modalAddAppAdv" tabindex="-1" role="dialog" aria-labelledby="modalAddAppAdvLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <!--<div class="modal-header">
                <h4 class="modal-title" id="modalAddWorkLabel"><i class="ion-university"></i> Education</h4>
            </div>-->
            <div class="modal-body">
                
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modalAddAppAdvLabel"><i class="fa fa-quote-left"></i> Add Internal Advisory Remarks</h4>
                
                <form class="form-horizontal">
                <div class="box-body">
                    
                    <div class="form-group">
                        <label for="selectFollowUpTemplate" class="col-sm-4 control-label">Follow-up Template :</label>
                        <div class="col-sm-8">
                            <select class="form-control" id="selectFollowUpTemplate"></select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="textFollowUpRemarks" class="col-sm-4 control-label">Add Follow-up Remarks :</label>
                        <div class="col-sm-8">
                            <textarea class="form-control" id="textFollowUpRemarks"></textarea>
                        </div>
                    </div>
                    
                    
                </div>
                </form>
                
            </div>
            
            <div class="modal-footer">
                <div class="row">
                    <div class="col-sm-3 col-sm-offset-9">
                        <a href="#" type="button" class="btn btn-sm btn-flat btn-block btn-primary" data-dismiss="modal">Add</a>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>