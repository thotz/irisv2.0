<div class="modal fade" id="modalMessageBox" tabindex="-1" role="dialog" aria-labelledby="modalMessageBoxLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            
            <form class="form-horizontal">
                
                <div class="modal-header">
                    <h4 class="modal-title" id="modalMessageBoxLabel">Compose SMS</h4>
                </div>
                
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">

                            <div class="form-group">
                                <label for="inputName" class="col-sm-3 control-label">Name :</label>
                                <div class="col-sm-9">
                                    <input type="text" id="inputName" class="form-control" value="Applicant's Name" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="selectTemplate" class="col-sm-3 control-label">Template :</label>
                                <div class="col-sm-6">
                                    <select id="selectTemplate" class="form-control">
                                        <option value="">--</option>
                                        <option value="Door 4-5, Waterside Living Complex, L. Pacana St. Licoan, Cagayan de Oro City Tel (88)8521171 Fax (88)8562974">OFFICE CDO</option>
                                        <option value="1st &amp; 2nd Flr. ATC Bldg., 128 F. Ramos St. Cebu City 6000 (beside St. Paul College) Tel (32)2539493 Fax (32)2383946">OFFICE CEBU</option>
                                        <option value="1059 Metropolitan Ave. San Antonio Village, Makati City (near corner of Pasong Tamo, accross SHOPWISE Supermarket) Tel 8956911 Fax 8953585">OFFICE MANILA</option>
                                        <option value="National Highway, Barangay Alangilan, Batangas City (at the back of Athena School, in front of Analyn Subd.) Fax (43)7237796">OFFICE BATANGAS</option>
                                        <option value="Units A-C, 3rd Floor Blue Building, JP Rizal Extension Barangay 38-D, Davao City (along Claveria, near Mormons church)">OFFICE DAVAO</option>
                                        <option value="Rm 304 A. Chan Bldg. Lacson St. Mandalagan, Bacolod City (in front of Robinsons Place) Tel/Fax (034)4410386">OFFICE BACOLOD</option>
                                        <option value="Please report to East West tomorrow for medical examination. Bring medical fee of P2580 and additional of 200 (ECG) for 40 yrs old and above, passport copy, four(4) copies of 2x2 pictures, two(2) clean boiled bottles one must contain thumbsize sample of stool (label the 2 bottles with your name). Eight(8) hours fasting is required. For more information please call 8956911.">MEDICAL</option>
                                        <option value="This is from East West Agency, your medical is pending due to (medical findings). You are requested to report back to the clinic ASAP. Please give us update once you are finished by calling 8956911 or by replying to this message. Thank you.">MEDICAL PENDING</option>
                                        <option value="To view our complete list of jobs 4 abroad, pls visit www.eastwest.com.ph and go to Job Openings.">JOB OPENINGS</option>
                                        <option value="Hi Mr. [Name]! We have current job openings for you and we shall be glad to assist you once again.  Please visit our website at www.eastwest.com.ph and go to applicant Log-in to activate your application with us and check our JOB OPENINGS and apply.  You may also visit our nearest branch office for faster processing of your application.  We hope to hear from you soon!  Good luck!">DEPLOYED APPLICANT</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="textMessage" class="col-sm-3 control-label">Message :</label>
                                <div class="col-sm-9">
                                    <textarea type="text" id="textMessage" class="form-control"></textarea>
                                    <small>0/1200</small>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <div class="row">
                                <div class="col-md-3 col-sm-3 col-sm-offset-6">
                                    <a href="#" class="btn btn-primary btn-sm btn-block btn-flat">Save</a>
                                </div>
                                <div class="col-md-3 col-sm-3">
                                    <a href="#" class="btn btn-default btn-sm btn-block btn-flat" data-dismiss="modal" aria-label="Close">Cancel</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    
            </form>
            
        </div>
    </div>
</div>


