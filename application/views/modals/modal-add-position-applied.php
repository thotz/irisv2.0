<div class="modal fade" id="modalPositionApplied" tabindex="-1" role="dialog" aria-labelledby="modalPositionAppliedLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <!--<div class="modal-header">
                <h4 class="modal-title" id="modalAddWorkLabel"><i class="ion-university"></i> Education</h4>
            </div>-->
            <div class="modal-body">
                
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modalPositionAppliedLabel"><i class="fa fa-briefcase"></i> Position Applied</h4>
                
                <form class="form-horizontal">
                <div class="box-body">
                    
                    
                    <div class="form-group">
                        <label for="selectPositionTitle" class="col-sm-3 control-label">Position Title<span class="required">*</span>:</label>
                        <div class="col-sm-9">
                            <select class="form-control" id="selectPositionTitle"></select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="inputExpectedSalary" class="col-sm-3 control-label">Expected Salary<span class="required">*</span>:</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="inputExpectedSalary" />
                        </div>
                    </div>
                    
                </div>
                </form>
                
            </div>
            
            <div class="modal-footer">
                <div class="row">
                    <div class="col-sm-3 col-sm-offset-9">
                        <a href="#" type="button" class="btn btn-sm btn-flat btn-block btn-primary" data-dismiss="modal">Add</a>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>