<div class="modal fade" id="modalAlert" tabindex="-1" role="dialog" aria-labelledby="modalAlertLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <!--<div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">IRIS Alert</h4>
            </div>-->
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        
                <div class="nav-tabs-custom">
                    
                    <ul class="nav nav-tabs pull-right">
                        <li class=""><a href="#alert-processing" data-toggle="tab" aria-expanded="false">Processing</a></li>
                        <li class=""><a href="#alert-operations" data-toggle="tab" aria-expanded="false">Operations</a></li>
                        <li class=""><a href="#alert-recruitment" data-toggle="tab" aria-expanded="false">Recruitment</a></li>
                        <li class="active"><a href="#alert-unresolve-inbox" data-toggle="tab" aria-expanded="true">Unresolved Inbox</a></li>
                        <li class="pull-left header"><i class="fa fa-exclamation-triangle"></i> IRIS Alert</li>
                    </ul>
                
                    <div class="tab-content">
                        
                        <div class="tab-pane active" id="alert-unresolve-inbox">
                            <div class="row">
                                
                            
                            <table class="table table-condense">
                                <tbody>
                                    
                                    <tr>
                                        <td width="15%">
                                            <a href="#" class="btn btn-default btn-xs btn-flat btn-block">Count</a>
                                        </td>
                                        <td width="35%">
                                            <a href="#">UNRESOLVED INBOX - SMS</a>
                                        </td>
                                        <td width="15%">
                                            <a href="#" class="btn btn-default btn-xs btn-flat btn-block">Count</a>
                                        </td>
                                        <td width="35%">
                                            <a href="#">UNRESOLVED INBOX - ONLINE</a>
                                        </td>
                                    </tr>
                                    
                                </tbody>
                            </table>
                            
                            </div>
                        </div>
                        
                        <div class="tab-pane" id="alert-recruitment">
                            
                            <table class="table table-condense">
                                <tbody>

                                    <tr>
                                        <td width="15%">
                                            <a href="#" class="btn btn-default btn-xs btn-flat btn-block">Count</a>
                                        </td>
                                        <td width="35%">
                                            <a href="#">FOR ASSESSMENT - ONLINE</a>
                                        </td>
                                        <td width="15%">
                                            <a href="#" class="btn btn-default btn-xs btn-flat btn-block" disabled="disabled">
                                                <i class="fa fa-circle-o-notch fa-spin"></i>
                                                Counting
                                            </a>
                                        </td>
                                        <td width="35%">
                                            <a href="#">FOR ASSESSMENT - PRA/HH/JF</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="15%" class="text-center">987</td>
                                        <td width="35%">
                                            <a href="#">INITIAL LINEUP - ONLINE</a>
                                        </td>
                                    </tr>
                                    <tr class="highlight">
                                        <td width="15%">
                                            <a href="#" class="btn btn-default btn-xs btn-flat btn-block">Count</a>
                                        </td>
                                        <td width="35%">
                                            <a href="#">FOR CV REVIEW</a>
                                        </td>
                                        <td width="15%">
                                            <a href="#" class="btn btn-default btn-xs btn-flat btn-block">Count</a>
                                        </td>
                                        <td width="35%">
                                            <a href="#">FOR CV SENDING</a>
                                        </td>
                                    </tr>
                                    <tr class="highlight">
                                        <td width="15%">
                                            <a href="#" class="btn btn-default btn-xs btn-flat btn-block">Count</a>
                                        </td>
                                        <td width="35%">
                                            <a href="#">FOR FOLLOW UP SELECTION RESULT</a>
                                        </td>
                                        <td width="15%">
                                            <a href="#" class="btn btn-default btn-xs btn-flat btn-block">Count</a>
                                        </td>
                                        <td width="35%">
                                            <a href="#">FOR TRANSFER TO OPBD</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="15%">
                                            <a href="#" class="btn btn-default btn-xs btn-flat btn-block">Count</a>
                                        </td>
                                        <td width="35%">
                                            <a href="#">FOR MR CLOSING</a>
                                        </td>
                                        <td width="15%">
                                            <a href="#" class="btn btn-default btn-xs btn-flat btn-block">Count</a>
                                        </td>
                                        <td width="35%">
                                            <a href="#">FOR CV DISPOSAL</a>
                                        </td>
                                    </tr> 
                                    <tr>
                                        <td colspan="2">
                                            <a href="#">PROJECT &amp; MR DISTRIBUTION REPORT</a>
                                        </td>
                                        <td colspan="2">
                                            <a href="#">LINE UP MONITORING</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <a href="#">INTERVIEW MONITORING</a>
                                        </td>
                                        <td colspan="2">
                                            <a href="#">CLIENT INTERVIEW REPORT</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <a href="#">RECRUITMENT MISSION CALENDAR</a>
                                        </td>
                                        <td colspan="2">
                                            <a href="#">PERFORMANCE REPORT</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <a href="#">ONLINE RESPONDENTS SUMMARY REPORT</a>
                                        </td>
                                        <td colspan="2">
                                            <a href="#">DATABANK COUNT PER APPLICANT SOURCE</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <a href="#">DATABANK COUNT PER CATEGORY</a>
                                        </td>
                                        <td colspan="2">
                                            <a href="#">SALARY RANGE</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <a href="#">CV SENDING MONITORING REPORT</a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            
                        </div>
                        
                        <div class="tab-pane" id="alert-operations">
                            
                            <table class="table table-condense">
                                <tbody>
                                    	
                                    <tr>
                                        <td colspan="2">
                                            <a href="#">APPLICANTS WITHOUT APPROVAL</a>
                                        </td>
                                        <td colspan="2">
                                            <a href="#">MEDICAL MONITORING CHART</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <a href="#">WEEKLY CALENDAR - STATUS/MONITORING REPORT</a>
                                        </td>
                                        <td colspan="2">
                                            <a href="#">MONTHLY VISA MONITORING REPORT</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <a href="#">PRE-EMPLOYMENT ORIENTATION</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="15%">
                                            <a href="#" class="btn btn-default btn-xs btn-flat btn-block">Count</a>
                                        </td>
                                        <td width="35%">
                                            <a href="#">CV SENDING SELECTION</a>
                                        </td>
                                        <td width="15%">
                                            <a href="#" class="btn btn-default btn-xs btn-flat btn-block">Count</a>
                                        </td>
                                        <td width="35%">
                                            <a href="#">FOR MEDICAL - MED SEC NO DECKING</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="15%">
                                            <a href="#" class="btn btn-default btn-xs btn-flat btn-block">Count</a>
                                        </td>
                                        <td width="35%">
                                            <a href="#">FOR MEDICAL - MED SEC DECKING</a>
                                        </td>
                                        <td width="15%">
                                            <a href="#" class="btn btn-default btn-xs btn-flat btn-block">Count</a>
                                        </td>
                                        <td width="35%">
                                            <a href="#">FOR MEDICAL - OPERATION</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="15%">
                                            <a href="#" class="btn btn-default btn-xs btn-flat btn-block">Count</a>
                                        </td>
                                        <td width="35%">
                                            <a href="#">GO MEDICAL</a>
                                        </td>
                                        <td width="15%">
                                            <a href="#" class="btn btn-default btn-xs btn-flat btn-block">Count</a>
                                        </td>
                                        <td width="35%">
                                            <a href="#">PENDING MEDICAL </a>
                                        </td>
                                    </tr>
                                    <tr class="highlight">
                                        <td width="15%">
                                            <a href="#" class="btn btn-default btn-xs btn-flat btn-block">Count</a>
                                        </td>
                                        <td width="35%">
                                            <a href="#">FOR ENDORSEMENT CTD - OPTN</a>
                                        </td>
                                        <td width="15%">
                                            <a href="#" class="btn btn-default btn-xs btn-flat btn-block">Count</a>
                                        </td>
                                        <td width="35%">
                                            <a href="#">FIT TO WORK NCTD - OPTN</a>
                                        </td>
                                    </tr>
                                    <tr class="highlight">
                                        <td width="15%">
                                            <a href="#" class="btn btn-default btn-xs btn-flat btn-block">Count</a>
                                        </td>
                                        <td width="35%">
                                            <a href="#">FIT TO WORK NONKSA - OPTN</a>
                                        </td>
                                        <td width="15%">
                                            <a href="#" class="btn btn-default btn-xs btn-flat btn-block">Count</a>
                                        </td>
                                        <td width="35%">
                                            <a href="#">FOR VISA DOCUMENT PREPARATION</a>
                                        </td>
                                    </tr>
                                    <tr class="highlight">
                                        <td width="15%">
                                            <a href="#" class="btn btn-default btn-xs btn-flat btn-block">Count</a>
                                        </td>
                                        <td width="35%">
                                            <a href="#">FOR VISA DOC SENDING - NONKSA - OPTN</a>
                                        </td>
                                        <td width="15%">
                                            <a href="#" class="btn btn-default btn-xs btn-flat btn-block">Count</a>
                                        </td>
                                        <td width="35%">
                                            <a href="#">AWAITING VISA - NONKSA</a>
                                        </td>
                                    </tr>
                                    <tr class="highlight">
                                        <td width="15%">
                                            <a href="#" class="btn btn-default btn-xs btn-flat btn-block">Count</a>
                                        </td>
                                        <td width="35%">
                                            <a href="#">AWAITING VISA - NONKSA FINAL STATUS</a>
                                        </td>
                                        <td width="15%">
                                            <a href="#" class="btn btn-default btn-xs btn-flat btn-block">Count</a>
                                        </td>
                                        <td width="35%">
                                            <a href="#">VISA RECEIVED - NONKSA</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="15%">
                                            <a href="#" class="btn btn-default btn-xs btn-flat btn-block">Count</a>
                                        </td>
                                        <td width="35%">
                                            <a href="#">ENDORSED - DOC SECTION</a>
                                        </td>
                                        <td width="15%">
                                            <a href="#" class="btn btn-default btn-xs btn-flat btn-block">Count</a>
                                        </td>
                                        <td width="35%">
                                            <a href="#">VISA SUBMISSION</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="15%">
                                            <a href="#" class="btn btn-default btn-xs btn-flat btn-block">Count</a>
                                        </td>
                                        <td width="35%">
                                            <a href="#">VISA SUBMITTED</a>
                                        </td>
                                        <td width="15%">
                                            <a href="#" class="btn btn-default btn-xs btn-flat btn-block">Count</a>
                                        </td>
                                        <td width="35%">
                                            <a href="#">POEA SUBMISSION</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="15%">
                                            <a href="#" class="btn btn-default btn-xs btn-flat btn-block">Count</a>
                                        </td>
                                        <td width="35%">
                                            <a href="#">POEA SUBMITTED</a>
                                        </td>
                                    </tr>
                                    <tr class="highlight">
                                        <td width="15%">
                                            <a href="#" class="btn btn-default btn-xs btn-flat btn-block">Count</a>
                                        </td>
                                        <td width="35%">
                                            <a href="#">FOR BOOKING REQUEST</a>
                                        </td>
                                        <td width="15%">
                                            <a href="#" class="btn btn-default btn-xs btn-flat btn-block">Count</a>
                                        </td>
                                        <td width="35%">
                                            <a href="#">FOR BOOKING REQUEST - FINAL STATUS</a>
                                        </td>
                                    </tr>
                                    <tr class="highlight">
                                        <td width="15%">
                                            <a href="#" class="btn btn-default btn-xs btn-flat btn-block">Count</a>
                                        </td>
                                        <td width="35%">
                                            <a href="#">BOOKING REQUEST - PTA</a>
                                        </td>
                                        <td width="15%">
                                            <a href="#" class="btn btn-default btn-xs btn-flat btn-block">Count</a>
                                        </td>
                                        <td width="35%">
                                            <a href="#">BOOKING REQUEST - PTA FINAL STATUS</a>
                                        </td>
                                    </tr>
                                    <tr class="highlight">
                                        <td width="15%">
                                            <a href="#" class="btn btn-default btn-xs btn-flat btn-block">Count</a>
                                        </td>
                                        <td width="35%">
                                            <a href="#">BOOKING REQUEST - LPT</a>
                                        </td>
                                        <td width="15%">
                                            <a href="#" class="btn btn-default btn-xs btn-flat btn-block">Count</a>
                                        </td>
                                        <td width="35%">
                                            <a href="#">BOOKING REQUEST - LPT FINAL STATUS</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="15%">
                                            <a href="#" class="btn btn-default btn-xs btn-flat btn-block">Count</a>
                                        </td>
                                        <td width="35%">
                                            <a href="#">FOR DEPLOYMENT</a>
                                        </td>
                                    </tr>
                                    <tr class="highlight">
                                        <td width="15%">
                                            <a href="#" class="btn btn-default btn-xs btn-flat btn-block">Count</a>
                                        </td>
                                        <td width="35%">
                                            <a href="#">DEPLOYED FOR UPDATING</a>
                                        </td>
                                        <td width="15%">
                                            <a href="#" class="btn btn-default btn-xs btn-flat btn-block">Count</a>
                                        </td>
                                        <td width="35%">
                                            <a href="#">DEPLOYED</a>
                                        </td>
                                    </tr>
                                
                                </tbody>
                            </table>
                            
                        </div>
                        
                        <div class="tab-pane" id="alert-processing">
                            
                            <table class="table table-condense">
                                <tbody>
                                    <tr>
                                        <td width="15%">
                                            <a href="#" class="btn btn-default btn-xs btn-flat btn-block">Count</a>
                                        </td>
                                        <td width="35%">
                                            <a href="#">EXPIRING/EXPIRED OEC</a>
                                        </td>
                                        <td width="15%">
                                            <a href="#" class="btn btn-default btn-xs btn-flat btn-block">Count</a>
                                        </td>
                                        <td width="35%">
                                            <a href="#">EXPIRING ACCRE</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="15%">
                                            <a href="#" class="btn btn-default btn-xs btn-flat btn-block">Count</a>
                                        </td>
                                        <td width="35%">
                                            <a href="#">FOR RENEWAL OF ACCRE</a>
                                        </td>
                                        <td width="15%">
                                            <a href="#" class="btn btn-default btn-xs btn-flat btn-block">Count</a>
                                        </td>
                                        <td width="35%">
                                            <a href="#">FOR INTIAL ACCRE</a>
                                        </td>
                                    </tr>
                                
                                </tbody>
                            </table>
                            
                        </div>
                        
                    </div>
                    
                </div>
            </div>
            
            <!--<div class="modal-footer">
                <button type="button" class="btn btn-sm btn-flat btn-default" data-dismiss="modal">Close</button>
                <p class="pull-left text-sm">Click <button type="button" class="btn btn-xs btn-flat btn-warning">New Applicant</button> if record is not found.</p>
            </div>-->
            
        </div>
    </div>
</div>