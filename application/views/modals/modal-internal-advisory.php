<div class="modal fade" id="modalInternalAdvisory" tabindex="-1" role="dialog" aria-labelledby="modalInternalAdvisoryLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            
            <form class="form">
                
                <!--<div class="modal-header">
                    <h4 class="modal-title" id="modalInternalAdvisoryLabel">Compose SMS</h4>
                </div>-->
                
                <div class="modal-body">
                    <!--<div class="row">
                        <div class="col-md-10 col-md-offset-1">-->

                            <div class="form-group">
                                <label for="textInternalAdvisory">Internal Advisory</label>
                                <textarea class="form-control" id="textInternalAdvisory" placeholder="Please write actual reason">
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus egestas libero sed venenatis suscipit. Phasellus sit amet diam sed nisl mollis molestie vitae ut odio. Fusce dui eros, laoreet at congue non, venenatis aliquet elit.

Etiam eu tempus tortor, posuere viverra eros. Nulla a tortor a sapien porttitor dapibus ac eu ante. Sed tincidunt accumsan lacus at laoreet. Nulla id egestas mauris. Nullam quis elementum urna. Pellentesque tincidunt lectus ut lectus iaculis, eget fermentum arcu mattis. Ut pulvinar, orci ut viverra tempor, velit orci sollicitudin justo, non fringilla quam lectus eu nisl.
                                </textarea>
                            </div>
                            <div class="form-group">
                                <label for="textApplicantAdvisory">Applicant Advisory</label>
                                <br /><p>You are requesting the applicant to report to office for PRE-SCREENING. Please type your message to the applicant:</p>
                                <textarea class="form-control" id="textApplicantAdvisory">Thank you for applying to East West. Please take note of your computer number: C-0045-16 which you will be using in all your transaction and follow up with East West. Just in case you will be changing your mobile number, kindly notify us through text or edit your profile in our website. Kindly visit www.eastwest.com.ph and go to APPLICANT LOG IN, then go to EDIT PROFILE and fill up your application completely for faster processing. MORE JOB OPENINGS AWAITS FOR YOU THERE!</textarea>
                            </div>
                            
                        <!--</div>
                    </div>-->
                </div>
                
                <div class="modal-footer">
                    <!--<div class="row">
                        <div class="col-md-10 col-md-offset-1">-->
                            
                            <div class="row">
                                <div class="col-md-3 col-md-offset-9 col-sm-3 col-sm-offset-9">
                                    <a href="#" class="btn btn-primary btn-sm btn-block btn-flat" data-dismiss="modal" data-toggle="modal" data-target="#modalInternalAdvisoryPreview">Next</a>
                                </div>
                            </div>
                            
                        <!--</div>
                    </div>-->
                </div>
                    
            </form>
            
        </div>
    </div>
</div>


