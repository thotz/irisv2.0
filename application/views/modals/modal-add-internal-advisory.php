<div class="modal fade" id="modalAddIntlAdv" tabindex="-1" role="dialog" aria-labelledby="modalmodalAddIntlAdvLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <!--<div class="modal-header">
                <h4 class="modal-title" id="modalAddWorkLabel"><i class="ion-university"></i> Education</h4>
            </div>-->
            <div class="modal-body">
                
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modalAddIntlAdvLabel"><i class="fa fa-quote-left"></i> Add Internal Advisory Remarks</h4>
                
                <form class="form-horizontal">
                <div class="box-body">
                    
                    <div class="form-group">
                        <div class="col-sm-12">
                            <textarea class="form-control" id="textAddIntlAdv"></textarea>
                        </div>
                    </div>
                    
                    
                </div>
                </form>
                
            </div>
            
            <div class="modal-footer">
                <div class="row">
                    <div class="col-sm-3 col-sm-offset-9">
                        <a href="#" type="button" class="btn btn-sm btn-flat btn-block btn-primary" data-dismiss="modal">Add</a>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>