<div class="modal fade" id="modalSearch" tabindex="-1" role="dialog" aria-labelledby="modalSearchLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <!--<div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Search Modal</h4>
            </div>-->
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        
                <div class="nav-tabs-custom">
                    
                    <ul class="nav nav-tabs pull-right">
                        <li id="tab_modalsearch_mr" class=""><a href="#search-tab-mr" data-toggle="tab" aria-expanded="false">MR</a></li>
                        <li id="tab_modalsearch_name" class=""><a href="#search-tab-name" data-toggle="tab" aria-expanded="false">Name</a></li>
                        <li id="tab_modalsearch_computer" class="active"><a href="#search-tab-computer" data-toggle="tab" aria-expanded="true">Computer</a></li>
                        <li class="pull-left header"><i class="fa fa-search"></i> Search Portal</li>
                    </ul>
                
                    <div class="tab-content">
                        
                        <!-- Form: Search by Computer No. -->
                        <div class="tab-pane active" id="search-tab-computer">

                            <!-- ALERT MESSAGE [ERROR]
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <i class="icon fa fa-ban"></i> <strong>ERROR!</strong> No record found.
                            </div> -->
                            
                            <form class="form-horizontal" name="form_search_computer" id="form_search_computer" method="POST">
                                <div class="form-group">
                                	<input type="hidden" name="search_tab" value="computer" />
                                	<input type="hidden" name="search_param" value="applicant_id" />
                                	<input type="hidden" name="search_param_additional" value="" />
                                	<input type="hidden" name="search_link" value="reception/followup/applicant_info" />
                                	<input type="hidden" name="page" id="page_computer" value="0" />
                                    <label for="searchcomputer" class="col-sm-3 control-label">Search by Computer No.:</label>
                                    <div class="col-sm-6 has-feedback">
                                        <input type="text" class="form-control" name="search_keyword" id="search_computer" placeholder="A-0000-00">
                                        <span class="glyphicon glyphicon-search form-control-feedback"></span>
                                    </div>
                                </div>
                                <!--<div class="form-group">
                                    <div class="col-sm-3 col-sm-offset-3">
                                        <a href="#" class="btn btn-primary btn-flat btn-block">Search</a>
                                    </div>
                                </div>-->
                            </form>
                            
                            <table id="tableSearchResult" class="table table-hover table-bordered table-stripe table-condensed"></table>
                            
                            <?php //include APPPATH.'views/modals/data-table/table-search-result.php';?>
                            
                        </div>
                        
                        <!-- Form: Search by Name -->
                        <div class="tab-pane" id="search-tab-name">

                            <!-- ALERT MESSAGE [ERROR]
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <i class="icon fa fa-ban"></i> <strong>ERROR!</strong> No record found.
                            </div> -->
                            
                            <form class="form-horizontal" name="form_search_name" id="form_search_name" method="POST">
                                <div class="form-group">
                                	<input type="hidden" name="search_tab" value="name" />
                                	<input type="hidden" name="search_param" value="applicant_id" />
                                	<input type="hidden" name="search_param_additional" value="" />
                                	<input type="hidden" name="search_link" value="reception/followup/applicant_info" />
                                	<input type="hidden" name="page" id="page_name" value="0" />
                                    <label for="searchComputerName" class="col-sm-3 control-label">Search by Name:</label>
                                    <div class="col-sm-6 has-feedback">
                                        <input type="text" class="form-control" name="search_keyword" id="search_name" placeholder="Last Name, First Name">
                                        <span class="glyphicon glyphicon-search form-control-feedback"></span>
                                    </div>
                                </div>
                                <!--<div class="form-group">
                                    <div class="col-sm-3 col-sm-offset-3">
                                        <a href="#" class="btn btn-primary btn-flat btn-block">Search</a>
                                    </div>
                                </div>-->
                            </form>
                            
                            <table id="tableSearchResultName" class="table table-hover table-bordered table-stripe table-condensed"></table>
                            
                           	<?php //include APPPATH.'views/modals/data-table/table-search-result_name.php';?>
                            
							<div class="table-custom-pagination" data-toggle="buttons">
							    <a href="#" class="btn btn-sm btn-default btn-flat" style="display:none;" id="pager_l_name"><i class="fa fa-caret-left"></i></a>
							    <span id="txt_page_name" style="display:none;"><small>Records <strong><span id="span_pageno_name"></span></strong> to <strong><span id="span_total_pages_name">20</span></strong> of <strong><span id="span_all_total_name">99</span></strong></small></span>
							    <a href="#" class="btn btn-sm btn-default btn-flat" style="display:none;" id="pager_r_name"><i class="fa fa-caret-right"></i></a>
							</div>
                            
                        </div>
                        
                        
                        <div class="tab-pane" id="search-tab-mr">

                            <!-- ALERT MESSAGE [ERROR]
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <i class="icon fa fa-ban"></i> <strong>ERROR!</strong> No record found.
                            </div> -->
                            
                            <form class="form-horizontal" name="form_search_mr" id="form_search_mr" method="POST">
                                <div class="form-group">
                                	<input type="hidden" name="search_tab" value="mr" />
                                	<input type="hidden" name="search_param" value="applicant_id" />
                                	<input type="hidden" name="search_param_additional" value="" />
                                	<input type="hidden" name="search_link" value="reception/followup/applicant_info" />
                                	<input type="hidden" name="page" id="page_mr" value="0" />
                                    <label for="searchMR" class="col-sm-3 control-label">Search by MR:</label>
                                    <div class="col-sm-6 has-feedback">
                                        <input type="text" class="form-control" name="search_keyword" id="search_mr" placeholder="Reference No.">
                                        <span class="glyphicon glyphicon-search form-control-feedback"></span>
                                    </div>
                                    <div class="col-sm-3">
                                        <select class="form-control" name="search_mr_status" id="search_mr_status">
                                            <option value="">All Status</option>
                                            <option value="Active" selected="selected">Active</option>
                                            <option value="On Hold">On Hold</option>
                                            <option value="Close">Close</option>
                                        </select>
                                    </div>
                                </div>
                                <!--<div class="form-group">
                                    <div class="col-sm-3 col-sm-offset-3">
                                        <a href="#" class="btn btn-primary btn-flat btn-block">Search</a>
                                    </div>
                                </div>-->
                            </form>
                            
                            <table id="tableSearchResultMR" class="table table-hover table-bordered table-stripe table-condensed"></table>
                            
                            <?php //include APPPATH.'views/modals/data-table/table-search-result_mr.php';?>
                            
							<div class="table-custom-pagination" data-toggle="buttons">
							    <a href="#" class="btn btn-sm btn-default btn-flat" style="display:none;" id="pager_l_mr"><i class="fa fa-caret-left"></i></a>
							    <span id="txt_page_mr" style="display:none;"><small>Records <strong><span id="span_pageno_mr"></span></strong> to <strong><span id="span_total_pages_mr">20</span></strong> of <strong><span id="span_all_total_mr">99</span></strong></small></span>
							    <a href="#" class="btn btn-sm btn-default btn-flat" style="display:none;" id="pager_r_mr"><i class="fa fa-caret-right"></i></a>
							</div>
                            
                        </div>
                        
                    </div>
                    
                </div>
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-flat btn-default" data-dismiss="modal">Close</button>
                <p class="pull-left text-sm">Click <button type="button" onclick="javascript:window.location='<?=base_url()?>reception/create_applicant/';" class="btn btn-xs btn-flat btn-warning">New Applicant</button> if record is not found.</p>
            </div>
            
        </div>
    </div>
</div>

