<div class="modal fade" id="modalAddWork" tabindex="-1" role="dialog" aria-labelledby="modalAddWorkLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <!--<div class="modal-header">
                <h4 class="modal-title" id="modalAddWorkLabel"><i class="ion-university"></i> Education</h4>
            </div>-->
            <div class="modal-body">
                
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modalAddWorkLabel"><i class="fa fa-briefcase"></i> Work</h4>
                
                <form class="form-horizontal">
                <div class="box-body">
                        
                    <div class="form-group">
                        <label for="checkboxStatus" class="col-sm-3 control-label"></label>
                        <div class="col-sm-9">
                            <label class="user-status"><input type="checkbox" id="checkboxStatus" class="flat-blue" name="not-applicable" value="not-applicable" /> <strong>Fresh Graduate</strong> (No Working Experience)</label>
                        </div>
                    </div>
                        
                    <hr />
                    
                    <div class="form-group">
                        <label for="inputCompany" class="col-sm-3 control-label">Company<span class="required">*</span>:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="inputCompany" />
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="inputPosition" class="col-sm-3 control-label">Position<span class="required">*</span>:</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="inputPosition" />
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="inputDepartment" class="col-sm-3 control-label">Department<span class="required">*</span>:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="inputDepartment" />
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="inputSalary" class="col-sm-3 control-label">Salary<span class="required">*</span>:</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="inputSalary" />
                        </div>
                    </div>
                        
                    <div class="form-group">
                        <label for="selectDateFromMonth" class="col-sm-3 control-label">Date From<span class="required">*</span>:</label>
                        <div class="col-sm-3">
                            <select class="form-control" id="selectDateFromMonth">
                                <option value="Month">Month</option>
                                <option value="January">January</option>
                                <option value="February">February</option>
                                <option value="March">March</option>
                                <option value="April">April</option>
                                <option value="May">May</option>
                                <option value="June">June</option>
                                <option value="July">July</option>
                                <option value="August">August</option>
                                <option value="September">September</option>
                                <option value="October">October</option>
                                <option value="November">November</option>
                                <option value="December">December</option>
                            </select>
                        </div>
                        <div class="col-sm-3">
                            <select class="form-control" id="selectDateFromYear">
                                <option value="">YYYY</option><option value="2016">2016</option><option value="2015">2015</option><option value="2014">2014</option><option value="2013">2013</option><option value="2012">2012</option><option value="2011">2011</option><option value="2010">2010</option><option value="2009">2009</option><option value="2008">2008</option><option value="2007">2007</option><option value="2006">2006</option><option value="2005">2005</option><option value="2004">2004</option><option value="2003">2003</option><option value="2002">2002</option><option value="2001">2001</option><option value="2000">2000</option><option value="1999">1999</option><option value="1998">1998</option><option value="1997">1997</option><option value="1996">1996</option><option value="1995">1995</option><option value="1994">1994</option><option value="1993">1993</option><option value="1992">1992</option><option value="1991">1991</option><option value="1990">1990</option><option value="1989">1989</option><option value="1988">1988</option><option value="1987">1987</option><option value="1986">1986</option><option value="1985">1985</option><option value="1984">1984</option><option value="1983">1983</option><option value="1982">1982</option><option value="1981">1981</option><option value="1980">1980</option><option value="1979">1979</option><option value="1978">1978</option><option value="1977">1977</option><option value="1976">1976</option><option value="1975">1975</option><option value="1974">1974</option><option value="1973">1973</option><option value="1972">1972</option><option value="1971">1971</option><option value="1970">1970</option><option value="1969">1969</option><option value="1968">1968</option><option value="1967">1967</option><option value="1966">1966</option>
                            </select>
                        </div>
                    </div>
                        
                    <div class="form-group">
                        <label for="selectDateToMonth" class="col-sm-3 control-label">Date To<span class="required">*</span>:</label>
                        <div class="col-sm-3">
                            <select class="form-control" id="selectDateToMonth">
                                <option value="Month">Month</option>
                                <option value="January">January</option>
                                <option value="February">February</option>
                                <option value="March">March</option>
                                <option value="April">April</option>
                                <option value="May">May</option>
                                <option value="June">June</option>
                                <option value="July">July</option>
                                <option value="August">August</option>
                                <option value="September">September</option>
                                <option value="October">October</option>
                                <option value="November">November</option>
                                <option value="December">December</option>
                            </select>
                        </div>
                        <div class="col-sm-3">
                            <select class="form-control" id="selectDateToYear">
                                <option value="">YYYY</option><option value="2016">2016</option><option value="2015">2015</option><option value="2014">2014</option><option value="2013">2013</option><option value="2012">2012</option><option value="2011">2011</option><option value="2010">2010</option><option value="2009">2009</option><option value="2008">2008</option><option value="2007">2007</option><option value="2006">2006</option><option value="2005">2005</option><option value="2004">2004</option><option value="2003">2003</option><option value="2002">2002</option><option value="2001">2001</option><option value="2000">2000</option><option value="1999">1999</option><option value="1998">1998</option><option value="1997">1997</option><option value="1996">1996</option><option value="1995">1995</option><option value="1994">1994</option><option value="1993">1993</option><option value="1992">1992</option><option value="1991">1991</option><option value="1990">1990</option><option value="1989">1989</option><option value="1988">1988</option><option value="1987">1987</option><option value="1986">1986</option><option value="1985">1985</option><option value="1984">1984</option><option value="1983">1983</option><option value="1982">1982</option><option value="1981">1981</option><option value="1980">1980</option><option value="1979">1979</option><option value="1978">1978</option><option value="1977">1977</option><option value="1976">1976</option><option value="1975">1975</option><option value="1974">1974</option><option value="1973">1973</option><option value="1972">1972</option><option value="1971">1971</option><option value="1970">1970</option><option value="1969">1969</option><option value="1968">1968</option><option value="1967">1967</option><option value="1966">1966</option>
                            </select>
                        </div>
                    </div>
                        
                </div>
                </form>
                
            </div>
            
            <div class="modal-footer">
                <div class="row">
                    <div class="col-sm-3 col-sm-offset-9">
                        <a href="#" type="button" class="btn btn-sm btn-flat btn-block btn-primary" data-dismiss="modal">Add</a>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>