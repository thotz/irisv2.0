<div class="modal fade" id="modalAddLanguage" tabindex="-1" role="dialog" aria-labelledby="modalAddLanguageLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <!--<div class="modal-header">
                <h4 class="modal-title" id="modalAddWorkLabel"><i class="ion-university"></i> Education</h4>
            </div>-->
            <div class="modal-body">
                
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modalAddLanguageLabel"><i class="fa fa-comments"></i> Language</h4>
                
                <form class="form-horizontal">
                <div class="box-body">
                    
                    <div class="form-group">
                        <label for="inputLanguage" class="col-sm-4 control-label">Language<span class="required">*</span>:</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputLanguage" />
                            <small>Except "FILIPINO / TAGALOG"</small>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="selectProficiencies" class="col-sm-4 control-label">Proficiencies<span class="required">*</span>:</label>
                        <div class="col-sm-8">
                            <select class="form-control" id="selectProficiencies">
                                <option value=""></option>
                                 <option value="Read">Read</option>
                                <option value="Spoken">Spoken</option>
                                <option value="Written">Written</option>
                                <option value="Read and Spoken">Read and Spoken</option>
                                <option value="Read and Written">Read and Written</option>
                                <option value="Spoken and Written">Spoken and Written</option>
                                <option value="Spoken, Written and Read">Spoken, Written and Read</option>
                            </select>
                        </div>
                    </div>
                    
                </div>
                </form>
                
            </div>
            
            <div class="modal-footer">
                <div class="row">
                    <div class="col-sm-3 col-sm-offset-9">
                        <a href="#" type="button" class="btn btn-sm btn-flat btn-block btn-primary" data-dismiss="modal">Add</a>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>