<div class="modal fade" id="modalInternalAdvisoryPreview" tabindex="-1" role="dialog" aria-labelledby="modalInternalAdvisoryPreviewLabel">
    <div class="modal-dialog modal-lg modal-xlg" role="document">
        <div class="modal-content">
            
            <form class="form">
                
                <!--<div class="modal-header">
                    <h4 class="modal-title" id="modalInternalAdvisoryPreviewLabel">Compose SMS</h4>
                </div>-->
                
                <div class="modal-body">

                    <!-- ALERT MESSAGE [SUCCESS] -->
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <i class="icon fa fa-check"></i> <strong>SUCCESS!</strong> New record is successfully added. Then click the button <strong>Follow Up</strong>.
                    </div>

                    <!-- LOADING -->
                    <div class="alert alert-loading alert-dismissable text-center">
                        <span>L</span>&nbsp;<i class="fa fa-cog fa-spin"></i>&nbsp;<span>ADING</span>
                    </div>

                    <div class="form-group">
                        <label>Internal Advisory</label>
                        <div class="text-preview-blue">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus egestas libero sed venenatis suscipit. Phasellus sit amet diam sed nisl mollis molestie vitae ut odio. Fusce dui eros, laoreet at congue non, venenatis aliquet elit.</p>
                            <p>Etiam eu tempus tortor, posuere viverra eros. Nulla a tortor a sapien porttitor dapibus ac eu ante. Sed tincidunt accumsan lacus at laoreet. Nulla id egestas mauris. Nullam quis elementum urna. Pellentesque tincidunt lectus ut lectus iaculis, eget fermentum arcu mattis. Ut pulvinar, orci ut viverra tempor, velit orci sollicitudin justo, non fringilla quam lectus eu nisl.</p>
                        </div>
                    </div>
                    
                    
                    <div class="row">
                        <div class="col-md-4">
                            
                            <div class="form-group">
                                <label>Applicant Advisory (online)</label>
                                <br />
                                <div class="text-preview-green">
                                    <p>Hi SAMPLE,</p>
                                    <p>Thank you for applying to East West. Please take note of your computer number: C-0045-16 which you will be using in all your transaction and follow up with East West. Just in case you will be changing your mobile number, kindly notify us through text or edit your profile in our website. Kindly visit www.eastwest.com.ph and go to APPLICANT LOG IN, then go to EDIT PROFILE and fill up your application completely for faster processing. MORE JOB OPENINGS AWAITS FOR YOU THERE!</p>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-4">
                            
                            <div class="form-group">
                                <label>Applicant Advisory (email)</label>
                                <br />
                                <div class="text-preview-green">
                                    <p>Dear Sample,</p>
                                    <p>Thank you for applying to East West. Please take note of your computer number: C-0045-16 which you will be using in all your transaction and follow up with East West. Just in case you will be changing your mobile number, kindly notify us through text or edit your profile in our website. Kindly visit www.eastwest.com.ph and go to APPLICANT LOG IN, then go to EDIT PROFILE and fill up your application completely for faster processing. MORE JOB OPENINGS AWAITS FOR YOU THERE!</p>
                                    <br />
                                    <p>Best Regards,</p>
                                    <p>East West Placement Center, Inc.</p>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-4">
                            
                            <div class="form-group">
                                <label>Applicant Advisory (sms):</label>
                                <br />
                                <div class="text-preview-green">
                                    <p>EASTWEST: Hi SAMPLE! Thank you for applying to East West. Please take note of your computer number: C-0045-16 which you will be using in all your transaction and follow up with East West. Just in case you will be changing your mobile number, kindly notify us through text or edit your profile in our website. Kindly visit www.eastwest.com.ph and go to APPLICANT LOG IN, then go to EDIT PROFILE and fill up your application completely for faster processing. MORE JOB OPENINGS AWAITS FOR YOU THERE!</p>
                                </div>
                            </div>

                        </div>
                    </div>
                    
                </div>
                
                <div class="modal-footer">
                    <!--<div class="row">
                        <div class="col-md-10 col-md-offset-1">-->
                            
                            <div class="row">
                                <div class="col-md-2 col-md-offset-3 col-sm-2 col-sm-offset-4 col-xs-3">
                                    <a href="#" class="btn btn-default btn-sm btn-block btn-flat" data-dismiss="modal" data-toggle="modal" data-target="#modalInternalAdvisory">Back</a>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-9">
                                    <a href="#" class="btn btn-primary btn-sm btn-block btn-flat" data-dismiss="modal">OK</a>
                                </div>
                            </div>
                            
                        <!--</div>
                    </div>-->
                </div>
                    
            </form>
            
        </div>
    </div>
</div>


