
<table id="tableSearchResultName" class="table table-hover table-bordered table-stripe table-condensed">
    <thead>
        <tr>			
            <th>COMPUTER NO.</th>
            <th>NAME</th>
            <th>BIRTHDATE</th>
            <th>DB STATUS</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>A-4354-16</td>
            <td>ANAMA, JEFFREY BACONGA</td>
            <td>1983-05-12</td>
            <td>OPERATIONS</td>
            <td class="text-right"><a href="" type="button" class="btn btn-default btn-sm btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="View File"><i class="fa fa-folder-open"></i></a></td>
        </tr>
        <tr>
            <td>A-1586-16</td>
            <td>ANABELLE, AGUSTINO DELA CRUZ</td>
            <td>1981-07-03</td>
            <td>ON POOL</td>
            <td class="text-right"><a href="" type="button" class="btn btn-default btn-sm btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="View File"><i class="fa fa-folder-open"></i></a></td>
        </tr>
        <tr>
            <td>A-1303-16</td>
            <td>ANACION, RENE RELLEVE</td>
            <td>1982-09-26</td>
            <td>ACTIVE</td>
            <td class="text-right"><a href="" type="button" class="btn btn-default btn-sm btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="View File"><i class="fa fa-folder-open"></i></a></td>
        </tr>
        <tr>
            <td>K-3749-15</td>
            <td>ANADON, JEFRIN BAYLON</td>
            <td>1985-04-02</td>
            <td>ACTIVE</td>
            <td class="text-right"><a href="" type="button" class="btn btn-default btn-sm btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="View File"><i class="fa fa-folder-open"></i></a></td>
        </tr>
        <tr>
            <td>K-2625-15</td>
            <td>ANABEZA, ROY PINOHERMOSO</td>
            <td>1986-01-10</td>
            <td>OPERATION</td>
            <td class="text-right"><a href="" type="button" class="btn btn-default btn-sm btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="View File"><i class="fa fa-folder-open"></i></a></td>
        </tr>
    </tbody>
</table>