
<table id="tableSearchResultMR" class="table table-hover table-bordered table-stripe table-condensed">
    <thead>
        <tr>			
            <th>Reference No.</th>
            <th>Principal</th>
            <th>Principal Code</th>
            <th>Status</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>AMS-01K15-E1</td>
            <td>AL MAJAL SERVICE MASTER .</td>
            <td>AMS</td>
            <td>Active</td>
            <td class="text-right"><a href="" type="button" class="btn btn-default btn-sm btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="View MR"><i class="fa fa-folder-open"></i></a></td>
        </tr>
        <tr>
            <td>MUSAED.O &AMP; M-01K15-E1</td>
            <td>MUSAED EL SEIF AND SONS . OPERATIONS &AMP; MAINTENANCE</td>
            <td>MUSAED</td>
            <td>Active</td>
            <td class="text-right"><a href="" type="button" class="btn btn-default btn-sm btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="View MR"><i class="fa fa-folder-open"></i></a></td>
        </tr>
        <tr>
            <td>SAVOLAKSA-01L12</td>
            <td>SAVOLA GROUP PANDA/AZIZIA MARKET GROUP BRANCH/SAVOLA CO .</td>
            <td>SGP</td>
            <td>On Hold</td>
            <td class="text-right"><a href="" type="button" class="btn btn-default btn-sm btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="View MR"><i class="fa fa-folder-open"></i></a></td>
        </tr>
        <tr>
            <td>SOL.EAF-05I13-E1</td>
            <td>SAUDI OGER LTD. . E.A. FAKIEH M & E CONTRACTING</td>
            <td>SOL</td>
            <td>Close</td>
            <td class="text-right"><a href="" type="button" class="btn btn-default btn-sm btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="View MR"><i class="fa fa-folder-open"></i></a></td>
        </tr>
        <tr>
            <td>FSA-01J13-E1</td>
            <td>FREYSSINET,S.A. .</td>
            <td>FSA</td>
            <td>Close</td>
            <td class="text-right"><a href="" type="button" class="btn btn-default btn-sm btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="View MR"><i class="fa fa-folder-open"></i></a></td>
        </tr>
    </tbody>
</table>