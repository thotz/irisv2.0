<div class="modal fade" id="modalAddComputerSkill" tabindex="-1" role="dialog" aria-labelledby="modalAddComputerSkillLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <!--<div class="modal-header">
                <h4 class="modal-title" id="modalAddWorkLabel"><i class="ion-university"></i> Education</h4>
            </div>-->
            <div class="modal-body">
                
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modalAddComputerSkillLabel"><i class="fa fa-file-desktop"></i> Computer Skills</h4>
                
                <form class="form-horizontal">
                <div class="box-body">
                        
                    <div class="form-group">
                        <label for="checkboxStatus" class="col-sm-4 control-label"></label>
                        <div class="col-sm-8">
                            <label class="user-status"><input type="checkbox" id="checkboxStatus" class="flat-blue" name="not-applicable" value="not-applicable" /> <strong>Not Applicable</strong> (No Computer Skills)</label>
                        </div>
                    </div>
                        
                    <hr />
                    
                    <div class="form-group">
                        <label for="inputComputerSkillLevel" class="col-sm-4 control-label">Computer Skills Level<span class="required">*</span>:</label>
                        <div class="col-sm-4">
                            <select class="form-control" id="inputComputerSkillLevel">
                                <option value="">Select Level</option>
                                <option value="Basic">Basic</option>
                                <option value="Intermediate">Intermediate</option>
                                <option value="Advance">Advance</option>
                                <option value="Expert">Expert</option>
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="textComputerApplication" class="col-sm-4 control-label">Type of Computer Application<span class="required">*</span>:</label>
                        <div class="col-sm-8">
                            <textarea class="form-control" id="textComputerApplication"></textarea>
                            <small>Pls. separate with comma (,) each application.</small>
                        </div>
                    </div>
                    
                </div>
                </form>
                
            </div>
            
            <div class="modal-footer">
                <div class="row">
                    <div class="col-sm-3 col-sm-offset-9">
                        <a href="#" type="button" class="btn btn-sm btn-flat btn-block btn-primary" data-dismiss="modal">Add</a>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>