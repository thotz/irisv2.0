<div class="modal fade" id="modalAddEducation" tabindex="-1" role="dialog" aria-labelledby="modalAddEducationLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <!--<div class="modal-header">
                <h4 class="modal-title" id="modalAddEducationLabel"><i class="ion-university"></i> Education</h4>
            </div>-->
            <div class="modal-body">
                
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modalAddEducationLabel"><i class="fa fa-graduation-cap"></i> Education</h4>
                
                <form class="form-horizontal">
                <div class="box-body">
                        
                    <div class="form-group">
                        <label for="checkboxStatus" class="col-sm-3 control-label"></label>
                        <div class="col-sm-9">
                            <label class="user-status"><input type="checkbox" id="checkboxStatus" class="flat-blue" name="not-applicable" value="not-applicable" /> <strong>Not Applicable</strong> (No Education)</label>
                        </div>
                    </div>
                        
                    <hr />
                    
                    <div class="form-group">
                        <label for="selectEducation" class="col-sm-3 control-label">Email<span class="required">*</span>:</label>
                        <div class="col-sm-9">
                            <select class="form-control" id="selectEducation">
                                <option value="High School Diploma">High School Diploma</option>
                                <option value="Vocational Diploma / Short Course Certificate">Vocational Diploma / Short Course Certificate</option>
                                <option value="Technical Course">Technical Course</option>
                                <option value="College Level (Undergraduate)">College Level (Undergraduate)</option>
                                <option value="Bachelor's / College Degree">Bachelor's / College Degree</option>
                                <option value="Post Graduate Diploma / Master's Degree">Post Graduate Diploma / Master's Degree</option>
                                <option value="Prof'l License(Passed Board/Bar/Prof'l License Exam)">Prof'l License(Passed Board/Bar/Prof'l License Exam)</option>
                                <option value="Doctorate Degree">Doctorate Degree</option>
                            </select>
                        </div>
                    </div>
                        
                    <div class="form-group">
                        <label for="selectDateFrom" class="col-sm-3 control-label">Date From<span class="required">*</span>:</label>
                        <div class="col-sm-3">
                            <select class="form-control" id="selectDateFrom">
                                <option value="">YYYY</option><option value="2016">2016</option><option value="2015">2015</option><option value="2014">2014</option><option value="2013">2013</option><option value="2012">2012</option><option value="2011">2011</option><option value="2010">2010</option><option value="2009">2009</option><option value="2008">2008</option><option value="2007">2007</option><option value="2006">2006</option><option value="2005">2005</option><option value="2004">2004</option><option value="2003">2003</option><option value="2002">2002</option><option value="2001">2001</option><option value="2000">2000</option><option value="1999">1999</option><option value="1998">1998</option><option value="1997">1997</option><option value="1996">1996</option><option value="1995">1995</option><option value="1994">1994</option><option value="1993">1993</option><option value="1992">1992</option><option value="1991">1991</option><option value="1990">1990</option><option value="1989">1989</option><option value="1988">1988</option><option value="1987">1987</option><option value="1986">1986</option><option value="1985">1985</option><option value="1984">1984</option><option value="1983">1983</option><option value="1982">1982</option><option value="1981">1981</option><option value="1980">1980</option><option value="1979">1979</option><option value="1978">1978</option><option value="1977">1977</option><option value="1976">1976</option><option value="1975">1975</option><option value="1974">1974</option><option value="1973">1973</option><option value="1972">1972</option><option value="1971">1971</option><option value="1970">1970</option><option value="1969">1969</option><option value="1968">1968</option><option value="1967">1967</option><option value="1966">1966</option>
                            </select>
                        </div>
                        <label for="selectDateTo" class="col-sm-3 control-label">Date To<span class="required">*</span>:</label>
                        <div class="col-sm-3">
                            <select class="form-control" id="selectDateTo">
                                <option value="">YYYY</option><option value="2016">2016</option><option value="2015">2015</option><option value="2014">2014</option><option value="2013">2013</option><option value="2012">2012</option><option value="2011">2011</option><option value="2010">2010</option><option value="2009">2009</option><option value="2008">2008</option><option value="2007">2007</option><option value="2006">2006</option><option value="2005">2005</option><option value="2004">2004</option><option value="2003">2003</option><option value="2002">2002</option><option value="2001">2001</option><option value="2000">2000</option><option value="1999">1999</option><option value="1998">1998</option><option value="1997">1997</option><option value="1996">1996</option><option value="1995">1995</option><option value="1994">1994</option><option value="1993">1993</option><option value="1992">1992</option><option value="1991">1991</option><option value="1990">1990</option><option value="1989">1989</option><option value="1988">1988</option><option value="1987">1987</option><option value="1986">1986</option><option value="1985">1985</option><option value="1984">1984</option><option value="1983">1983</option><option value="1982">1982</option><option value="1981">1981</option><option value="1980">1980</option><option value="1979">1979</option><option value="1978">1978</option><option value="1977">1977</option><option value="1976">1976</option><option value="1975">1975</option><option value="1974">1974</option><option value="1973">1973</option><option value="1972">1972</option><option value="1971">1971</option><option value="1970">1970</option><option value="1969">1969</option><option value="1968">1968</option><option value="1967">1967</option><option value="1966">1966</option>
                            </select>
                        </div>
                    </div>
                        
                    <div class="form-group">
                        <label for="inputSchool" class="col-sm-3 control-label">School<span class="required">*</span>:</label>
                        <div class="col-sm-9">
                            <input type="text" id="inputSchool" class="form-control" placeholder="Name of School" />
                        </div>
                    </div>
                        
                    <div class="form-group">
                        <label for="inputCourse" class="col-sm-3 control-label">Course<span class="required">*</span>:</label>
                        <div class="col-sm-9">
                            <input type="text" id="inputCourse" class="form-control" placeholder="Bachelor..." />
                        </div>
                    </div>
                        
                    <div class="form-group">
                        <label for="inputLocation" class="col-sm-3 control-label">Location<span class="required">*</span>:</label>
                        <div class="col-sm-9">
                            <input type="text" id="inputLocation" class="form-control" placeholder="School Address" />
                        </div>
                    </div>
                        
                    <div class="form-group">
                        <label for="inputFieldOfStudy" class="col-sm-3 control-label">Field of Study<span class="required">*</span>:</label>
                        <div class="col-sm-9">
                            <select id="inputFieldOfStudy" class="form-control">
									<option value=""></option><option value="N/A">N/A</option><option value="Advertising / Media">Advertising / Media</option><option value="Agriculture">Agriculture</option><option value="Airline Transport">Airline Transport</option><option value="Architecture / Urban Studies">Architecture / Urban Studies</option><option value="Art &amp; Design">Art &amp; Design</option><option value="Biology">Biology</option><option value="BioTechnology">BioTechnology</option><option value="Business Studies/Administration/Management">Business Studies/Administration/Management</option><option value="Chemistry">Chemistry</option><option value="Commerce">Commerce</option><option value="Computer Science/Information technology">Computer Science/Information technology</option><option value="Dentistry">Dentistry</option><option value="Economics">Economics</option><option value="Editing and Publication">Editing and Publication</option><option value="Education/Teaching/Training">Education/Teaching/Training</option><option value="Engineering(Aviation/Aeronautics/Astronautics)">Engineering(Aviation/Aeronautics/Astronautics)</option><option value="Engineering(Chemical)">Engineering(Chemical)</option><option value="Engineering(Civil)">Engineering(Civil)</option><option value="Engineering(Computer/Telecommunication)">Engineering(Computer/Telecommunication)</option><option value="Engineering(Electrical/Electronic)">Engineering(Electrical/Electronic)</option><option value="Engineering(Environmental/Health/Safety)">Engineering(Environmental/Health/Safety)</option><option value="Engineering(Industrial)">Engineering(Industrial)</option><option value="Engineering(Marine)">Engineering(Marine)</option><option value="Engineering(Material Science)">Engineering(Material Science)</option><option value="Engineering(Mechanical)">Engineering(Mechanical)</option><option value="Engineering(Metal Fabrication/Tool &amp; Die/Welding)">Engineering(Metal Fabrication/Tool &amp; Die/Welding)</option><option value="Engineering(Metallurgical)">Engineering(Metallurgical)</option><option value="Engineering(Others)">Engineering(Others)</option><option value="Engineering(Petroleum/Oil/Gas)">Engineering(Petroleum/Oil/Gas)</option><option value="Finance/Accountancy/Banking">Finance/Accountancy/Banking</option><option value="Food and Beverage Preparation/Service Management">Food and Beverage Preparation/Service Management</option><option value="Geographical Science">Geographical Science</option><option value="Hospitality/Tourism Management">Hospitality/Tourism Management</option><option value="Human Resource Management">Human Resource Management</option><option value="Humanities/Liberal Arts">Humanities/Liberal Arts</option><option value="Land Transport">Land Transport</option><option value="Law">Law</option><option value="Library Management">Library Management</option><option value="Linguistics/Translation &amp; Interpretation">Linguistics/Translation &amp; Interpretation</option><option value="Mass Communications">Mass Communications</option><option value="Mathematics">Mathematics</option><option value="Medical Science">Medical Science</option><option value="Medicine">Medicine</option><option value="Merchant Marine">Merchant Marine</option><option value="Music/Performing Arts Studies">Music/Performing Arts Studies</option><option value="Nursing">Nursing</option><option value="Others">Others</option><option value="Personal Services &amp; Building/Ground Services">Personal Services &amp; Building/Ground Services</option><option value="Pharmacy/Pharmacology">Pharmacy/Pharmacology</option><option value="Physics">Physics</option><option value="Protective Services &amp; Management">Protective Services &amp; Management</option><option value="Quantity Survey">Quantity Survey</option><option value="Sales &amp; Marketing">Sales &amp; Marketing</option><option value="Science &amp; Technology">Science &amp; Technology</option><option value="Secretarial">Secretarial</option><option value="Textile/Fashion Design &amp; Production">Textile/Fashion Design &amp; Production</option><option value="Veterinary">Veterinary</option>
                            </select>
                        </div>
                    </div>
                        
                    <div class="form-group">
                        <label for="checkboxLicense" class="col-sm-3 control-label">License/Certificate:</label>
                        <div class="col-sm-9">
                            <label class="user-status"><input type="checkbox" id="checkboxLicense" class="flat-blue" name="License" value="Yes" /> Yes </label>
                        </div>
                    </div>
                        
                </div>
                </form>
                
            </div>
            
            <div class="modal-footer">
                <div class="row">
                    <div class="col-sm-3 col-sm-offset-9">
                        <a href="#" type="button" class="btn btn-sm btn-flat btn-block btn-primary" data-dismiss="modal">Add</a>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>