
                        <!-- APPLICANT REPORT TODAY -->
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title" data-widget="collapse">Applicant Information</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                                    <!--<button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>-->
                                </div>
                            </div>
                            <div class="box-body">
                                
                                <!-- APPLICANT PROFILE -->
                                <div class="row">
                                    <div class="col-md-12">
                                        
                                        <div class="row">
                                        
                                            <div class="col-md-3 col-sm-6 col-xs-6">
                                                <div class="applicant-main-profile">
                                                    
                                                    <h4 class="status status-<?=strtolower($app_info['status'])?>" id="applicant_status"><?=$app_info['status']?></h4>
                                                    <img src="<?=base_url();?>public/images/applicant-thumbnail.png" width="90%" class="applicant-thumbnail" />
                                                    <div class="applicant-name"><?=$app_info['fname']?> <?=$app_info['mname']?> <?=$app_info['lname']?></div>
                                                    <div class="applicant-number">Applicant No.: <strong><?=$app_info['applicant_id']?></strong></div>
                                                    <!-- Applicant CV's and Documents -->
                                                    <?php if (document_file_exists("iriscvwordpdf",$app_info['cv_word'])) {?>
                                                    	<a href="<?=FILE_LINKED.'iriscvwordpdf/'.$app_info['cv_word']?>" target="_new" class="btn btn-default btn-sm btn-flat btn-block btn-application-cv-word"><img src="<?=base_url();?>public/images/icon-word-file.png" /> Applicant CV (.doc)</a>
                                                    <?php } ?>
                                                    
                                                    <?php if (document_file_exists("iriscvwordpdf",$app_info['cv_pdf'])) {?>
                                                    	<a href="<?=FILE_LINKED.'iriscvwordpdf/'.$app_info['cv_pdf']?>" target="_new" class="btn btn-default btn-sm btn-flat btn-block btn-application-cv-pdf"><img src="<?=base_url();?>public/images/icon-pdf-file.png" /> Applicant CV (.pdf)</a>
                                                    <?php } ?>
                                                    
                                                    <?php if (document_file_exists("documents",$app_info['cv_applicant_other_doc'])) {?>
                                                    	<a href="<?=FILE_LINKED.'documents/'.$app_info['cv_applicant_other_doc']?>" target="_new" class="btn btn-default btn-sm btn-flat btn-block btn-application-cv-jpg"><img src="<?=base_url();?>public/images/icon-jpg.png" /> Certificate (.jpg)</a>
                                                    <?php } ?>
                                                    
                                                </div>
                                            </div>
                                            			
                                            <div class="col-md-9 col-sm-12 col-xs-12">
                                                
                                                <form class="form-horizontal display-info">
                                                <div class="form-group form-group-heading">
                                                    <label class="col-sm-3 col-sm-offset-6 control-label">Last Reported :</label>
                                                    <div class="col-sm-3 control-display"><?=dateformat($app_info['date_reported'])?></div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">Date Applied :</label>
                                                    <div class="col-sm-3 control-display"><?=dateformat($app_info['apply_date'])?></div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">RM : </label>
                                                    <div class="col-sm-3 control-display"><?=($app_info['rm']!='')? $app_info['rm']:"";?> &nbsp;&nbsp;&nbsp;<span class="control-label">RSO/TE:</span> <?=($app_info['rso']!='') ? $app_info['rso'] : $evaluation;?> <?=isset($replacement['status']) ? $replacement['status'] : ''?></div>
                                                    <label class="col-sm-3 control-label">Replacement :</label>
                                                    <div class="col-sm-3 control-display"><?=isset($replacement['user']) ? $replacement['user'] : ''?></div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">Address :</label>
                                                    <div class="col-sm-9 control-display"><?=$app_info['address1']?></div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">City :</label>
                                                    <div class="col-sm-3 control-display"><?=$app_info['city_name']." - ".$app_info['province']?></div>
                                                    <label class="col-sm-3 control-label">Zip Code :</label>
                                                    <div class="col-sm-3 control-display"><?=$app_info['address_zip']?></div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">Branch Applied :</label>
                                                    <div class="col-sm-3 control-display"><?=$app_info['app_br']?></div>
                                                    <label class="col-sm-3 control-label">Nearest Branch :</label>
                                                    <div class="col-sm-3 control-display"><?=$app_info['near_br']?></div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">Birth Date :</label>
                                                    <div class="col-sm-3 control-display"><?=dateformat($app_info['birthdate'])?></div>
                                                    <label class="col-sm-3 control-label">Age :</label>
                                                    <div class="col-sm-3 control-display"><?=get_age($app_info['birthdate'])?></div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">Religion :</label>
                                                    <div class="col-sm-3 control-display"><?=$app_info['religion']?></div>
                                                    <label class="col-sm-3 control-label">Gender :</label>
                                                    <div class="col-sm-3 control-display"><?=(strtolower($app_info['sex']) == 'm') ? 'Male' : 'Female'?></div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="inputMobileNum" class="col-sm-3 control-label">Mobile No. :</label>
                                                    <div class="col-sm-3"><input id="cellphone" name="cellphone" type="text" value="<?=$app_info['cellphone']?>" onkeypress="return isNumberKey(event);" class="form-control input-sm" /><?//=prefix_cellphone($app_info["cellphone"], 'input');?></div>
                                                    <label for="inputTelNum" class="col-sm-3 control-label">Home Tel. No. :</label>
                                                    <div class="col-sm-3"><?//=$app_info['area_code']?><input id="home_phone" name="home_phone" type="text" value="<?=$app_info['home_phone']?>" class="form-control input-sm" /></div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="inputOtherNum" class="col-sm-3 control-label">Other Tel No. :</label>
                                                    <div class="col-sm-3"><input id="office_phone" name="office_phone" type="text" value="<?=$app_info['office_phone']?>" class="form-control input-sm" /></div>
                                                    <label class="col-sm-3 control-label">Skype :</label>
                                                    <div class="col-sm-3 control-display"><?=$app_info['skypemail']?></div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">Email :</label>
                                                    <div class="col-sm-9 control-display"><?=$app_info['email']?></div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">Method of application :</label>
                                                    <div class="col-sm-3 control-display"><?=$app_info['cv_source']?></div>
                                                    <label class="col-sm-3 control-label">Source of Application :</label>
                                                    <div class="col-sm-3 control-display"><?=$app_info['source']?></div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">Agent :</label>
                                                    <div class="col-sm-3 control-display"><?=$app_info['agent']?></div>
                                                    <label class="col-sm-3 control-label">Agent Mobile :</label>
                                                    <div class="col-sm-3 control-display"><?=$app_info['agent_mobile']?></div>
                                                </div>
                                            </form>
                                                
                                            </div>
                                            
                                        </div>
                                        
                                    </div>
                                </div>
                                <!-- end of APPLICANT PROFILE -->
                                
                                
                            </div>
                            <div class="box-footer">
                                <div class="row">
                                    <div class="col-md-3 col-sm-4">
                                        <a href="../applicant/applicant-info-edit.php" class="btn btn-info btn-block btn-flat" data-toggle="modal" data-target="#modalMessageBox"><i class="fa fa-comments-o"></i> Message Box</a>
                                    </div>
                                    <div class="col-md-2 col-md-offset-5 col-sm-4">
                                        <a href="../applicant/applicant-info-edit.php" class="btn btn-default btn-block btn-flat"><i class="fa fa-pencil"></i> Edit Profile</a>
                                    </div>
                                    <div class="col-md-2 col-sm-4">
                                        <a href="#" class="btn btn-primary btn-block btn-flat"><i class="fa fa-save"></i> Save</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end of APPLICANT REPORT TODAY -->