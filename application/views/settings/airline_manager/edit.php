<?php
	$default_msg = "Airline is successfully updated.";

	if($this->session->flashdata('add_air_success') != ''){
		$default_msg = $this->session->flashdata('add_air_success');
		$msg = TRUE;
	}
?>
            <!-- MAIN CONTENT -->
            <section class="content">
            
                <div class="row">
                    <div class="col-md-12">
                                
                        <!-- ACCESS LEVEL FORM -->
                        <?php echo form_open('settings/airline_manager/edit/'.$airline_details[0]['airline_id'], 'class="form-horizontal"');?>
            
                            <!-- ACCESS LEVEL -->
                            <div class="box">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Edit Airline</h3>
                                </div>
                                <div class="box-body">

<?php if($msg != ""):?>
	<?php if ($error):?>
                                    <!-- ALERT MESSAGE [ERROR] -->
                                    <div class="alert alert-danger alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <i class="icon fa fa-ban"></i> <strong>ERROR!</strong> <?php echo $msg;?>
                                    </div>
	<?php else:?>
                                    <!-- ALERT MESSAGE [SUCCESS] -->
                                    <div class="alert alert-success alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <i class="icon fa fa-check"></i> <strong>SUCCESS!</strong> <?php echo $default_msg;?>
                                    </div>
	<?php endif;?>
<?php endif;?>

                                    <div class="row">
                                        <div class="col-md-10 col-md-offset-1">


                                            <!-- PESO Name -->
                                            <div class="form-group">
                                                <label for="inputAirName" class="col-sm-2 control-label">Airline Name<span class="required">*</span>:</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" name="inputAirName" value="<?php echo set_value('inputAirName', $airline_details[0]['name']);?>" placeholder="Name">
                                                </div>
                                            </div>

                                            <!-- City & Phone -->
                                            <div class="form-group">
                                                <label for="inputCode" class="col-sm-2 control-label">Code<span class="required">*</span>:</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" name="inputCode" value="<?php echo set_value('inputCode', $airline_details[0]['code']);?>" placeholder="Airline Code">
                                                </div>
                                            </div>

                                            <!-- Address -->
                                            <div class="form-group">
                                                <label for="txtAddress" class="col-sm-2 control-label">Address<span class="required">*</span>:</label>
                                                <div class="col-sm-10">
                                                    <textarea class="form-control" name="txtAddress"><?php echo set_value('txtAddress', $airline_details[0]['address']);?></textarea>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>

                                </div>
                                
                                <div class="box-footer">

                                    <div class="row">
                                        <div class="col-md-10 col-md-offset-1">

                                            <div class="form-group">
                                                <div class="col-sm-2 col-sm-offset-2">
                                                    <!-- <a href="#" type="button" class="btn btn-block btn-primary btn-flat" id="btnSubmit">Submit</a> -->
                                                    <button type="submit" class="btn btn-block btn-primary btn-flat" id="btnEdit" name="btnEdit">Submit</button>
                                                </div>
                                                <div class="col-sm-2">
                                                    <a href="<?php echo base_url()?>settings/airline_manager/index" type="button" class="btn btn-block btn-default btn-flat" id="btnCancel">Cancel</a>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    
                                </div>

                            </div>
                        
                        <?php echo form_close();?>
                        
                    </div>
                    
                </div>
            
            </section>
            <!-- end of MAIN CONTENT -->