<?php
	$default_msg = "Storage is successfully updated.";

	if($this->session->flashdata('add_st_success') != ''){
		$default_msg = $this->session->flashdata('add_st_success');
		$msg = TRUE;
	}
?>
            <!-- MAIN CONTENT -->
            <section class="content">
            
                <div class="row">
                    <div class="col-md-12">
                                
                        <?php echo form_open('settings/storage_manager/edit/'.$storage_users[0]['user_id'], 'class="form-horizontal"');?>
            
                            <div class="box">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Edit Storage</h3>
                                </div>
                                <div class="box-body">

<?php if($msg != ""):?>
	<?php if ($error):?>
                                    <!-- ALERT MESSAGE [ERROR] -->
                                    <div class="alert alert-danger alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <i class="icon fa fa-ban"></i> <strong>ERROR!</strong> <?php echo $msg;?>
                                    </div>
	<?php else:?>
                                    <!-- ALERT MESSAGE [SUCCESS] -->
                                    <div class="alert alert-success alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <i class="icon fa fa-check"></i> <strong>SUCCESS!</strong> <?php echo $default_msg;?>
                                    </div>
	<?php endif;?>
<?php endif;?>

                                    <div class="row">
                                        <div class="col-md-10 col-md-offset-1">

                                            <div class="form-group">
                                                <label for="inputUserName" class="col-sm-2 control-label">Username<span class="required">*</span>:</label>
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control" name="inputUserName" value="<?php echo set_value('inputUserName', $storage_users[0]['username']);?>" placeholder="Username">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="inputName" class="col-sm-2 control-label">Storage Name<span class="required">*</span>:</label>
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control" name="inputName" value="<?php echo set_value('inputName', $storage_users[0]['name']);?>" placeholder="Name">
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                                
                                <div class="box-footer">

                                    <div class="row">
                                        <div class="col-md-10 col-md-offset-1">

                                            <div class="form-group">
                                                <div class="col-sm-2 col-sm-offset-2">
                                                    <button type="submit" class="btn btn-block btn-primary btn-flat" id="" name="btnEdit">Submit</button>
                                                </div>
                                                <div class="col-sm-2">
                                                    <a href="<?php echo base_url();?>settings/storage_manager/index" type="button" class="btn btn-block btn-default btn-flat" id="btnCancel">Cancel</a>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    
                                </div>

                            </div>
                        
                        <?php echo form_close();?>
                         
                        
                    </div>
                    
                </div>
            
            </section>
            <!-- end of MAIN CONTENT -->