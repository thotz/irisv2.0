            <!-- MAIN CONTENT -->
            <section class="content">
                
            
                <!-- User Table Box -->
                <div class="row">
                    <div class="col-md-12">
                        
                        <!-- User Table -->
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">User's List</h3>
                                <a href="#" class="btn btn-success btn-sm btn-flat btn-export-excel pull-right">Export to Excel</a>
                                <a href="<?=base_url();?>settings/user_manager/add" class="btn btn-primary btn-sm btn-flat btn-add-new pull-right"><i class="ion-person-add" data-pack="default" data-tags="users, staff, head, human, member, new"></i> Add New User</a>
                            </div>
                            <div class="box-body">
	                            <table id="tableUserList" class="table table-hover table-bordered table-stripe table-no-margin">
								    <thead>
								        <tr>
								            <th width="15%">Username</th>
								            <th width="35%">Name</th>
								            <th width="15%">Branch</th>
								            <th width="15%">Access</th>
								            <th width="15%">Status</th>
								            <th width="5%"></th>
								        </tr>
								    </thead>
								    <tbody>
								    	<?php if(isset($all_user)):?>
			                                <?php foreach ($all_user as $user_info):?>
										        <tr>
										            <td><?php echo $user_info->username;?></td>
										            <td><?php echo strtoupper($user_info->fname)." ".strtoupper($user_info->mname)." ".strtoupper($user_info->lname);?></td>
										            <td><?php echo $user_info->branch;?></td>
										            <td><?php echo $user_info->access;?></td>
										            <td><?php echo ($user_info->status==1)?"Active":"Inactive";?></td>
										            <td class="text-right">
										            	<!-- <button type="button" class="btn btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></button> -->
										            	<a href="<?=base_url();?>settings/user_manager/edit/<?php echo $user_info->user_id;?>" class="btn btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
									            	</td>
										        </tr>
			                                <?php endforeach;?>
		                                <?php endif;?>
                                    </tbody>
								</table>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- end of User Table Box -->

            </section>
            <!-- end of MAIN CONTENT -->