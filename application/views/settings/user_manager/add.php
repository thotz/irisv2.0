            <!-- MAIN CONTENT -->
            <section class="content">
            
                <div class="row">
                    <div class="col-md-12">
            
                        
                        <!-- Form: Add User -->
                        <form name="add_user" action="add" method="POST" class="form-horizontal">
                            
                            
                            <!-- Add User Box -->
                            <div class="box">  
                                
                                <div class="box-header with-border">
                                    <h3 class="box-title">Add User</h3>
                                </div>
                                
                                <div class="box-body">
<?php if($msg != ""):?>
	<?php if ($error):?>
                                    <!-- ALERT MESSAGE [ERROR] -->
                                    <div class="alert alert-danger alert-dismissabl">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>
                                        <i class="icon fa fa-ban"></i> <strong>ERROR!</strong> <?php echo $msg;?>
                                    </div>
	<?php else:?>
                                    <!-- ALERT MESSAGE [SUCCESS] -->
                                    <div class="alert alert-success alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>
                                        <i class="icon fa fa-check"></i> <strong>SUCCESS!</strong> User is successfully added.
                                    </div>
	<?php endif;?>
<?php endif;?>
                                    <div class="row">
                                        <div class="col-md-10 col-md-offset-1">
                                            
                                            <!-- User Status -->
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">User Status:</label>
                                                <div class="col-sm-10">
                                                    <label class="user-status"><input type="radio" name="radStatus" value="1" <?php echo  set_radio('radStatus', '1', TRUE); ?> /> Active</label>
                                                    <label class="user-status"><input type="radio" name="radStatus" value="0" <?php echo  set_radio('radStatus', '0'); ?> /> Inactive</label>
                                                </div>
                                            </div>

                                            <!-- EW ID Number & User Name -->
                                            <div class="form-group">
                                                <label for="inputIDNum" class="col-sm-2 control-label">EW ID No:</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" id="inputEWID" name="inputEWID" value="<?php echo set_value('inputEWID');?>" placeholder="###">
                                                </div>
                                                <label for="inputUserName" class="col-sm-2 control-label">User Name<span class="required">*</span>:</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" id="inputUsername" name="inputUsername" value="<?php echo set_value('inputUsername');?>" placeholder="username">
                                                </div>
                                            </div>

                                            <!-- Full Name -->
                                            <div class="form-group">
                                                <label for="inputLastName" class="col-sm-2 control-label">Name<span class="required">*</span>:</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" id="inputLastName" name="inputLastName" value="<?php echo set_value('inputLastName');?>" placeholder="Last Name">
                                                </div>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" id="inputFirstName" name="inputFirstName" value="<?php echo set_value('inputFirstName');?>" placeholder="First Name">
                                                </div>
                                                <div class="col-sm-2">
                                                    <input type="text" class="form-control" id="inputMI" name="inputMI" value="<?php echo set_value('inputMI');?>" maxlength="2" placeholder="M.I.">
                                                </div>
                                            </div>

                                            <!-- Position & Code -->
                                            <div class="form-group">
                                                <label for="inputPosition" class="col-sm-2 control-label">Position:</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" id="inputPosition" name="inputPosition" value="<?php echo set_value('inputPosition');?>" placeholder="Position">
                                                </div>
                                                <label for="inputCode" class="col-sm-2 control-label">Code:</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" id="inputCode" name="inputCode" value="<?php echo set_value('inputCode');?>" placeholder="">
                                                </div>
                                            </div>

                                            <!-- Department -->
                                            <div class="form-group">
                                                <label for="inputDepartment" class="col-sm-2 control-label">Department:</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" id="inputDepartment" name="inputDepartment" value="<?php echo set_value('inputDepartment');?>" placeholder="Department">
                                                </div>
                                            </div>

                                            <!-- Contact Info -->
                                            <div class="form-group">
                                                <label for="inputTel" class="col-sm-2 control-label">Telephone No.:</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" id="inputTel" name="inputTel" value="<?php echo set_value('inputTel');?>" placeholder="Telephone No.">
                                                </div>
                                                <label for="inputLocal" class="col-sm-2 control-label">Local No.:</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" id="inputLocal" name="inputLocal" value="<?php echo set_value('inputLocal');?>" placeholder="Local No.">
                                                </div>
                                            </div>

                                            <!-- Mobile & Skype -->
                                            <div class="form-group">
                                                <label for="inputEWMobile" class="col-sm-2 control-label">East West Mobile:</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" id="inputEWMobile" name="inputEWMobile" value="<?php echo set_value('inputEWMobile');?>" placeholder="(###)-##-###-####">
                                                </div>
                                                <label for="inputSkype" class="col-sm-2 control-label">Skype:</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" id="inputSkype" name="inputSkype" value="<?php echo set_value('inputSkype');?>" placeholder="ewpci.username">
                                                </div>
                                            </div>

                                            <!-- Corporate Email & Applicant Email -->
                                            <div class="form-group">
                                                <label for="inputCorpEmail" class="col-sm-2 control-label">Corporate Email:</label>
                                                <div class="col-sm-4">
                                                    <input type="email" class="form-control" id="inputCorpEmail" name="inputCorpEmail" value="<?php echo set_value('inputCorpEmail');?>" placeholder="username@eastwest.com.ph">
                                                </div>
                                                <label for="inputApplEmail" class="col-sm-2 control-label">Applicant Email:</label>
                                                <div class="col-sm-4">
                                                    <input type="email" class="form-control" id="inputApplEmail" name="inputApplEmail" value="<?php echo set_value('inputApplEmail');?>" placeholder="username@address.com">
                                                </div>
                                            </div>

                                            <!-- Password -->
                                            <div class="form-group">
                                                <label for="inputPassword" class="col-sm-2 control-label">Password<span class="required">*</span>:</label>
                                                <div class="col-sm-4">
                                                    <input type="password" class="form-control" id="inputPassword" name="inputPassword" placeholder="Password">
                                                </div>
                                                <label for="inputConfirmPassword" class="col-sm-2 control-label">Confirm Password<span class="required">*</span>:</label>
                                                <div class="col-sm-4">
                                                    <input type="password" class="form-control" id="inputConfirmPassword" name="inputConfirmPassword" placeholder="Confirm Password">
                                                </div>
                                            </div>

                                            <!-- Branch & Access Level -->
                                            <div class="form-group">
                                                <label for="selectBranch" class="col-sm-2 control-label">Branch<span class="required">*</span>:</label>
                                                <div class="col-sm-4">
                                                	<?php echo form_dropdown('selectBranch', $branches, $this->input->post('selectBranch'), 'class="form-control"');?>
                                                </div>
                                                <label for="selectBranch" class="col-sm-2 control-label">Access Level<span class="required">*</span>:</label>
                                                <div class="col-sm-4">
                                                	<?php echo form_dropdown('selectAccLvl', $access, $this->input->post('selectAccLvl'), 'class="form-control"');?>
                                                </div>
                                            </div>

                                            <!-- Evaluator -->
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Evaluator:</label>
                                                <div class="col-sm-10">
                                                    <label class="user-status"><input type="radio" name="radEvaluator" value="1" <?php echo  set_radio('radEvaluator', '1'); ?> /> Yes</label>
                                                    <label class="user-status"><input type="radio" name="radEvaluator" value="0" <?php echo  set_radio('radEvaluator', '0', TRUE); ?> /> No</label>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                </div>

                                <div class="box-footer">

                                    <div class="row">
                                        <div class="col-md-10 col-md-offset-1">

                                            <!-- Submit Button -->
                                            <div class="form-group">
                                                <div class="col-sm-2 col-sm-offset-4">
                                                    <!-- demo link only -->
                                                    <!--<a href="user-manager.php" class="btn btn-block btn-primary btn-flat" id="btnAdd">Add User</a>-->
                                                    <button type="submit" class="btn btn-block btn-primary btn-flat" id="btnAdd" name="btnAdd">Add User</button>
                                                </div>
                                                <div class="col-sm-2">
                                                    <!-- demo link only -->
                                                    <a href="<?=base_url();?>settings/user_manager" class="btn btn-block btn-default btn-flat" id="btnCancel">Cancel</a> 
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    
                                </div>

                            </div>
                            <!-- end of Add User Box -->
                            
                        </form>
                        <!-- end of Form: Add User -->

                    </div>
                </div>

            </section>
            <!-- end of MAIN CONTENT -->