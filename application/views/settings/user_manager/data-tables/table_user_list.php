<table id="tableUserList" class="table table-hover table-bordered table-stripe table-no-margin">
    <thead>
        <tr>
            <th width="15%">Username</th>
            <th width="35%">Name</th>
            <th width="15%">Branch</th>
            <th width="15%">Access</th>
            <th width="15%">Status</th>
            <th width="5%"></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>abby</td>
            <td>ABEGAIL A. ABELEDA</td>
            <td>MNL</td>
            <td>OPERATIONS</td>
            <td>Active</td>
            <td class="text-right"><button type="button" class="btn btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></button></td>
        </tr>
        <tr>
            <td>abdul</td>
            <td>ABDUL JABBAR T. ISMAEL</td>
            <td>MNL</td>
            <td>OPERATIONS</td>
            <td>Active</td>
            <td class="text-right"><button type="button" class="btn btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></button></td>
        </tr>
        <tr>
            <td>adam</td>
            <td>ADAM G. GANGGANGAN</td>
            <td>MNL</td>
            <td>OPERATIONS</td>
            <td>Inactive</td>
            <td class="text-right"><button type="button" class="btn btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></button></td>
        </tr>
        <tr>
            <td>admin</td>
            <td>administrator</td>
            <td>MNL</td>
            <td>ADMINISTRATOR</td>
            <td>Active</td>
            <td class="text-right"><button type="button" class="btn btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></button></td>
        </tr>
        <tr>
            <td>adolf</td>
            <td>ADOLFO B. RAMISO JR.</td>
            <td>MNL</td>
            <td>ADMINISTRATOR</td>
            <td>Active</td>
            <td class="text-right"><button type="button" class="btn btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></button></td>
        </tr>
        <tr>
            <td>adrian</td>
            <td>ADRIAN JAY C. GARCIA</td>
            <td>MNL</td>
            <td>ADMINISTRATOR</td>
            <td>Active</td>
            <td class="text-right"><button type="button" class="btn btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></button></td>
        </tr>
        <tr>
            <td>aileen</td>
            <td>Aileen Ilaigan</td>
            <td>DVO</td>
            <td>OPTN AND RECRUIT</td>
            <td>Inactive</td>
            <td class="text-right"><button type="button" class="btn btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></button></td>
        </tr>
        <tr>
            <td>alastair</td>
            <td>ALASTAIR B. LAMBO</td>
            <td>CBU</td>
            <td>OPTN AND RECRUIT</td>
            <td>Active</td>
            <td class="text-right"><button type="button" class="btn btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></button></td>
        </tr>
        <tr>
            <td>alex</td>
            <td>Alexander Ancheta</td>
            <td>MNL</td>
            <td>RECRUITMENT</td>
            <td>Inactive</td>
            <td class="text-right"><button type="button" class="btn btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></button></td>
        </tr>
        <tr>
            <td>alfanta</td>
            <td>JASON CALISO ALFANTA</td>
            <td>CDO</td>
            <td>RECRUITMENT</td>
            <td>Inactive</td>
            <td class="text-right"><button type="button" class="btn btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></button></td>
        </tr>
        <tr>
            <td>allan</td>
            <td>Allan Bongbong</td>
            <td>CBU</td>
            <td>OPTN AND RECRUIT</td>
            <td>Active</td>
            <td class="text-right"><button type="button" class="btn btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></button></td>
        </tr>
        <tr>
            <td>alpha</td>
            <td>ALPHA B. NUNEZ</td>
            <td>MNL</td>
            <td>OPTN AND RECRUIT</td>
            <td>Active</td>
            <td class="text-right"><button type="button" class="btn btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></button></td>
        </tr>
        <tr>
            <td>alvin</td>
            <td>ALVIN C. NARITO</td>
            <td>MNL</td>
            <td>OPERATIONS</td>
            <td>Active</td>
            <td class="text-right"><button type="button" class="btn btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></button></td>
        </tr>
    </tbody>
</table>