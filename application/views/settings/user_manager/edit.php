<?php
	$default_msg = "User is successfully updated.";

	if($this->session->flashdata('add_success') != ''){
		$default_msg = $this->session->flashdata('add_success');
		$msg = TRUE;
	}
?>
            <!-- MAIN CONTENT -->
            <section class="content">
            
                <div class="row">
                    <div class="col-md-12">
            
                        
                        <!-- Form: Edit User -->
                        <form name="edit_user" action="<?=base_url();?>settings/user_manager/edit/<?php echo $user_details['user_id']; ?>" method="POST" class="form-horizontal">
                            
                            
                            <!-- Edit User Box -->
                            <div class="box">  
                                
                                <div class="box-header with-border">
                                    <h3 class="box-title">Edit User</h3>
                                </div>
                                
                                <div class="box-body">
<?php if($msg != ""):?>
	<?php if ($error):?>
                                    <!-- ALERT MESSAGE [ERROR] -->
                                    <div class="alert alert-danger alert-dismissabl">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>
                                        <i class="icon fa fa-ban"></i> <strong>ERROR!</strong> <?php echo $msg;?>
                                    </div>
	<?php else:?>
                                    <!-- ALERT MESSAGE [SUCCESS] -->
                                    <div class="alert alert-success alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>
                                        <i class="icon fa fa-check"></i> <strong>SUCCESS!</strong> <?php echo $default_msg;?>
                                    </div>
	<?php endif;?>
<?php endif;?>
                                    <div class="row">
                                        <div class="col-md-10 col-md-offset-1">
                                            
                                            <!-- User Status -->
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">User Status:</label>
                                                <div class="col-sm-10">
                                                    <label class="user-status"><input type="radio" name="radStatus" value="1" <?php echo  set_radio('radStatus', '1', $user_details['status'] === "1" ? TRUE:FALSE); ?> /> Active</label>
                                                    <label class="user-status"><input type="radio" name="radStatus" value="0" <?php echo  set_radio('radStatus', '0', $user_details['status'] === "0" ? TRUE:FALSE); ?> /> Inactive</label>
                                                </div>
                                            </div>

                                            <!-- EW ID Number & User Name -->
                                            <div class="form-group">
                                                <label for="inputIDNum" class="col-sm-2 control-label">EW ID No:</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" id="inputEWID" name="inputEWID" value="<?php echo set_value('inputEWID', @$user_details['ew_id']);?>" placeholder="###" <?php echo ($user_details['ew_id']!='')?"disabled=\"disabled\"":"";?> >
                                                </div>
                                                <label for="inputUserName" class="col-sm-2 control-label">User Name<span class="required">*</span>:</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" id="inputUsername" name="inputUsername" value="<?php echo set_value('inputUsername', @$user_details['username']);?>" placeholder="username" disabled="disabled">
                                                </div>
                                            </div>

                                            <!-- Full Name -->
                                            <div class="form-group">
                                                <label for="inputLastName" class="col-sm-2 control-label">Name<span class="required">*</span>:</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" id="inputLastName" name="inputLastName" value="<?php echo set_value('inputLastName', @$user_details['lname']);?>" placeholder="Last Name">
                                                </div>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" id="inputFirstName" name="inputFirstName" value="<?php echo set_value('inputFirstName', @$user_details['fname']);?>" placeholder="First Name">
                                                </div>
                                                <div class="col-sm-2">
                                                    <input type="text" class="form-control" id="inputMI" name="inputMI" value="<?php echo set_value('inputMI', @$user_details['mname']);?>" maxlength="2" placeholder="M.I.">
                                                </div>
                                            </div>

                                            <!-- Position & Code -->
                                            <div class="form-group">
                                                <label for="inputPosition" class="col-sm-2 control-label">Position:</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" id="inputPosition" name="inputPosition" value="<?php echo set_value('inputPosition', @$user_details['position']);?>" placeholder="Position">
                                                </div>
                                                <label for="inputCode" class="col-sm-2 control-label">Code:</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" id="inputCode" name="inputCode" value="<?php echo set_value('inputCode', @$user_details['pos_code']);?>" placeholder="">
                                                </div>
                                            </div>

                                            <!-- Department -->
                                            <div class="form-group">
                                                <label for="inputDepartment" class="col-sm-2 control-label">Department:</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" id="inputDepartment" name="inputDepartment" value="<?php echo set_value('inputDepartment', @$user_details['department']);?>" placeholder="Department">
                                                </div>
                                            </div>

                                            <!-- Contact Info -->
                                            <div class="form-group">
                                                <label for="inputTel" class="col-sm-2 control-label">Telephone No.:</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" id="inputTel" name="inputTel" value="<?php echo set_value('inputTel', @$user_details['telno']);?>" placeholder="Telephone No.">
                                                </div>
                                                <label for="inputLocal" class="col-sm-2 control-label">Local No.:</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" id="inputLocal" name="inputLocal" value="<?php echo set_value('inputLocal', @$user_details['loc_no']);?>" placeholder="Local No.">
                                                </div>
                                            </div>

                                            <!-- Mobile & Skype -->
                                            <div class="form-group">
                                                <label for="inputEWMobile" class="col-sm-2 control-label">East West Mobile:</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" id="inputEWMobile" name="inputEWMobile" value="<?php echo set_value('inputEWMobile', @$user_details['ew_mob']);?>" placeholder="(###)-##-###-####">
                                                </div>
                                                <label for="inputSkype" class="col-sm-2 control-label">Skype:</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" id="inputSkype" name="inputSkype" value="<?php echo set_value('inputSkype', @$user_details['skype']);?>" placeholder="ewpci.username">
                                                </div>
                                            </div>

                                            <!-- Corporate Email & Applicant Email -->
                                            <div class="form-group">
                                                <label for="inputCorpEmail" class="col-sm-2 control-label">Corporate Email:</label>
                                                <div class="col-sm-4">
                                                    <input type="email" class="form-control" id="inputCorpEmail" name="inputCorpEmail" value="<?php echo set_value('inputCorpEmail', @$user_details['corp_email']);?>" placeholder="username@eastwest.com.ph">
                                                </div>
                                                <label for="inputApplEmail" class="col-sm-2 control-label">Applicant Email:</label>
                                                <div class="col-sm-4">
                                                    <input type="email" class="form-control" id="inputApplEmail" name="inputApplEmail" value="<?php echo set_value('inputApplEmail', @$user_details['app_email']);?>" placeholder="username@address.com">
                                                </div>
                                            </div>

                                            <!-- Password -->
                                            <div class="form-group">
                                                <label for="inputPassword" class="col-sm-2 control-label">Password:</label>
                                                <div class="col-sm-4">
                                                    <input type="password" class="form-control" id="inputPassword" name="inputPassword" placeholder="Password">
                                                </div>
                                                <label for="inputConfirmPassword" class="col-sm-2 control-label">Confirm Password:</label>
                                                <div class="col-sm-4">
                                                    <input type="password" class="form-control" id="inputConfirmPassword" name="inputConfirmPassword" placeholder="Confirm Password">
                                                </div>
                                            </div>

                                            <!-- Branch & Access Level -->
                                            <div class="form-group">
                                                <label for="selectBranch" class="col-sm-2 control-label">Branch<span class="required">*</span>:</label>
                                                <div class="col-sm-4">
                                                	<?php echo form_dropdown('selectBranch', $branches, (NULL===$this->input->post('selectBranch'))? $user_details['branch_id']:$this->input->post('selectBranch'), 'class="form-control"');?>
                                                </div>
                                                <label for="selectBranch" class="col-sm-2 control-label">Access Level<span class="required">*</span>:</label>
                                                <div class="col-sm-4">
                                                	<?php echo form_dropdown('selectAccLvl', $access, (NULL===$this->input->post('selectAccLvl'))? $user_details['access_id']:$this->input->post('selectAccLvl'), 'class="form-control"');?>
                                                </div>
                                            </div>

                                            <!-- Evaluator -->
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Evaluator:</label>
                                                <div class="col-sm-10">
                                                    <label class="user-status"><input type="radio" name="radEvaluator" value="1" <?php echo  set_radio('radEvaluator', '1', $user_details['evaluator'] === "1" ? TRUE:FALSE); ?> /> Yes</label>
                                                    <label class="user-status"><input type="radio" name="radEvaluator" value="0" <?php echo  set_radio('radEvaluator', '0', $user_details['evaluator'] === "0" ? TRUE:FALSE); ?> /> No</label>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                </div>

                                <div class="box-footer">

                                    <div class="row">
                                        <div class="col-md-10 col-md-offset-1">

                                            <!-- Submit Button -->
                                            <div class="form-group">
                                                <div class="col-sm-2 col-sm-offset-4">
                                                    <!-- demo link only -->
                                                    <!--<a href="user-manager.php" class="btn btn-block btn-primary btn-flat" id="btnAdd">Add User</a>-->
                                                    <button type="submit" class="btn btn-block btn-primary btn-flat" id="btnEdit" name="btnEdit">Edit User</button>
                                                </div>
                                                <div class="col-sm-2">
                                                    <!-- demo link only -->
                                                    <a href="<?=base_url();?>settings/user_manager" class="btn btn-block btn-default btn-flat" id="btnCancel">Cancel</a> 
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    
                                </div>

                            </div>
                            <!-- end of Edit User Box -->
                            
                        </form>
                        <!-- end of Form: Edit User -->

                    </div>
                </div>

            </section>
            <!-- end of MAIN CONTENT -->