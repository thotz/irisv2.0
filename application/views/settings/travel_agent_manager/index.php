<?php
	$error = $this->session->flashdata('tagent_del_error');
	$msg = $this->session->flashdata('tagent_del_msg');
?>
			<!-- MAIN CONTENT -->
            <section class="content">
            
                <div class="row">
                    <div class="col-md-12">
            
                        <!-- ACCESS LEVEL -->
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">Travel Agent List</h3>
                                <a href="#" class="btn btn-success btn-sm btn-flat btn-export-excel pull-right">Export to Excel</a>
                                <a href="<?php echo base_url();?>settings/travel_agent_manager/add" class="btn btn-primary btn-sm btn-flat btn-add-new pull-right"><i class="fa fa-plus"></i> Add Travel Agent</a>
                            </div>
                            <div class="box-body">
  
<?php if($msg != ''):?>
	<?php if($error == TRUE):?>
                                    <!-- ALERT MESSAGE [ERROR] -->
                                    <div class="alert alert-danger alert-dismissabl">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>
                                        <i class="icon fa fa-ban"></i> <strong>ERROR!</strong> Unabled to delete. <?php echo $msg;?>
                                    </div>
	<?php else :?>
                                    <!-- ALERT MESSAGE [SUCCESS] -->
                                    <div class="alert alert-success alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>
                                        <i class="icon fa fa-check"></i> <strong>SUCCESS!</strong> <?php echo $msg;?>
                                    </div>
	<?php endif;?>
<?php endif;?>
                                
                                <!-- Data Table (Access Menu) -->
                                <div class="row">
                                    <div class="col-md-12">
                                    
									<table id="tableTravelAgentList" class="table table-hover table-bordered table-stripe table-no-margin table-condensed">
									    <thead>
									        <tr>			
									            <th>Travel Agent</th>
									            <th>Code</th>
									            <th>Address</th>
									            <th>Contact Person</th>
									            <th></th>
									        </tr>
									    </thead>
									    <tbody>
	                                        <?php if(isset($travelagent_list)):?>
	                                        	<?php foreach ($travelagent_list as $val):?>
											        <tr>
											            <td><?php echo $val['name']?></td>
											            <td><?php echo strtoupper($val['code'])?></td>
											            <td><?php echo $val['address']?></td>
											            <td><?php echo $val['contact']?></td>
											            <td class="text-right">
											            	<a href="<?php echo base_url();?>settings/travel_agent_manager/edit/<?php echo $val['travelagent_id']?>" class="btn btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
											            	<a href="<?php echo base_url();?>settings/travel_agent_manager/delete/<?php echo $val['travelagent_id']?>" onclick="if(confirm('Are you sure you want to delete this record?')){return true;}else{return false;}" class="btn btn-sm btn-danger btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash"></i></a>
										            	</td>
											        </tr>
	                                        	<?php endforeach;?>
	                                        <?php endif;?>
									    </tbody>
									</table>
                                    
                                    </div>
                                </div>
                                <!-- end of Data Table (Access Menu) -->
                                
                            </div>
                            
                        </div>
                        <!-- end of ACCESS LEVEL -->
                        
                    </div>
                    
                </div>
                
            </section>
            <!-- end of MAIN CONTENT -->