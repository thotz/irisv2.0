            <!-- MAIN CONTENT -->
            <section class="content">
            
                <div class="row">
                    <div class="col-md-12">
                                
                        <!-- ACCESS LEVEL FORM -->
                        <?php echo form_open('settings/travel_agent_manager/add', 'class="form-horizontal"');?>
            
                            <!-- ACCESS LEVEL -->
                            <div class="box">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Add Travel Agent</h3>
                                </div>
                                <div class="box-body">

<?php if($msg != ""):?>
	<?php if ($error):?>
                                    <!-- ALERT MESSAGE [ERROR] -->
                                    <div class="alert alert-danger alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <i class="icon fa fa-ban"></i> <strong>ERROR!</strong> <?php echo $msg;?>
                                    </div>
	<?php else:?>
                                    <!-- ALERT MESSAGE [SUCCESS] -->
                                    <div class="alert alert-success alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <i class="icon fa fa-check"></i> <strong>SUCCESS!</strong> TESDA Office is successfully added.
                                    </div>
	<?php endif;?>
<?php endif;?>

                                    <div class="row">
                                        <div class="col-md-10 col-md-offset-1">


                                            <!-- Travel Agent -->
                                            <div class="form-group">
                                                <label for="inputName" class="col-sm-2 control-label">Travel Agent Name<span class="required">*</span>:</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" name="inputName" value="<?php echo set_value('inputName')?>" placeholder="Name">
                                                </div>
                                            </div>

                                            <!-- Code -->
                                            <div class="form-group">
                                                <label for="inputCode" class="col-sm-2 control-label">Code<span class="required">*</span>:</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" name="inputCode" value="<?php echo set_value('inputCode')?>" placeholder="Code">
                                                </div>
                                            </div>
                                            
                                            <!-- Address -->
                                            <div class="form-group">
                                                <label for="txtAddress" class="col-sm-2 control-label">Address<span class="required">*</span>:</label>
                                                <div class="col-sm-10">
                                                    <textarea class="form-control" name="txtAddress"><?php echo set_value('txtAddress')?></textarea>
                                                </div>
                                            </div>
                                            
                                            <!-- Phone & Fax -->
                                            <div class="form-group">
                                                <label for="inputPhone" class="col-sm-2 control-label">Contact No.<span class="required">*</span>:</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" name="inputPhone" value="<?php echo set_value('inputPhone')?>" placeholder="Phone/Mobile">
                                                </div>
                                                <label for="inputFax" class="col-sm-2 control-label">Fax No.:</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" name="inputFax" value="<?php echo set_value('inputFax')?>" placeholder="Fax No.">
                                                </div>
                                            </div>

                                            <!-- Contact -->
                                            <div class="form-group">
                                                <label for="inputContact" class="col-sm-2 control-label">Contact Person<span class="required">*</span>:</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" name="inputContact" value="<?php echo set_value('inputContact')?>" placeholder="Name">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                
                                <div class="box-footer">

                                    <div class="row">
                                        <div class="col-md-10 col-md-offset-1">

                                            <div class="form-group">
                                                <div class="col-sm-2 col-sm-offset-2">
                                                    <!-- <a href="tesda-office-manager.php" type="button" class="btn btn-block btn-primary btn-flat" id="btnSubmit">Submit</a> -->
                                                    <button type="submit" class="btn btn-block btn-primary btn-flat" id="btnAdd" name="btnAdd">Submit</button>
                                                </div>
                                                <div class="col-sm-2">
                                                    <a href="<?php echo base_url()?>settings/travel_agent_manager/index" type="button" class="btn btn-block btn-default btn-flat" id="btnCancel">Cancel</a>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    
                                </div>

                            </div>
                        
                        <?php echo form_close();?>
                        
                    </div>
                    
                </div>
            
            </section>
            <!-- end of MAIN CONTENT -->