<?php
	$default_msg = "POEA Office is successfully updated.";

	if($this->session->flashdata('add_poea_success') != ''){
		$default_msg = $this->session->flashdata('add_poea_success');
		$msg = TRUE;
	}
?>
			<!-- MAIN CONTENT -->
            <section class="content">
            
                <div class="row">
                    <div class="col-md-12">
                                
                        <?php echo form_open('settings/poea_manager/edit/'.$poea_details[0]['poea_id'], 'class="form-horizontal"');?>
            
                            <div class="box">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Edit POEA</h3>
                                </div>
                                <div class="box-body">
<?php if($msg != ""):?>
	<?php if ($error):?>
                                    <!-- ALERT MESSAGE [ERROR] -->
                                    <div class="alert alert-danger alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <i class="icon fa fa-ban"></i> <strong>ERROR!</strong> <?php echo $msg;?>
                                    </div>
	<?php else:?>
                                    <!-- ALERT MESSAGE [SUCCESS] -->
                                    <div class="alert alert-success alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <i class="icon fa fa-check"></i> <strong>SUCCESS!</strong> <?php echo $default_msg;?>
                                    </div>
	<?php endif;?>
<?php endif;?>
                                    <div class="row">
                                        <div class="col-md-10 col-md-offset-1">

                                            <div class="form-group">
                                                <label for="inputPOEAName" class="col-sm-2 control-label">POEA Name<span class="required">*</span>:</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" name="inputPOEAName" value="<?php echo set_value('inputPOEAName', $poea_details[0]['name']);?>" placeholder="POEA Name">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="inputHead" class="col-sm-2 control-label">Head<span class="required">*</span>:</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" name="inputHead" value="<?php echo set_value('inputHead', $poea_details[0]['head']);?>" placeholder="Contact Person">
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label for="inputPosition" class="col-sm-2 control-label">Position:</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" name="inputPosition" value="<?php echo set_value('inputPosition', $poea_details[0]['position']);?>" placeholder="Position">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="inputPhone" class="col-sm-2 control-label">Contact No.:</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" name="inputPhone" value="<?php echo set_value('inputPhone', $poea_details[0]['tel']);?>" placeholder="###">
                                                </div>
                                                <label for="inputEmail" class="col-sm-2 control-label">Email Address:</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" name="inputEmail" value="<?php echo set_value('inputEmail', $poea_details[0]['email']);?>" placeholder="Email Address">
                                                </div>
                                            </div>

                                            <!-- Address -->
                                            <div class="form-group">
                                                <label for="txtAddress" class="col-sm-2 control-label">Address:</label>
                                                <div class="col-sm-10">
                                                    <textarea class="form-control" name="txtAddress"><?php echo set_value('txtAddress', $poea_details[0]['address']);?></textarea>
                                                </div>
                                            </div>
                                            
                                            <!-- City & Phone -->
                                            <div class="form-group">
                                                <label for="selectCity" class="col-sm-2 control-label">City:</label>
                                                <div class="col-sm-4">
													<?php echo form_dropdown('selectCity', $city, (NULL===$this->input->post('selectCity'))? $poea_details[0]['city_id']:$this->input->post('selectCity'), 'class="form-control"');?>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                                
                                <div class="box-footer">

                                    <div class="row">
                                        <div class="col-md-10 col-md-offset-1">

                                            <div class="form-group">
                                                <div class="col-sm-2 col-sm-offset-2">
                                                    <button type="submit" class="btn btn-block btn-primary btn-flat" id="btnEdit" name="btnEdit">Submit</button>
                                                </div>
                                                <div class="col-sm-2">
                                                    <a href="<?php echo base_url()?>settings/poea_manager/index" type="button" class="btn btn-block btn-default btn-flat" id="btnCancel">Cancel</a>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    
                                </div>

                            </div>
                        
                        <?php echo form_close();?>
                        
                    </div>
                    
                </div>
            
            </section>
            <!-- end of MAIN CONTENT -->