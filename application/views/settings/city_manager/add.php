            <!-- MAIN CONTENT -->
            <section class="content">
            
                <div class="row">
                    <div class="col-md-12">
                                
                        <?php echo form_open('settings/city_manager/add', 'class="form-horizontal"');?>
            
                            <div class="box">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Add City</h3>
                                </div>
                                <div class="box-body">

<?php if($msg != ""):?>
	<?php if ($error):?>
                                    <!-- ALERT MESSAGE [ERROR] -->
                                    <div class="alert alert-danger alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <i class="icon fa fa-ban"></i> <strong>ERROR!</strong> <?php echo $msg;?>
                                    </div>
	<?php else:?>
                                    <!-- ALERT MESSAGE [SUCCESS] -->
                                    <div class="alert alert-success alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <i class="icon fa fa-check"></i> <strong>SUCCESS!</strong> City is successfully added.
                                    </div>
	<?php endif;?>
<?php endif;?>

                                    <div class="row">
                                        <div class="col-md-10 col-md-offset-1">

                                            <div class="form-group">
                                                <label for="inputName" class="col-sm-2 control-label">City Name<span class="required">*</span>:</label>
                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control" name="inputName" value="<?php echo set_value('inputName');?>" placeholder="Name">
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label for="inputProvince" class="col-sm-2 control-label">Province:</label>
                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control" name="inputProvince" value="<?php echo set_value('inputProvince');?>" placeholder="Province">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="inputZip" class="col-sm-2 control-label">Zip Code:</label>
                                                <div class="col-sm-3">
                                                    <input type="text" class="form-control" name="inputZip" value="<?php echo set_value('inputZip');?>" placeholder="###">
                                                </div>
                                                
                                                <label for="inputRegion" class="col-sm-2 control-label">Region:</label>
                                                <div class="col-sm-3">
                                                    <input type="text" class="form-control" name="inputRegion" value="<?php echo set_value('inputRegion');?>" placeholder="###">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="inputACode" class="col-sm-2 control-label">Area Code:</label>
                                                <div class="col-sm-3">
                                                    <input type="text" class="form-control" name="inputACode" value="<?php echo set_value('inputACode');?>" placeholder="###">
                                                </div>
                                                
                                                <label for="selectBranch" class="col-sm-2 control-label">Branch:</label>
                                                <div class="col-sm-3">
                                                    <?php echo form_dropdown('selectBranch', $branches, $this->input->post('selectBranch'), 'class="form-control"');?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                
                                <div class="box-footer">

                                    <div class="row">
                                        <div class="col-md-10 col-md-offset-1">

                                            <div class="form-group">
                                                <div class="col-sm-2 col-sm-offset-2">
                                                    <button type="submit" class="btn btn-block btn-primary btn-flat" id="btnAdd" name="btnAdd">Submit</button>
                                                </div>
                                                <div class="col-sm-2">
                                                    <a href="<?php echo base_url();?>settings/city_manager/index" type="button" class="btn btn-block btn-default btn-flat" id="btnCancel">Cancel</a>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    
                                </div>

                            </div>
                        
                        <?php echo form_close();?>
                         
                        
                    </div>
                    
                </div>
            
            </section>
            <!-- end of MAIN CONTENT -->