<?php
	$default_msg = "City is successfully updated.";

	if($this->session->flashdata('add_city_success') != ''){
		$default_msg = $this->session->flashdata('add_city_success');
		$msg = TRUE;
	}
?>
            <!-- MAIN CONTENT -->
            <section class="content">
            
                <div class="row">
                    <div class="col-md-12">
                                
                        <?php echo form_open('settings/city_manager/edit/'.$city[0]['city_id'], 'class="form-horizontal"');?>
            
                            <div class="box">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Edit City</h3>
                                </div>
                                <div class="box-body">

<?php if($msg != ""):?>
	<?php if ($error):?>
                                    <!-- ALERT MESSAGE [ERROR] -->
                                    <div class="alert alert-danger alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <i class="icon fa fa-ban"></i> <strong>ERROR!</strong> <?php echo $msg;?>
                                    </div>
	<?php else:?>
                                    <!-- ALERT MESSAGE [SUCCESS] -->
                                    <div class="alert alert-success alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <i class="icon fa fa-check"></i> <strong>SUCCESS!</strong> <?php echo $default_msg;?>
                                    </div>
	<?php endif;?>
<?php endif;?>

                                    <div class="row">
                                        <div class="col-md-10 col-md-offset-1">

                                            <div class="form-group">
                                                <label for="inputName" class="col-sm-2 control-label">City Name<span class="required">*</span>:</label>
                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control" name="inputName" value="<?php echo set_value('inputName', $city[0]['name']);?>" placeholder="Name">
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label for="inputProvince" class="col-sm-2 control-label">Province:</label>
                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control" name="inputProvince" value="<?php echo set_value('inputProvince', $city[0]['province']);?>" placeholder="Province">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="inputZip" class="col-sm-2 control-label">Zip Code:</label>
                                                <div class="col-sm-3">
                                                    <input type="text" class="form-control" name="inputZip" value="<?php echo set_value('inputZip', $city[0]['zipcode']);?>" placeholder="###">
                                                </div>
                                                
                                                <label for="inputRegion" class="col-sm-2 control-label">Region:</label>
                                                <div class="col-sm-3">
                                                    <input type="text" class="form-control" name="inputRegion" value="<?php echo set_value('inputRegion', $city[0]['region']);?>" placeholder="###">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="inputACode" class="col-sm-2 control-label">Area Code:</label>
                                                <div class="col-sm-3">
                                                    <input type="text" class="form-control" name="inputACode" value="<?php echo set_value('inputACode', $city[0]['area_code']);?>" placeholder="###">
                                                </div>
                                                
                                                <label for="selectBranch" class="col-sm-2 control-label">Branch:</label>
                                                <div class="col-sm-3">
                                                    <?php echo form_dropdown('selectBranch', $branches, (NULL===$this->input->post('selectBranch'))?$city[0]['city_branch']:$this->input->post('selectBranch'), 'class="form-control"');?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                
                                <div class="box-footer">

                                    <div class="row">
                                        <div class="col-md-10 col-md-offset-1">

                                            <div class="form-group">
                                                <div class="col-sm-2 col-sm-offset-2">
                                                    <button type="submit" class="btn btn-block btn-primary btn-flat" id="btnAdd" name="btnEdit">Submit</button>
                                                </div>
                                                <div class="col-sm-2">
                                                    <a href="<?php echo base_url();?>settings/city_manager/index" type="button" class="btn btn-block btn-default btn-flat" id="btnCancel">Cancel</a>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    
                                </div>

                            </div>
                        
                        <?php echo form_close();?>
                         
                        
                    </div>
                    
                </div>
            
            </section>
            <!-- end of MAIN CONTENT -->