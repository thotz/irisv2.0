<?php
	$error = $this->session->flashdata('city_del_error');
	$msg = $this->session->flashdata('city_del_msg');
?>
            <!-- MAIN CONTENT -->
            <section class="content">
         
                <div class="row">
                    <div class="col-md-12">
            
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">City Manager</h3>
                                <a href="#" class="btn btn-sm btn-success btn-sm btn-flat btn-export-excel pull-right">Export to Excel</a>
                                <a href="<?php echo base_url();?>settings/city_manager/add" class="btn btn-sm btn-primary btn-sm btn-flat btn-add-new pull-right"><i class="fa fa-plus"></i> Add City</a>
                            </div>
                            <div class="box-body">
                                    
<?php if($msg != ''):?>
	<?php if($error == TRUE):?>
                                    <!-- ALERT MESSAGE [ERROR] -->
                                    <div class="alert alert-danger alert-dismissabl">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>
                                        <i class="icon fa fa-ban"></i> <strong>ERROR!</strong> Unabled to delete. <?php echo $msg;?>
                                    </div>
	<?php else :?>
                                    <!-- ALERT MESSAGE [SUCCESS] -->
                                    <div class="alert alert-success alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>
                                        <i class="icon fa fa-check"></i> <strong>SUCCESS!</strong> <?php echo $msg;?>
                                    </div>
	<?php endif;?>
<?php endif;?>
                                    
                            	<div class="row">
									<div class="col-md-12">
		                                <table id="tableCity" class="table table-hover table-bordered table-stripe table-no-margin">
		                                    <thead>
		                                        <tr>	
		                                            <th width="15%">City</th>
		                                            <th width="15%">Province</th>
		                                            <th width="15%">Zip Code</th>
		                                            <th width="15%">Area Code</th>
		                                            <th width="15%">Branch</th>
		                                            <th width="15%">Region</th>
		                                            <th class="text-right" width="10%"></th>
		                                        </tr>
		                                    </thead>
		                                    <tbody>
												<?php if(isset($city)):?>
		                                        	<?php foreach ($city as $val):?>
												        <tr>
												            <td><?php echo $val['name']?></td>
												            <td><?php echo $val['province']?></td>
												            <td><?php echo $val['zipcode']?></td>
												            <td><?php echo $val['area_code']?></td>
												            <td><?php echo isset($branches[$val['city_branch']])?$branches[$val['city_branch']]:"";?></td>
												            <td><?php echo $val['region']?></td>
												            <td class="text-right">
												            	<a href="<?php echo base_url();?>settings/city_manager/edit/<?php echo $val['city_id']?>" class="btn btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
												            	<a href="<?php echo base_url();?>settings/city_manager/delete/<?php echo $val['city_id']?>" onclick="if(confirm('Are you sure you want to delete this record?')){return true;}else{return false;}" class="btn btn-sm btn-danger btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash"></i></a>
											            	</td>
												        </tr>
		                                        	<?php endforeach;?>
		                                        <?php endif;?>
		                                    </tbody>
		                                </table>

                                    </div>
                                </div>
                                
                            </div>
                            
                        </div>
                        
                    </div>
                    
                </div>
            
            </section>
            <!-- end of MAIN CONTENT -->