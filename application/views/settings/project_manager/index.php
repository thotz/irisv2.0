<?php
	$error = $this->session->flashdata('pn_del_error');
	$msg = $this->session->flashdata('pn_del_msg');
?>
            <!-- MAIN CONTENT -->
            <section class="content">
         
                <div class="row">
                    <div class="col-md-12">
            
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">Project List</h3>
                                <a href="#" class="btn btn-sm btn-success btn-sm btn-flat btn-export-excel pull-right">Export to Excel</a>
                                <a href="<?php echo base_url();?>settings/project_manager/add" class="btn btn-sm btn-primary btn-sm btn-flat btn-add-new pull-right"><i class="fa fa-plus"></i> Add Project</a>
                            </div>
                            <div class="box-body">
                                    
<?php if($msg != ''):?>
	<?php if($error == TRUE):?>
                                    <!-- ALERT MESSAGE [ERROR] -->
                                    <div class="alert alert-danger alert-dismissabl">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>
                                        <i class="icon fa fa-ban"></i> <strong>ERROR!</strong> Unabled to delete. <?php echo $msg;?>
                                    </div>
	<?php else :?>
                                    <!-- ALERT MESSAGE [SUCCESS] -->
                                    <div class="alert alert-success alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>
                                        <i class="icon fa fa-check"></i> <strong>SUCCESS!</strong> <?php echo $msg;?>
                                    </div>
	<?php endif;?>
<?php endif;?>
                                    
                            	<div class="row">
									<div class="col-md-12">
		                                <table id="tablePN" class="table table-hover table-bordered table-stripe table-no-margin">
		                                    <thead>
		                                        <tr>	
		                                            <th width="60%">Project Name</th>
		                                            <th width="15%">Recruitment PM</th>
		                                            <th width="15%">Operation PM</th>
		                                            <th class="text-right" width="10%"></th>
		                                        </tr>
		                                    </thead>
		                                    <tbody>
												<?php if(isset($project_nature)):?>
		                                        	<?php foreach ($project_nature as $val):?>
												        <tr>
												            <td><?php echo $val['name']?></td>
												            <td><?php echo (isset($users[$val['user_id']])?$users[$val['user_id']]:"")?></td>
												            <td><?php echo (isset($users[$val['user_id1']])?$users[$val['user_id1']]:"")?></td>
												            <td class="text-right">
												            	<a href="<?php echo base_url();?>settings/project_manager/edit/<?php echo $val['project_nature_id']?>" class="btn btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
												            	<a href="<?php echo base_url();?>settings/project_manager/delete/<?php echo $val['project_nature_id']?>" onclick="if(confirm('Are you sure you want to delete this record?')){return true;}else{return false;}" class="btn btn-sm btn-danger btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash"></i></a>
											            	</td>
												        </tr>
		                                        	<?php endforeach;?>
		                                        <?php endif;?>
		                                    </tbody>
		                                </table>

                                    </div>
                                </div>
                                
                            </div>
                            
                        </div>
                        
                    </div>
                    
                </div>
            
            </section>
            <!-- end of MAIN CONTENT -->