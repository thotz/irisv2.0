           <!-- MAIN CONTENT -->
            <section class="content">
            
                <div class="row">
                    <div class="col-md-12">
            
                        <!-- ACCESS LEVEL -->
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">Agent's List</h3>
                                <a href="#" class="btn btn-success btn-sm btn-flat btn-export-excel pull-right">Export to Excel</a>
                                <a href="<?=base_url()?>settings/agent_manager/add" class="btn btn-primary btn-sm btn-flat btn-add-new pull-right"><i class="ion-person-add" data-pack="default" data-tags="users, staff, head, human, member, new"></i> Add New Agent</a>
                            </div>
                            <div class="box-body">
                                
                                
                                <!-- Data Table (Access Menu) -->
                                <div class="row">
                                    <div class="col-md-12">
                                    
										<table id="tableAgentList" class="table table-hover table-bordered table-stripe table-no-margin">
										    <thead>
										        <tr>
										            <th width="20%">AGENT</th>
										            <th width="5%">BRANCH</th>
										            <th width="10%">CODE</th>
										            <th width="10%">MOBILE</th>
										            <th width="10%">PHONE</th>
										            <th width="25%">ADDRESS</th>
										            <th width="5%">STATUS</th>
										            <th width="15%">REMARKS</th>
										            <th width="5%"></th>
										        </tr>
										    </thead>
										    <tbody>
	                                        <?php if(isset($agent_list)):?>
	                                        	<?php foreach ($agent_list as $val):?>
											        <tr>
											            <td><?php echo $val['lastname'].", ".$val['firstname']?></td>
											            <td><?php echo $val['branch']?></td>
											            <td><?php echo $val['agent_code']?></td>
											            <td><?php echo $val['mobile']?></td>
											            <td><?php echo $val['telephone']?></td>
											            <td><?php echo $val['address']?></td>
											            <td><?php echo $agent_status[$val['status']]?></td>
											            <td><?php echo $val['remarks']?></td>
											            <td class="text-right">
											            	<a href="<?php echo base_url();?>settings/agent_manager/edit/<?php echo $val['agent_id']?>" class="btn btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
										            	</td>
											        </tr>
	                                        	<?php endforeach;?>
	                                        <?php endif;?>
											</tbody>
										</table>
                                    </div>
                                </div>
                                <!-- end of Data Table (Access Menu) -->
                                
                            </div>
                            
                        </div>
                        <!-- end of ACCESS LEVEL -->
                        
                    </div>
                    
                </div>
                
            </section>
            <!-- end of MAIN CONTENT -->