            <!-- MAIN CONTENT -->
            <section class="content">
            
                <div class="row">
                    <div class="col-md-12">
                                
                        <!-- ACCESS LEVEL FORM -->
                        <?php echo form_open('settings/agent_manager/add', 'class="form-horizontal"');?>
            
                            <!-- ACCESS LEVEL -->
                            <div class="box">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Add Agent</h3>
                                </div>
                                <div class="box-body">
<?php if($msg != ""):?>
	<?php if ($error):?>
                                    <!-- ALERT MESSAGE [ERROR] -->
                                    <div class="alert alert-danger alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>
                                        <i class="icon fa fa-ban"></i> <strong>ERROR!</strong> <?php echo $msg;?>
                                    </div>
	<?php else:?>
                                    <!-- ALERT MESSAGE [SUCCESS] -->
                                    <div class="alert alert-success alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>
                                        <i class="icon fa fa-check"></i> <strong>SUCCESS!</strong> Agent "NAME" is successfully added.
                                    </div>
	<?php endif;?>
<?php endif;?>
                                    <div class="row">
                                        <div class="col-md-10 col-md-offset-1">

                                            <!-- Status -->
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Status:</label>
                                                <div class="col-sm-10">
                                                    <label class="user-status"><input type="radio" name="radStatus" value="1" <?php echo set_radio('radStatus', '1', TRUE); ?> /> Active</label>
                                                    <label class="user-status"><input type="radio" name="radStatus" value="2" <?php echo set_radio('radStatus', '2'); ?> /> Inactive</label>
                                                    <label class="user-status"><input type="radio" name="radStatus" value="3" <?php echo set_radio('radStatus', '3'); ?> /> Cancelled</label>
                                                </div>
                                            </div>

                                            <!-- Full Name -->
                                            <div class="form-group">
                                                <label for="inputLastName" class="col-sm-2 control-label">Name<span class="required">*</span>:</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" name="inputLastName" value="<?php echo set_value('inputLastName');?>" placeholder="Last Name">
                                                </div>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" name="inputFirstName" value="<?php echo set_value('inputFirstName');?>" placeholder="First Name">
                                                </div>
                                                <div class="col-sm-2">
                                                    <input type="text" class="form-control" name="inputMI" value="<?php echo set_value('inputMI');?>" placeholder="M.I.">
                                                </div>
                                            </div>

                                            <!-- Position & Code -->
                                            <div class="form-group">
                                                <label for="inputPosition" class="col-sm-2 control-label">Branch<span class="required">*</span>:</label>
                                                <div class="col-sm-4">
                                                    <?php echo form_dropdown('selectBranch', $branches, $this->input->post('selectBranch'), 'class="form-control"');?>
                                                </div>
                                                <label for="inputCode" class="col-sm-2 control-label">Code<span class="required">*</span>:</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" name="inputCode" value="<?php echo set_value('inputCode');?>" placeholder="">
                                                </div>
                                            </div>

                                            <!-- Contact Info -->
                                            <div class="form-group">
                                                <label for="inputMobile" class="col-sm-2 control-label">Mobile No.<span class="required">*</span>:</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" name="inputMobile" value="<?php echo set_value('inputMobile');?>" placeholder="Mobile No.">
                                                </div>
                                                <label for="inputPhone" class="col-sm-2 control-label">Phone No.:</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" name="inputPhone" value="<?php echo set_value('inputPhone');?>" placeholder="Phone No.">
                                                </div>
                                            </div>

                                            <!-- Address -->
                                            <div class="form-group">
                                                <label for="txtAddress" class="col-sm-2 control-label">Address<span class="required">*</span>:</label>
                                                <div class="col-sm-10">
                                                    <textarea class="form-control" name="txtAddress"><?php echo set_value('txtAddress');?></textarea>
                                                </div>
                                            </div>

                                            <!-- Remarks -->
                                            <div class="form-group">
                                                <label for="txtRemarks" class="col-sm-2 control-label">Remarks:</label>
                                                <div class="col-sm-10">
                                                    <textarea class="form-control" name="txtRemarks"><?php echo set_value('txtRemarks');?></textarea>
                                                </div>
                                            </div>
                                            
                                            
                                            
                                            
                                            

                                        </div>
                                    </div>

                                </div>
                                
                                <div class="box-footer">

                                    <div class="row">
                                        <div class="col-md-10 col-md-offset-1">

                                            <div class="form-group">
                                                <div class="col-sm-2 col-sm-offset-2">
                                                    <!-- <a href="agent-manager.php" type="button" class="btn btn-block btn-primary btn-flat" id="btnSubmit">Submit</a> -->
                                                    <button type="submit" class="btn btn-block btn-primary btn-flat" id="btnAdd" name="btnAdd">Submit</button>
                                                </div>
                                                <div class="col-sm-2">
                                                    <a href="<?=base_url()?>settings/agent_manager/" type="button" class="btn btn-block btn-default btn-flat" id="btnCancel">Cancel</a>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    
                                </div>

                            </div>
                            <!-- end of ACCESS LEVEL -->
                        
                        <?php echo form_close();?>
                        <!-- end of ACCESS LEVEL FORM -->
                        
                    </div>
                    
                </div>
            
            </section>
            <!-- end of MAIN CONTENT -->