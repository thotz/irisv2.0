<table id="tableAgentList" class="table table-hover table-bordered table-stripe table-no-margin">
    <thead>
        <tr>
            <th width="20%">AGENT</th>
            <th width="5%">BRANCH</th>
            <th width="10%">CODE</th>
            <th width="10%">MOBILE</th>
            <th width="10%">PHONE</th>
            <th width="25%">ADDRESS</th>
            <th width="5%">STATUS</th>
            <th width="15%">REMARKS</th>
            <th width="5%"></th>
        </tr>
    </thead>
    <tbody>        				 			 
        <tr>
            <td>ABABON, QUIELBHORN</td>
            <td>MNL</td>
            <td>ABABON</td>
            <td>09067828243</td>
            <td></td>
            <td>PACO, MANILA</td>
            <td>Active</td>
            <td></td>
            <td class="text-right"><button type="button" class="btn btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></button></td>
        </tr>
        <tr>
            <td>ABDULGANI, GUIAMALUDIN</td>
            <td>DVO</td>
            <td>ABDULGANI</td>
            <td>0926-5250488</td>
            <td></td>
            <td>NAMUKEN, SULTAN MASTURA, MAGUINDANAO</td>
            <td>Active</td>
            <td>W/ NBI</td>
            <td class="text-right"><button type="button" class="btn btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></button></td>
        </tr>
        <tr>				 			
            <td>ABDULLAH, FAUJIAH</td>
            <td>DVO</td>
            <td>ABDULLAH</td>
            <td>0906-7850692</td>
            <td></td>
            <td>010 1ST ROAD SPDA, SEMBA, DATU ODIN SINSUAT, MAGUINDANAO</td>
            <td>Active</td>
            <td>W/ NBI</td>
            <td class="text-right"><button type="button" class="btn btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></button></td>
        </tr>
        <tr>
            <td>ABDULRAUF, ASLIAH</td>
            <td>CDO</td>
            <td>ABDULRAUF</td>
            <td>0919-2633816</td>
            <td></td>
            <td>RECLAMATION SITE, PADIAN, MARAWI CITY</td>
            <td>Active</td>
            <td></td>
            <td class="text-right"><button type="button" class="btn btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></button></td>
        </tr>
        <tr>				 			 
            <td>ABELLA, FE</td>
            <td>CBU</td>
            <td>ABELLA</td>
            <td>0909-4789557</td>
            <td></td>
            <td>MAGDUGO, TOLEDO, CEBU</td>
            <td>Active</td>
            <td></td>
            <td class="text-right"><button type="button" class="btn btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></button></td>
        </tr>		 			 
        <tr>
            <td>ABABON, QUIELBHORN</td>
            <td>MNL</td>
            <td>ABABON</td>
            <td>09067828243</td>
            <td></td>
            <td>PACO, MANILA</td>
            <td>Active</td>
            <td></td>
            <td class="text-right"><button type="button" class="btn btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></button></td>
        </tr>
        <tr>
            <td>ABDULGANI, GUIAMALUDIN</td>
            <td>DVO</td>
            <td>ABDULGANI</td>
            <td>0926-5250488</td>
            <td></td>
            <td>NAMUKEN, SULTAN MASTURA, MAGUINDANAO</td>
            <td>Active</td>
            <td>W/ NBI</td>
            <td class="text-right"><button type="button" class="btn btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></button></td>
        </tr>
        <tr>				 			
            <td>ABDULLAH, FAUJIAH</td>
            <td>DVO</td>
            <td>ABDULLAH</td>
            <td>0906-7850692</td>
            <td></td>
            <td>010 1ST ROAD SPDA, SEMBA, DATU ODIN SINSUAT, MAGUINDANAO</td>
            <td>Active</td>
            <td>W/ NBI</td>
            <td class="text-right"><button type="button" class="btn btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></button></td>
        </tr>
        <tr>
            <td>ABDULRAUF, ASLIAH</td>
            <td>CDO</td>
            <td>ABDULRAUF</td>
            <td>0919-2633816</td>
            <td></td>
            <td>RECLAMATION SITE, PADIAN, MARAWI CITY</td>
            <td>Active</td>
            <td></td>
            <td class="text-right"><button type="button" class="btn btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></button></td>
        </tr>
        <tr>				 			 
            <td>ABELLA, FE</td>
            <td>CBU</td>
            <td>ABELLA</td>
            <td>0909-4789557</td>
            <td></td>
            <td>MAGDUGO, TOLEDO, CEBU</td>
            <td>Active</td>
            <td></td>
            <td class="text-right"><button type="button" class="btn btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></button></td>
        </tr>		 			 
        <tr>
            <td>ABABON, QUIELBHORN</td>
            <td>MNL</td>
            <td>ABABON</td>
            <td>09067828243</td>
            <td></td>
            <td>PACO, MANILA</td>
            <td>Active</td>
            <td></td>
            <td class="text-right"><button type="button" class="btn btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></button></td>
        </tr>
        <tr>
            <td>ABDULGANI, GUIAMALUDIN</td>
            <td>DVO</td>
            <td>ABDULGANI</td>
            <td>0926-5250488</td>
            <td></td>
            <td>NAMUKEN, SULTAN MASTURA, MAGUINDANAO</td>
            <td>Active</td>
            <td>W/ NBI</td>
            <td class="text-right"><button type="button" class="btn btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></button></td>
        </tr>
        <tr>				 			
            <td>ABDULLAH, FAUJIAH</td>
            <td>DVO</td>
            <td>ABDULLAH</td>
            <td>0906-7850692</td>
            <td></td>
            <td>010 1ST ROAD SPDA, SEMBA, DATU ODIN SINSUAT, MAGUINDANAO</td>
            <td>Active</td>
            <td>W/ NBI</td>
            <td class="text-right"><button type="button" class="btn btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></button></td>
        </tr>
        <tr>
            <td>ABDULRAUF, ASLIAH</td>
            <td>CDO</td>
            <td>ABDULRAUF</td>
            <td>0919-2633816</td>
            <td></td>
            <td>RECLAMATION SITE, PADIAN, MARAWI CITY</td>
            <td>Active</td>
            <td></td>
            <td class="text-right"><button type="button" class="btn btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></button></td>
        </tr>
        <tr>				 			 
            <td>ABELLA, FE</td>
            <td>CBU</td>
            <td>ABELLA</td>
            <td>0909-4789557</td>
            <td></td>
            <td>MAGDUGO, TOLEDO, CEBU</td>
            <td>Active</td>
            <td></td>
            <td class="text-right"><button type="button" class="btn btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></button></td>
        </tr>		 			 
        <tr>
            <td>ABABON, QUIELBHORN</td>
            <td>MNL</td>
            <td>ABABON</td>
            <td>09067828243</td>
            <td></td>
            <td>PACO, MANILA</td>
            <td>Active</td>
            <td></td>
            <td class="text-right"><button type="button" class="btn btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></button></td>
        </tr>
        <tr>
            <td>ABDULGANI, GUIAMALUDIN</td>
            <td>DVO</td>
            <td>ABDULGANI</td>
            <td>0926-5250488</td>
            <td></td>
            <td>NAMUKEN, SULTAN MASTURA, MAGUINDANAO</td>
            <td>Active</td>
            <td>W/ NBI</td>
            <td class="text-right"><button type="button" class="btn btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></button></td>
        </tr>
        <tr>				 			
            <td>ABDULLAH, FAUJIAH</td>
            <td>DVO</td>
            <td>ABDULLAH</td>
            <td>0906-7850692</td>
            <td></td>
            <td>010 1ST ROAD SPDA, SEMBA, DATU ODIN SINSUAT, MAGUINDANAO</td>
            <td>Active</td>
            <td>W/ NBI</td>
            <td class="text-right"><button type="button" class="btn btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></button></td>
        </tr>
        <tr>
            <td>ABDULRAUF, ASLIAH</td>
            <td>CDO</td>
            <td>ABDULRAUF</td>
            <td>0919-2633816</td>
            <td></td>
            <td>RECLAMATION SITE, PADIAN, MARAWI CITY</td>
            <td>Active</td>
            <td></td>
            <td class="text-right"><button type="button" class="btn btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></button></td>
        </tr>
        <tr>				 			 
            <td>ABELLA, FE</td>
            <td>CBU</td>
            <td>ABELLA</td>
            <td>0909-4789557</td>
            <td></td>
            <td>MAGDUGO, TOLEDO, CEBU</td>
            <td>Active</td>
            <td></td>
            <td class="text-right"><button type="button" class="btn btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></button></td>
        </tr>
    </tbody>
</table>