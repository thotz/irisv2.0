<?php
	$default_msg = "PESO Office is successfully updated.";

	if($this->session->flashdata('add_peso_success') != ''){
		$default_msg = $this->session->flashdata('add_peso_success');
		$msg = TRUE;
	}
?>
			<!-- MAIN CONTENT -->
            <section class="content">
            
                <div class="row">
                    <div class="col-md-12">
                                
                        <!-- ACCESS LEVEL FORM -->
                        <?php echo form_open('settings/peso_manager/edit/'.$peso_details[0]['peso_id'], 'class="form-horizontal"');?>
            
                            <!-- ACCESS LEVEL -->
                            <div class="box">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Edit PESO</h3>
                                </div>
                                <div class="box-body">
<?php if($msg != ""):?>
	<?php if ($error):?>
                                    <!-- ALERT MESSAGE [ERROR] -->
                                    <div class="alert alert-danger alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <i class="icon fa fa-ban"></i> <strong>ERROR!</strong> <?php echo $msg;?>
                                    </div>
	<?php else:?>
                                    <!-- ALERT MESSAGE [SUCCESS] -->
                                    <div class="alert alert-success alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <i class="icon fa fa-check"></i> <strong>SUCCESS!</strong> <?php echo $default_msg;?>
                                    </div>
	<?php endif;?>
<?php endif;?>
                                    <div class="row">
                                        <div class="col-md-10 col-md-offset-1">

                                            <!-- Status -->
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Status:</label>
                                                <div class="col-sm-10">
                                                    <label class="user-status"><input type="radio" class="flat-blue" name="radStatus" value="1" <?php echo  set_radio('radStatus', '1', $peso_details[0]['active'] === "1" ? TRUE:FALSE); ?> /> Active</label>
                                                    <label class="user-status"><input type="radio" class="flat-blue" name="radStatus" value="0" <?php echo  set_radio('radStatus', '0', $peso_details[0]['active'] === "0" ? TRUE:FALSE); ?> /> Inactive</label>
                                                </div>
                                            </div>

                                            <!-- PESO Name -->
                                            <div class="form-group">
                                                <label for="inputPESOName" class="col-sm-2 control-label">PESO Name<span class="required">*</span>:</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" name="inputPESOName" value="<?php echo set_value('inputPESOName', $peso_details[0]['name']);?>" placeholder="PESO Name">
                                                </div>
                                            </div>

                                            <!-- Address -->
                                            <div class="form-group">
                                                <label for="txtAddress" class="col-sm-2 control-label">Address<span class="required">*</span>:</label>
                                                <div class="col-sm-10">
                                                    <textarea class="form-control" name="txtAddress"><?php echo set_value('txtAddress', $peso_details[0]['streetaddress']);?></textarea>
                                                </div>
                                            </div>
                                            
                                            <!-- City & Phone -->
                                            <div class="form-group">
                                                <label for="selectCity" class="col-sm-2 control-label">City<span class="required">*</span>:</label>
                                                <div class="col-sm-4">
													<?php echo form_dropdown('selectCity', $city, (NULL===$this->input->post('selectCity'))? $peso_details[0]['city_id']:$this->input->post('selectCity'), 'class="form-control"');?>
                                                </div>
                                                <label for="inputPhone" class="col-sm-2 control-label">Phone No.:</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" name="inputPhone" value="<?php echo set_value('inputPhone', $peso_details[0]['tel']);?>" placeholder="Phone No.">
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                                
                                <div class="box-footer">

                                    <div class="row">
                                        <div class="col-md-10 col-md-offset-1">

                                            <div class="form-group">
                                                <div class="col-sm-2 col-sm-offset-2">
                                                    <button type="submit" class="btn btn-block btn-primary btn-flat" id="btnEdit" name="btnEdit">Submit</button>
                                                </div>
                                                <div class="col-sm-2">
                                                    <a href="<?php echo base_url()?>settings/peso_manager/index" type="button" class="btn btn-block btn-default btn-flat" id="btnCancel">Cancel</a>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    
                                </div>

                            </div>
                            <!-- end of ACCESS LEVEL -->
                        
                        <?php echo form_close();?>
                        <!-- end of ACCESS LEVEL FORM -->
                        
                    </div>
                    
                </div>
            
            </section>
            <!-- end of MAIN CONTENT -->