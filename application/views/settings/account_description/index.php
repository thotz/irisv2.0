<?php
	$error = $this->session->flashdata('accdesc_del_error');
	$msg = $this->session->flashdata('accdesc_del_msg');
?>
            <!-- MAIN CONTENT -->
            <section class="content">
         
                <div class="row">
                    <div class="col-md-12">
            
                        <!-- ACCOUNT DESCRIPTION -->
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">Account Description Manager</h3>
                                <a href="#" class="btn btn-sm btn-success btn-sm btn-flat btn-export-excel pull-right">Export to Excel</a>
                                <a href="<?php echo base_url();?>settings/account_description/add" class="btn btn-sm btn-primary btn-sm btn-flat btn-add-new pull-right"><i class="fa fa-plus"></i> Add Account Description</a>
                            </div>
                            <div class="box-body">
                                    
<?php if($msg != ''):?>
	<?php if($error == TRUE):?>
                                    <!-- ALERT MESSAGE [ERROR] -->
                                    <div class="alert alert-danger alert-dismissabl">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>
                                        <i class="icon fa fa-ban"></i> <strong>ERROR!</strong> Unabled to delete. <?php echo $msg;?>
                                    </div>
	<?php else :?>
                                    <!-- ALERT MESSAGE [SUCCESS] -->
                                    <div class="alert alert-success alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>
                                        <i class="icon fa fa-check"></i> <strong>SUCCESS!</strong> <?php echo $msg;?>
                                    </div>
	<?php endif;?>
<?php endif;?>
                                    
                            	<div class="row">
									<div class="col-md-12">
		                                <table id="tableAccDesc" class="table table-hover table-bordered table-stripe table-no-margin">
		                                    <thead>
		                                        <tr>	
		                                            <th width="90%">Account Description</th>
		                                            <th class="text-right" width="10%"></th>
		                                        </tr>
		                                    </thead>
		                                    <tbody>
												<?php if(isset($acc_description)):?>
		                                        	<?php foreach ($acc_description as $val):?>
												        <tr>
												            <td><?php echo $val['mode']?></td>
												            <td class="text-right">
												            	<a href="<?php echo base_url();?>settings/account_description/edit/<?php echo $val['id']?>" class="btn btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
												            	<a href="<?php echo base_url();?>settings/account_description/delete/<?php echo $val['id']?>" onclick="if(confirm('Are you sure you want to delete this record?')){return true;}else{return false;}" class="btn btn-sm btn-danger btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash"></i></a>
											            	</td>
												        </tr>
		                                        	<?php endforeach;?>
		                                        <?php endif;?>
		                                    </tbody>
		                                </table>

                                    </div>
                                </div>
                                
                            </div>
                            
                        </div>
                        <!-- end of ACCOUNT DESCRIPTION -->
                        
                    </div>
                    
                </div>
            
            </section>
            <!-- end of MAIN CONTENT -->