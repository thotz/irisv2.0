            <!-- MAIN CONTENT -->
            <section class="content">
            
                <div class="row">
                    <div class="col-md-12">
                                
                        <?php echo form_open('settings/broker_manager/add', 'class="form-horizontal"');?>
            
                            <div class="box">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Add Broker</h3>
                                </div>
                                <div class="box-body">

<?php if($msg != ""):?>
	<?php if ($error):?>
                                    <!-- ALERT MESSAGE [ERROR] -->
                                    <div class="alert alert-danger alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <i class="icon fa fa-ban"></i> <strong>ERROR!</strong> <?php echo $msg;?>
                                    </div>
	<?php else:?>
                                    <!-- ALERT MESSAGE [SUCCESS] -->
                                    <div class="alert alert-success alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <i class="icon fa fa-check"></i> <strong>SUCCESS!</strong> Broker is successfully added.
                                    </div>
	<?php endif;?>
<?php endif;?>

                                    <div class="row">
                                        <div class="col-md-10 col-md-offset-1">

                                            <div class="form-group">
                                                <label for="inputBrokerName" class="col-sm-2 control-label">Name<span class="required">*</span>:</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" name="inputBrokerName" value="<?php echo set_value('inputBrokerName');?>" placeholder="Broker Name">
                                                </div>
                                            </div>

                                            <!-- Address -->
                                            <div class="form-group">
                                                <label for="txtAddress" class="col-sm-2 control-label">Address:</label>
                                                <div class="col-sm-10">
                                                    <textarea class="form-control" name="txtAddress"><?php echo set_value('txtAddress');?></textarea>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="selectCountry" class="col-sm-2 control-label">Country:</label>
                                                <div class="col-sm-4">
													<?php echo form_dropdown('selectCountry', $country, $this->input->post('selectCountry'), 'class="form-control"');?>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="selectPrincipal" class="col-sm-2 control-label">Principal:</label>
                                                <div class="col-sm-10">
													<?php echo form_dropdown('selectPrincipal', $principals, $this->input->post('selectPrincipal'), 'class="form-control"');?>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="inputFax" class="col-sm-2 control-label">Fax No.:</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" name="inputFax" value="<?php echo set_value('inputFax');?>" placeholder="###">
                                                </div>
                                                <label for="inputTel" class="col-sm-2 control-label">Tel No.:</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" name="inputTel" value="<?php echo set_value('inputTel');?>" placeholder="###">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="inputContact" class="col-sm-2 control-label">Contact Person<span class="required">*</span>:</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" name="inputContact" value="<?php echo set_value('inputContact');?>" placeholder="Name">
                                                </div>
                                                <label for="inputContactPos" class="col-sm-2 control-label">Position:</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" name="inputContactPos" value="<?php echo set_value('inputContactPos');?>" placeholder="Position">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="txtContactDtl" class="col-sm-2 control-label">Details:</label>
                                                <div class="col-sm-10">
                                                    <textarea class="form-control" name="txtContactDtl"><?php echo set_value('txtContactDtl');?></textarea>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="inputAccr" class="col-sm-2 control-label">Accreditation No.:</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" name="inputAccr" value="<?php echo set_value('inputAccr');?>" placeholder="###">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="" class="col-sm-2 control-label">Valid Until:</label>
                                                <div class="col-sm-2">
                                                	<?=dateselectmonth("valid_month", $this->input->post('valid_month'), 'class="form-control"', "whole");?>
                                                </div>
                                                <div class="col-sm-2">
                                                	<?=dateselectday("valid_day", $this->input->post('valid_day'), 'class="form-control"');?>
                                                </div>
                                                <div class="col-sm-2">
                                                	<?=dateselectyear("valid_year", $this->input->post('valid_year'), 'class="form-control"');?>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="inputRecOffcr" class="col-sm-2 control-label">Recruitment Officer:</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" name="inputRecOffcr" value="<?php echo set_value('inputRecOffcr');?>" placeholder="">
                                                </div>
                                            </div>


                                        </div>
                                    </div>

                                </div>
                                
                                <div class="box-footer">

                                    <div class="row">
                                        <div class="col-md-10 col-md-offset-1">

                                            <div class="form-group">
                                                <div class="col-sm-2 col-sm-offset-2">
                                                    <!-- <a href="#" type="button" class="btn btn-block btn-primary btn-flat" id="btnSubmit">Submit</a> -->
                                                    <button type="submit" class="btn btn-block btn-primary btn-flat" id="btnAdd" name="btnAdd">Submit</button>
                                                </div>
                                                <div class="col-sm-2">
                                                    <a href="<?php echo base_url()?>settings/broker_manager/index" type="button" class="btn btn-block btn-default btn-flat" id="btnCancel">Cancel</a>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    
                                </div>

                            </div>
                        
                        <?php echo form_close();?>
                        
                    </div>
                    
                </div>
            
            </section>
            <!-- end of MAIN CONTENT -->