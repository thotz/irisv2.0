<?php
	$default_msg = "Broker is successfully updated.";

	if($this->session->flashdata('add_broker_success') != ''){
		$default_msg = $this->session->flashdata('add_broker_success');
		$msg = TRUE;
	}
?>
			<!-- MAIN CONTENT -->
            <section class="content">
            
                <div class="row">
                    <div class="col-md-12">
                                
                        <?php echo form_open('settings/broker_manager/edit/'.$broker_details[0]['broker_id'], 'class="form-horizontal"');?>
            
                            <div class="box">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Edit Broker</h3>
                                </div>
                                <div class="box-body">
<?php if($msg != ""):?>
	<?php if ($error):?>
                                    <!-- ALERT MESSAGE [ERROR] -->
                                    <div class="alert alert-danger alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <i class="icon fa fa-ban"></i> <strong>ERROR!</strong> <?php echo $msg;?>
                                    </div>
	<?php else:?>
                                    <!-- ALERT MESSAGE [SUCCESS] -->
                                    <div class="alert alert-success alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <i class="icon fa fa-check"></i> <strong>SUCCESS!</strong> <?php echo $default_msg;?>
                                    </div>
	<?php endif;?>
<?php endif;?>
                                    <div class="row">
                                        <div class="col-md-10 col-md-offset-1">

                                            <div class="form-group">
                                                <label for="inputBrokerName" class="col-sm-2 control-label">Name<span class="required">*</span>:</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" name="inputBrokerName" value="<?php echo set_value('inputBrokerName', $broker_details[0]['name']);?>" placeholder="Broker Name">
                                                </div>
                                            </div>

                                            <!-- Address -->
                                            <div class="form-group">
                                                <label for="txtAddress" class="col-sm-2 control-label">Address:</label>
                                                <div class="col-sm-10">
                                                    <textarea class="form-control" name="txtAddress"><?php echo set_value('txtAddress', $broker_details[0]['address']);?></textarea>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="selectCountry" class="col-sm-2 control-label">Country:</label>
                                                <div class="col-sm-4">
													<?php echo form_dropdown('selectCountry', $country, (NULL===$this->input->post('selectCountry'))? $broker_details[0]['country_id']:$this->input->post('selectCountry'), 'class="form-control"');?>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="selectPrincipal" class="col-sm-2 control-label">Principal:</label>
                                                <div class="col-sm-10">
													<?php echo form_dropdown('selectPrincipal', $principals, (NULL===$this->input->post('selectPrincipal'))? $broker_details[0]['principal_id']:$this->input->post('selectPrincipal'), 'class="form-control"');?>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="inputFax" class="col-sm-2 control-label">Fax No.:</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" name="inputFax" value="<?php echo set_value('inputFax', $broker_details[0]['fax']);?>" placeholder="###">
                                                </div>
                                                <label for="inputTel" class="col-sm-2 control-label">Tel No.:</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" name="inputTel" value="<?php echo set_value('inputTel', $broker_details[0]['telephone']);?>" placeholder="###">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="inputContact" class="col-sm-2 control-label">Contact Person<span class="required">*</span>:</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" name="inputContact" value="<?php echo set_value('inputContact', $broker_details[0]['contact_person']);?>" placeholder="Name">
                                                </div>
                                                <label for="inputContactPos" class="col-sm-2 control-label">Position:</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" name="inputContactPos" value="<?php echo set_value('inputContactPos',$broker_details[0]['contact_position']);?>" placeholder="Position">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="txtContactDtl" class="col-sm-2 control-label">Details:</label>
                                                <div class="col-sm-10">
                                                    <textarea class="form-control" name="txtContactDtl"><?php echo set_value('txtContactDtl', $broker_details[0]['contact_details']);?></textarea>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="inputAccr" class="col-sm-2 control-label">Accreditation No.:</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" name="inputAccr" value="<?php echo set_value('inputAccr', $broker_details[0]['acc_no']);?>" placeholder="###">
                                                </div>
                                            </div>
                                                	<?php
                                                	if ($broker_details[0]['acc_date'] != '' && $broker_details[0]['acc_date'] != '0000-00-00'){
                                                		list ($acc_year, $acc_month, $acc_day) = @split ('[-]', $broker_details[0]['acc_date']);
                                                	} else {
                                                		$acc_year = "";
                                                		$acc_month="";
                                                		$acc_day="";
                                                	}
                                                	?>
                                            <div class="form-group">
                                                <label for="" class="col-sm-2 control-label">Valid Until:</label>
                                                <div class="col-sm-2">
                                                	<?=dateselectmonth("valid_month", (NULL===$this->input->post('valid_month'))? $acc_month:$this->input->post('valid_month'), 'class="form-control"', "whole");?>
                                                </div>
                                                <div class="col-sm-2">
                                                	<?=dateselectday("valid_day", (NULL===$this->input->post('valid_day'))? $acc_day:$this->input->post('valid_day'), 'class="form-control"');?>
                                                </div>
                                                <div class="col-sm-2">
                                                	<?=dateselectyear("valid_year", (NULL===$this->input->post('valid_year'))? $acc_year:$this->input->post('valid_year'), 'class="form-control"');?>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="inputRecOffcr" class="col-sm-2 control-label">Recruitment Officer:</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" name="inputRecOffcr" value="<?php echo set_value('inputRecOffcr', $broker_details[0]['RO']);?>" placeholder="">
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                                
                                <div class="box-footer">

                                    <div class="row">
                                        <div class="col-md-10 col-md-offset-1">

                                            <div class="form-group">
                                                <div class="col-sm-2 col-sm-offset-2">
                                                    <button type="submit" class="btn btn-block btn-primary btn-flat" id="btnEdit" name="btnEdit">Submit</button>
                                                </div>
                                                <div class="col-sm-2">
                                                    <a href="<?php echo base_url()?>settings/broker_manager/index" type="button" class="btn btn-block btn-default btn-flat" id="btnCancel">Cancel</a>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    
                                </div>

                            </div>
                        
                        <?php echo form_close();?>
                        
                    </div>
                    
                </div>
            
            </section>
            <!-- end of MAIN CONTENT -->