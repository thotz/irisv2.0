<?php
	$default_msg = "General Category is successfully updated.";

	if($this->session->flashdata('add_gencat_success') != ''){
		$default_msg = $this->session->flashdata('add_gencat_success');
		$msg = TRUE;
	}
?>
            <!-- MAIN CONTENT -->
            <section class="content">
            
                <div class="row">
                    <div class="col-md-12">
                                
                        <?php echo form_open('settings/general_category_manager/edit/'.$gen_category[0]['gen_category_id'], 'class="form-horizontal"');?>
            
                            <div class="box">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Edit General Category</h3>
                                </div>
                                <div class="box-body">

<?php if($msg != ""):?>
	<?php if ($error):?>
                                    <!-- ALERT MESSAGE [ERROR] -->
                                    <div class="alert alert-danger alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <i class="icon fa fa-ban"></i> <strong>ERROR!</strong> <?php echo $msg;?>
                                    </div>
	<?php else:?>
                                    <!-- ALERT MESSAGE [SUCCESS] -->
                                    <div class="alert alert-success alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <i class="icon fa fa-check"></i> <strong>SUCCESS!</strong> <?php echo $default_msg;?>
                                    </div>
	<?php endif;?>
<?php endif;?>

                                    <div class="row">
                                        <div class="col-md-10 col-md-offset-1">

                                            <div class="form-group">
                                                <label for="inputName" class="col-sm-2 control-label">Name<span class="required">*</span>:</label>
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control" name="inputName" value="<?php echo set_value('inputName', $gen_category[0]['name']);?>" placeholder="Name">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="selectCat1" class="col-sm-2 control-label">Category 1:</label>
                                                <div class="col-sm-4">
                                                    <?php echo form_dropdown('selectCat1', $category, (NULL===$this->input->post('selectCat1'))?$gen_category[0]['type1']:$this->input->post('selectCat1'), 'class="form-control"');?>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="selectCat2" class="col-sm-2 control-label">Category 2:</label>
                                                <div class="col-sm-4">
                                                    <?php echo form_dropdown('selectCat2', $category, (NULL===$this->input->post('selectCat2'))?$gen_category[0]['type2']:$this->input->post('selectCat2'), 'class="form-control"');?>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>

                                </div>
                                
                                <div class="box-footer">

                                    <div class="row">
                                        <div class="col-md-10 col-md-offset-1">

                                            <div class="form-group">
                                                <div class="col-sm-2 col-sm-offset-2">
                                                    <button type="submit" class="btn btn-block btn-primary btn-flat" id="btnAdd" name="btnEdit">Submit</button>
                                                </div>
                                                <div class="col-sm-2">
                                                    <a href="<?php echo base_url();?>settings/general_category_manager/index" type="button" class="btn btn-block btn-default btn-flat" id="btnCancel">Cancel</a>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    
                                </div>

                            </div>
                        
                        <?php echo form_close();?>
                         
                        
                    </div>
                    
                </div>
            
            </section>
            <!-- end of MAIN CONTENT -->