<?php
	$error = $this->session->flashdata('clinic_del_error');
	$msg = $this->session->flashdata('clinic_del_msg');
?>
			<!-- MAIN CONTENT -->
            <section class="content">
            
                <div class="row">
                    <div class="col-md-12">
            
                        <!-- ACCESS LEVEL -->
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">Clinic List</h3>
                                <a href="#" class="btn btn-success btn-sm btn-flat btn-export-excel pull-right">Export to Excel</a>
                                <a href="<?=base_url()?>settings/clinic_manager/add" class="btn btn-primary btn-sm btn-flat btn-add-new pull-right"><i class="fa fa-plus"></i> Add New Clinic</a>
                            </div>
                            <div class="box-body">
                                
<?php if($msg != ''):?>
	<?php if($error == TRUE):?>
                                    <!-- ALERT MESSAGE [ERROR] -->
                                    <div class="alert alert-danger alert-dismissabl">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>
                                        <i class="icon fa fa-ban"></i> <strong>ERROR!</strong> Unabled to delete. <?php echo $msg;?>
                                    </div>
	<?php else :?>
                                    <!-- ALERT MESSAGE [SUCCESS] -->
                                    <div class="alert alert-success alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>
                                        <i class="icon fa fa-check"></i> <strong>SUCCESS!</strong> <?php echo $msg;?>
                                    </div>
	<?php endif;?>
<?php endif;?>
                                
                                <!-- Data Table -->
                                <div class="row">
                                    <div class="col-md-12">
                                    
                                        <table id="tableClinicList" class="table table-hover table-bordered table-stripe table-no-margin table-condensed">
										    <thead>
										        <tr>
										            <th>Clinic Name</th>
										            <th>Code</th>
										            <th>Contact</th>
										            <th>Contact Person</th>
										            <th></th>
										        </tr>
										    </thead>
										    <tbody>
	                                        <?php if(isset($clinic_list)):?>
	                                        	<?php foreach ($clinic_list as $val):?>
											        <tr>
											            <td><?php echo $val['name']?></td>
											            <td><?php echo strtoupper($val['code'])?></td>
											            <td><?php echo $val['telephone']?></td>
											            <td><?php echo $val['contact']?></td>
											            <td class="text-right">
											            	<a href="<?php echo base_url();?>settings/clinic_manager/edit/<?php echo $val['clinic_id']?>" class="btn btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
											            	<a href="<?php echo base_url();?>settings/clinic_manager/delete/<?php echo $val['clinic_id']?>" onclick="if(confirm('Are you sure you want to delete this clinic?')){return true;}else{return false;}" class="btn btn-sm btn-danger btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash"></i></a>
										            	</td>
											        </tr>
	                                        	<?php endforeach;?>
	                                        <?php endif;?>
										       <!-- <tr>				 			 
										            <td>Abakkus Med Diagnostic Services</td>
										            <td>Abakkus</td>
										            <td>822-2219</td>
										            <td></td>
										            <td class="text-right" style="width:65px;">
										                <div class="btn-group">
										                    <button type="button" class="btn btn-sm btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></button>
										                    <button type="button" class="btn btn-sm btn-danger btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash"></i></button>
										                </div>
										            </td>
										        </tr>
										        <tr>				 			 
										            <td>ABM Medical &amp; Industrial Clinic</td>
										            <td>ABM</td>
										            <td>02-8819221</td>
										            <td>Dr. Mallari / Dra. Mallari</td>
										            <td class="text-right">
										                <div class="btn-group">
										                    <button type="button" class="btn btn-sm btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></button>
										                    <button type="button" class="btn btn-sm btn-danger btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash"></i></button>
										                </div>
										            </td>
										        </tr>
										        <tr>				 			 
										            <td>CDMC Multi-Specialist Diagnostic Center</td>
										            <td>CDMC</td>
										            <td>088-856-3045</td>
										            <td>Dr. German Tristan Casino</td>
										            <td class="text-right">
										                <div class="btn-group">
										                    <button type="button" class="btn btn-sm btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></button>
										                    <button type="button" class="btn btn-sm btn-danger btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash"></i></button>
										                </div>
										            </td>
										        </tr>
										        <tr>				 			 
										            <td>Christian E. Cangco Medical</td>
										            <td>Cangco</td>
										            <td>239-7440 up to 42</td>
										            <td></td>
										            <td class="text-right">
										                <div class="btn-group">
										                    <button type="button" class="btn btn-sm btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></button>
										                    <button type="button" class="btn btn-sm btn-danger btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash"></i></button>
										                </div>
										            </td>
										        </tr> --!>
										    </tbody>
										</table>
                                    
                                    </div>
                                </div>
                                <!-- end of Data Table (Access Menu) -->
                                
                            </div>
                            
                        </div>
                        <!-- end of ACCESS LEVEL -->
                        
                    </div>
                    
                </div>
                
            </section>
            <!-- end of MAIN CONTENT -->