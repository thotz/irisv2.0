<!-- MAIN CONTENT -->
            <section class="content">
            
                <div class="row">
                    <div class="col-md-12">
                                
                        <!-- ACCESS LEVEL FORM -->
                        <?php echo form_open('settings/clinic_manager/add', 'class="form-horizontal"');?>
            
                            <!-- ACCESS LEVEL -->
                            <div class="box">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Add Clinic</h3>
                                </div>
                                <div class="box-body">
<?php if($msg != ""):?>
	<?php if ($error):?>
                                    <!-- ALERT MESSAGE [ERROR] -->
                                    <div class="alert alert-danger alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <i class="icon fa fa-ban"></i> <strong>ERROR!</strong> <?php echo $msg;?>
                                    </div>
	<?php else:?>
                                    <!-- ALERT MESSAGE [SUCCESS] -->
                                    <div class="alert alert-success alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <i class="icon fa fa-check"></i> <strong>SUCCESS!</strong> Clinic is successfully added.
                                    </div>
	<?php endif;?>
<?php endif;?>
                                    <div class="row">
                                        <div class="col-md-10 col-md-offset-1">

                                            <!-- Clinic Name -->
                                            <div class="form-group">
                                                <label for="inputClinicName" class="col-sm-2 control-label">Clinic Name<span class="required">*</span>:</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" name="inputClinicName" value="<?php echo set_value('inputClinicName');?>" placeholder="Clinic Name">
                                                </div>
                                            </div>
                                            
                                            <!-- Contact Person -->
                                            <!-- <div class="form-group">
                                                <label for="inputLastName" class="col-sm-2 control-label">Contact Person<span class="required">*</span>:</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" name="inputLastName" value="<?php echo set_value('inputLastName');?>" placeholder="Last Name">
                                                </div>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" name="inputFirstName" value="<?php echo set_value('inputFirstName');?>" placeholder="First Name">
                                                </div>
                                                <div class="col-sm-2">
                                                    <input type="text" class="form-control" name="inputMI" value="<?php echo set_value('inputMI');?>" placeholder="M.I.">
                                                </div>
                                            </div> -->
                                            
                                            <div class="form-group">
                                                <label for="inputContact" class="col-sm-2 control-label">Contact Person<span class="required">*</span>:</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" name="inputContact" value="<?php echo set_value('inputContact');?>" placeholder="Name">
                                                </div>
                                            </div>

                                            <!-- Contact Info -->
                                            <div class="form-group">
                                                <label for="inputFax" class="col-sm-2 control-label">Fax No.:</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" name="inputFax" value="<?php echo set_value('inputFax');?>" placeholder="Fax No.">
                                                </div>
                                                <label for="inputPhone" class="col-sm-2 control-label">Contact No.<span class="required">*</span>:</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" name="inputPhone" value="<?php echo set_value('inputPhone');?>" placeholder="Phone/Mobile">
                                                </div>
                                            </div>
                                                
                                            <!-- Email Address & Code -->
                                            <div class="form-group">
                                                <label for="inputCode" class="col-sm-2 control-label">Code<span class="required">*</span>:</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" name="inputCode" value="<?php echo set_value('inputCode');?>" placeholder="">
                                                </div>
                                                <label for="inputEmailAddress" class="col-sm-2 control-label">Email Address:</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" name="inputEmailAddress" value="<?php echo set_value('inputEmailAddress');?>" placeholder="Email">
                                                </div>
                                            </div>

                                            <!-- Address -->
                                            <div class="form-group">
                                                <label for="txtAddress" class="col-sm-2 control-label">Address<span class="required">*</span>:</label>
                                                <div class="col-sm-10">
                                                    <textarea class="form-control" name="txtAddress"><?php echo set_value('txtAddress');?></textarea>
                                                </div>
                                            </div>

                                            <!-- Remarks -->
                                            <div class="form-group">
                                                <label for="txtRemarks" class="col-sm-2 control-label">Remarks:</label>
                                                <div class="col-sm-10">
                                                    <textarea class="form-control" name="txtRemarks"><?php echo set_value('txtRemarks');?></textarea>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                                
                                <div class="box-footer">

                                    <div class="row">
                                        <div class="col-md-10 col-md-offset-1">

                                            <div class="form-group">
                                                <div class="col-sm-2 col-sm-offset-2">
                                                    <button type="submit" class="btn btn-block btn-primary btn-flat" id="btnAdd" name="btnAdd">Submit</button>
                                                </div>
                                                <div class="col-sm-2">
                                                    <a href="index" type="button" class="btn btn-block btn-default btn-flat" id="btnCancel">Cancel</a>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    
                                </div>

                            </div>
                            <!-- end of ACCESS LEVEL -->
                        
                        <?php echo form_close();?>
                        <!-- end of ACCESS LEVEL FORM -->
                        
                    </div>
                    
                </div>
            
            </section>
            <!-- end of MAIN CONTENT -->