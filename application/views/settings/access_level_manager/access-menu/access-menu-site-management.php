<div class="box box-mini box-default box-solid collapsed-box">
    <div class="box-header">
        <h4 data-widget="collapse">Site Management</h4>
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-plus"></i></button>
            <!--<button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>-->
        </div>
    </div>
    <div class="box-body">
        <table class="table table-hover table-stripe table-bordered table-modification">
            <tbody>
                <tr>
                    <td colspan="2"><label><input type="checkbox" class="checkboxTitle"> Announcements</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td colspan="2"><label><input type="checkbox" class="checkboxTitle"> Job Alert</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td colspan="2"><label><input type="checkbox" class="checkboxTitle"> Job Openings</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td colspan="4"><label><input type="checkbox" class="checkboxTitle"> Feedback</label></td>
                </tr>
                <tr>
                    <td colspan="2"><label><input type="checkbox" class="checkboxTitle"> Applicant Advisory</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td colspan="4"><label><input type="checkbox" class="checkboxTitle"> Activity Chart</label></td>
                </tr>
                <tr>
                    <td colspan="4"><label><input type="checkbox" class="checkboxTitle"> Attendance Monitor</label></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>