<div class="box box-mini box-default box-solid collapsed-box">
    <div class="box-header">
        <h4 data-widget="collapse">Medical</h4>
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-plus"></i></button>
            <!--<button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>-->
        </div>
    </div>
    <div class="box-body">
        <table class="table table-hover table-stripe table-bordered table-modification">
            <tbody>
                <tr>
                    <td colspan="2"><label><input type="checkbox" class="checkboxTitle"> Follow Up</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td colspan="2"><label><input type="checkbox" class="checkboxTitle"> Activity</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> Medical Form</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> For Medical - MED SEC</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> Go Medical</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> Pending Medical</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> Medical Monitoring Chart</label></td>
                </tr>
                <tr>
                    <td colspan="2"><label><input type="checkbox" class="checkboxTitle"> Assignment</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td colspan="4"><label><input type="checkbox" class="checkboxTitle"> Communications</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> Communication Reference</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> Compose</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> SMS Inbox</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> SMS Inbox Unresolved</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> SMS Sent</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> SMS Report</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> SMS Mass Sending</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle">  SMS Templates</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> Inbox Transfer</label></td>
                </tr>
                <tr>
                    <td colspan="2"><label><input type="checkbox" class="checkboxTitle"> Reports</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> Medical Report</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> Medical Per Clinic Report</label></td>
                </tr>
                <tr>
                    <td colspan="2"><label><input type="checkbox" class="checkboxTitle"> Settings</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> Clinic Manager</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>