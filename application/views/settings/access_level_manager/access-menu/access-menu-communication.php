<div class="box box-mini box-default box-solid collapsed-box">
    <div class="box-header">
        <h4 data-widget="collapse">Communication</h4>
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-plus"></i></button>
            <!--<button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>-->
        </div>
    </div>
    <div class="box-body">
        <table class="table table-hover table-stripe table-bordered table-modification">
            <tbody>
                <tr>
                    <td colspan="2"><label><input type="checkbox" class="checkboxTitle"> Communication Reference</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td colspan="4"><label><input type="checkbox" class="checkboxTitle"> Compose</label></td>
                </tr>
                <tr>
                    <td colspan="4"><label><input type="checkbox" class="checkboxTitle"> SMS Inbox</label></td>
                </tr>
                <tr>
                    <td colspan="4"><label><input type="checkbox" class="checkboxTitle"> SMS Inbox Unresolve</label></td>
                </tr>
                <tr>
                    <td colspan="4"><label><input type="checkbox" class="checkboxTitle"> SMS Sent</label></td>
                </tr>
                <tr>
                    <td colspan="4"><label><input type="checkbox" class="checkboxTitle"> SMS Report</label></td>
                </tr>
                <tr>
                    <td colspan="4"><label><input type="checkbox" class="checkboxTitle"> SMS Mass Sending</label></td>
                </tr>
                <tr>
                    <td colspan="2"><label><input type="checkbox" class="checkboxTitle"> SMS Templates</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td colspan="4"><label><input type="checkbox" class="checkboxTitle"> Inbox Transfer</label></td>
                </tr>
                <tr>
                    <td colspan="4"><label><input type="checkbox" class="checkboxTitle"> Compose</label></td>
                </tr>
                <tr>
                    <td colspan="4"><label><input type="checkbox" class="checkboxTitle"> Inbox - SMS</label></td>
                </tr>
                <tr>
                    <td colspan="4"><label><input type="checkbox" class="checkboxTitle"> Inbox - Online Message</label></td>
                </tr>
                <tr>
                    <td colspan="4"><label><input type="checkbox" class="checkboxTitle"> Outbox - SMS</label></td>
                </tr>
                <tr>
                    <td colspan="4"><label><input type="checkbox" class="checkboxTitle"> Outbox - Online Message</label></td>
                </tr>
                <tr>
                    <td colspan="4"><label><input type="checkbox" class="checkboxTitle"> General Inbox - SMS</label></td>
                </tr>
                <tr>
                    <td colspan="4"><label><input type="checkbox" class="checkboxTitle"> General Inbox - Online Message</label></td>
                </tr>
                <tr>
                    <td colspan="4"><label><input type="checkbox" class="checkboxTitle"> General Outbox - SMS</label></td>
                </tr>
                <tr>
                    <td colspan="4"><label><input type="checkbox" class="checkboxTitle"> General Outbox - Online Message</label></td>
                </tr>
                <tr>
                    <td colspan="4"><label><input type="checkbox" class="checkboxTitle"> Reports</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> Inbox Summary - SMS</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> Inbox Summary - Online Message</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> Outbox Summary Per Telco</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> Undelivered SMS</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> Resolved Messages Monitoring</label></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>