<div class="box box-mini box-default box-solid collapsed-box">
    <div class="box-header">
        <h4 data-widget="collapse">Processing</h4>
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-plus"></i></button>
            <!--<button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>-->
        </div>
    </div>
    <div class="box-body">
        <table class="table table-hover table-stripe table-bordered table-modification">
            <tbody>
                <tr>
                    <td colspan="2"><label><input type="checkbox" class="checkboxTitle"> Follow Up</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td colspan="2"><label><input type="checkbox" class="checkboxTitle"> Activity</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> JO Request</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> Fit to Work NCTD</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> For Endorsement</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> JO Allocation*</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> VISA Allocation</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> OEC Request</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> VISA Verification</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> Visa Trasmittal - NONKSA</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> Contract Preparation</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle">  PDOS Cert for Printing</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> Booking Request LPT</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> Booking Request PTA</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> 201 File Request</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> Documents for Authentication</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td colspan="4"><label><input type="checkbox" class="checkboxTitle"> Communications</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> Communication Reference</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> Compose</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> SMS Inbox</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> SMS Inbox Unresolved</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> SMS Sent</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> SMS Report</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> SMS Mass Sending</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> SMS Templates</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> Inbox Transfer</label></td>
                </tr>
                <tr>
                    <td colspan="2"><label><input type="checkbox" class="checkboxTitle"> Reports</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle">  Expiring/Expired OEC</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> Expiring/Expired VISA</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> Expiring/Expired Block VISA*</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> Expiring/Expired Accreditation*</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> VISA Report</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> POEA Job Order Report*</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> Job Order List</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> VISA List</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> JO Request for Sending to POEA</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> Sent to POEA - Awaiting Approval</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> Over All Block VISA Master List - Valid</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> Over All Block VISA Master List - Expired</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> By Principal Report</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> By VISA Number Report</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> Job Order Detailed Report</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> Job Order Summary Report</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> Contract Preparation Report</label></td>
                </tr>
                <tr>
                    <td colspan="2"><label><input type="checkbox" class="checkboxTitle"> Settings</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle">  VISA Manager</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle">  POEA Manager</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle">  LPT Manager</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle">  PTA Manager</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>