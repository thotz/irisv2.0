<div class="box box-mini box-default box-solid collapsed-box">
    <div class="box-header">
        <h4 data-widget="collapse">Reception</h4>
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-plus"></i></button>
            <!--<button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>-->
        </div>
    </div>
    <div class="box-body">
        <table class="table table-hover table-stripe table-bordered table-modification">
            <tbody>
                <tr>
                    <td colspan="4"><label><input type="checkbox"> Follow-up</label></td>
                </tr>
                <tr>
                    <td colspan="4"><label><input type="checkbox" class="checkboxTitle"> Activity</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> New Applicant</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> For Encoding - With Line-up</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> For Encoding - Walk-in</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle">  For Encoding - Online</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> Photoshoot</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> CV Tracker</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> Applicant Pager *</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> Distribute - Walk-in</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> Distribute to Employer</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> For 201 Scanning - Deployed</label></td>
                </tr>
                <tr>
                    <td colspan="4"><label><input type="checkbox" class="checkboxTitle"> Setting</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> City Manager</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> Source Manager</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td colspan="4"><label><input type="checkbox" class="checkboxTitle"> Assignment</label></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>