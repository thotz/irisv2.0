<div class="box box-mini box-default box-solid collapsed-box">
    <div class="box-header">
        <h4 data-widget="collapse">Operations</h4>
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-plus"></i></button>
            <!--<button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>-->
        </div>
    </div>
    <div class="box-body">
        <table class="table table-hover table-stripe table-bordered table-modification">
            <tbody>
                <tr>
                    <td colspan="2"><label><input type="checkbox" class="checkboxTitle"> Communication Reference</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td colspan="2"><label><input type="checkbox" class="checkboxTitle"> Activity</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> Documentation</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> Status Changer</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> Pre-Employment Orientation</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> Employment Offer Encoding *</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> Document Library</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> MRREF Reconcilliation*</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> Status Monitoring</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> PLDB Monitoring</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> Medical Monitoring Chart</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> For Medical - OPTN</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> Pending Medical - Final Status*</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> For Endorsement CTD - OPTN</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> Fit to Work NCTD - OPTN</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> Fit to Work Non-KSA*</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> For VISA Doc Sending Non-KSA*</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> Awaiting VISA Non-KSA</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> Awaiting VISA Non-KSA Final Status*</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> VISA Recieved Non-KSA</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> VISA Allocation Denied</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> RFP Denied</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> For Booking Request</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> Booking Request LPT</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> Booking Request PTA</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> Notification of Deployment*</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> Deployed for Updating</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> Deployment Changer</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> MRREF for Closure*</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> Authentication</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> Pictures Order</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> Interview Report</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> Employment Offer*</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> Excess Databank</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> For Medical - Final Status*</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> Pending Medical - OPTN</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> Expiring/Expired Documents*</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> Finalization of Status*</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td colspan="2"><label><input type="checkbox" class="checkboxTitle"> Assignment</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>



                <tr>
                    <td colspan="2"><label><input type="checkbox" class="checkboxTitle"> Communications</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> Communication Reference</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> Compose</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> SMS Inbox</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> SMS Inbox Unresolved</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> SMS Sent</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> SMS Report</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> SMS Mass Sending</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> SMS Templates</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td colspan="2"><label><input type="checkbox" class="checkboxTitle"> Settings</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td colspan="2"><label><input type="checkbox" class="checkboxTitle"> Assignment</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> MR Manager - Operation</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> Recruitment Fee Guide</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> Restriction of Accounts</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> Project Distribution</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> Clinic Manager</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> Travel Agent Manager</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> Airline Manager</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> Doc Type Manager</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td colspan="2"><label><input type="checkbox" class="checkboxTitle"> MR Manager - Recruitment</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td colspan="4"><label><input type="checkbox" class="checkboxTitle"> Recruitment</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> Assessment Form</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> Line Up/Mobilization</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td colspan="4"><label><input type="checkbox" class="checkboxTitle"> CV Sending Transmittal</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> Mailed CV's</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> Databank CV's</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> Interview Manager</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> Client Interview</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> Extraction of PS/FM</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> Final Line Up</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td colspan="4"><label><input type="checkbox" class="checkboxTitle"> Operations</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> Authentication</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> Medical Form</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> Applicant Reservation</label></td>
                </tr>
                <tr>
                    <td colspan="4"><label><input type="checkbox" class="checkboxTitle"> Processing</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> POEA Job Order Manager</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> Visa Manager</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> Visa Verification</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> Ticketing (LPT) Manager</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> Ticketing (PTA) Manager</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> Transmittal for Qatar</label></td>
                </tr>
                <tr>
                    <td colspan="4"><label><input type="checkbox" class="checkboxTitle"> Documentation</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td><label><input type="checkbox" class="checkboxTitle"> Assignment Box</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxModify"> Modify</label></td>
                    <td width="15%" class="text-right"><label><input type="checkbox" class="checkboxDelete"> Delete</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> Line up/Mobilization</label></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td colspan="3"><label><input type="checkbox" class="checkboxTitle"> Overview</label></td>
                </tr>

            </tbody>
        </table>
    </div>
</div>