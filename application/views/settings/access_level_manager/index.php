<?php
	$error = $this->session->flashdata('del_error');
	$msg = $this->session->flashdata('del_msg');
?>
            <!-- MAIN CONTENT -->
            <section class="content">
            
                <div class="row">
                    <div class="col-md-12">
            
                        <!-- ACCESS LEVEL -->
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">Access Level</h3>
                                <a href="#" class="btn btn-success btn-sm btn-flat btn-export-excel pull-right">Export to Excel</a>
                                <a href="<?=base_url()?>settings/access_level_manager/add" class="btn btn-primary btn-sm btn-flat btn-add-new pull-right"><i class="fa fa-plus"></i> Add New Access</a>
                            </div>
                            <div class="box-body">
<?php if($msg != ''):?>
	<?php if($error == TRUE):?>
                                    <!-- ALERT MESSAGE [ERROR] -->
                                    <div class="alert alert-danger alert-dismissabl">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>
                                        <i class="icon fa fa-ban"></i> <strong>ERROR!</strong> Unabled to delete. <?php echo $msg;?>
                                    </div>
	<?php else :?>
                                    <!-- ALERT MESSAGE [SUCCESS] -->
                                    <div class="alert alert-success alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>
                                        <i class="icon fa fa-check"></i> <strong>SUCCESS!</strong> Access is successfully deleted.
                                    </div>
	<?php endif;?>
<?php endif;?>

								<table id="tableAccessMenu" class="table table-hover table-bordered table-stripe table-no-margin">
								    <thead>
								        <tr>
								            <th width="5%">#</th>
								            <th width="50%">Access Name</th>
								            <th width="30%">Code</th>
								            <th class="text-right" width="15%"></th>
								        </tr>
								    </thead>
								    <tbody>
								    <?php if (isset($all_access)):?>
								    	<?php foreach ($all_access as $acc_dtl):?>
									        <tr>
									            <td><?php echo @$counter += 1?>.</td>
									            <td><?php echo $acc_dtl['name']?></td>
									            <td><?php echo $acc_dtl['codename']?></td>
									            <td class="text-right">
									                <div class="btn-group">
									                    <!--<button type="button" class="btn btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Modify"><i class="fa fa-pencil"></i></button> -->
									                    <a href="<?=base_url();?>settings/access_level_manager/edit/<?php echo $acc_dtl['access_id'];?>" class="btn btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Modify"><i class="fa fa-pencil"></i></a>
									                    <!-- <button type="button" class="btn btn-danger btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash"></i></button> -->
									                    <a href="<?=base_url();?>settings/access_level_manager/delete/<?php echo $acc_dtl['access_id'];?>" class="btn btn-danger btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Delete" onclick="if(confirm('Are you sure you want to delete this access?')){return true;}else{return false;}"><i class="fa fa-trash"></i></a>
									                </div>
									            </td>
									        </tr>
								    	<?php endforeach;?>
								    <?php endif;?>
									</tbody>
								</table>
                            </div>
                        </div>
                        <!-- end of ACCESS LEVEL -->
                        
                    </div>
                </div>

            </section>
            <!-- end of MAIN CONTENT -->