<?php
	if($this->session->flashdata('add_acc_success') != ''){
		$default_msg = $this->session->flashdata('add_acc_success');
		$msg = TRUE;
	}
?>
		<!-- MAIN CONTENT -->
		<section class="content">

			<!-- ACCESS LEVEL FORM -->
			<form name="edit_access" action="<?=base_url();?>settings/access_level_manager/edit/<?php echo $all_access[$access_id]['access_id']; ?>" method="POST" class="form-horizontal">

				<!-- EDIT ACCESS INFO -->
                <div class="row">
                    <div class="col-md-12">

                            <!-- ACCESS LEVEL -->
                            <div class="box">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Edit Access Level</h3>
                                </div>
                                <div class="box-body">
<?php if($msg != ""):?>
	<?php if ($error):?>
                                    <!-- ALERT MESSAGE [ERROR] -->
                                    <div class="alert alert-danger alert-dismissabl">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>
                                        <i class="icon fa fa-ban"></i> <strong>ERROR!</strong> <?php echo $msg;?>
                                    </div>
	<?php else:?>
                                    <!-- ALERT MESSAGE [SUCCESS] -->
                                    <div class="alert alert-success alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>
                                        <i class="icon fa fa-check"></i> <strong>SUCCESS!</strong> <?php echo $default_msg;?>
                                    </div>
	<?php endif;?>
<?php endif;?>
                                    <div class="row">
                                        <div class="col-md-10 col-md-offset-1">

                                            <div class="form-group">
                                                <label for="inputAccessName" class="col-sm-2 control-label">Access Name<span class="required">*</span>:</label>
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control" id="inputAccessName" name="inputAccessName" placeholder="Access Name" value="<?php echo set_value('inputAccessName', @$all_access[$access_id]['name']);?>">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="inputCodeName" class="col-sm-2 control-label">Code Name<span class="required">*</span>:</label>
                                                <div class="col-sm-3">
                                                    <input type="text" class="form-control" id="inputCodeName" name="inputCodeName" placeholder="Code Name" value="<?php echo set_value('inputCodeName', @$all_access[$access_id]['codename']);?>">
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                </div>

                            </div>
                            <!-- end of ACCESS LEVEL -->
                        
                    </div>
                </div>
				<!-- end of EDIT ACCESS INFO -->


				<div class="row">
                    <div class="col-md-12">
                        
                        <!-- MODIFY MENU ACCESS -->
                        <div class="box box-collapsed">
                            <div class="box-header with-border">
                                <h3 class="box-title">Modify Access Menu for <strong><?php echo strtoupper($all_access[$access_id]['name'])?></strong></h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                                    <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                                </div>
                            </div>
                            <div class="box-body">
                            
                                <div class="row">
                                    <div class="col-md-12">
								    	<?php if(isset($menu_all)):?>
								    		<!-- MAIN MENU -->
			                                <?php foreach ($menu_all as $main_dtl):?>
												<div class="box box-mini box-default box-solid collapsed-box">
												    <div class="box-header">
												        <h4 data-widget="collapse"><?php echo str_ireplace($breaks, " ", $main_dtl['desc'])?></h4>
												        <div class="box-tools pull-right">
												            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-plus"></i></button>
												            <!--<button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>-->
												        </div>
												    </div>
												    <div class="box-body">
												    	<table class="table table-hover table-stripe table-bordered table-modification">
            												<tbody>
														    <?php if(isset($main_dtl['sub'])):?>
														    	<!-- SUB MENU 1 -->
														    	<?php foreach ($main_dtl['sub'] as $sub_dtl1):?>
													                <tr>
													                    <td colspan="4">
													                    	<label>
													                    		<?php echo form_checkbox('access_menu['.$sub_dtl1['menu_id'].'][]', $sub_dtl1['menu_id'], set_checkbox('access_menu['.$sub_dtl1['menu_id'].']', $sub_dtl1['menu_id'], in_array($all_access[$access_id]['access_id'], explode(",",strip_quotes($sub_dtl1['access'])))), 'class="checkboxTitle" id="cb'.$sub_dtl1['menu_id'].'" menuinfo="'.$access_id.'|'.$sub_dtl1['menu_id'].'|'.urlencode($sub_dtl1['access']).'"')?>
													                    		<?php echo str_ireplace($breaks, " ", $sub_dtl1['desc']);?>
												                    		</label>
												                    	</td>
													                </tr>
													                <?php if (isset($sub_dtl1['sub'])):?>
													                	<!-- SUB MENU 2 -->
														                <?php foreach ($sub_dtl1['sub'] as $sub_dtl2):?>
															                <tr>
															                    <td width="5%"></td>
															                    <td>
															                    	<label>
															                    		<?php echo form_checkbox('access_menu['.$sub_dtl2['menu_id'].'][]', $sub_dtl2['menu_id'], set_checkbox('access_menu['.$sub_dtl2['menu_id'].']', $sub_dtl2['menu_id'], in_array($all_access[$access_id]['access_id'], explode(",",strip_quotes($sub_dtl2['access'])))), 'class="checkboxTitle SubMenu" id="cb'.$sub_dtl2['menu_id'].'" parentid="cb'.$sub_dtl1['menu_id'].'" menuinfo="'.$access_id.'|'.$sub_dtl2['menu_id'].'|'.urlencode($sub_dtl2['access']).'"')?>
															                    		<?php echo str_ireplace($breaks, " ", $sub_dtl2['desc'])?>
														                    		</label>
													                    		</td>
															                    <td width="15%" class="text-right">
															                    	<?php if ($sub_dtl2['act']==1):?>
															                    		<label><?php echo form_checkbox('access_menu['.$sub_dtl2['menu_id'].'][]', 'mod', set_checkbox('access_menu['.$sub_dtl2['menu_id'].']', 'mod', in_array($all_access[$access_id]['access_id'], explode(",",strip_quotes($sub_dtl2['access_act'])))), 'class="checkboxModify" parentid="cb'.$sub_dtl2['menu_id'].'" menuinfo="'.$access_id.'|'.$sub_dtl2['menu_id'].'|'.urlencode($sub_dtl2['access_act']).'"')?> Modify</label>
															                    	<?php endif;?>
														                    	</td>
															                    <td width="15%" class="text-right">
															                    	<?php if ($sub_dtl2['del']==1):?>
															                    		<label><?php echo form_checkbox('access_menu['.$sub_dtl2['menu_id'].'][]', 'del', set_checkbox('access_menu['.$sub_dtl2['menu_id'].']', 'del', in_array($all_access[$access_id]['access_id'], explode(",",strip_quotes($sub_dtl2['access_del'])))), 'class="checkboxDelete" parentid="cb'.$sub_dtl2['menu_id'].'" menuinfo="'.$access_id.'|'.$sub_dtl2['menu_id'].'|'.urlencode($sub_dtl2['access_del']).'"')?> Delete</label>
															                    	<?php endif;?>
														                    	</td>
															                </tr>
														                <?php endforeach;?>
													                <?php endif;?>
														    	<?php endforeach;?>
														    <?php endif;?>
															</tbody>
												        </table>
													</div>
												</div>
			                                <?php endforeach;?>
		                                <?php endif;?>
                                    </div>
                                </div>
                            </div>
                            
                            	<!-- SAVE/CANCEL -->
								<div class="box-footer">
                                    <div class="row">
                                        <div class="col-md-10 col-md-offset-1">

                                            <div class="form-group">
                                                <div class="col-sm-2 col-sm-offset-2">
                                                    <button type="submit" class="btn btn-block btn-primary btn-flat" id="btnEdit" name="btnEdit">Save</button>
                                                </div>
                                                <div class="col-sm-2">
                                                    <a href="<?=base_url()?>settings/access_level_manager" type="button" class="btn btn-block btn-default btn-flat" id="btnCancel">Cancel</a>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                <!-- END SAVE/CANCEL -->
                                
                        </div>
                        <!-- end of MODIFY MENU ACCESS -->
                        
                    </div>
                </div>
			</form>
			<!-- end of ACCESS LEVEL FORM -->

		</section>
		<!-- end of MAIN CONTENT -->