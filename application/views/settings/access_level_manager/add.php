            <!-- MAIN CONTENT -->
            <section class="content">

				<!-- ACCESS LEVEL FORM -->
				<?php echo form_open('settings/access_level_manager/add', 'class="form-horizontal"');?>

                <div class="row">
                    <div class="col-md-12">

                            <!-- ACCESS LEVEL -->
                            <div class="box">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Add Access Level</h3>
                                </div>
                                <div class="box-body">

<?php if($msg != ""):?>
	<?php if ($error):?>
                                    <!-- ALERT MESSAGE [ERROR] -->
                                    <div class="alert alert-danger alert-dismissabl">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>
                                        <i class="icon fa fa-ban"></i> <strong>ERROR!</strong> <?php echo $msg;?>
                                    </div>
	<?php else:?>
                                    <!-- ALERT MESSAGE [SUCCESS] -->
                                    <div class="alert alert-success alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>
                                        <i class="icon fa fa-check"></i> <strong>SUCCESS!</strong> Access is successfully added.
                                    </div>
	<?php endif;?>
<?php endif;?>


                                    <div class="row">
                                        <div class="col-md-10 col-md-offset-1">

                                            <div class="form-group">
                                                <label for="inputAccessName" class="col-sm-2 control-label">Access Name<span class="required">*</span>:</label>
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control" id="inputAccessName" name="inputAccessName" placeholder="Access Name" value="<?php echo set_value('inputAccessName');?>">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="inputCodeName" class="col-sm-2 control-label">Code Name<span class="required">*</span>:</label>
                                                <div class="col-sm-3">
                                                    <input type="text" class="form-control" id="inputCodeName" name="inputCodeName" placeholder="Code Name" value="<?php echo set_value('inputCodeName');?>">
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                
								<!-- SAVE/CANCEL -->
								<div class="box-footer">
                                    <div class="row">
                                        <div class="col-md-10 col-md-offset-1">

                                            <div class="form-group">
                                                <div class="col-sm-2 col-sm-offset-2">
                                                    <button type="submit" class="btn btn-block btn-primary btn-flat" id="btnAdd" name="btnAdd">Save</button>
                                                </div>
                                                <div class="col-sm-2">
                                                    <a href="<?=base_url()?>settings/access_level_manager" type="button" class="btn btn-block btn-default btn-flat" id="btnCancel">Cancel</a>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                <!-- END SAVE/CANCEL -->
                                
                            </div>
                            <!-- end of ACCESS LEVEL -->

                        
                    </div>
                </div>
                
				<?php echo form_close();?>
				<!-- end of ACCESS LEVEL FORM -->
			
            </section>
            <!-- end of MAIN CONTENT -->