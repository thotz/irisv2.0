<table id="tableAccessMenu" class="table table-hover table-bordered table-stripe table-no-margin">
    <thead>
        <tr>
            <th width="5%">#</th>
            <th width="50%">Access Name</th>
            <th width="30%">Code</th>
            <th class="text-right" width="15%"></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>1.</td>
            <td>ADMINISTRATOR</td>
            <td>ADMIN</td>
            <td class="text-right">
                <div class="btn-group">
                    <button type="button" class="btn btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Modify"><i class="fa fa-pencil"></i></button>
                    <button type="button" class="btn btn-danger btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash"></i></button>
                </div>
            </td>
        </tr>
        <tr>
            <td>2.</td>
            <td>CUSTOMER SERVICE REPRESENTATIVE</td>
            <td>CSR</td>
            <td class="text-right">
                <div class="btn-group">
                    <button type="button" class="btn btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Modify"><i class="fa fa-pencil"></i></button>
                    <button type="button" class="btn btn-danger btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash"></i></button>
                </div>
            </td>
        </tr>
        <tr>
            <td>3.</td>
            <td>LEGAL</td>
            <td>LEGAL</td>
            <td class="text-right">
                <div class="btn-group">
                    <button type="button" class="btn btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Modify"><i class="fa fa-pencil"></i></button>
                    <button type="button" class="btn btn-danger btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash"></i></button>
                </div>
            </td>
        </tr>
        <tr>
            <td>4.</td>
            <td>OPERATIONS</td>
            <td>OPTN</td>
            <td class="text-right">
                <div class="btn-group">
                    <button type="button" class="btn btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Modify"><i class="fa fa-pencil"></i></button>
                    <button type="button" class="btn btn-danger btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash"></i></button>
                </div>
            </td>
        </tr>
        <tr>
            <td>5.</td>
            <td>OPERATIONS &amp; RECRUIT</td>
            <td>BRCH</td>
            <td class="text-right">
                <div class="btn-group">
                    <button type="button" class="btn btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Modify"><i class="fa fa-pencil"></i></button>
                    <button type="button" class="btn btn-danger btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash"></i></button>
                </div>
            </td>
        </tr>
        <tr>
            <td>6.</td>
            <td>OPERATIONS, RECRUIT &amp; CUSTOMER SERVICE REPRESENTATIVE</td>
            <td>ORC</td>
            <td class="text-right">
                <div class="btn-group">
                    <button type="button" class="btn btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Modify"><i class="fa fa-pencil"></i></button>
                    <button type="button" class="btn btn-danger btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash"></i></button>
                </div>
            </td>
        </tr>
        <tr>
            <td>7.</td>
            <td>PROCESSING</td>
            <td>PROCESSING</td>
            <td class="text-right">
                <div class="btn-group">
                    <button type="button" class="btn btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Modify"><i class="fa fa-pencil"></i></button>
                    <button type="button" class="btn btn-danger btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash"></i></button>
                </div>
            </td>
        </tr>
        <tr>
            <td>8.</td>
            <td>RA</td>
            <td>RA</td>
            <td class="text-right">
                <div class="btn-group">
                    <button type="button" class="btn btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Modify"><i class="fa fa-pencil"></i></button>
                    <button type="button" class="btn btn-danger btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash"></i></button>
                </div>
            </td>
        </tr>
        <tr>
            <td>9.</td>
            <td>RECEP-CLIENT ASST</td>
            <td>RA2</td>
            <td class="text-right">
                <div class="btn-group">
                    <button type="button" class="btn btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Modify"><i class="fa fa-pencil"></i></button>
                    <button type="button" class="btn btn-danger btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash"></i></button>
                </div>
            </td>
        </tr>
        <tr>
            <td>10.</td>
            <td>RECEPTION-HO</td>
            <td>RECEP</td>
            <td class="text-right">
                <div class="btn-group">
                    <button type="button" class="btn btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Modify"><i class="fa fa-pencil"></i></button>
                    <button type="button" class="btn btn-danger btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash"></i></button>
                </div>
            </td>
        </tr>
        <tr>
            <td>11.</td>
            <td>RECRUITMENT</td>
            <td>RECT</td>
            <td class="text-right">
                <div class="btn-group">
                    <button type="button" class="btn btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Modify"><i class="fa fa-pencil"></i></button>
                    <button type="button" class="btn btn-danger btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash"></i></button>
                </div>
            </td>
        </tr>
        <tr>
            <td>12.</td>
            <td>RM/GH</td>
            <td>RM</td>
            <td class="text-right">
                <div class="btn-group">
                    <button type="button" class="btn btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Modify"><i class="fa fa-pencil"></i></button>
                    <button type="button" class="btn btn-danger btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash"></i></button>
                </div>
            </td>
        </tr>
        <tr>
            <td>13.</td>
            <td>RSO</td>
            <td>RSO</td>
            <td class="text-right">
                <div class="btn-group">
                    <button type="button" class="btn btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Modify"><i class="fa fa-pencil"></i></button>
                    <button type="button" class="btn btn-danger btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash"></i></button>
                </div>
            </td>
        </tr>
        <tr>
            <td>14.</td>
            <td>RSR-TRAINEE</td>
            <td>RSR</td>
            <td class="text-right">
                <div class="btn-group">
                    <button type="button" class="btn btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Modify"><i class="fa fa-pencil"></i></button>
                    <button type="button" class="btn btn-danger btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash"></i></button>
                </div>
            </td>
        </tr>
        <tr>
            <td>15.</td>
            <td>TRAINING CENTER</td>
            <td>EWTC</td>
            <td class="text-right">
                <div class="btn-group">
                    <button type="button" class="btn btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Modify"><i class="fa fa-pencil"></i></button>
                    <button type="button" class="btn btn-danger btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash"></i></button>
                </div>
            </td>
        </tr>
        <tr>
            <td>16.</td>
            <td>USER</td>
            <td>USR</td>
            <td class="text-right">
                <div class="btn-group">
                    <button type="button" class="btn btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Modify"><i class="fa fa-pencil"></i></button>
                    <button type="button" class="btn btn-danger btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash"></i></button>
                </div>
            </td>
        </tr>
    </tbody>
</table>