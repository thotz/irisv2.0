           <!-- MAIN CONTENT -->
            <section class="content">
            
                <div class="row">
                    <div class="col-md-12">
            
                        <!-- Add User Box -->
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">List of Applicant for Photo Shoot</h3>
                            </div>
                            <div class="box-body">
                                
                                <table id="tablePhotoshoot" class="table table-hover table-stripe table-bordered table-no-margin">
                                    <thead>
                                        <tr>
                                            <th>Applicant ID</th>
                                            <th>Name</th>
                                            <th>Ref No.</th>
                                            <th>Status</th>
                                            <th>Purpose</th>
                                            <th>Branch</th>
                                            <th>Method of Application</th>
                                            <th>Type</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php if(isset($ps_data)):?>
                                    	<?php foreach ($ps_data as $val):?>
                                    		<tr>
	                                            <td><?php echo $val['applicant_id']?></td>
	                                            <td><?php echo $val['lname'].", ".$val['fname']." ".$val['mname']?></td>
	                                            <td><?php echo $val['ref_no']?></td>
	                                            <td><?php echo $val['status']?></td>
	                                            <td>
	                                            <?php
		                                            if ($val['status'] == 'OPERATIONS') {
		                                            	echo "Completing Documents";
		                                            } else {
		                                            	if ($val['status'] == 'ACTIVE') {
		                                            		if(!is_null($val['from_date']) && !is_null($val['to_date'])){
		                                            			if(strtotime($val['from_date'])>=strtotime("today") && strtotime($val['to_date'])<=strtotime("today")){
		                                            				echo "For Employer";		                                            				
		                                            			}
		                                            		}else{
		                                            			echo "For CV Evaluation";
		                                            		}
		                                            	} else {
		                                            		echo "For CV Evaluation";
		                                            	}
		                                            }
	                                            ?>
	                                            </td>
	                                            <td><?php echo $val['branch']?></td>
	                                            <td><?php echo $val['cv_source']?></td>
	                                            <td>
	                                            <?php
		                                            if ($val['pictaken'] == '') {
		                                            	echo "no picture";
		                                            } else if ($val['pictaken'] == 'upload') {
		                                            	echo "uploaded";
		                                            } else {
		                                            	echo $val['pictaken'];
		                                            }
	                                            ?>
	                                            </td>
	                                            <td class="text-right">
	                                            	<?php if($val['pictaken'] == 'Allowed'):?>
	                                                	<a class="btn btn-default btn-flat btn-sm btn-switch active" data-toggle="button" aria-pressed="true" autocomplete="off" onclick="ps_update(false,'<?php echo $val['applicant_id'];?>', '<?php echo urlencode($val['picture'])?>');"></a>
	                                                <?php else:?>
	                                                	<a class="btn btn-default btn-flat btn-sm btn-switch" data-toggle="button" aria-pressed="false" autocomplete="off" onclick="ps_update(true, '<?php echo $val['applicant_id'];?>');"></a>
	                                                <?php endif;?>
	                                            </td>
	                                        </tr>
                                    	<?php endforeach;?>
                                    <?php endif;?>
                                    </tbody>
                                </table>
                                
                            </div>
                            
                        </div>
                        <!-- end of Add User Box -->
                        
                    </div>
                </div>

            </section>
