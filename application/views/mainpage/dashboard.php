			<!-- Javascript Code : Line Chart Data -->
			<?=$mylinechart?>

            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>Dashboard<small>Control panel</small></h1>
                <!--<ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li><a href="#">Examples</a></li>
                    <li class="active">Blank page</li>
                </ol>-->
            </section>

            <!-- Main content -->
            <section class="content">
            
                <div class="row">
                
                    <div class="col-md-12">
                        
                        <!-- Source of Application (Line Graph) -->
                        <div class="box box-primary" id="boxSourceOfApplication">
                            <div class="box-header with-border">
                                <h3 class="box-title">Source of Application</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                                    <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                                </div>
                            </div>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-lg-3 col-lg-offset-3 col-md-5 col-md-offset-1 col-sm-6 col-xs-6 applicant-counter">
                                        <div class="counter">
                                            <small>Total No. of Applicants</small><br/><?=$total_app?>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-5 col-sm-6 col-xs-6 principal-counter">
                                        <div class="counter">
                                            <small>Total No. of Principals</small><br/><?=$total_prin?>
                                        </div>
                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-md-12">
                                        <p class="chart-date-range">Applicants: <strong>1<sup>st</sup> <?=$from_month?></strong> to <strong>31<sup>st</sup> <?=$to_month?></strong></p>
                                        <div class="chart">
                                            <canvas id="lineChart" style="height:250px"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer">
                                <p>
                                    <span class="label label-primary">East West Online</span>
                                    <span class="label label-danger">Commercial Online</span>
                                    <span class="label label-success">Direct</span>
                                </p>
                            </div>
                        </div>
                        
                    </div>
                    
                </div>
                
                <div class="row">
                
                    <!-- Applicant Status (Donut Chart) -->
                    <div class="col-md-8">
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">Applicant Status</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                                    <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                                </div>
                            </div>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="box-body chart-responsive">
                                            <div class="chart" id="chartApplicantStatus" style="height: 300px; position: relative;"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 applicant-status-info">
                                        
                                        <div class="row applicant-status-info-list">
                                            <div class="col-xs-8"><i class="fa fa-circle active"></i> Active Applicants</div>
                                            <div class="col-xs-2 counter">59</div>
                                            <div class="col-xs-2 percentage">00%</div>
                                        </div>
                                        <div class="row applicant-status-info-list">
                                            <div class="col-xs-8"><i class="fa fa-circle blacklisted"></i> Black Listed Applicants</div>
                                            <div class="col-xs-2 counter">30</div>
                                            <div class="col-xs-2 percentage">00%</div>
                                        </div>
                                        <div class="row applicant-status-info-list">
                                            <div class="col-xs-8"><i class="fa fa-circle deadfile"></i> Dead File Applicants</div>
                                            <div class="col-xs-2 counter">59</div>
                                            <div class="col-xs-2 percentage">00%</div>
                                        </div>
                                        <div class="row applicant-status-info-list">
                                            <div class="col-xs-8"><i class="fa fa-circle deployed"></i> Deployed Applicants</div>
                                            <div class="col-xs-2 counter">10</div>
                                            <div class="col-xs-2 percentage">00%</div>
                                        </div>
                                        <div class="row applicant-status-info-list">
                                            <div class="col-xs-8"><i class="fa fa-circle excess"></i> Excess Applicants</div>
                                            <div class="col-xs-2 counter">35</div>
                                            <div class="col-xs-2 percentage">00%</div>
                                        </div>
                                        <div class="row applicant-status-info-list">
                                            <div class="col-xs-8"><i class="fa fa-circle onpool"></i> On Pool Applicants</div>
                                            <div class="col-xs-2 counter">15</div>
                                            <div class="col-xs-2 percentage">00%</div>
                                        </div>
                                        <div class="row applicant-status-info-list">
                                            <div class="col-xs-8"><i class="fa fa-circle operation"></i> Operation Applicants</div>
                                            <div class="col-xs-2 counter">30</div>
                                            <div class="col-xs-2 percentage">00%</div>
                                        </div>
                                        <div class="row applicant-status-info-list">
                                            <div class="col-xs-8"><i class="fa fa-circle pooling"></i> Pooling Applicants</div>
                                            <div class="col-xs-2 counter">46</div>
                                            <div class="col-xs-2 percentage">00%</div>
                                        </div>
                                        <div class="row  applicant-status-info-list">
                                            <div class="col-xs-8"><i class="fa fa-circle reserved"></i> Reserved Applicants</div>
                                            <div class="col-xs-2 counter">99</div>
                                            <div class="col-xs-2 percentage">00%</div>
                                        </div>
                                        <div class="row  applicant-status-info-list">
                                            <div class="col-xs-8"><i class="fa fa-circle workabroad"></i> Work in Abroad</div>
                                            <div class="col-xs-2 counter">60</div>
                                            <div class="col-xs-2 percentage">00%</div>
                                        </div>
                                    
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.box -->
                    </div>
                
                    <!-- Schedule -->
                    <div class="col-md-4">
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">Schedule</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                                    <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                                </div>
                            </div>
                            <div class="box-body">
                                <h3>Tuesday, <strong>23</strong><sup>rd</sup> of <strong>February</strong>, 2016</h3>
                                <ul class="schedule-list">
                                    <li><a href="#"><strong>NSH-01K15-E1</strong> - EASTWEST CEBU - CBU</a></li>
                                    <li><a href="#"><strong>JPK-01L15-E1 </strong> - EASTWEST MAKATI - MNL</a></li>
                                    <li><a href="#"><strong>ES.MUSAED O&amp;M-01L15-E1</strong> - EASTWEST MAKATI - MNL</a></li>
                                    <li><a href="#"><strong>CCCQ.FR-03J15-E1</strong> - EASTWEST BATANGAS - BTG</a></li>
                                    <li><a href="#"><strong>NSH-01K15-E1</strong> - EASTWEST CEBU - CBU</a></li>
                                    <li><a href="#"><strong>JPK-01L15-E1 </strong> - EASTWEST MAKATI - MNL</a></li>
                                </ul>
                                <input type="button" class="btn btn-block btn-sm btn-flat btn-primary" value="View all Schedule" />
                            </div>
                        </div>
                    </div>
                    
                </div>
                
                <div class="row">
                
                    <!-- Bar Graph -->
                    <div class="col-md-6">
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">Bar Chart</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                                    <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                                </div>
                            </div>
                            <div class="box-body">
                                <div class="box-body chart-responsive">
                                    <div class="chart" id="bar-chart" style="height: 300px;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                
                    <!-- Line Chart -->
                    <div class="col-md-6">
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">Line Chart</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                                    <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                                </div>
                            </div>
                            <div class="box-body">
                                <div class="box-body chart-responsive">
                                    <div class="chart" id="line-chart" style="height: 300px;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
                
                <div class="row hide">
                
                    <div class="col-md-3">
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">col-md-3</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                                    <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                                </div>
                            </div>
                            <div class="box-body">
                                Start creating your amazing application!
                            </div><!-- /.box-body -->
                            <div class="box-footer">
                                Footer
                            </div><!-- /.box-footer-->
                        </div><!-- /.box -->
                    </div>
                
                    <div class="col-md-9">
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">col-md-9</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                                    <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                                </div>
                            </div>
                            <div class="box-body">
                                Start creating your amazing application!
                            </div><!-- /.box-body -->
                            <div class="box-footer">
                                Footer
                            </div><!-- /.box-footer-->
                        </div><!-- /.box -->
                    </div>
                    
                </div>
                
                <div class="row hide">
                
                    <div class="col-md-4">
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">col-md-4</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                                    <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                                </div>
                            </div>
                            <div class="box-body">
                                Start creating your amazing application!
                            </div><!-- /.box-body -->
                            <div class="box-footer">
                                Footer
                            </div><!-- /.box-footer-->
                        </div><!-- /.box -->
                    </div>
                
                    <div class="col-md-4">
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">col-md-4</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                                    <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                                </div>
                            </div>
                            <div class="box-body">
                                Start creating your amazing application!
                            </div><!-- /.box-body -->
                            <div class="box-footer">
                                Footer
                            </div><!-- /.box-footer-->
                        </div><!-- /.box -->
                    </div>
                
                    <div class="col-md-4">
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">col-md-4</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                                    <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                                </div>
                            </div>
                            <div class="box-body">
                                Start creating your amazing application!
                            </div><!-- /.box-body -->
                            <div class="box-footer">
                                Footer
                            </div><!-- /.box-footer-->
                        </div><!-- /.box -->
                    </div>
                    
                </div>
                
                <div class="row hide">
                
                    <div class="col-md-3">
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">col-md-3</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                                    <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                                </div>
                            </div>
                            <div class="box-body">
                                Start creating your amazing application!
                            </div><!-- /.box-body -->
                            <div class="box-footer">
                                Footer
                            </div><!-- /.box-footer-->
                        </div><!-- /.box -->
                    </div>
                
                    <div class="col-md-3">
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">col-md-3</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                                    <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                                </div>
                            </div>
                            <div class="box-body">
                                Start creating your amazing application!
                            </div><!-- /.box-body -->
                            <div class="box-footer">
                                Footer
                            </div><!-- /.box-footer-->
                        </div><!-- /.box -->
                    </div>
                
                    <div class="col-md-3">
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">col-md-3</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                                    <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                                </div>
                            </div>
                            <div class="box-body">
                                Start creating your amazing application!
                            </div><!-- /.box-body -->
                            <div class="box-footer">
                                Footer
                            </div><!-- /.box-footer-->
                        </div><!-- /.box -->
                    </div>
                
                    <div class="col-md-3">
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">col-md-3</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                                    <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                                </div>
                            </div>
                            <div class="box-body">
                                Start creating your amazing application!
                            </div><!-- /.box-body -->
                            <div class="box-footer">
                                Footer
                            </div><!-- /.box-footer-->
                        </div><!-- /.box -->
                    </div>
                    
                </div>
            
            </section>
            
            