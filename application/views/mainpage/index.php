
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>Dashboard<small>Control panel</small></h1>
                <!--<ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li><a href="#">Examples</a></li>
                    <li class="active">Blank page</li>
                </ol>-->
            </section>

            <!-- Main content -->
            <section class="content">
            
                <div class="row">
                
                    <div class="col-md-12">
                        <!-- Default box -->
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">Full</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                                    <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                                </div>
                            </div>
                            <div class="box-body">
                                <?=$sample_full?>
                            </div><!-- /.box-body -->
                            <div class="box-footer">
                                <?php
                                foreach ($arr_test as $val) {
                                	print $val."<br>";
                                	
                                }
                                ?>
                            </div><!-- /.box-footer-->
                        </div><!-- /.box -->
                    </div>
                    
                </div>
                
                <div class="row">
                
                    <div class="col-md-6">
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">col-md-6</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                                    <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                                </div>
                            </div>
                            <div class="box-body">
                                Start creating your amazing application!
                            </div><!-- /.box-body -->
                            <div class="box-footer">
                                Footer
                            </div><!-- /.box-footer-->
                        </div><!-- /.box -->
                    </div>
                
                    <div class="col-md-6">
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">col-md-6</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                                    <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                                </div>
                            </div>
                            <div class="box-body">
                                Start creating your amazing application!
                            </div><!-- /.box-body -->
                            <div class="box-footer">
                                Footer
                            </div><!-- /.box-footer-->
                        </div><!-- /.box -->
                    </div>
                    
                </div>
                
                <div class="row">
                
                    <div class="col-md-3">
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">col-md-3</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                                    <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                                </div>
                            </div>
                            <div class="box-body">
                                Start creating your amazing application!
                            </div><!-- /.box-body -->
                            <div class="box-footer">
                                Footer
                            </div><!-- /.box-footer-->
                        </div><!-- /.box -->
                    </div>
                
                    <div class="col-md-9">
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">col-md-9</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                                    <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                                </div>
                            </div>
                            <div class="box-body">
                                Start creating your amazing application!
                            </div><!-- /.box-body -->
                            <div class="box-footer">
                                Footer
                            </div><!-- /.box-footer-->
                        </div><!-- /.box -->
                    </div>
                    
                </div>
                
                <div class="row">
                
                    <div class="col-md-4">
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">col-md-4</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                                    <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                                </div>
                            </div>
                            <div class="box-body">
                                Start creating your amazing application!
                            </div><!-- /.box-body -->
                            <div class="box-footer">
                                Footer
                            </div><!-- /.box-footer-->
                        </div><!-- /.box -->
                    </div>
                
                    <div class="col-md-4">
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">col-md-4</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                                    <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                                </div>
                            </div>
                            <div class="box-body">
                                Start creating your amazing application!
                            </div><!-- /.box-body -->
                            <div class="box-footer">
                                Footer
                            </div><!-- /.box-footer-->
                        </div><!-- /.box -->
                    </div>
                
                    <div class="col-md-4">
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">col-md-4</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                                    <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                                </div>
                            </div>
                            <div class="box-body">
                                Start creating your amazing application!
                            </div><!-- /.box-body -->
                            <div class="box-footer">
                                Footer
                            </div><!-- /.box-footer-->
                        </div><!-- /.box -->
                    </div>
                    
                </div>
                
                <div class="row">
                
                    <div class="col-md-3">
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">col-md-3</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                                    <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                                </div>
                            </div>
                            <div class="box-body">
                                Start creating your amazing application!
                            </div><!-- /.box-body -->
                            <div class="box-footer">
                                Footer
                            </div><!-- /.box-footer-->
                        </div><!-- /.box -->
                    </div>
                
                    <div class="col-md-3">
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">col-md-3</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                                    <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                                </div>
                            </div>
                            <div class="box-body">
                                Start creating your amazing application!
                            </div><!-- /.box-body -->
                            <div class="box-footer">
                                Footer
                            </div><!-- /.box-footer-->
                        </div><!-- /.box -->
                    </div>
                
                    <div class="col-md-3">
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">col-md-3</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                                    <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                                </div>
                            </div>
                            <div class="box-body">
                                Start creating your amazing application!
                            </div><!-- /.box-body -->
                            <div class="box-footer">
                                Footer
                            </div><!-- /.box-footer-->
                        </div><!-- /.box -->
                    </div>
                
                    <div class="col-md-3">
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">col-md-3</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                                    <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                                </div>
                            </div>
                            <div class="box-body">
                                Start creating your amazing application!
                            </div><!-- /.box-body -->
                            <div class="box-footer">
                                Footer
                            </div><!-- /.box-footer-->
                        </div><!-- /.box -->
                    </div>
                    
                </div>
            
            </section>