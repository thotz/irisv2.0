		        <!-- Login Header -->
		        <div class="login-header">
		            <img src="<?=base_url();?>public/images/iris-header-logo.png" class="iris-logo" alt="IRIS Logo" />
		            Interactive Recruitment Information System
		        </div>
		
		        <div class="login-body">
		        <!-- Login Form -->
		        <div class="login-box">
		
		            <h1 class="text-center">Member's Login</h1>
		
		            <div class="login-box-body">
		
		                <p class="login-box-msg">Please input your username and password.</p>
						<?php if (isset($msg)) echo "<p class=\"login-box-msg text-danger\"> ERROR: ". $msg . "</p>"; ?>
		
		                <form name=""login"" action="login" method="POST">
		
		                    <div class="form-group has-feedback">
		                        <input type="text" class="form-control" placeholder="Username" name="username" tabindex="1">
		                        <span class="fa fa-envelope form-control-feedback"></span>
		                    </div>
		
		                    <div class="form-group has-feedback">
		                        <input type="password" class="form-control" placeholder="Password" name="password" tabindex="2">
		                        <span class="fa fa-lock form-control-feedback"></span>
		                    </div>
		
		                    <div class="row">
		                        <div class="col-xs-4 col-xs-offset-8">
		                            <button type="submit" class="btn btn-primary btn-block btn-flat" name="submit" value="Submit" tabindex="3">Sign In</button>
		                        </div><!-- /.col -->
		                    </div>
		
		                </form>
		
		            </div><!-- /.login-box-body -->
		
		        </div><!-- /.login-box -->
		        </div>
		
		        <!-- Login Footer -->
		        <div class="login-footer text-center">
		            <img src="<?=base_url();?>public/images/iris-footer-logo.png" class="iris-logo" alt="IRIS Logo" /><br />
		            Interactive Recruitment Information System. Version 2.<br />
		            Copyright 2016 East West Placement Center, Inc., All Rights Reserved.
		        </div>