	<?php /* Result Data */
		if((in_array($this->session->userdata['iris_user_access'],array('1')))){
			$show_delete = true;
		}else{
			$show_delete = false;
		}
		
		foreach ($tradetest as $value) {
			?>
                                            
			<li>
				<div class="btn-group pull-right">
					<button type="button" class="btn btn-sm btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Edit" onclick="popup_recruitment_assessment('tradetest','<?=$value["applicant_id"]?>','<?=$value["tradetest_id"]?>')"><i class="fa fa-pencil"></i></button>
					<button type="button" class="btn btn-sm btn-danger btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Delete" onclick="delete_recruitment_assessment('tradetest','<?=$value["applicant_id"]?>','<?=$value["tradetest_id"]?>')"><i class="fa fa-trash"></i></button>
				</div>
				<h3><strong><?=$value['title']?></strong></h3>
				<div class="row">
					<div class="col-md-12">
						<p><i class="fa fa-building"></i> <?=$value['center']?></p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<p><i class="fa fa-calendar"></i> <?=dateformat($value['trade_date'], 'M, d Y')?></p>
					</div>
					<div class="col-md-6">
						<p><i class="ion-ribbon-a"></i> <?=$value['grade']?></p>
					</div>
				</div>
			</li>
	<?php } ?>

