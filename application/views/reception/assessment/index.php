	<script>
		var applicant_id = '<?=$applicant_id?>';
	</script>

            <!-- MAIN CONTENT -->
            <section class="content">
            
                <div class="row">
                    <div class="col-md-12">
            
						<?=$applicant_information?>
                        
                       	<?=$applicant_link_menu;?>
                       	
                       	<?=$my_javascript_data;?>
                        
                        <!-- EDUCATION -->
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title" data-widget="collapse"><i class="fa fa-graduation-cap"></i> Education</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-default btn-xs btn-flat btn-box-add" data-remote="false" onclick="popup_recruitment_assessment('education','<?=$applicant_id?>')"><i class="ion-plus"></i> Add Data</button>
                                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-plus"></i></button>
                                     
                                </div>
                            </div>
                            <div class="box-body">
                                <ul class="profile-info-list" id="assessment_education_view">
                                	Loading please wait ...
                                </ul>
                            </div>
                            <div class="box-footer">
                                <ul class="data-logs" id="datalogs-education"></ul>
                                <span>
                                    <button href="#" class="btn btn-xs btn-default btn-flat show-logs-education" onclick="assessment_show_logs('education');"><small><i class="ion-plus"></i> Show All</small></button>
                                    <button href="#" class="btn btn-xs btn-default btn-flat hide-logs-education" onclick="assessment_hide_logs('education');" style="display:none;"><small><i class="ion-minus"></i> Show Less</small></button>
                                </span>
                            </div>
                        </div>
                        <!-- end of EDUCATION -->
                        
                        <!-- WORK HISTORY -->
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title" data-widget="collapse"><i class="fa fa-briefcase"></i> Work History</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-default btn-xs btn-flat btn-box-add" data-remote="false" onclick="popup_recruitment_assessment('employment','<?=$applicant_id?>')"><i class="ion-plus"></i> Add Data</button>
                                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-plus"></i></button>
                                     
                                </div>
                            </div>
                            <div class="box-body">
                                <ul class="profile-info-list" id="assessment_employment_view">
                                	Loading please wait ...
                                </ul>
                            </div>
                            <div class="box-footer">
                                <ul class="data-logs" id="datalogs-employment"></ul>
                                <span>
                                    <button href="#" class="btn btn-xs btn-default btn-flat show-logs-employment" onclick="assessment_show_logs('employment');"><small><i class="ion-plus"></i> Show All</small></button>
                                    <button href="#" class="btn btn-xs btn-default btn-flat hide-logs-employment" onclick="assessment_hide_logs('employment');" style="display:none;"><small><i class="ion-minus"></i> Show Less</small></button>
                                </span>
                            </div>
                        </div>
                        <!-- end of WORK HISTORY -->
                        
                        <div class="row">
                            <div class="col-md-6">
                                <!-- LICENSES / CERTIFICATIONS -->
                                <div class="box">
                                    <div class="box-header with-border">
                                        <h3 class="box-title" data-widget="collapse"><i class="fa fa-certificate"></i> Licenses &amp; Certifications</h3>
                                        <div class="box-tools pull-right">
                                            <button class="btn btn-default btn-xs btn-flat btn-box-add" data-toggle="false" onclick="popup_recruitment_assessment('licenses','<?=$applicant_id?>')"><i class="ion-plus"></i> Add Data</button>
                                            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                                <i class="fa fa-plus"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="box-body">
                                        <ul class="profile-info-list" id="assessment_licenses_view">
                                        	Loading please wait ...
                                        </ul>
                                    </div>
                                    <div class="box-footer">
		                                <ul class="data-logs" id="datalogs-licenses"></ul>
		                                <span>
		                                    <button href="#" class="btn btn-xs btn-default btn-flat show-logs-licenses" onclick="assessment_show_logs('licenses');"><small><i class="ion-plus"></i> Show All</small></button>
		                                    <button href="#" class="btn btn-xs btn-default btn-flat hide-logs-licenses" onclick="assessment_hide_logs('licenses');" style="display:none;"><small><i class="ion-minus"></i> Show Less</small></button>
		                                </span>
                                    </div>
                                </div>
                                <!-- end of LICENSES / CERTIFICATIONS -->
                            </div>
                            <div class="col-md-6">
                                <!-- TRAINING / SEMINARS -->
                                <div class="box">
                                    <div class="box-header with-border">
                                        <h3 class="box-title" data-widget="collapse"><i class="fa ion-ios-lightbulb"></i> Training and Seminars</h3>
                                        <div class="box-tools pull-right">
                                            <button class="btn btn-default btn-xs btn-flat btn-box-add" data-toggle="false" onclick="popup_recruitment_assessment('training','<?=$applicant_id?>')"><i class="ion-plus"></i> Add Data</button>
                                            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                                <i class="fa fa-plus"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="box-body">
                                        <ul class="profile-info-list" id="assessment_training_view">
                                        	Loading please wait ...
                                        </ul>
                                    </div>
                                    <div class="box-footer">
		                                <ul class="data-logs" id="datalogs-training"></ul>
		                                <span>
		                                    <button href="#" class="btn btn-xs btn-default btn-flat show-logs-training" onclick="assessment_show_logs('training');"><small><i class="ion-plus"></i> Show All</small></button>
		                                    <button href="#" class="btn btn-xs btn-default btn-flat hide-logs-training" onclick="assessment_hide_logs('training');" style="display:none;"><small><i class="ion-minus"></i> Show Less</small></button>
		                                </span>
                                    </div>
                                </div>
                                <!-- end of TRAINING / SEMINARS -->
                            </div>
                        </div>
                        
                        
                        <div class="row">
                            <div class="col-md-6">
                                <!-- TRADE TEST -->
                                <div class="box">
                                    <div class="box-header with-border">
                                        <h3 class="box-title" data-widget="collapse"><i class="fa fa-file-text"></i> Trade Test</h3>
                                        <div class="box-tools pull-right">
                                            <button class="btn btn-default btn-xs btn-flat btn-box-add" data-toggle="false" onclick="popup_recruitment_assessment('tradetest','<?=$applicant_id?>')"><i class="ion-plus"></i> Add Data</button>
                                            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-plus"></i></button>

                                        </div>
                                    </div>
                                    <div class="box-body">
                                        <ul class="profile-info-list" id="assessment_tradetest_view">
                                        	Loading please wait ...
                                        </ul>
                                    </div>
                                    <div class="box-footer">
		                                <ul class="data-logs" id="datalogs-tradetest"></ul>
		                                <span>
		                                    <button href="#" class="btn btn-xs btn-default btn-flat show-logs-tradetest" onclick="assessment_show_logs('tradetest');"><small><i class="ion-plus"></i> Show All</small></button>
		                                    <button href="#" class="btn btn-xs btn-default btn-flat hide-logs-tradetest" onclick="assessment_hide_logs('tradetest');" style="display:none;"><small><i class="ion-minus"></i> Show Less</small></button>
		                                </span>
                                    </div>
                                </div>
                                <!-- end of TRADE TEST -->
                            </div>
                            <div class="col-md-6">
                                <!-- BENEFICIARIES -->
                                <div class="box">
                                    <div class="box-header with-border">
                                        <h3 class="box-title" data-widget="collapse"><i class="fa fa-users"></i> Beneficiaries</h3>
                                        <div class="box-tools pull-right">
                                            <button class="btn btn-default btn-xs btn-flat btn-box-add" data-toggle="false" onclick="popup_recruitment_assessment('beneficiaries','<?=$applicant_id?>')"><i class="ion-plus"></i> Add Data</button>
                                            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-plus"></i></button>

                                        </div>
                                    </div>
                                    <div class="box-body">
                                        <ul class="profile-info-list" id="assessment_beneficiaries_view">
                                        	Loading please wait ...
                                        </ul>
                                    </div>
                                    <div class="box-footer">
		                                <ul class="data-logs" id="datalogs-beneficiaries"></ul>
		                                <span>
		                                    <button href="#" class="btn btn-xs btn-default btn-flat show-logs-beneficiaries" onclick="assessment_show_logs('beneficiaries');"><small><i class="ion-plus"></i> Show All</small></button>
		                                    <button href="#" class="btn btn-xs btn-default btn-flat hide-logs-beneficiaries" onclick="assessment_hide_logs('beneficiaries');" style="display:none;"><small><i class="ion-minus"></i> Show Less</small></button>
		                                </span>
                                    </div>
                                </div>
                                <!-- end of BENEFICIARIES -->
                            </div>
                        </div>
                        
                        
                        <div class="row">
                            <div class="col-md-4">
                                <!-- COMPUTER SKILLS -->
                                <div class="box">
                                    <div class="box-header with-border">
                                        <h3 class="box-title" data-widget="collapse"><i class="fa fa-desktop"></i> Computer Skills</h3>
                                        <div class="box-tools pull-right">
                                            <button class="btn btn-default btn-xs btn-flat btn-box-add" data-toggle="modal" data-target="#modalAddComputerSkill"><i class="ion-plus"></i> Add Data</button>
                                            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-plus"></i></button>

                                        </div>
                                    </div>
                                    <div class="box-body">
                                        <ul class="profile-info-list" id="assessment_computerskills_view">
                                        	Loading please wait ...
                                            <!-- <li>
                                                <div class="btn-group pull-right">
                                                    <button type="button" class="btn btn-sm btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></button>
                                                    <button type="button" class="btn btn-sm btn-danger btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash"></i></button>
                                                </div>
                                                <h3><strong>Advance</strong></h3>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <p><i class="fa fa-desktop"></i> Adobe Photoshop, Adobe Illustrator, Adobe Dreamweaver, Corel Draw, Bracket, Notepad++, Microsoft Office.</p>
                                                    </div>
                                                </div>
                                            </li> -->
                                        </ul>
                                    </div>
                                    <div class="box-footer">
		                                <ul class="data-logs" id="datalogs-computerskills"></ul>
		                                <span>
		                                    <button href="#" class="btn btn-xs btn-default btn-flat show-logs-computerskills" onclick="assessment_show_logs('computerskills');"><small><i class="ion-plus"></i> Show All</small></button>
		                                    <button href="#" class="btn btn-xs btn-default btn-flat hide-logs-computerskills" onclick="assessment_hide_logs('computerskills');" style="display:none;"><small><i class="ion-minus"></i> Show Less</small></button>
		                                </span>
                                    </div>
                                </div>
                                <!-- end of COMPUTER SKILLS -->
                            </div>
                            <div class="col-md-4">
                                <!-- LANGUAGE -->
                                <div class="box">
                                    <div class="box-header with-border">
                                        <h3 class="box-title" data-widget="collapse"><i class="fa fa-comments"></i> Language</h3>
                                        <div class="box-tools pull-right">
                                            <button class="btn btn-default btn-xs btn-flat btn-box-add" data-toggle="modal" data-target="#modalAddLanguage"><i class="ion-plus"></i> Add Data</button>
                                            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-plus"></i></button>

                                        </div>
                                    </div>
                                    <div class="box-body">
                                        <ul class="profile-info-list">
                                            <li>
                                                <div class="row">
                                                    <div class="btn-group pull-right">
                                                        <button type="button" class="btn btn-sm btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></button>
                                                        <button type="button" class="btn btn-sm btn-danger btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash"></i></button>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <p><i class="fa fa-comment"></i> <strong>English</strong> - Spoken, Written and Read.</p>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="box-footer">
                                        <ul class="data-logs" id="datalogsLanguage">
                                            <li><small>Jan 8, 2016 9:39 pm - edit Through Online Application</small></li>
                                            <li><small>Jan 5, 2016 7:19 am - edit Through Online Application</small></li>
                                            <li><small>Dec 24, 2015 3:38 pm - add Through Online Application</small></li>
                                            <li>
                                                <button href="#" class="btn btn-xs btn-default btn-flat show-logs"><small><i class="ion-plus"></i> Show All</small></button>
                                                <button href="#" class="btn btn-xs btn-default btn-flat hide-logs" style="display:none;"><small><i class="ion-minus"></i> Show Less</small></button>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- end of LANGUAGE -->
                        
                            </div>
                            <div class="col-md-4">
                                <!-- DRIVER's LICENSE -->
                                <div class="box">
                                    <div class="box-header with-border">
                                        <h3 class="box-title" data-widget="collapse"><i class="fa fa-car"></i> Driver's License</h3>
                                        <div class="box-tools pull-right">
                                            <button class="btn btn-default btn-xs btn-flat btn-box-add" data-toggle="modal" data-target="#modalAddDriversLicense"><i class="ion-plus"></i> Add Data</button>
                                            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-plus"></i></button>
                                        </div>
                                    </div>
                                    <div class="box-body">
                                        <ul class="profile-info-list">
                                            <li>
                                                <div class="btn-group pull-right">
                                                    <button type="button" class="btn btn-sm btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></button>
                                                    <button type="button" class="btn btn-sm btn-danger btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash"></i></button>
                                                </div>
                                                <h3><strong>Local Driver's License</strong></h3>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <p><i class="fa fa-credit-card-alt"></i> Restriction: 123</p>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="box-footer">
                                        <ul class="data-logs" id="datalogsDriversLicense">
                                            <li><small>Jan 8, 2016 9:39 pm - edit Through Online Application</small></li>
                                            <li><small>Jan 5, 2016 7:19 am - edit Through Online Application</small></li>
                                            <li><small>Dec 24, 2015 3:38 pm - add Through Online Application</small></li>
                                            <li>
                                                <button href="#" class="btn btn-xs btn-default btn-flat show-logs"><small><i class="ion-plus"></i> Show All</small></button>
                                                <button href="#" class="btn btn-xs btn-default btn-flat hide-logs" style="display:none;"><small><i class="ion-minus"></i> Show Less</small></button>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- end of DRIVER's LICENSE -->
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-6">
                                <!-- PREFERRED POSITION -->
                                <div class="box">
                                    <div class="box-header with-border">
                                        <h3 class="box-title" data-widget="collapse"><i class="fa ion-android-star"></i> Preferred Position</h3>
                                        <div class="box-tools pull-right">
                                            <button class="btn btn-default btn-xs btn-flat btn-box-add" data-toggle="false" onclick="popup_recruitment_assessment('prefered','<?=$applicant_id?>')"><i class="ion-plus"></i> Add Data</button>
                                            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-plus"></i></button>
                                        </div>
                                    </div>
                                    <div class="box-body">
                                        <ul class="profile-info-list" id="assessment_prefered_view">
                                        	Loading please wait ...
                                        </ul>
                                    </div>
                                    <div class="box-footer">
		                                <ul class="data-logs" id="datalogs-prefered"></ul>
		                                <span>
		                                    <button href="#" class="btn btn-xs btn-default btn-flat show-logs-prefered" onclick="assessment_show_logs('prefered');"><small><i class="ion-plus"></i> Show All</small></button>
		                                    <button href="#" class="btn btn-xs btn-default btn-flat hide-logs-prefered" onclick="assessment_hide_logs('prefered');" style="display:none;"><small><i class="ion-minus"></i> Show Less</small></button>
		                                </span>
                                    </div>
                                </div>
                                <!-- end of PREFERRED POSITION -->
                            </div>
                            <div class="col-md-6">
                                <!-- JOBS OPENING APPLIED -->
                                <div class="box">
                                    <div class="box-header with-border">
                                        <h3 class="box-title" data-widget="collapse"><i class="fa fa-black-tie"></i> Job Opening Applied</h3>
                                        <div class="box-tools pull-right">
                                            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                                        </div>
                                    </div>
                                    <div class="box-body">
                                        <ul class="profile-info-list" id="assessment_position_view">
                                        	Loading please wait ...
                                        </ul>
                                    </div>
                                    <div class="box-footer">
		                                <ul class="data-logs" id="datalogs-position"></ul>
		                                <span>
		                                    <button href="#" class="btn btn-xs btn-default btn-flat show-logs-position" onclick="assessment_show_logs('position');"><small><i class="ion-plus"></i> Show All</small></button>
		                                    <button href="#" class="btn btn-xs btn-default btn-flat hide-logs-position" onclick="assessment_hide_logs('position');" style="display:none;"><small><i class="ion-minus"></i> Show Less</small></button>
		                                </span>
                                    </div>
                                </div>
                                <!-- end of JOBS OPENING APPLIED -->
                            </div>
                        </div>
                        
                        
                        
                        
                        <!-- STATUS OF INTERVIEW -->
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title" data-widget="collapse"><i class="fa fa-comments"></i> Status of Interview</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-default btn-xs btn-flat btn-box-add" data-toggle="false" onclick="popup_recruitment_assessment('cvlocation','<?=$applicant_id?>')"><i class="ion-plus"></i> Add Data</button>
                                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-plus"></i></button>
                                </div>
                            </div>
                            <div class="box-body" id="assessment_cvlocation_view">
                                Loading please wait ...
                                <!-- <table class="table table-bordered table-hover table-stripe table-condensed">
                                    <thead> 	 			 	 	 	 	 	 
 	 	 		 	 	 	
                                        <tr>
                                            <th>Evaluator</th>
                                            <th>Mode</th>
                                            <th>Origin</th>
                                            <th>Remarks</th>
                                            <th>Date Created</th>
                                            <th>Creator</th>
                                            <th>Date Edit</th>
                                            <th>Editor</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>cristel</td>
                                            <td>RC</td>
                                            <td>DATA BANK</td>
                                            <td>Reviewed CV for FABTECH </td>
                                            <td>Feb 26, 2016</td>
                                            <td>cristel</td>
                                            <td>Feb 26, 2016</td>
                                            <td>cristel</td>
                                            <td>
                                                <div class="btn-group pull-right">
                                                    <a href="#" type="button" class="btn btn-sm btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                                                    <a href="#" type="button" class="btn btn-sm btn-danger btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <br />
                                <a href="#" class="btn btn-default btn-xs btn-flat">Load More</a>
                                 -->
                            </div>
                            
                        </div>
                        <!-- end of STATUS OF INTERVIEW -->
                        
                        <!-- GENERAL ASSESSMENT -->
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title" data-widget="collapse"><i class="fa fa-paste"></i> General Assessment</h3>
                                <div class="box-tools pull-right">
                                	<button class="btn btn-default btn-xs btn-flat btn-box-add" data-toggle="false" onclick="popup_recruitment_assessment('general','<?=$applicant_id?>')"><i class="ion-plus"></i> Add Data</button>
                                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-plus"></i></button>
                                </div>
                            </div>
                            <div class="box-body" id="assessment_general_view">
                            	Loading please wait ...	 	 	 	
                            </div>
                            <div class="box-footer">
                                <ul class="data-logs" id="datalogs-general"></ul>
                                <span>
                                    <button href="#" class="btn btn-xs btn-default btn-flat show-logs-general" onclick="assessment_show_logs('general');"><small><i class="ion-plus"></i> Show All</small></button>
                                    <button href="#" class="btn btn-xs btn-default btn-flat hide-logs-general" onclick="assessment_hide_logs('general');" style="display:none;"><small><i class="ion-minus"></i> Show Less</small></button>
                                </span>
                            </div>
                        </div>
                        <!-- end of GENERAL ASSESSMENT -->
                    
                    </div>
                    
                </div>
                
            </section>
            <!-- end of MAIN CONTENT -->