	<table class="table table-bordered table-hover table-stripe table-condensed">
		<thead> 	 			 	 	 	 	 	 
			<tr>
				<th>Evaluator</th>
				<th>Mode</th>
				<th>Origin</th>
				<th>Remarks</th>
				<th>Date Created</th>
				<th>Creator</th>
				<th>Date Edit</th>
				<th>Editor</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
										
		<?php /* Result Data */
			$delete_access_admin=array(1);
			
			if((in_array($this->session->userdata['iris_user_access'], $delete_access_admin))){
				$show_delete = true;
			}else{
				$show_delete = false;
			}
			
			$query = $this->db->query("select * from users where user_id='".$this->session->userdata['iris_user_access']."'");
			$user_information = $query->row_array();

			$counter = 1;
			foreach ($cvlocation as $key=>$value) {
				$applicant_id = $value['applicant_id'];
				$apply_date = $value['apply_date'];
				$evaluator = $value['evaluator'];
				$creator = $value['creator'];
				$edit_by = $value['edit_by'];
				
				$origin = ($value["date_created"] == $apply_date) ? 'NEW APPLICANT' : 'DATA BANK';
				$style_tr = ($counter>1) ? " class=\"tdInt\" style=\"display:none;\"" : "";
				if ($counter == 1) { ?>
					<input type="hidden" id="cv_mode" name="cv_mode" value="<?=$value['status']?>">
				<?php } ?>

				<tr <?=$style_tr?>>
					<td><?=stripslashes($evaluator)?></td>
					<td><?=$value['status']?></td>
					<td><?=$origin?></td>
					<td style="min-width:40%;"><?=nl2br($value["remarks"])?></td>
					<td><?=dateformat($value['date_created'], 'M, Y')?></td>
					<td><?=$creator?></td>
					<td><?=dateformat($value['date_edit'], 'M, Y')?></td>
					<td><?=$edit_by?></td>
					
					<td>
						<div class="btn-group pull-right">
							<button type="button" class="btn btn-sm btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Edit" onclick="popup_recruitment_assessment('cvlocation','<?=$value["applicant_id"]?>','<?=$value["cv_location_id"]?>')"><i class="fa fa-pencil"></i></button>
							<button type="button" class="btn btn-sm btn-danger btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Delete" onclick="delete_recruitment_assessment('cvlocation','<?=$value["applicant_id"]?>','<?=$value["cv_location_id"]?>')"><i class="fa fa-trash"></i></button>
						</div>
					</td>
				</tr>
		<?php $counter++; } ?>
		
		</tbody>
	</table>
	<br />
	<?php 
	if(count($cvlocation) > 1) { ?>
		<tr>
			<td colspan="6">
				<span id="btnmore_Int" class="btn btn-default btn-xs btn-flat" onclick="toggle_Advisory('.tdInt', 1, 1, '#btnmore_Int', '#btnless_Int');" style="cursor: pointer;">  Read More</span>
				<span id="btnless_Int" class="btn btn-default btn-xs btn-flat" onclick="toggle_Advisory('.tdInt', 0, 1, '#btnmore_Int', '#btnless_Int');" style="cursor: pointer; display: none;">  Hide</span>
			</td>
		</tr>
	<?php }
	?>
					


