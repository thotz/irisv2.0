	<?php /* Result Data */
		if((in_array($this->session->userdata['iris_user_access'],array('1')))){
			$show_delete = true;
		}else{
			$show_delete = false;
		}
		
		foreach ($education as $value) {
			$from_date = ($value['from_date']) ? $value['from_date'] : "";
			$to_date = ($value['to_date']) ? $value['to_date'] : "";
			?>
			<li class="col-md-6">
				<div class="btn-group pull-right">
					<button type="button" class="btn btn-sm btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Edit" onclick="popup_recruitment_assessment('education','<?=$value["applicant_id"]?>','<?=$value["id"]?>')"><i class="fa fa-pencil"></i></button>
					<?php if ($show_delete) {?>
					<button type="button" class="btn btn-sm btn-danger btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Delete" onclick="delete_recruitment_assessment('education','<?=$value["applicant_id"]?>','<?=$value["id"]?>')"><i class="fa fa-trash"></i></button>
					<?php } ?>
				</div>
				<h3><strong><?=$value['education']?></strong> - <?=$value['course']?></h3>
				<p><i class="fa fa-university"></i> <?=$value['school']?></p>
				<p><i class="fa fa-map-marker"></i> <?=$value['location']?></p>
				<p><i class="fa fa-calendar"></i> <?=$from_date?> - <?=$to_date?></p>
			</li>
	<?php } ?>

