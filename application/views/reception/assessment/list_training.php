	<?php /* Result Data */
		if((in_array($this->session->userdata['iris_user_access'],array('1')))){
			$show_delete = true;
		}else{
			$show_delete = false;
		}
		
		foreach ($training as $value) {
			?>
			<li>
				<div class="btn-group pull-right">
					<button type="button" class="btn btn-sm btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Edit" onclick="popup_recruitment_assessment('training','<?=$value["applicant_id"]?>','<?=$value["training_id"]?>')"><i class="fa fa-pencil"></i></button>
					<button type="button" class="btn btn-sm btn-danger btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Delete" onclick="delete_recruitment_assessment('training','<?=$value["applicant_id"]?>','<?=$value["training_id"]?>')"><i class="fa fa-trash"></i></button>
				</div>
				<h3><strong><?=$value['title']?></strong></h3>
				<div class="row">
					<div class="col-md-12">
						<p><i class="fa fa-map-marker"></i> <?=$value['center']?></p>
					</div>
				</div>
				<div class="row">
					<!-- <div class="col-md-6">
						<p><i class="fa fa-calendar"></i> <?=dateformat($value['date'], 'M, Y')?> - <?=dateformat($value['date_to'], 'M, Y')?></p>
					</div> -->
					<div class="col-md-6">
						<p><i class="fa fa-clock-o"></i> <?=$value['duration']?></p>
					</div>
				</div>
			</li>
	<?php } ?>

