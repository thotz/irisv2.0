<table class="table table-bordered table-hover table-stripe table-condensed">
	<thead> 	 			 	 	 	 	 	 

		<tr>
			<th>Date</th>
			<th>Assesor</th>
			<th>Edit by</th>
			<th>Assessment Detail</th>
			<th>Recommended Position</th>
			<th>Nature of Project Experience</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<?php /* Result Data */
			$delete_access_admin=array(1);
			
			if((in_array($this->session->userdata['iris_user_access'], $delete_access_admin))){
				$show_delete = true;
			}else{
				$show_delete = false;
			}
			
			$query = $this->db->query("select * from users where user_id='".$this->session->userdata['iris_user_access']."'");
			$user_information = $query->row_array();
			
			foreach ($general as $value) {
				$applicant_id = $value['applicant_id'];
				$evaluator = $this->db->query("select username from users where user_id='".$value['evaluator']."'")->row_array();
				$edit_by = $this->db->query("select username from users where user_id='".$value['edit_by']."'")->row_array();
				
				$position = "";
				$position1 = $this->db->query("select name from positions where position_id='".$value['position1']."'")->row_array();
				$position2 = $this->db->query("select name from positions where position_id='".$value['position2']."'")->row_array();
				$position3 = $this->db->query("select name from positions where position_id='".$value['position3']."'")->row_array();
					
				$position .= $position1['name'];
				if ($position2['name']) $position .= " / ".$position2['name'];
				if ($position3['name']) $position .= " / ".$position3['name'];
				
				$natureofbusiness = "";
				$natureofbusiness1 = $this->db->query("select name from job_spec where jobspec_id='".$value['natureofbusiness1']."'")->row_array();
				$natureofbusiness2 = $this->db->query("select name from job_spec where jobspec_id='".$value['natureofbusiness2']."'")->row_array();
				$natureofbusiness3 = $this->db->query("select name from job_spec where jobspec_id='".$value['natureofbusiness3']."'")->row_array();
				
				$natureofbusiness .= $natureofbusiness1['name'];
				if ($natureofbusiness2['name']) $natureofbusiness .= " : ".$natureofbusiness2['name'];
				if ($natureofbusiness3['name']) $natureofbusiness .= " : ".$natureofbusiness3['name'];
				
				
				if ($user_information['user_id'] == $value['evaluator'] || in_array($this->session->userdata['iris_user_access'], $delete_access_admin)) {
					$allow_edit_gen = "<a href=\"#\" type=\"button\" onclick=\"popup_recruitment_assessment('general','{$applicant_id}','{$value['assessment_id']}'); return false;\" class=\"btn btn-sm btn-default btn-flat\" data-placement=\"top\" data-original-title=\"Edit\"><i class=\"fa fa-pencil\"></i></a>";
				} else {
					$allow_edit_gen = "<span style=\"\" ><font>Edit</font></span>";
				}
				?>

				<tr>
					<td><?=dateformat($value['date_created'], 'M, Y')?></td>
					<td><?=$evaluator['username']?></td>
					<td><?=$edit_by['username']?></td>
					<td><?=$value['details']?></td>
					<td><?=$position?></td>
					<td><?=$natureofbusiness?></td>
					<td style="width:113px;">
						<div class="btn-group pull-right">
							<!-- <a href="#" type="button" class="btn btn-sm btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></a> -->
							<?=$allow_edit_gen?>
							<a href="#" type="button" class="btn btn-sm btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Print"><i class="fa fa-print"></i></a>
							<a href="#" type="button" class="btn btn-sm btn-danger btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash"></i></a>
						</div>
					</td>
				</tr>
		<?php } ?>
	
	</tbody>
</table>
<br />
<!-- <a href="#" class="btn btn-default btn-xs btn-flat">Load More</a> -->
