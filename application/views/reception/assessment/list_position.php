	<?php /* Result Data */
		if((in_array($this->session->userdata['iris_user_access'],array('1')))){
			$show_delete = true;
		}else{
			$show_delete = false;
		}
		
		foreach ($position as $value) {
			$company = ($value['prin_name']) ? $value['prin_name'] : '-';
			$ref_no = ($value['ref_no']) ? $value['ref_no'] : '-';
			$manpower_rid = $value['manpower_rid'];
			$status_mr = $value['status_mr'];
			$line_up_id = $value['line_up_id'];
			$for_confirm_initial = $value['for_confirm_initial'];
			$for_confirm_mode = $value['for_confirm_mode'];
			$position_id = $value['position_id'];
			$applicant_id = $value['applicant_id'];
			
			$my_position = $this->db->query("select name from positions where position_id='".$position_id."'")->row_array();
			
			if ($position_id) {
				$html_for_confirm = "";
				$html_delete = "";
					
				if ($line_up_id && $for_confirm_initial) {
					if ($for_confirm_initial == 'done' || $for_confirm_initial == 'relineup') {
						if (strtolower($status_mr) != 'active') {
							$html_for_confirm = "LU - " . $status_mr;
						} else {
							$html_for_confirm = "LU";
						}
					} else if ($for_confirm_mode) {
						$html_for_confirm = $for_confirm_mode;
					} else {
						if ($line_up_id) {
							if (strtolower($status_mr) != 'active') {
								$html_for_confirm = "Category - " . $status_mr;
							} else {
								if (!in_array("'".$manpower_rid."'", $result_lineup)) {
									$html_for_confirm = "<input type=\"button\" style=\"cursor:pointer;\" onclick=\"for_confirmation_box('".$applicant_id."','".$line_up_id."','confirm');\" value=\"Confirm Lineup\" title=\"confirm\" name=\"submitlineup_confirm\">&nbsp;";
									$html_for_confirm .= "<input type=\"button\" style=\"cursor:pointer;\" onclick=\"for_confirmation_box('".$applicant_id."','".$line_up_id."','NQ');\" value=\"LU - Not Suitable\" title=\"NQ\" name=\"submitlineup_confirm\">&nbsp;";
									$html_for_confirm .= "<input type=\"button\" style=\"cursor:pointer;\" onclick=\"for_confirmation_box('".$applicant_id."','".$line_up_id."','PS');\" value=\"Set PS\" title=\"PS\" name=\"submitlineup_confirm\">&nbsp;";
									$html_delete = "<span style=\"cursor:pointer;\" onclick=\"delete_recruitment_assessment('position', '{$applicant_id}','{$line_up_id}');\"><font color=\"maroon\">Delete</font></span>";
								} else  {
									$html_for_confirm = "Already LU - $ref_no";
								}
							}
						}
					}
				} else {
				
					$html_delete = "<span style=\"cursor:pointer;\" onclick=\"delete_recruitment_assessment('position', '{$applicant_id}','{$line_up_id}');\"><font color=\"maroon\">Delete</font></span>";
				}
			
			?>
			<li>
			<h3><strong><?=$my_position['name']?></strong></h3>
				<div class="row">
					<div class="col-md-12">
						<p><i class="fa fa-building"></i><strong>Company:</strong> <?=$company?></p>
					</div>
					<div class="col-md-12">
						<p><i class="fa fa-hashtag"></i> <strong>MR Ref. No.:</strong> <?=$ref_no?></p>
					</div>
					<div class="col-md-12">
						<p><i class="fa fa-th-list"></i> <strong>Initial Line up - For Confirmation:</strong> <?=$html_for_confirm?></p>
					</div>
				</div>
		   </li>
	<?php } 
	} ?>

