	<?php /* Result Data */
		if((in_array($this->session->userdata['iris_user_access'],array('1')))){
			$show_delete = true;
		}else{
			$show_delete = false;
		}
		
		foreach ($employment as $value) {
			$country = $this->db->query("select name from country where country_id='".$value['country_id']."'")->row_array();
			$nature = $this->db->query("select name from job_spec where jobspec_id='".$value['natureofbusiness']."'")->row_array();
			
			?>
			
			<li>
				<div class="btn-group pull-right">
					<button type="button" class="btn btn-sm btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Edit" onclick="popup_recruitment_assessment('employment','<?=$value["applicant_id"]?>','<?=$value["id"]?>')"><i class="fa fa-pencil"></i></button>
					<button type="button" class="btn btn-sm btn-danger btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Delete" onclick="delete_recruitment_assessment('employment','<?=$value["applicant_id"]?>','<?=$value["id"]?>')"><i class="fa fa-trash"></i></button>
				</div>
				<h3><strong><?=$value['position']?></strong></h3>
				<div class="row">
					<div class="col-md-12">
						<p><i class="fa fa-building"></i> <strong><?=$value['company']?></strong> - <?=$nature['name']?></p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<p><i class="fa fa-file-text"></i> <?=$value['qualification']?></p>
					</div>
				</div>
				<div class="row">
					<!-- <div class="col-md-3">
						<p><i class="fa fa-users"></i> B.A.U.X. Department</p>
					</div>
					<div class="col-md-3">
						<p><i class="fa fa-money"></i> Php 00,000.00</p>
					</div> -->
					<div class="col-md-3">
						<p><i class="fa fa-map-marker"></i> <?=$country['name']?></p>
					</div>
					<div class="col-md-3">
						<p><i class="fa fa-calendar"></i> <?=dateformat($value['from_date'], 'M, Y')?> - <?=dateformat($value['to_date'], 'M, Y')?></p>
					</div>
				</div>
				
			</li>
	<?php } ?>

