            <!-- MAIN CONTENT -->
            <section class="content">
            
                <div class="row">
                    <div class="col-md-12">
            
						<?=$applicant_information?>
                        
                        <?=$applicant_link_menu;?>
                        
                        <!-- <div class="btn-group applicant-btn-group" role="group" aria-label="...">
                            <a href="../applicant/applicant-report-today.php" class="btn-link">Reported Today</a>
                            <a href="../applicant/applicant-overview.php" class="btn-link">Overview</a>
                            <a href="../applicant/applicant-personal.php" class="btn-link">Personal</a>
                            <a href="../applicant/applicant-education.php" class="btn-link">Education</a>
                            <a href="../applicant/applicant-licenses-certification.php" class="btn-link">Licenses / Certifications</a>
                            <a href="../applicant/applicant-work-history.php" class="btn-link">Work History</a>
                            <a href="../applicant/applicant-reference.php" class="btn-link">Reference</a>
                            <a href="../applicant/applicant-training-seminar.php" class="btn-link">Training / Seminars</a>
                            <a href="../applicant/applicant-trade-test.php" class="btn-link">Trade Test</a>
                            <a href="../applicant/applicant-info-edit.php" class="btn-link">Edit Profile</a>
                            <a href="../applicant/applicant-cv-tracker.php" class="btn-link">CV Trakcer</a>
                            <a href="../applicant/applicant-position-applied.php" class="btn-link">Position Applied</a>
                            <a href="../applicant/applicant-assessment.php" class="btn-link">Assessment</a>
                            <a href="../applicant/applicant-lineup.php" class="btn-link active">Line-up</a>
                            <a href="../applicant/applicant-selections.php" class="btn-link">Selections</a>
                            <a href="../applicant/applicant-advisory.php" class="btn-link">Advisory</a>
                            <a href="../applicant/applicant-history.php" class="btn-link">History</a>
                        </div> -->
                        
                        <!-- LINE UP HISTORY -->
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title" data-widget="collapse"><i class="fa fa-file-text"></i> Line Up History</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-default btn-xs btn-flat btn-box-add" data-toggle="modal" data-target="#modalAddEducation"><i class="ion-plus"></i> Add Multiple MR 1</button>
                                    <button class="btn btn-default btn-xs btn-flat btn-box-add" data-toggle="modal" data-target="#modalAddEducation"><i class="ion-plus"></i> Add Multiple MR 2</button>
                                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-plus"></i></button>
                                </div>
                            </div>
                            <div class="box-body">
                                <ul class="profile-info-list">
                                    <li>
                                        <div class="btn-group pull-right">
                                            <button type="button" class="btn btn-sm btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></button>
                                            <button type="button" class="btn btn-sm btn-danger btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash"></i></button>
                                        </div>
                                        <h3><strong>FABTECH-01B16-E1 </strong></h3>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <p><strong>POSITION :</strong> OPTR,MACHINE,CNC</p>
                                            </div>
                                            <div class="col-md-6">
                                                <p><strong>PRINCIPAL :</strong> METAL FABRICATION TECHNOLOGY - FABTECH CO. WLL</p>
                                            </div>
                                            <div class="col-md2">
                                                <p><strong>ACTIVITY :</strong> CV</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <p><strong>INTERVIEW SCHEDULE :</strong></p>
                                            </div>
                                            <div class="col-md-6">
                                                <p><strong>INTERVIEW DATE :</strong></p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <p><strong>ACTIVITY :</strong> CV</p>
                                            </div>
                                            <div class="col-md-4">
                                                <p><strong>LINE-UP RESULT :</strong> CR</p>
                                            </div>
                                            <div class="col-md-4">
                                                <p><strong>BRANCH :</strong> MNL</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <p><strong>MODE :</strong></p>
                                            </div>
                                            <div class="col-md-4">
                                                <p><strong>REMARKS :</strong> For CV sending</p>
                                            </div>
                                            <div class="col-md-4">
                                                <p><strong>CV STATUS :</strong> Reviewed</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <p><strong>TRANSMITAL NO. :</strong></p>
                                            </div>
                                            <div class="col-md-4">
                                                <p><strong>CV SENT DATE :</strong></p>
                                            </div>
                                            <div class="col-md-4">
                                                <p><strong>ACCEPTANCE :</strong></p>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="btn-group pull-right">
                                            <button type="button" class="btn btn-sm btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></button>
                                            <button type="button" class="btn btn-sm btn-danger btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash"></i></button>
                                        </div>
                                        <h3><strong>WMI-01A16-E1 </strong></h3>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <p><strong>POSITION :</strong> OPTR,MACHINE,CNC</p>
                                            </div>
                                            <div class="col-md-6">
                                                <p><strong>PRINCIPAL :</strong> WAJHAT METAL INDUSTRIES</p>
                                            </div>
                                            <div class="col-md-2">
                                                <p><strong>ACTIVITY :</strong> CV</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <p><strong>INTERVIEW SCHEDULE :</strong></p>
                                            </div>
                                            <div class="col-md-6">
                                                <p><strong>INTERVIEW DATE :</strong></p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <p><strong>LINE-UP RESULT :</strong> CR</p>
                                            </div>
                                            <div class="col-md-4">
                                                <p><strong>BRANCH :</strong> MNL</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <p><strong>MODE :</strong></p>
                                            </div>
                                            <div class="col-md-4">
                                                <p><strong>REMARKS :</strong> For CV sending</p>
                                            </div>
                                            <div class="col-md-4">
                                                <p><strong>CV STATUS :</strong> Reviewed</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <p><strong>TRANSMITAL NO. :</strong></p>
                                            </div>
                                            <div class="col-md-4">
                                                <p><strong>CV SENT DATE :</strong></p>
                                            </div>
                                            <div class="col-md-4">
                                                <p><strong>ACCEPTANCE :</strong></p>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="box-footer">
                                <ul class="data-logs" id="datalogsEducation">
                                    <li><small>Jan 8, 2016 9:39 pm - edit Through Online Application</small></li>
                                    <li><small>Jan 5, 2016 7:19 am - edit Through Online Application</small></li>
                                    <li><small>Dec 24, 2015 3:38 pm - add Through Online Application</small></li>
                                    <li>
                                        <button href="#" class="btn btn-xs btn-default btn-flat show-logs"><small><i class="ion-plus"></i> Show All</small></button>
                                        <button href="#" class="btn btn-xs btn-default btn-flat hide-logs" style="display:none;"><small><i class="ion-minus"></i> Show Less</small></button>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- end of LINE UP HISTORY -->
                        
                        <!-- LINE UP HISTORY -->
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title" data-widget="collapse"><i class="fa fa-file-text"></i> Other Line up Status <small>Category (On Hold, Close) / Line up for Confirmation</small></h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-plus"></i></button>
                                </div>
                            </div>
                            <div class="box-body">
                                <ul class="profile-info-list">
                                    <li>
                                        <div class="btn-group pull-right">
                                            <button type="button" class="btn btn-sm btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></button>
                                            <button type="button" class="btn btn-sm btn-danger btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash"></i></button>
                                        </div>
                                        <h3><strong>AJI-01J15-E1  </strong></h3>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <p><strong>POSITION :</strong> CNC MACHINE OPERATOR</p>
                                            </div>
                                            <div class="col-md-6">
                                                <p><strong>PRINCIPAL :</strong> AL AJIAL FACTORY CO. LTD.</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <p><strong>CATEGORY STATUS :</strong> ActiveR</p>
                                            </div>
                                            <div class="col-md-4">
                                                <p><strong>MODE :</strong> NQ</p>
                                            </div>
                                            <div class="col-md-4">
                                                <p><strong>CONFIRMED BY :</strong> crisanto</p>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="box-footer">
                                <ul class="data-logs" id="datalogsEducation">
                                    <li><small>Jan 8, 2016 9:39 pm - edit Through Online Application</small></li>
                                    <li><small>Jan 5, 2016 7:19 am - edit Through Online Application</small></li>
                                    <li><small>Dec 24, 2015 3:38 pm - add Through Online Application</small></li>
                                    <li>
                                        <button href="#" class="btn btn-xs btn-default btn-flat show-logs"><small><i class="ion-plus"></i> Show All</small></button>
                                        <button href="#" class="btn btn-xs btn-default btn-flat hide-logs" style="display:none;"><small><i class="ion-minus"></i> Show Less</small></button>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    
                </div>
                
            </section>
            <!-- end of MAIN CONTENT -->