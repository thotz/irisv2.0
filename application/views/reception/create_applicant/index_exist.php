            <!-- MAIN CONTENT -->
            <section class="content">
            
                <div class="row">
                    <div class="col-md-12">
                                
                        <!-- CREATE NEW APPLICATION FORM -->
                        <form class="form-horizontal applicant-profile" name="form_create_applicant" id="form_create_applicant" method="POST" action="<?=base_url()?>reception/create_applicant/add" enctype="multipart/form-data">
                            <input type="hidden" name="old_applicant_id" id="old_applicant_id" value="<?=$old_applicant_id?>">
                            <input type="hidden" name="new_applicant_id" id="new_applicant_id" value="">
                            <input type="hidden" name="action" id="action" value="<?=$action?>">
                            <input type="hidden" name="work_availability" id="work_availability" value="<?=$personal_info['work_availability']?>">
                            <input type="hidden" name="branch_id" id="branch_id" value="<?=$this->session->userdata['iris_user_branch_id']?>">
                            
                            <!-- CREATE NEW APPLICATION -->
                            <div class="box">
                            		
	                                <!-- <div class="box-header with-border">
	                                    <h3 class="box-title">Create New Applicant</h3>
	                                </div> -->
	                                <div class="box-body">
	
	                                    <!-- ALERT MESSAGE [ERROR] 
	                                    <div class="alert alert-danger alert-dismissable">
	                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>
	                                        <i class="icon fa fa-ban"></i> <strong>ERROR!</strong> Please fill all required fields.
	                                    </div>-->
	
	                                    <!-- ALERT MESSAGE [SUCCESS] 
	                                    <div class="alert alert-success alert-dismissable">
	                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>
	                                        <i class="icon fa fa-check"></i> <strong>SUCCESS!</strong> Applicant "NAME or APPLICANT No." is successfully added.
	                                    </div>-->
	
	                                    <div class="row">
	                                        <div class="col-md-10 col-md-offset-1">
	
	                                            <!-- Branch & Date -->
	                                            <div class="form-group">
	                                            	<?php if ($old_applicant_id) {?>
	                                                <label class="col-sm-2 control-label">Applicant No. (old):</label>
	                                                <div class="col-sm-10 control-display"><strong><?=$old_applicant_id?></strong></div>
	                                                <?php } ?>
	                                                <label class="col-sm-2 control-label">Applied Branch:</label>
	                                                <div class="col-sm-4 control-display"><strong><?=$branch['name']?></strong></div>
	                                                <!-- Current Date -->
	                                                <label class="col-sm-2 control-label">Application Date :</label>
	                                                <div class="col-sm-4 control-display"><?=dateformat(Date('Y-m-d'))?></div>
	                                            </div>
	
	                                            <!-- Position -->
	                                            <div class="form-group">
	                                                <label for="selectPreferredPosition" class="col-sm-2 control-label">Preferred Position<span class="required">*</span>:</label>
	                                                <div class="col-sm-4">
	                                                    <select name="position_ids[]" class="form-control form-control-multiple">
	                                                        <option value=""></option>
	                                                        <?=$preferred['position1']?>
	                                                    </select>
	                                                    <select name="position_ids[]" class="form-control form-control-multiple">
	                                                        <option value=""></option>
	                                                        <?=$preferred['position2']?>
	                                                    </select>
	                                                    <select name="position_ids[]" class="form-control form-control-multiple">
	                                                        <option value=""></option>
	                                                        <?=$preferred['position3']?>
	                                                    </select>
	                                                </div>
	                                                <label for="selectJobSpecialization" class="col-sm-2 control-label">Job Specialization<span class="required">*</span>:<br/><small>(For Online or Work Abroad)</small></label>
	                                                <div class="col-sm-4">
	                                                    <select name="jobspec_ids[]" class="form-control form-control-multiple">
	                                                        <option value=""></option>
	                                                        <?=$specialization['natureofbusiness1']?>
	                                                    </select>
	                                                    <select name="jobspec_ids[]" class="form-control form-control-multiple">
	                                                        <option value=""></option>
	                                                        <?=$specialization['natureofbusiness2']?>
	                                                    </select>
	                                                    <select name="jobspec_ids[]" class="form-control form-control-multiple">
	                                                        <option value=""></option>
	                                                        <?=$specialization['natureofbusiness3']?>
	                                                    </select>
	                                                </div>
	                                            </div>
	                                            
	                                            <!-- Preferred Position -->
	                                            <div class="form-group">
	                                                <label for="selectPreferredPosition" class="col-sm-2 control-label">Company Applied Position<span class="required">*</span>:</label>
	                                                <div class="col-sm-10">
	                                                    <select name="mr_positions[]" id="mr_position1" class="form-control form-control-multiple">
	                                                        <option value=""></option>
	                                                        <?=$applied_position['manpower_position']?>
	                                                    </select>
	                                                    <select name="mr_positions[]" id="mr_position2" class="form-control form-control-multiple">
	                                                        <option value=""></option>
	                                                        <?=$applied_position['manpower_position']?>
	                                                    </select>
	                                                    <select name="mr_positions[]" id="mr_position3" class="form-control form-control-multiple">
	                                                        <option value=""></option>
	                                                        <?=$applied_position['manpower_position']?>
	                                                    </select>
	                                                </div>
	                                            </div>
	
	                                            <!-- Name -->
	                                            <div class="form-group">
	                                                <label for="inputLastName" class="col-sm-2 control-label">Name<span class="required">*</span>:</label>
	                                                <div class="col-sm-4">
	                                                    <input type="text" id="lname" name="lname" value="<?=isset($personal_info['lname']) ? $personal_info['lname'] : ''?>" class="form-control" placeholder="Last Name" />
	                                                </div>
	                                                <div class="col-sm-4">
	                                                    <input type="text" id="fname" name="fname" value="<?=isset($personal_info['fname']) ? $personal_info['fname'] : ''?>" class="form-control" placeholder="First Name"/>
	                                                </div>
	                                                <div class="col-sm-2">
	                                                    <input type="text" id="mname" name="mname" value="<?=isset($personal_info['mname']) ? $personal_info['mname'] : ''?>" class="form-control" id="inputMiddleName" placeholder="M.I.">
	                                                </div>
	                                                <br />
	                                                <div class="col-sm-2 col-sm-offset-2">
	                                                    <label class="user-status"><input type="checkbox" class="flat-blue" id="lastext" name="lastext" value="1" /> Jr. </label>
	                                                </div>
	                                                <div class="col-sm-4">
	                                                    <label class="user-status tempclass"><input type="checkbox" class="flat-blue" id="no_mname" name="no_mname" value="1" /> No Middle Name </label>
	                                                </div>
	                                            </div>
	
	                                            <!-- Birthday -->
	                                            <div class="form-group">
	                                                <label for="selectBirthMonth" class="col-sm-2 control-label">Birthday<span class="required">*</span>:</label>
                                                	<?php
                                                	if (isset($personal_info["applicant_id"]) && $personal_info["applicant_id"]){
                                                		list ($bday_year, $bday_month, $bday_day) = @split ('[-]', $personal_info[birthdate]);
                                                	} else {
                                                		$bday_year = "";$bday_month="";$bday_day="";
                                                	}
                                                	
                                                	?>
	                                                <div class="col-sm-2">
	                                                	<?=dateselectmonth("bday_month", $bday_month, 'class="form-control"', "whole");?>
	                                                </div>
	                                                <div class="col-sm-2">
	                                                	<?=dateselectday("bday_day", $bday_day, 'class="form-control"');?>
	                                                </div>
	                                                <div class="col-sm-2">
	                                                	<?=dateselectyear("bday_year", $bday_year, 'class="form-control"');?>
	                                                </div>
	                                                <div class="col-sm-3">
	                                                    <!-- <a href="#" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-calendar"></i></a> -->
	                                                    <label><input type="checkbox" name="is_dependent" id="is_dependent" value="1" <?=(isset($personal_info['is_dependent']) && $personal_info['is_dependent']=='Y')?"checked=\"checked\"":"";?> /> (check if applicant is Dependent)</label>
	                                                </div>
	                                            </div>
	                                            
	                                            <!-- Gender -->
	                                            <div class="form-group">
	                                                <label class="col-sm-2 control-label">Gender<span class="required">*</span>:</label>
	                                                <div class="col-sm-10">
	                                                	<?php foreach ($gender as $key=>$value) {
	                                                		$str_style = "";
	                                                		if (isset($personal_info["sex"]) && $personal_info["sex"] == $key) {
	                                                			$str_style = ' checked="checked"';
	                                                		}
	                                                		?>
	                                                    	<label class="user-status"><input type="radio" class="flat-blue" name="sex" value="<?=$key?>" <?=$str_style?>/> <?=$value?></label>
	                                                    <?php } ?>
	                                                </div>
	                                            </div>
	
	                                            <!-- Contact Info -->
	                                            <div class="form-group">
	                                            	<label for="inputMobile" class="col-sm-2 control-label">Mobile No.<span class="required">*</span>:</label>
	                                            	<div class="col-sm-4">
	                                            	<?php if ($old_applicant_id && $action == 'add_deployed') { ?>
	                                                    <input type="text" class="form-control" name="cellphone" id="cellphone" onkeypress="return isNumberKey(event);" maxlength="11" value="" placeholder="ex: 09177340642">
	                                            	<?php } else { ?>
	                                                    <input type="text" class="form-control" name="cellphone" id="cellphone" onkeypress="return isNumberKey(event);" maxlength="11" value="<?=isset($personal_info["cellphone"]) ? $personal_info["cellphone"] : ''?>" placeholder="ex: 09177340642">
	                                            	<?php } ?>
	                                            	</div>
	                                                <label for="inputOtherTel" class="col-sm-2 control-label">Other Tel. No.:</label>
	                                                <div class="col-sm-4">
	                                                    <input type="text" class="form-control" name="office_phone" id="office_phone" value="<?=isset($personal_info["office_phone"]) ? $personal_info["office_phone"] : ''?>" placeholder="ex: 09191112222,09221112222">
	                                                </div>
	                                            </div>
	
	                                            <!-- Email -->
	                                            <div class="form-group">
	                                                <label for="inputEmail" class="col-sm-2 control-label">Email:</label>
	                                                <div class="col-sm-4">
	                                                    <input type="text" class="form-control" name="email" id="email" value="<?=isset($personal_info["email"]) ? $personal_info["email"] : ''?>" placeholder="">
	                                                </div>
	                                            </div>
	
	                                            <!-- Address -->
	                                            <div class="form-group">
	                                                <label for="txtAddress" class="col-sm-2 control-label">Present Address<span class="required">*</span>:</label>
	                                                <div class="col-sm-10">
	                                                    <textarea class="form-control" name="address1" id="address1"><?=isset($personal_info["address1"]) ? $personal_info["address1"] : ''?></textarea>
	                                                </div>
	                                            </div>
	
	                                            <!-- Municipal & Nearest Branch -->
	                                            <div class="form-group">
	                                                <label for="selectMunicipal" class="col-sm-2 control-label">Municipal<span class="required">*</span>:</label>
	                                                <div class="col-sm-4">
	                                                    <select name="address_city" id="address_city" class="form-control">
			                                                <option value=""></option>
	                                                    	<?=$municipality?>
	                                                    </select>
	                                                </div>
	                                                <label for="selectNearestBranch" class="col-sm-2 control-label">Nearest Branch<span class="required">*</span>:</label>
	                                                <div class="col-sm-4">
	                                                	<select id="branch_id1" name="branch_id1" class="form-control">
	                                                	<?php if ($nearest_branch) { ?>
	                                                		<?=$nearest_branch?>
	                                                	<?php } else { ?>
	                                                    	
	                                                    <?php } ?>
	                                                    </select>
	                                                </div>
	                                            </div>
	
	                                            <!-- Method & Source -->
	                                            <div class="form-group">
	                                                <label for="selectMethods" class="col-sm-2 control-label">Methods of Application<span class="required">*</span>:</label>
	                                                <div class="col-sm-4">
	                                                    <select id="cv_source" name="cv_source" class="form-control">
	                                                    <?=$method_application?>
	                                                    </select>
	                                                </div>
	                                                <label for="selectSource" class="col-sm-2 control-label">Source of Application<span class="required">*</span>:</label>
	                                                <div class="col-sm-4">
	                                                    <select name="source_id" id="source_id" class="form-control">
	                                                    <?=$res_source?>
	                                                    </select>
	                                                </div>
	                                            </div>
	                                            
	                                            <!-- Refferal -->
	                                            <div class="form-group" id="refid" <?=$refferal_data['style_reffered']?>>
	                                                <label for="selectRefferal" class="col-sm-2 control-label">Reffered<span class="required">*</span>:</label>
	                                                <div class="col-sm-4">
	                                                    <select id="referral" name="referral" class="form-control">
															<option value=""></option>
															<?=$refferal_data['data']?>
	                                                    </select>
	                                                </div>
	                                            </div>
	                                            
	                                            <!-- Agent -->
	                                            <div class="form-group" id="myagent" <?=$agent_data['style_agent']?>>
	                                                <label for="selectAgent" class="col-sm-2 control-label">Agent<span class="required">*</span>:</label>
	                                                <div class="col-sm-4">
	                                                    <select id="agent_id" name="agent_id" class="form-control">
	                                                    <?=$agent_data['data']?>
	                                                    </select>
	                                                </div>
	                                            </div>
	                                            
	                                            <!-- PRA -->
	                                            <div class="form-group" id="mypra" <?=$pra_data['style_pra']?>>
	                                                <label for="selectCity" class="col-sm-2 control-label">PRA City:</label>
	                                                <div class="col-sm-4">
	                                                    <select id="pra_city_id" name="pra_city_id" class="form-control">
	                                                    <option value=""></option>
	                                                    <?=$pra_data['city']?>
	                                                    </select>
	                                                </div>
	                                                <label for="selectLeader" class="col-sm-2 control-label">PRA Leader:</label>
	                                                <div class="col-sm-4">
	                                                    <select name="pra_user_id" id="pra_user_id" class="form-control">
	                                                    <?=$pra_data['leader']?>
	                                                    </select>
	                                                </div>
	                                            </div>
	                                            
	                                            <!-- PRA Date -->
	                                            <div class="form-group" id="mypra2" <?=$pra_data['style_pra']?>>
	                                                <label for="selectPraDate" class="col-sm-2 control-label">PRA Date:</label>
                                                	<?php
                                                	if ($personal_info["pra_date"]){
                                                		list ($pra_date_year, $pra_date_month, $pra_date_day) = @split ('[-]', $personal_info[pra_date]);
                                                	} else {
                                                		$pra_date_year = "";$pra_date_month="";$pra_date_day="";
                                                	}
                                                	
                                                	?>
	                                                <div class="col-sm-2">
	                                                	<?=dateselectmonth("pra_date_month", $pra_date_month, 'class="form-control"', "whole");?>
	                                                </div>
	                                                <div class="col-sm-2">
	                                                	<?=dateselectday("pra_date_day", $pra_date_day, 'class="form-control"');?>
	                                                </div>
	                                                <div class="col-sm-2">
	                                                	<?=dateselectyear("pra_date_year", $pra_date_year, 'class="form-control"');?>
	                                                </div>
	                                            </div>
	                                            
	                                            <!-- Gender -->
	                                            <div class="form-group">
												<?php
												
													$work_availability_array = explode(":", $personal_info['work_availability']);
													$my_availability = isset($work_availability_array[0]) ? $work_availability_array[0] : "";
													if ($my_availability == 2) {
														$my_month_count = isset($work_availability_array[1]) ? $work_availability_array[1] : "";
													} else {
														$my_month = isset($work_availability_array[1]) ? $work_availability_array[1] : "";
													}
													$my_year = isset($work_availability_array[2]) ? $work_availability_array[2] : "";
													?>
	                                                <label class="col-sm-2 control-label">Work Availability<span class="required">*</span>:</label>
	                                                <div class="col-sm-10">
	                                                    <label class="user-status"><input type="radio" class="flat-blue" value="1" name="availability" onclick="check_availability(this);" <?=($my_availability == 1) ? 'checked=""' : '';?>/> I can start for work as soon as possible. </label>
	                                                    <br />
	                                                    <label class="user-status"><input type="radio" class="flat-blue" value="2" name="availability" onclick="check_availability(this);" <?=($my_availability == 2) ? 'checked=""' : '';?>/> I can start for work 
	                                                        <select>
	                                                            <option value="1">1</option>
	                                                            <option value="2">2</option>
	                                                            <option value="3">3</option>
	                                                            <option value="4">4</option>
	                                                            <option value="5">5</option>
	                                                            <option value="6">6</option>
	                                                            <option value="7">7</option>
	                                                            <option value="8">8</option>
	                                                            <option value="9">9</option>
	                                                            <option value="10">10</option>
	                                                            <option value="11">11</option>
	                                                            <option value="12">12</option>
	                                                        </select>
	                                                        month(s) after the notice.  </label>
	                                                    <br />
	                                                    <label class="user-status"><input type="radio" class="flat-blue" value="3" name="availability" onclick="check_availability(this);" <?=($my_availability == 3) ? 'checked=""' : '';?>/> Im looking for my next job. I can start work after
	                                                        <?=dateselectmonth("availability_month", $my_month);?> month and 
	                                                        <select>
	                                                            <option value="YYYY">YYYY</option>
	                                                            <option value="2015">2015</option>
	                                                            <option value="2016">2016</option>
	                                                        </select>
	                                                        year.  </label>
	                                                </div>
	                                            </div>
	
	                                            <!-- Attached CV's and other Documents -->
	                                            <div class="form-group">
	                                                <label for="inputFileCV" class="col-sm-2 control-label">Applicant CV<span class="required">*</span>:<br /><small>Word</small></label>
	                                                <?php if (document_file_exists("iriscvwordpdf",$applicant_cv["cv_applicant"])) {?>
		                                                <div class="col-sm-4">
		                                                    <A href="<?=FILE_LINKED."iriscvwordpdf/".$applicant_cv["cv_applicant"]?>" target="_blank"><?=basename($applicant_cv["cv_applicant"]);?></A>&nbsp;<a href="sqldelete.php?what=activity_applicant_cv&cv_applicant=<?=$applicant_cv["cv_applicant"]?>&applicant_id=&Action=" onclick="return confirm('Are you sure you want to remove this cv?');"><font color="maroon">delete</font></a>
															<input type="hidden" id="cv_applicant" name="cv_applicant" value="<?=$applicant_cv["cv_applicant"]?>">
		                                                </div>
		                                                <div class="col-sm-6 control-display">
		                                                    <p class="text-muted">Request updated CV if attached here is not updated.</p>
		                                                </div>
	                                                <?php } else { ?>
		                                                <div class="col-sm-4">
		                                                    <input id="cv_applicant" name="cv_applicant" type="file" class="form-control" onchange="return CheckExtensionword(this)" />
		                                                </div>
		                                                <div class="col-sm-6 control-display">
		                                                    <p class="text-muted">Request updated CV if attached here is not updated.</p>
		                                                </div>
	                                                <?php } ?>
	                                            </div>
	
	                                            <!-- Attached CV's and other Documents -->
	                                            <div class="form-group">
	                                                <label for="inputFileCertificate" class="col-sm-2 control-label">CV and Certificate<span class="required">*</span>:</label>
	                                                <?php if (document_file_exists("iriscvwordpdf",$applicant_cv["cv_applicant_pdf"])) {?>
		                                                <div class="col-sm-4">
		                                                    <A href="<?=FILE_LINKED."iriscvwordpdf/".$applicant_cv["cv_applicant_pdf"]?>" target="_blank"><?=basename($applicant_cv["cv_applicant_pdf"]);?></A>&nbsp;<a href="sqldelete.php?what=activity_applicant_cv&cv_applicant=<?=$applicant_cv["cv_applicant_pdf"]?>&applicant_id=&Action=" onclick="return confirm('Are you sure you want to remove this cv?');"><font color="maroon">delete</font></a>
															<input type="hidden" id="cv_applicant_pdf" name="cv_applicant_pdf" value="<?=$applicant_cv["cv_applicant_pdf"]?>">
		                                                </div>
		                                                <div class="col-sm-6 control-display">
		                                                    <p class="text-muted">Request updated CV if attached here is not updated.</p>
		                                                </div>
	                                                <?php } else { ?>
		                                                <div class="col-sm-4">
		                                                    <input id="cv_applicant_pdf" name="cv_applicant_pdf" type="file" class="form-control" onchange="return CheckExtensionpdf(this)" />
		                                                </div>
		                                                <div class="col-sm-6 control-display">
		                                                    <p class="text-muted">Request updated CV if attached here is not updated.</p>
		                                                </div>
	                                                <?php } ?>
	                                            </div>
	                                            
	                                            <!-- Bottom Part -->
	                                            <div class="form-group">
	                                                <!--<label class="col-sm-2 control-label">Gender<span class="required">*</span>:</label>-->
	                                                <div class="col-sm-10 col-sm-offset-2">
	                                                    <label class="user-status"><input type="radio" class="flat-blue" name="is_walkin" id="is_walkin1" value="1" value="Walk-in" <?=$reporting_data['style_walkin']?>/> Walk-in</label>
	                                                    <label class="user-status"><input type="radio" class="flat-blue" name="is_walkin" id="is_walkin0" value="0" value="not-reporting" <?=$reporting_data['style_issuance']?>/> For issuance of computer number only. (Applicant did not report yet)</label>
	                                                </div>
	                                            </div>
	                                            
	                                        </div>
	                                    </div>
	
	                                </div>
	                                
	                                <div class="box-footer">
	                                    
	                                    <div class="row">
	                                        <div class="col-md-10 col-md-offset-1">
	
	                                            <div class="row">
	                                                <div class="col-md-3 col-md-offset-2 col-sm-4 col-sm-offset-2 col-xs-9">
	                                                    <a href="javascript:void(0);" class="btn btn-primary btn-block btn-flat" id="btnSubmit">Save</a>
	                                                </div>
	                                                <div class="col-md-2 col-sm-3 col-xs-3">
	                                                    <a href="../dashboard/dashboard.php" type="button" class="btn btn-block btn-default btn-flat" id="btnCancel">Cancel</a>
	                                                </div>
										            <div class="modal-body" id="on_processing_display" style="display:none;">
										               <img src="<?=base_url()?>public/images/load.png" class="fa-spin">&nbsp; processing please wait ...
										 			</div>

	                                            </div>
	                                            
	                                        </div>

	                                    </div>
	                                    
	                                </div>
                            </div>
                            <!-- end of CREATE NEW APPLICATION -->
                        
                        </form>
                        <!-- end of CREATE NEW APPLICATION FORM -->
                        
                    </div>
                    
                </div>
            
            </section>
            <!-- end of MAIN CONTENT -->