            <!-- MAIN CONTENT -->
            <section class="content">
            
                <div class="row">
                    <div class="col-md-12">
                                
                        <!-- CREATE NEW APPLICATION FORM -->
                        <form class="form-horizontal applicant-profile">
            
                            <!-- CREATE NEW APPLICATION -->
                            <div class="box">
                            	<form name="form_create_applicant" id="form_create_applicant" method="GET" enctype="multipart/form-data">
                            		<input type="hidden" name="old_applicant_id" id="old_applicant_id" value="">
                            		<input type="hidden" name="action" id="action" value="">
                            		
	                                <!-- <div class="box-header with-border">
	                                    <h3 class="box-title">Create New Applicant</h3>
	                                </div> -->
	                                <div class="box-body">
	
	                                    <!-- ALERT MESSAGE [ERROR] 
	                                    <div class="alert alert-danger alert-dismissable">
	                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>
	                                        <i class="icon fa fa-ban"></i> <strong>ERROR!</strong> Please fill all required fields.
	                                    </div>-->
	
	                                    <!-- ALERT MESSAGE [SUCCESS] 
	                                    <div class="alert alert-success alert-dismissable">
	                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>
	                                        <i class="icon fa fa-check"></i> <strong>SUCCESS!</strong> Applicant "NAME or APPLICANT No." is successfully added.
	                                    </div>-->
	
	                                    <div class="row">
	                                        <div class="col-md-10 col-md-offset-1">
	
	                                            <!-- Branch & Date -->
	                                            <div class="form-group">
	                                                <label class="col-sm-2 control-label">Applied Branch:</label>
	                                                <div class="col-sm-4 control-display"><strong><?=$branch['name']?></strong></div>
	                                                <!-- Current Date -->
	                                                <label class="col-sm-2 control-label">Application Date :</label>
	                                                <div class="col-sm-4 control-display"><?=dateformat(Date('Y-m-d'))?></div>
	                                            </div>
	
	                                            <!-- Position -->
	                                            <div class="form-group">
	                                                <label for="selectPreferredPosition" class="col-sm-2 control-label">Preferred Position<span class="required">*</span>:</label>
	                                                <div class="col-sm-4">
	                                                    <select name="position_ids[]" class="form-control form-control-multiple">
	                                                        <option value=""></option>
	                                                        <?=$preferred['position1']?>
	                                                    </select>
	                                                    <select name="position_ids[]" class="form-control form-control-multiple">
	                                                        <option value=""></option>
	                                                        <?=$preferred['position2']?>
	                                                    </select>
	                                                    <select name="position_ids[]" class="form-control form-control-multiple">
	                                                        <option value=""></option>
	                                                        <?=$preferred['position3']?>
	                                                    </select>
	                                                </div>
	                                                <label for="selectJobSpecialization" class="col-sm-2 control-label">Job Specialization<span class="required">*</span>:<br/><small>(For Online or Work Abroad)</small></label>
	                                                <div class="col-sm-4">
	                                                    <select name="jobspec_ids[]" class="form-control form-control-multiple">
	                                                        <option value=""></option>
	                                                        <?=$specialization['natureofbusiness1']?>
	                                                    </select>
	                                                    <select name="jobspec_ids[]" class="form-control form-control-multiple">
	                                                        <option value=""></option>
	                                                        <?=$specialization['natureofbusiness2']?>
	                                                    </select>
	                                                    <select name="jobspec_ids[]" class="form-control form-control-multiple">
	                                                        <option value=""></option>
	                                                        <?=$specialization['natureofbusiness3']?>
	                                                    </select>
	                                                </div>
	                                            </div>
	                                            
	                                            <!-- Preferred Position -->
	                                            <div class="form-group">
	                                                <label for="selectPreferredPosition" class="col-sm-2 control-label">Company Applied Position<span class="required">*</span>:</label>
	                                                <div class="col-sm-10">
	                                                    <select name="mr_positions[]" id="mr_position1" class="form-control form-control-multiple">
	                                                        <option value=""></option>
	                                                        <?=$applied_position['manpower_position']?>
	                                                    </select>
	                                                    <select name="mr_positions[]" id="mr_position2" class="form-control form-control-multiple">
	                                                        <option value=""></option>
	                                                        <?=$applied_position['manpower_position']?>
	                                                    </select>
	                                                    <select name="mr_positions[]" id="mr_position3" class="form-control form-control-multiple">
	                                                        <option value=""></option>
	                                                        <?=$applied_position['manpower_position']?>
	                                                    </select>
	                                                </div>
	                                            </div>
	
	                                            <!-- Name -->
	                                            <div class="form-group">
	                                                <label for="inputLastName" class="col-sm-2 control-label">Name<span class="required">*</span>:</label>
	                                                <div class="col-sm-4">
	                                                    <input type="text" id="lastname" name="lastname" value="" class="form-control" placeholder="Last Name" />
	                                                </div>
	                                                <div class="col-sm-4">
	                                                    <input type="text" id="firstname" name="firstname" value="" class="form-control" placeholder="First Name"/>
	                                                </div>
	                                                <div class="col-sm-2">
	                                                    <input type="text" id="mname" name="mname" value=" class="form-control" id="inputMiddleName" placeholder="M.I.">
	                                                </div>
	                                                <br />
	                                                <div class="col-sm-2 col-sm-offset-2">
	                                                    <label class="user-status"><input type="checkbox" class="flat-blue" id="lastext" name="lastext" value="1" checked="checked" /> Jr. </label>
	                                                </div>
	                                                <div class="col-sm-4">
	                                                    <label class="user-status"><input type="checkbox" onclick="set_no_midle();" class="flat-blue" id="no_mname" name="no_mname" value="1" checked="checked" /> No Middle Name </label>
	                                                </div>
	                                            </div>
	
	                                            <!-- Birthday -->
	                                            <div class="form-group">
	                                                <label for="selectBirthMonth" class="col-sm-2 control-label">Birthday<span class="required">*</span>:</label>
                                                	<?php
                                                		$bday_year = "";$bday_month="";$bday_day="";
                                                	
                                                	?>
	                                                <div class="col-sm-2">
	                                                	<?=dateselectmonth("bday_month", $bday_month, 'class="form-control"', "whole");?>
	                                                </div>
	                                                <div class="col-sm-2">
	                                                	<?=dateselectday("bday_day", $bday_day, 'class="form-control"');?>
	                                                </div>
	                                                <div class="col-sm-2">
	                                                	<?=dateselectyear("bday_year", $bday_year, 'class="form-control"');?>
	                                                </div>
	                                                <!--<div class="col-sm-3">
	                                                    <a href="#" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-calendar"></i></a>
	                                                    <label><input type="checkbox" name="dependent" /> (check if applicant is Dependent)</label>
	                                                </div>-->
	                                            </div>
	                                            
	                                            <!-- Gender -->
	                                            <div class="form-group">
	                                                <label class="col-sm-2 control-label">Gender<span class="required">*</span>:</label>
	                                                <div class="col-sm-10">
	                                                    <label class="user-status"><input type="radio" class="flat-blue" name="gender" value="male" /> Male</label>
	                                                    <label class="user-status"><input type="radio" class="flat-blue" name="gender" value="female" /> Female</label>
	                                                </div>
	                                            </div>
	
	                                            <!-- Contact Info -->
	                                            <div class="form-group">
	                                                <label for="inputMobile" class="col-sm-2 control-label">Mobile No.<span class="required">*</span>:</label>
	                                                <div class="col-sm-4">
	                                                    <input type="text" class="form-control" id="inputMobile" placeholder="####-###-####">
	                                                </div>
	                                                <label for="inputOtherTel" class="col-sm-2 control-label">Other Tel. No.:</label>
	                                                <div class="col-sm-4">
	                                                    <input type="text" class="form-control" id="inputOtherTel" placeholder="####-###-####">
	                                                </div>
	                                            </div>
	
	                                            <!-- Email -->
	                                            <div class="form-group">
	                                                <label for="inputEmail" class="col-sm-2 control-label">Email<span class="required">*</span>:</label>
	                                                <div class="col-sm-4">
	                                                    <input type="text" class="form-control" id="inputEmail" placeholder="">
	                                                </div>
	                                            </div>
	
	                                            <!-- Address -->
	                                            <div class="form-group">
	                                                <label for="txtAddress" class="col-sm-2 control-label">Present Address<span class="required">*</span>:</label>
	                                                <div class="col-sm-10">
	                                                    <textarea class="form-control" id="txtAddress"></textarea>
	                                                </div>
	                                            </div>
	
	                                            <!-- Municipal & Nearest Branch -->
	                                            <div class="form-group">
	                                                <label for="selectMunicipal" class="col-sm-2 control-label">Municipal<span class="required">*</span>:</label>
	                                                <div class="col-sm-4">
	                                                    <select id="selectMunicipal" class="form-control"></select>
	                                                </div>
	                                                <label for="selectNearestBranch" class="col-sm-2 control-label">Nearest Branch<span class="required">*</span>:</label>
	                                                <div class="col-sm-4">
	                                                    <select id="selectNearestBranch" class="form-control"></select>
	                                                </div>
	                                            </div>
	
	                                            <!-- Method & Source -->
	                                            <div class="form-group">
	                                                <label for="selectMethods" class="col-sm-2 control-label">Methods of Application<span class="required">*</span>:</label>
	                                                <div class="col-sm-4">
	                                                    <select id="selectMethods" class="form-control"></select>
	                                                </div>
	                                                <label for="selectSource" class="col-sm-2 control-label">Source of Application<span class="required">*</span>:</label>
	                                                <div class="col-sm-4">
	                                                    <select id="selectSource" class="form-control"></select>
	                                                </div>
	                                            </div>
	                                            
	                                            <!-- Gender -->
	                                            <div class="form-group">
	                                                <label class="col-sm-2 control-label">Work Availability<span class="required">*</span>:</label>
	                                                <div class="col-sm-10">
	                                                    <label class="user-status"><input type="radio" class="flat-blue" name="availability" value="asap" /> I can start for work as soon as possible. </label>
	                                                    <br />
	                                                    <label class="user-status"><input type="radio" class="flat-blue" name="availability" value="by-month" /> I can start for work 
	                                                        <select>
	                                                            <option value="1">1</option>
	                                                            <option value="2">2</option>
	                                                            <option value="3">3</option>
	                                                            <option value="4">4</option>
	                                                            <option value="5">5</option>
	                                                            <option value="6">6</option>
	                                                            <option value="7">7</option>
	                                                            <option value="8">8</option>
	                                                            <option value="9">9</option>
	                                                            <option value="10">10</option>
	                                                            <option value="11">11</option>
	                                                            <option value="12">12</option>
	                                                        </select>
	                                                        month(s) after the notice.  </label>
	                                                    <br />
	                                                    <label class="user-status"><input type="radio" class="flat-blue" name="availability" value="next-job" /> Im looking for my next job. I can start work after
	                                                        <select>
	                                                            <option value="MM">MM</option>
	                                                            <option value="JAN">JAN</option>
	                                                            <option value="FEB">FEB</option>
	                                                            <option value="MAR">MAR</option>
	                                                            <option value="APR">APR</option>
	                                                            <option value="MAY">MAY</option>
	                                                            <option value="7">7</option>
	                                                            <option value="8">8</option>
	                                                            <option value="9">9</option>
	                                                            <option value="10">10</option>
	                                                            <option value="11">11</option>
	                                                            <option value="12">12</option>
	                                                        </select>
	                                                    month and 
	                                                        <select>
	                                                            <option value="YYYY">YYYY</option>
	                                                            <option value="2015">2015</option>
	                                                            <option value="2016">2016</option>
	                                                        </select>
	                                                        year.  </label>
	                                                </div>
	                                            </div>
	
	                                            <!-- Attached CV's and other Documents -->
	                                            <div class="form-group">
	                                                <label for="inputFileCV" class="col-sm-2 control-label">Applicant CV<span class="required">*</span>:<br /><small>Word</small></label>
	                                                <div class="col-sm-4">
	                                                    <input id="inputFileCV" type="file" class="form-control" />
	                                                </div>
	                                                <div class="col-sm-6 control-display">
	                                                    <p class="text-muted">Request updated CV if attached here is not updated.</p>
	                                                </div>
	                                            </div>
	
	                                            <!-- Attached CV's and other Documents -->
	                                            <div class="form-group">
	                                                <label for="inputFileCertificate" class="col-sm-2 control-label">CV and Certificate<span class="required">*</span>:</label>
	                                                <div class="col-sm-4">
	                                                    <input id="inputFileCertificate" type="file" class="form-control" />
	                                                </div>
	                                                <div class="col-sm-6 control-display">
	                                                    <p class="text-muted">Request updated CV if attached here is not updated.</p>
	                                                </div>
	                                            </div>
	                                            
	                                            <!-- Bottom Part -->
	                                            <div class="form-group">
	                                                <!--<label class="col-sm-2 control-label">Gender<span class="required">*</span>:</label>-->
	                                                <div class="col-sm-10 col-sm-offset-2">
	                                                    <label class="user-status"><input type="radio" class="flat-blue" name="last-status" value="Walk-in" /> Walk-in</label>
	                                                    <label class="user-status"><input type="radio" class="flat-blue" name="last-status" value="not-reporting" /> For issuance of computer number only. (Applicant did not report yet)</label>
	                                                </div>
	                                            </div>
	                                            
	                                        </div>
	                                    </div>
	
	                                </div>
	                                
	                                <div class="box-footer">
	                                    
	                                    <div class="row">
	                                        <div class="col-md-10 col-md-offset-1">
	
	                                            <div class="row">
	                                                <div class="col-md-3 col-md-offset-2 col-sm-4 col-sm-offset-2 col-xs-9">
	                                                    <a href="javascript:void(0);" class="btn btn-primary btn-block btn-flat" id="btnSubmit">Save</a>
	                                                </div>
	                                                <div class="col-md-2 col-sm-3 col-xs-3">
	                                                    <a href="../dashboard/dashboard.php" type="button" class="btn btn-block btn-default btn-flat" id="btnCancel">Cancel</a>
	                                                </div>
	                                            </div>
	                                            
	                                        </div>
	                                    </div>
	                                    
	                                </div>
								</form>
                            </div>
                            <!-- end of CREATE NEW APPLICATION -->
                        
                        </form>
                        <!-- end of CREATE NEW APPLICATION FORM -->
                        
                    </div>
                    
                </div>
            
            </section>
            <!-- end of MAIN CONTENT -->