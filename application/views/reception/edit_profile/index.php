            <!-- MAIN CONTENT -->
            <section class="content">
            
                <div class="row">
                    
                    <div class="col-md-12">
            
                        <!-- APPLICANT REPORT TODAY -->
                        <div class="box box-no-header with-border">
                            <!--<div class="box-header">
                                <h3 class="box-title">Applicant</h3>
                                <a href="#" class="btn btn-default btn-sm btn-flat btn-add-new pull-right" data-toggle="modal" data-target="#modalMessageBox"><i class="fa fa-comments-o"></i> Message Box</a>
                                <a href="#" class="btn btn-default btn-sm btn-flat pull-right" data-toggle="modal" data-target="#modalSearch"><i class="fa fa-search"></i> Find Applicant</a>
                            </div>-->
                            <div class="box-body">
                                
                                <!-- APPLICANT PROFILE -->
                                <div class="row">
                                    <div class="col-md-12">
                                        
                                        <div class="row">
                                        
                                            <div class="col-md-3 col-sm-6 col-xs-6">
                                                <div class="applicant-main-profile">
                                                    
                                                    <h4 class="status status-active">ACTIVE</h4>
                                                    <img src="<?=base_url();?>public/images/applicant-thumbnail.png" width="90%" class="applicant-thumbnail" />
                                                    <p class="text-center text-muted"><small>Set / Change Applicant Photo</small></p>
                                                    <input type="file" id="filePhoto" class="form-control" />
                                                    <button class="btn btn-sm btn-primary btn-block btn-flat">Upload Photo</button>
                                                </div>
                                            </div>
                                            			
                                            <div class="col-md-9 col-sm-12 col-xs-12">
                                                
                                                <form class="form-horizontal applicant-profile">
                                                    
                                                <div class="form-group form-group-heading">
                                                    <label class="col-sm-3 col-sm-offset-6 control-label">Application Date:</label>
                                                    <div class="col-sm-3 control-display">January 08, 2016</div>
                                                </div>
                                                    
                                                <!-- Name -->
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Applicant No.:</label>
                                                    <div class="col-sm-10 control-display">A-1234-56</div>
                                                </div>
                                                    
                                                <!-- Name -->
                                                <div class="form-group">
                                                    <label for="inputLastName" class="col-sm-2 control-label">Name :</label>
                                                    <div class="col-sm-4"><input type="text" class="form-control" id="inputLastName" placeholder="Last Name" value="Saul" /></div>
                                                    <div class="col-sm-4"><input type="text" class="form-control" id="inputFirstName" placeholder="First Name" value="Karl Gerald" /></div>
                                                    <div class="col-sm-2"><input type="text" class="form-control" id="inputMiddleName" placeholder="M.I" value="S" /></div>
                                                </div>
                                                    
                                                <div class="form-group">
                                                    <label for="inputLastName" class="col-sm-2 control-label"></label>
                                                    <div class="col-sm-2">
                                                        <label class="user-status"><input type="checkbox" class="flat-blue" name="JR" value="JR" /> Jr. </label>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <label class="user-status"><input type="checkbox" class="flat-blue" name="JR" value="JR" /> No Middle Name </label>
                                                    </div>
                                                </div>
                                                    
                                                <!-- Birthday -->
                                                <div class="form-group">
                                                    <label for="selectBirthMonth" class="col-sm-2 control-label">Birthday<span class="required">*</span>:</label>
                                                    <div class="col-sm-2">
                                                        <select id="selectBirthMonth" class="form-control">
                                                            <option value="">MM</option>
                                                            <option value="01">01</option>
                                                            <option value="02" selected>02</option>
                                                            <option value="03">03</option>
                                                            <option value="04">04</option>
                                                            <option value="05">05</option>
                                                            <option value="06">06</option>
                                                            <option value="07">07</option>
                                                            <option value="08">08</option>
                                                            <option value="09">09</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <select id="selectBirthDay" class="form-control">
                                                            <option value="">DD</option>
                                                            <option value="01">01</option>
                                                            <option value="02">02</option>
                                                            <option value="03">03</option>
                                                            <option value="04">04</option>
                                                            <option value="05">05</option>
                                                            <option value="06">06</option>
                                                            <option value="07">07</option>
                                                            <option value="08">08</option>
                                                            <option value="09">09</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                            <option value="13">13</option>
                                                            <option value="14">14</option>
                                                            <option value="15">15</option>
                                                            <option value="16">16</option>
                                                            <option value="17">17</option>
                                                            <option value="18">18</option>
                                                            <option value="19">19</option>
                                                            <option value="20">20</option>
                                                            <option value="21">21</option>
                                                            <option value="22">22</option>
                                                            <option value="23" selected>23</option>
                                                            <option value="24">24</option>
                                                            <option value="25">25</option>
                                                            <option value="26">26</option>
                                                            <option value="27">27</option>
                                                            <option value="28">28</option>
                                                            <option value="29">29</option>
                                                            <option value="30">30</option>
                                                            <option value="31">31</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select id="selectBirthYear" class="form-control">
                                                            <option value="">YYYY</option>
                                                            <option value="1956">1956</option>
                                                            <option value="1957">1957</option>
                                                            <option value="1958">1958</option>
                                                            <option value="1959">1959</option>
                                                            <option value="1960">1960</option>
                                                            <option value="1961">1961</option>
                                                            <option value="1962">1962</option>
                                                            <option value="1963">1963</option>
                                                            <option value="1964">1964</option>
                                                            <option value="1965">1965</option>
                                                            <option value="1966">1966</option>
                                                            <option value="1967">1967</option>
                                                            <option value="1968">1968</option>
                                                            <option value="1969">1969</option>
                                                            <option value="1970">1970</option>
                                                            <option value="1971">1971</option>
                                                            <option value="1972">1972</option>
                                                            <option value="1973">1973</option>
                                                            <option value="1974">1974</option>
                                                            <option value="1975">1975</option>
                                                            <option value="1976">1976</option>
                                                            <option value="1977">1977</option>
                                                            <option value="1978">1978</option>
                                                            <option value="1979">1979</option>
                                                            <option value="1980">1980</option>
                                                            <option value="1981">1981</option>
                                                            <option value="1982">1982</option>
                                                            <option value="1983">1983</option>
                                                            <option value="1984">1984</option>
                                                            <option value="1985">1985</option>
                                                            <option value="1986">1986</option>
                                                            <option value="1987" selected>1987</option>
                                                            <option value="1988">1988</option>
                                                            <option value="1989">1989</option>
                                                            <option value="1990">1990</option>
                                                            <option value="1991">1991</option>
                                                            <option value="1992">1992</option>
                                                            <option value="1993">1993</option>
                                                            <option value="1994">1994</option>
                                                            <option value="1995">1995</option>
                                                            <option value="1996">1996</option>
                                                            <option value="1997">1997</option>
                                                            <option value="1998">1998</option>
                                                            <option value="1999">1999</option>
                                                            <option value="2000">2000</option>
                                                            <option value="2001">2001</option>
                                                            <option value="2002">2002</option>
                                                            <option value="2003">2003</option>
                                                            <option value="2004">2004</option>
                                                            <option value="2005">2005</option>
                                                            <option value="2006">2006</option>
                                                            <option value="2007">2007</option>
                                                            <option value="2008">2008</option>
                                                            <option value="2009">2009</option>
                                                            <option value="2010">2010</option>
                                                            <option value="2011">2011</option>
                                                            <option value="2012">2012</option>
                                                            <option value="2013">2013</option>
                                                            <option value="2014">2014</option>
                                                            <option value="2015">2015</option>
                                                            <option value="2016">2016</option>
                                                            <option value="2017">2017</option>
                                                            <option value="2018">2018</option>
                                                            <option value="2019">2019</option>
                                                            <option value="2020">2020</option>
                                                            <option value="2021">2021</option>
                                                            <option value="2022">2022</option>
                                                            <option value="2023">2023</option>
                                                            <option value="2024">2024</option>
                                                            <option value="2025">2025</option>
                                                            <option value="2026">2026</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            
                                                <!-- Gender -->
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Gender<span class="required">*</span>:</label>
                                                    <div class="col-sm-10">
                                                        <label class="user-status"><input type="radio" class="flat-blue" name="gender" value="male" checked="checked" /> Male</label>
                                                        <label class="user-status"><input type="radio" class="flat-blue" name="gender" value="female" /> Female</label>
                                                    </div>
                                                </div>

                                                <!-- Contact Info -->
                                                <div class="form-group">
                                                    <label for="inputMobile" class="col-sm-2 control-label">Mobile No.<span class="required">*</span>:</label>
                                                    <div class="col-sm-4">
                                                        <input type="text" class="form-control" id="inputMobile" placeholder="####-###-####" value="0923-934-0439" />
                                                    </div>
                                                    <label for="inputOtherTel" class="col-sm-2 control-label">Other Tel. No.:</label>
                                                    <div class="col-sm-4">
                                                        <input type="text" class="form-control" id="inputOtherTel" placeholder="####-###-####" value="0936-866-4464" />
                                                    </div>
                                                </div>

                                                <!-- Email -->
                                                <div class="form-group">
                                                    <label for="inputEmail" class="col-sm-2 control-label">Email<span class="required">*</span>:</label>
                                                    <div class="col-sm-4">
                                                        <input type="text" class="form-control" id="inputEmail" placeholder="" value="kaiserkagesa@gmail.com" />
                                                    </div>
                                                </div>
                                                    
                                                <!-- Method & Source -->
                                                <div class="form-group">
                                                    <label for="selectMethods" class="col-sm-2 control-label">Methods of Application<span class="required">*</span>:</label>
                                                    <div class="col-sm-4">
                                                        <select id="selectMethods" class="form-control">
                                                            <option value="Online">Online</option>
                                                        </select>
                                                    </div>
                                                    <label for="selectSource" class="col-sm-2 control-label">Source of Application<span class="required">*</span>:</label>
                                                    <div class="col-sm-4">
                                                        <select id="selectSource" class="form-control">
                                                            <option value="East West Website">East West Website</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <!-- Work Availability -->
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Work Availability<span class="required">*</span>:</label>
                                                    <div class="col-sm-10">
                                                        <label class="user-status"><input type="radio" class="flat-blue" name="availability" value="asap" checked="checked"/> I can start for work as soon as possible. </label>
                                                        <br />
                                                        <label class="user-status"><input type="radio" class="flat-blue" name="availability" value="by-month" /> I can start for work 
                                                            <select>
                                                                <option value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            </select>
                                                            month(s) after the notice.  </label>
                                                        <br />
                                                        <label class="user-status"><input type="radio" class="flat-blue" name="availability" value="next-job" /> Im looking for my next job. I can start work after
                                                            <select>
                                                                <option value="MM">MM</option>
                                                                <option value="JAN">JAN</option>
                                                                <option value="FEB">FEB</option>
                                                                <option value="MAR">MAR</option>
                                                                <option value="APR">APR</option>
                                                                <option value="MAY">MAY</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            </select>
                                                        month and 
                                                            <select>
                                                                <option value="YYYY">YYYY</option>
                                                                <option value="2015">2015</option>
                                                                <option value="2016">2016</option>
                                                            </select>
                                                            year.  </label>
                                                    </div>
                                                </div>

                                                <!-- Attached CV's and other Documents -->
                                                <div class="form-group">
                                                    <label for="inputFileCV" class="col-sm-2 control-label">Applicant CV<span class="required">*</span>:<br /><small>Word</small></label>
                                                    <div class="col-sm-4">
                                                        <input id="inputFileCV" type="file" class="form-control" />
                                                    </div>
                                                    <div class="col-sm-6 control-display">
                                                        <p class="text-primary">my-resume.doc <a href="#" class="btn btn-link-danger btn-flat" data-toggle="tooltip" data-placement="top" data-title="Delete"><i class="fa fa-trash"></i></a></p>
                                                    </div>
                                                </div>

                                                <!-- Attached CV's and other Documents -->
                                                <div class="form-group">
                                                    <label for="inputFileCertificate" class="col-sm-2 control-label">CV and Certificate<span class="required">*</span>:</label>
                                                    <div class="col-sm-4">
                                                        <input id="inputFileCertificate" type="file" class="form-control" />
                                                    </div>
                                                    <div class="col-sm-6 control-display">
                                                        <p class="text-primary">my-resume.pdf <a href="#" class="btn btn-link-danger btn-flat" data-toggle="tooltip" data-placement="top" data-title="Delete"><i class="fa fa-trash"></i></a></p>
                                                    </div>
                                                </div>

                                                <!-- Bottom Part -->
                                                <div class="form-group">
                                                    <!--<label class="col-sm-2 control-label">Gender<span class="required">*</span>:</label>-->
                                                    <div class="col-sm-10 col-sm-offset-2">
                                                        <label class="user-status"><input type="radio" class="flat-blue" name="last-status" value="Walk-in" checked="checked"/> Walk-in</label>
                                                        <label class="user-status"><input type="radio" class="flat-blue" name="last-status" value="not-reporting" /> For issuance of computer number only. (Applicant did not report yet)</label>
                                                    </div>
                                                </div>

                                                    
                                                    
                                                    
                                            </form>
                                                
                                            </div>
                                            
                                        </div>
                                        
                                    </div>
                                </div>
                                <!-- end of APPLICANT PROFILE -->
                            </div>
                        </div>
                        <!-- end of APPLICANT REPORT TODAY -->
                        
                        <?=$applicant_link_menu;?>
                        
                        <!-- <div class="btn-group applicant-btn-group" role="group" aria-label="...">
                            <a href="../applicant/applicant-report-today.php" class="btn-link">Reported Today</a>
                            <a href="../applicant/applicant-overview.php" class="btn-link">Overview</a>
                            <a href="../applicant/applicant-personal.php" class="btn-link">Personal</a>
                            <a href="../applicant/applicant-education.php" class="btn-link">Education</a>
                            <a href="../applicant/applicant-licenses-certification.php" class="btn-link">Licenses / Certifications</a>
                            <a href="../applicant/applicant-work-history.php" class="btn-link">Work History</a>
                            <a href="../applicant/applicant-reference.php" class="btn-link">Reference</a>
                            <a href="../applicant/applicant-training-seminar.php" class="btn-link">Training / Seminars</a>
                            <a href="../applicant/applicant-trade-test.php" class="btn-link">Trade Test</a>
                            <a href="../applicant/applicant-info-edit.php" class="btn-link active">Edit Profile</a>
                            <a href="../applicant/applicant-cv-tracker.php" class="btn-link">CV Trakcer</a>
                            <a href="../applicant/applicant-position-applied.php" class="btn-link">Position Applied</a>
                            <a href="../applicant/applicant-assessment.php" class="btn-link">Assessment</a>
                            <a href="../applicant/applicant-lineup.php" class="btn-link">Line-up</a>
                            <a href="../applicant/applicant-selections.php" class="btn-link">Selections</a>
                            <a href="../applicant/applicant-advisory.php" class="btn-link">Advisory</a>
                            <a href="../applicant/applicant-history.php" class="btn-link">History</a>
                        </div> -->
                        
                        <!-- APPLICANT REPORT TODAY -->
                        <div class="box with-border">
                            <div class="box-header">
                                <h3 class="box-title" data-widget="collapse">Personal Information</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                                    <!--<button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>-->
                                </div>
                            </div>
                            <div class="box-body">
                                
                                <!-- PERSONAL INFORMATION -->
                                <div class="row">
                                    <div class="col-md-12">
                                        
                                        <div class="applicant-main-profile">
                                            <form class="form-horizontal application-profile">
                                            

                                                <!-- Present Address -->
                                                <div class="form-group">
                                                    <label for="txtPresentAddress" class="col-sm-2 control-label">Present Address<span class="required">*</span>:</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" class="form-control" id="txtPresentAddress" value="Phase 1, Block 2, Lot 23, Makaturing Street, Brgy. Manresa, Quezon City" />
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="selectPresentAddMunicipality" class="col-sm-2 control-label">Present Municipality<span class="required">*</span>:</label>
                                                    <div class="col-sm-4">
                                                        <select id="selectPresentAddMunicipality" class="form-control"></select>
                                                    </div>
                                                </div>
                                                
                                                <!-- Permanent Address -->
                                                <div class="form-group">
                                                    <label for="txtPermanentAddress" class="col-sm-2 control-label">Permanent Address<span class="required">*</span>:</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" class="form-control" id="txtPermanenttAddress" value="Phase 1, Block 2, Lot 23, Makaturing Street, Brgy. Manresa, Quezon City" />
                                                        <label class="user-status pull-left"><input type="checkbox" class="flat-blue" name="address" value="same" /> Same as Present Address</label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="selectPermanentAddMunicipality" class="col-sm-2 control-label">Permanent Municipality<span class="required">*</span>:</label>
                                                    <div class="col-sm-4">
                                                        <select id="selectPermanentAddMunicipality" class="form-control"></select>
                                                    </div>
                                                </div>
                                                
                                                <!-- Branches -->
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Applied Branch:</label>
                                                    <div class="col-sm-4 control-display text-left">MNL - Manila</div>
                                                    <label for="selectNearBranch" class="col-sm-2 control-label">Near Branch:</label>
                                                    <div class="col-sm-4">
                                                        <select id="selectNearBranch" class="form-control">
                                                            <option value=""></option>
                                                            <option value="104"> BCD - BACOLOD &nbsp;  </option>
                                                            <option value="102"> BTG - BATANGAS &nbsp;  </option>
                                                            <option value="2" selected=""> CBU - CEBU &nbsp;  </option>
                                                            <option value="101"> CDO - CAGAYAN DE ORO &nbsp;  </option>
                                                            <option value="103"> DVO - DAVAO &nbsp;  </option>
                                                            <option value="105"> ILO - ILOILO &nbsp;  </option>
                                                            <option value="1"> MNL - MANILA &nbsp;  </option>
                                                            <option value="106"> ZMB - ZAMBOANGA &nbsp; </option>
                                                        </select>
                                                    </div>
                                                </div>
                                                
                                                <!-- Tel Num & Skype -->
                                                <div class="form-group">
                                                    <label for="selectTelNum" class="col-sm-2 control-label">Tel. No.:</label>
                                                    <div class="col-sm-1">
                                                        <select id="selectTelNum" class="form-control">
                                                            <option value="02">02</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <input type="text" id="inputTelNum" class="form-control" />
                                                    </div>
                                                    <label for="inputSkype" class="col-sm-2 control-label">Skype:</label>
                                                    <div class="col-sm-4">
                                                        <input type="text" id="inputSkype" class="form-control" placeholder="Skuype" value="ewpci.karl" />
                                                    </div>
                                                </div>
                                                
                                                <!-- Religion -->
                                                <div class="form-group">
                                                    <label for="selectReligion" class="col-sm-2 control-label">Religion</label>
                                                    <div class="col-sm-4">
                                                        <select id="selectReligion" class="form-control">
                                                            <option value="Muslim">Muslim</option>
                                                            <option value="Non-Muslim">Non-Muslim</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                
                                                <!-- Height & Weight -->
                                                <div class="form-group">
                                                    <label for="selectHeight" class="col-sm-2 control-label">Height<span class="required">*</span>:</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" id="inputHeight" class="form-control" />
                                                    </div>
                                                    <div class="col-sm-1">
                                                        <select id="selectHeightConvertion" class="form-control">
                                                            <option value="ft">ft.</option>
                                                            <option value="cm">cm.</option>
                                                        </select>
                                                    </div>
                                                    <label for="selectWeight" class="col-sm-2 control-label">Weight<span class="required">*</span>:</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" id="inputWeight" class="form-control" />
                                                    </div>
                                                    <div class="col-sm-1">
                                                        <select id="selectWeightConvertion" class="form-control">
                                                            <option value="lbs">lbs.</option>
                                                            <option value="kg">kg.</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                
                                                <!-- Civil Status & Birth Place -->
                                                <div class="form-group">
                                                    <label for="selectCivilStatus" class="col-sm-2 control-label">Civil Status<span class="required">*</span>:</label>
                                                    <div class="col-sm-4">
                                                        <select id="selectCivilStatus" class="form-control">
                                                            <option value=""></option>
                                                            <option value="Single">Single</option>
                                                            <option value="Married" selected="">Married</option>
                                                            <option value="Widowed">Widowed</option>
                                                            <option value="Separated">Separated</option>
                                                        </select>
                                                    </div>
                                                    <label for="inputBirthPlace" class="col-sm-2 control-label">Birth Place<span class="required">*</span>:</label>
                                                    <div class="col-sm-4">
                                                        <input type="text" id="inputBirthPlace" class="form-control" placeholder="Birth Place" value="Sta. Cruz, Metro Manila" />
                                                    </div>
                                                </div>
                                                    
                                                <!-- Spouse Name -->
                                                <div class="form-group">
                                                    <label for="inputSpouseLastName" class="col-sm-2 control-label">Spouse<span class="required">*</span>:</label>
                                                    <div class="col-sm-4"><input type="text" class="form-control" id="inputSpouseLastName" placeholder="Last Name" value="Nabong" /></div>
                                                    <div class="col-sm-4"><input type="text" class="form-control" id="inputSpouseFirstName" placeholder="First Name" value="Rona" /></div>
                                                    <div class="col-sm-2"><input type="text" class="form-control" id="inputSpouseMiddleName" placeholder="M.I" value="C" /></div>
                                                </div>
                                                    
                                                <!-- Father Name -->
                                                <div class="form-group">
                                                    <label for="inputFatherLastName" class="col-sm-2 control-label">Father<span class="required">*</span>:</label>
                                                    <div class="col-sm-4"><input type="text" class="form-control" id="inputFatherLastName" placeholder="Last Name" value="Saul" /></div>
                                                    <div class="col-sm-4"><input type="text" class="form-control" id="inputFatherFirstName" placeholder="First Name" value="Wilfredo" /></div>
                                                    <div class="col-sm-2"><input type="text" class="form-control" id="inputFatherMiddleName" placeholder="M.I" value="P" /></div>
                                                </div>
                                                    
                                                <!-- Mother Name -->
                                                <div class="form-group">
                                                    <label for="inputMotherLastName" class="col-sm-2 control-label">Mother<span class="required">*</span><br /><small>Maiden's Name</small>:</label>
                                                    <div class="col-sm-4"><input type="text" class="form-control" id="inputMotherLastName" placeholder="Last Name" value="Sarmiento" /></div>
                                                    <div class="col-sm-4"><input type="text" class="form-control" id="inputMotherFirstName" placeholder="First Name" value="Beviana" /></div>
                                                    <div class="col-sm-2"><input type="text" class="form-control" id="inputMotherMiddleName" placeholder="M.I" value="P" /></div>
                                                </div>
                                                
                                            </form>
                                        </div>
                                        
                                    </div>
                                </div>
                                <!-- PERSONAL INFORMATION -->
                                
                            </div>
                            <div class="box-footer">
                                <div class="row">
                                    <div class="col-sm-3 col-sm-offset-9 col-xs-12">
                                        <div class="btn btn-primary btn-block btn-flat">Update</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        

                        <!-- BENEFICIARIES -->
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title" data-widget="collapse"><i class="fa fa-users"></i> Beneficiaries</h3>
                                <div class="box-tools pull-right">
                                    <a href="#" class="btn btn-default btn-xs btn-flat btn-box-add" data-toggle="modal" data-target="#modalAddBeneficiaries"><i class="ion-plus"></i> Add Data</a>
                                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-plus"></i></button>
                                </div>
                            </div>
                            <div class="box-body">
                                <ul class="profile-info-list">
                                    <li>
                                        <div class="btn-group pull-right">
                                            <button type="button" class="btn btn-sm btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></button>
                                            <button type="button" class="btn btn-sm btn-danger btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash"></i></button>
                                        </div>
                                        <h3><strong>Saul, Rona Nabong</strong></h3>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <p><i class="fa fa-user-plus"></i> Spouse</p>
                                            </div>
                                            <div class="col-md-3">
                                                <p><i class="fa fa-birthday-cake"></i> March 20, 1987</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="btn-group pull-right">
                                            <button type="button" class="btn btn-sm btn-default btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></button>
                                            <button type="button" class="btn btn-sm btn-danger btn-flat" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash"></i></button>
                                        </div>
                                        <h3><strong>Saul, Merrie Brielle Nabong</strong></h3>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <p><i class="fa fa-user-plus"></i> Daughter</p>
                                            </div>
                                            <div class="col-md-3">
                                                <p><i class="fa fa-birthday-cake"></i> December 12, 2015</p>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="box-footer">
                                <ul class="data-logs" id="datalogsBeneficiaries">
                                    <li><small>Jan 8, 2016 9:39 pm - edit Through Online Application</small></li>
                                    <li><small>Jan 5, 2016 7:19 am - edit Through Online Application</small></li>
                                    <li><small>Dec 24, 2015 3:38 pm - add Through Online Application</small></li>
                                    <li>
                                        <button href="#" class="btn btn-xs btn-default btn-flat show-logs"><small><i class="ion-plus"></i> Show All</small></button>
                                        <button href="#" class="btn btn-xs btn-default btn-flat hide-logs" style="display:none;"><small><i class="ion-minus"></i> Show Less</small></button>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- end of BENEFICIARIES -->


                    </div>
                    
                </div>
                
            </section>
            <!-- end of MAIN CONTENT -->