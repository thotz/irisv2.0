            <!-- MAIN CONTENT -->
            <section class="content">
            
                <div class="row">
                    <div class="col-md-12">
            
						<?=$applicant_information;?>
						
						<?=$applicant_link_menu;?>
                        
                        <!-- <div class="btn-group applicant-btn-group" role="group" aria-label="...">
                            <a href="../applicant/applicant-report-today.php" class="btn-link active">Reported Today</a>
                            <a href="../applicant/applicant-overview.php" class="btn-link">Overview</a>
                            <a href="../applicant/applicant-personal.php" class="btn-link">Personal</a>
                            <a href="../applicant/applicant-education.php" class="btn-link">Education</a>
                            <a href="../applicant/applicant-licenses-certification.php" class="btn-link">Licenses / Certifications</a>
                            <a href="../applicant/applicant-work-history.php" class="btn-link">Work History</a>
                            <a href="../applicant/applicant-reference.php" class="btn-link">Reference</a>
                            <a href="../applicant/applicant-training-seminar.php" class="btn-link">Training / Seminars</a>
                            <a href="../applicant/applicant-trade-test.php" class="btn-link">Trade Test</a>
                            <a href="../applicant/applicant-info-edit.php" class="btn-link">Edit Profile</a>
                            <a href="../applicant/applicant-cv-tracker.php" class="btn-link">CV Trakcer</a>
                            <a href="../applicant/applicant-position-applied.php" class="btn-link">Position Applied</a>
                            <a href="../applicant/applicant-assessment.php" class="btn-link">Assessment</a>
                            <a href="../applicant/applicant-lineup.php" class="btn-link">Line-up</a>
                            <a href="../applicant/applicant-selections.php" class="btn-link">Selections</a>
                            <a href="../applicant/applicant-advisory.php" class="btn-link">Advisory</a>
                            <a href="../applicant/applicant-history.php" class="btn-link">History</a>
                        </div> -->
                        
                        <!-- LINE UP HISTORY -->
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title" data-widget="collapse">Line Up History</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                                </div>
                            </div>
                            <div class="box-body">
                                
                                <table class="table table-bordered table-hover table-stripe table-condensed">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <!-- <th>Applicant Status</th> -->
                                            <th>MR Refno</th>
                                            <th>Category Selected</th>
                                            <th>MR Status</th>
                                            <th>Category Status</th>
                                            <th>Interview Date</th>
                                            <th>Select Date</th>
                                            <th>Salary</th>
                                            <th>Selection Status</th>
                                            <th>Acceptance</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($lineup_history as $key=>$value) {
                                    	$salary = (intval($value['salary'])>0) ? $value['code']." ".$value['salary'].$value['currency_per'] : "&nbsp;";
                                    	$interview_date = ($value['interview_date']!='0000-00-00' && $value['interview_date']!='') ? dateformat($value['interview_date']) : "&nbsp;";
                                    	$select_date = ($value['select_date']!='0000-00-00' && $value['select_date']!='') ? dateformat($value['select_date']) : "&nbsp;";
                                    	?>
                                        <tr title="<?=$value['principal_company']?>">
                                            <td><?=$key + 1?>.</td>
                                            <!-- <td>ACTIVE</td> -->
                                            <td><?=$value['ref_no']?></td>
                                            <td><?=$value['position']?></td>
                                            <td><?=$value['mr_status']?></td>
                                            <td><?=$value['cat_status']?></td>
                                            <td><?=$interview_date?></td>
                                            <td><?=$select_date?></td>
                                            <td><?=$salary?></td>
                                            <td><?=$value['interview_status']?></td>
                                            <td><?=$value['acceptance']?></td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                                
                            </div>
                        </div>
                        <!-- end of LINE UP HISTORY -->
                        
                        <!-- FORMS -->
                        <div class="box">
                            
                            <form class="form-horizontal applicant-profile">
                                <div class="box-header with-border">
                                    <h3 class="box-title" data-widget="collapse">Follow Up</h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                                    </div>
                                </div>
                                <div class="box-body">

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Last Reporting Date :</label>
                                            <div class="col-sm-9 control-display">January 08, 2016</div>
                                        </div>
                                        <div class="form-group">
                                            <label for="selectFollowUpTemplate" class="col-sm-3 control-label">Follow-up Template :</label>
                                            <div class="col-sm-4">
                                                <select id="selectFollowUpTemplate" class="form-control">
                                                    <option value="">Please Select</option>
                                                    <option value="4">REPORTED:</option>
                                                    <option value="2">RECEIVED CALL:</option>
                                                    <option value="8">RECEIVED EMAIL:</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="txtFollowUpRemarks" class="col-sm-3 control-label">Follow-up Remarks :</label>
                                            <div class="col-sm-6">
                                                <textarea id="txtFollowUpRemarks" class="form-control"></textarea>
                                            </div>
                                        </div>

                                </div>
                                <div class="box-footer">
                                    <div class="row">
                                        <div class="col-md-2 col-md-offset-3 col-sm-3 col-sm-offset-3">
                                            <a href="#" class="btn btn-primary btn-block btn-flat">Save</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- end of FORMS -->
                        
                    </div>
                    
                </div>
                
            </section>
            <!-- end of MAIN CONTENT -->