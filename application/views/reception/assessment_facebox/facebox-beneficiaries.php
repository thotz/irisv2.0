            <div class="modal-body">
                
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modalAddLicenseCertificateLabel"><i class="fa fa-certificate"></i> Beneficiaries</h4>
                
                <form class="form-horizontal" method=post action='#' name='form_beneficiaries' id='form_beneficiaries'>
                	<input type="hidden" name="applicant_id" id="applicant_id" value="<?=$applicant_id?>">
                	<input type="hidden" name="id" id="id" value="<?=$id?>">

	                <div class="box-body">
	                        
	                    <div class="form-group">
	                        <label for="checkboxStatus" class="col-sm-4 control-label"></label>
	                        <div class="col-sm-8">
	                            <label class="user-status"><input type="checkbox" id="is_na_beneficiaries" name="is_na_beneficiaries" onclick="fn_for_na(this);" class="flat-blue" value="0" /> <strong>Not Applicable</strong> (No Training/Seminar)</label>
	                        </div>
	                    </div>
	                        
	                    <hr />
	                    
	                    <div class="form-group">
	                        <label for="inputLCNum" class="col-sm-4 control-label">Last Name<span class="required">*</span>:</label>
	                        <div class="col-sm-8">
	                            <input type="text" id="lname" name="lname" class="form-control" value="<?=isset($result['lname']) ? $result['lname'] : ''?>" placeholder="" />
	                        </div>
	                    </div>
	                    
	                    <div class="form-group">
	                        <label for="inputLCTaken" class="col-sm-4 control-label">First Name<span class="required">*</span>:</label>
	                        <div class="col-sm-8">
	                            <input type="text" id="fname" name="fname" class="form-control" value="<?=isset($result['fname']) ? $result['fname'] : ''?>" placeholder="" />
	                        </div>
	                    </div>
	                    
	                    <div class="form-group">
	                        <label for="inputLCNum" class="col-sm-4 control-label">Middle Name<span class="required">*</span>:</label>
	                        <div class="col-sm-8">
	                            <input type="text" id="mname" name="mname" class="form-control" value="<?=isset($result['mname']) ? $result['mname'] : ''?>" placeholder="" />
	                        </div>
	                    </div>
	                    
	                    <div class="form-group">
	                        <label for="inputDepartment" class="col-sm-4 control-label">Nature of Project Experience<span class="required">*</span>:</label>
	                        <div class="col-sm-8">
	                        	<select id="relationship" name="relationship" class="form-control">
	                        	<?=$select_relationship?>
	                        	</select>
	                        </div>
	                    </div>
	                    
	                    <div class="form-group">
	                        <label for="selectDateTakenMonth" class="col-sm-4 control-label">Date Taken<span class="required">*</span>:</label>
							<?php
								if (isset($result["bday"]) && $result["bday"]){
									list ($year, $month, $day) = @split ('[-]', $result['bday']);
								} else {
									$year="";$month="";$day="";
								}
							?>

	                        <div class="col-sm-3">
	                            <?=dateselectmonth("month", $month, 'class="form-control"');?>
	                        </div>
	                        <div class="col-sm-2">
	                            <?=dateselectday("day", $day, 'class="form-control"');?>
	                        </div>
	                        <div class="col-sm-3">
	                            <?=dateselectyear("year", $year, 'class="form-control"');?>
	                        </div>
	                    </div>
	                    
	                </div>
                </form>
                
            </div>
            
            <div class="modal-footer">
                <div class="row">
                    <div class="col-sm-3 col-sm-offset-6">
                        <a href="#" type="button" id="btn_submit_asses" name="btn_submit_asses" class="btn btn-sm btn-flat btn-block btn-primary"onclick="assessment_update('beneficiaries'); return false;" ><?=$btn_value?></a>
                    </div>
                    <div class="col-sm-3">
                        <a href="#" type="button" id="btn_close" name="btn_close" class="btn btn-sm btn-flat btn-block btn-default" onclick="jQuery(document).trigger('close.facebox'); return false;">Close</a>
                    </div>
		            <div class="modal-body" id="on_processing_display" style="display:none;">
		               <img src="<?=base_url()?>public/images/load.png" class="fa-spin">&nbsp; processing please wait ...
		 			</div>
                </div>
            </div>