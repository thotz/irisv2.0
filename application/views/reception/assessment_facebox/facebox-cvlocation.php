            <div class="modal-body">
                
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modalAddStatusInterviewLabel"><i class="fa fa-comments"></i> Status of Interview</h4>
                
                <form class="form-horizontal" method=post action='#' name='form_cvlocation' id='form_cvlocation'>
                	<input type="hidden" name="applicant_id" id="applicant_id" value="<?=$applicant_id?>">
                	<input type="hidden" name="id" id="id" value="<?=$id?>">

	                <div class="box-body">
	                    
	                    <div class="form-group">
	                        <label for="selectEvaluator" class="col-sm-4 control-label">Evaluator<span class="required">*</span>:</label>
	                        <div class="col-sm-4">
	                            <select class="form-control" id="user_id_dcv" name="user_id">
	                                <?=$select_evaluator?>
	                            </select>
	                        </div>
	                    </div>
	                    
	                    <div class="form-group">
	                        <label for="selectMode" class="col-sm-4 control-label">Mode<span class="required">*</span>:</label>
	                        <div class="col-sm-2">
	                            <select class="form-control" name="status_dcv" id="status_dcv">
	                                <?=$mode?>
	                            </select>
	                        </div>
	                    </div>
	                    
	                    <div class="form-group">
	                        <label for="textInternalAdvisory" class="col-sm-4 control-label">Internal Advisory<span class="required">*</span>:</label>
	                        <div class="col-sm-8">
	                            <textarea class="form-control" id="remarks" name="remarks"><?=isset($result['remarks']) ? $result['remarks'] : ''?></textarea>
	                        </div>
	                    </div>
	                    
	                </div>
                </form>
                
            </div>
            
            <div class="modal-footer">
                <div class="row">
                    <div class="col-sm-3 col-sm-offset-6">
                        <a href="#" type="button" id="btn_submit_asses" name="btn_submit_asses" class="btn btn-sm btn-flat btn-block btn-primary" onclick="assessment_update_cvlocation(); return false;" ><?=$btn_value?></a>
                    </div>
                    <div class="col-sm-3">
                        <a href="#" type="button" id="btn_close" name="btn_close" class="btn btn-sm btn-flat btn-block btn-default" onclick="jQuery(document).trigger('close.facebox'); return false;">Close</a>
                    </div>
		            <div class="modal-body" id="on_processing_display" style="display:none;">
		               <img src="<?=base_url()?>public/images/load.png" class="fa-spin">&nbsp; processing please wait ...
		 			</div>
                </div>
            </div>