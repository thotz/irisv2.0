<div class="modal-body">
                
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modalAddPreferredPositionLabel"><i class="fa ion-android-star"></i> Preferred Position</h4>
                
                <form class="form-horizontal" method=post action='#' name='form_prefered' id='form_prefered'>
                	<input type="hidden" name="applicant_id" id="applicant_id" value="<?=$applicant_id?>">
                	<input type="hidden" name="id" id="id" value="<?=$id?>">
	                <div class="box-body">
	                    
	                    <div class="form-group">
	                        <label for="selectPreferredPosition" class="col-sm-4 control-label">Preferred Position<span class="required">*</span>:</label>
	                        <div class="col-sm-8">
	                            <select name="position_ids[]" class="form-control form-control-multiple">
	                                <option value=""></option>
	                                <?=$preferred['position1']?>
	                            </select>
	                        </div>
	                    </div>
	                    <div class="form-group">
	                        <label for="selectJobSpecialization" class="col-sm-4 control-label">Job Specialization<span class="required">*</span>:</label>
	                        <div class="col-sm-8">
	                            <select name="jobspec_ids[]" class="form-control form-control-multiple">
	                                <option value=""></option>
	                                <?=$specialization['natureofbusiness1']?>
	                            </select>
	                        </div>
	                    </div>
	                    
	                    <hr />
	                    
	                    <div class="form-group">
	                        <label for="selectPreferredPosition02" class="col-sm-4 control-label">Preferred Position:</label>
	                        <div class="col-sm-8">
	                            <select name="position_ids[]" class="form-control form-control-multiple">
	                                <option value=""></option>
	                                <?=$preferred['position2']?>
	                            </select>
	                        </div>
	                    </div>
	                    <div class="form-group">
	                        <label for="selectJobSpecialization02" class="col-sm-4 control-label">Job Specialization:</label>
	                        <div class="col-sm-8">
	                            <select name="jobspec_ids[]" class="form-control form-control-multiple">
	                                <option value=""></option>
	                                <?=$specialization['natureofbusiness2']?>
	                            </select>
	                        </div>
	                    </div>
	                    
	                    <hr />
	                    
	                    <div class="form-group">
	                        <label for="selectPreferredPosition03" class="col-sm-4 control-label">Preferred Position:</label>
	                        <div class="col-sm-8">
	                            <select name="position_ids[]" class="form-control form-control-multiple">
	                                <option value=""></option>
	                                <?=$preferred['position3']?>
	                            </select>
	                        </div>
	                    </div>
	                    <div class="form-group">
	                        <label for="selectJobSpecialization03" class="col-sm-4 control-label">Job Specialization:</label>
	                        <div class="col-sm-8">
	                            <select name="jobspec_ids[]" class="form-control form-control-multiple">
	                                <option value=""></option>
	                                <?=$specialization['natureofbusiness3']?>
	                            </select>
	                        </div>
	                    </div>
	                    
	                </div>
                </form>
                
            </div>
            
            <div class="modal-footer">
                <div class="row">
                    <div class="col-sm-3 col-sm-offset-6">
                        <a href="#" type="button" id="btn_submit_asses" name="btn_submit_asses" class="btn btn-sm btn-flat btn-block btn-primary"onclick="assessment_update('prefered'); return false;" ><?=$btn_value?></a>
                    </div>
                    <div class="col-sm-3">
                        <a href="#" type="button" id="btn_close" name="btn_close" class="btn btn-sm btn-flat btn-block btn-default" onclick="jQuery(document).trigger('close.facebox'); return false;">Close</a>
                    </div>
		            <div class="modal-body" id="on_processing_display" style="display:none;">
		               <img src="<?=base_url()?>public/images/load.png" class="fa-spin">&nbsp; processing please wait ...
		 			</div>
                </div>
            </div>