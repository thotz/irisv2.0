<div class="modal-body">
                
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modalAddWorkLabel"><i class="fa fa-briefcase"></i> Work History</h4>
                
                <form class="form-horizontal" method=post action='#' name='form_employment' id='form_employment' enctype='multipart/form-data'>
                	<input type="hidden" name="applicant_id" id="applicant_id" value="<?=$applicant_id?>">
                	<input type="hidden" name="id" id="id" value="<?=$id?>">
	
	                <div class="box-body">
	                        
	                    <div class="form-group">
	                        <label for="checkboxStatus" class="col-sm-3 control-label"></label>
	                        <div class="col-sm-9">
	                            <label class="user-status"><input type="checkbox" id="is_na_employment" name="is_na_employment" onclick="fn_for_na(this);" class="flat-blue" value="0"/> <strong>Fresh Graduate</strong> (No Working Experience)</label>
	                        </div>
	                    </div>
	                        
	                    <hr />
	                    
	                    <div class="form-group">
	                        <label for="inputCompany" class="col-sm-3 control-label">Company<span class="required">*</span>:</label>
	                        <div class="col-sm-9">
	                            <input type="text" id="company" name="company" class="form-control" value="<?=isset($result['company']) ? $result['company'] : ''?>"/>
	                        </div>
	                    </div>
	                    
	                    <div class="form-group">
	                        <label for="inputPosition" class="col-sm-3 control-label">Position<span class="required">*</span>:</label>
	                        <div class="col-sm-6">
	                            <input type="text" id="position" name="position" class="form-control" value="<?=isset($result['position']) ? $result['position'] : ''?>"/>
	                        </div>
	                    </div>
	                    
	                    <div class="form-group">
	                        <label for="inputDepartment" class="col-sm-3 control-label">Nature of Project Experience<span class="required">*</span>:</label>
	                        <div class="col-sm-9">
	                        	<select id="natureofbusiness" name="natureofbusiness" class="form-control">
	                        	<?=$natureofproject?>
	                        	</select>
	                        </div>
	                    </div>
	                    
	                    <div class="form-group">
	                        <label for="inputDepartment" class="col-sm-3 control-label">Country<span class="required">*</span>:</label>
	                        <div class="col-sm-9">
	                        	<select id="country_id" name="country_id" class="form-control">
	                        	<?=$country?>
	                        	</select>
	                        </div>
	                    </div>
	                        
	                    <div class="form-group">
	                        <label for="selectDateFromMonth" class="col-sm-3 control-label">Date From<span class="required">*</span>:</label>
							<?php
								if (isset($result["from_date"]) && $result["from_date"]){
									list ($from_year, $from_month, $from_day) = @split ('[-]', $result['from_date']);
								} else {
									$from_year = "";$from_month="";$from_day="";
								}
								
							?>
	                        <div class="col-sm-3">
	                            <?=dateselectmonth("from_month", $from_month, 'class="form-control"');?>
	                        </div>
	                        <div class="col-sm-3">
	                            <?=dateselectyear("from_year", $from_year, 'class="form-control"');?>
	                        </div>
	                    </div>
	                        
	                    <div class="form-group">
	                        <label for="selectDateToMonth" class="col-sm-3 control-label">Date To<span class="required">*</span>:</label>
							<?php
								if (isset($result["to_date"]) && $result["to_date"]){
									list ($to_year, $to_month, $to_day) = @split ('[-]', $result['to_date']);
								} else {
									$to_year = "";$to_month="";$to_day="";
								}								

							?>
	                        <div class="col-sm-3">
	                            <?=dateselectmonth("to_month", $to_month, 'class="form-control"');?>
	                        </div>
	                        <div class="col-sm-3">
	                            <?=dateselectyear("to_year", $to_year, 'class="form-control"');?>
	                        </div>
	                    </div>
	                    <div class="form-group">
	                        <label for="inputDepartment" class="col-sm-3 control-label">Brief Job Description<span class="required">*</span>:</label>
	                        <div class="col-sm-9">
	                        	<textarea id="qualification" name="qualification" rows="3" cols="80" class="form-control"><?=isset($result['qualification']) ? $result['qualification'] : ''?></textarea>
	                        </div>
	                    </div>
	                        
	                </div>
                </form>
                
            </div>
            
            <div class="modal-footer">
                <div class="row">
                    <div class="col-sm-3 col-sm-offset-6">
                        <a href="#" type="button" id="btn_submit_asses" name="btn_submit_asses" class="btn btn-sm btn-flat btn-block btn-primary"onclick="assessment_update('employment'); return false;" ><?=$btn_value?></a>
                    </div>
                    <div class="col-sm-3">
                        <a href="#" type="button" id="btn_close" name="btn_close" class="btn btn-sm btn-flat btn-block btn-default" onclick="jQuery(document).trigger('close.facebox'); return false;">Close</a>
                    </div>
		            <div class="modal-body" id="on_processing_display" style="display:none;">
		               <img src="<?=base_url()?>public/images/load.png" class="fa-spin">&nbsp; processing please wait ...
		 			</div>
                </div>
            </div>