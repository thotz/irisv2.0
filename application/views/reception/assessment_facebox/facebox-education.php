			<div class="modal-body">
                
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modalAddEducationLabel"><i class="fa fa-graduation-cap"></i> Education</h4>
                
                <form class="form-horizontal" method=post action='#' name='form_education' id='form_education' enctype='multipart/form-data'>
                	<input type="hidden" name="applicant_id" id="applicant_id" value="<?=$applicant_id?>">
                	<input type="hidden" name="id" id="id" value="<?=$id?>">
	                <div class="box-body">
	                        
	                    <div class="form-group">
	                        <label for="checkboxStatus" class="col-sm-3 control-label"></label>
	                        <div class="col-sm-9">
	                            <label class="user-status"><input type="checkbox" id="is_na_education" name="is_na_education" onclick="fn_for_na(this);" class="flat-blue" value="0" /> <strong>Not Applicable</strong> (No Education)</label>
	                        </div>
	                    </div>
	                        
	                    <hr />
	                    
	                    <div class="form-group">
	                        <label for="selectEducation" class="col-sm-3 control-label">Education<span class="required">*</span>:</label>
	                        <div class="col-sm-9">
	                            <select class="form-control" id="education" name="education">
		                            <?=$select_educ_level?>
	                            </select>
	                        </div>
	                    </div>
	                        
	                    <div class="form-group">
	                        <label for="selectDateFrom" class="col-sm-3 control-label">Date From<span class="required">*</span>:</label>
							<?php
								$from_date = isset($result['from_date']) ? $result['from_date'] : "0000";
								$to_date = isset($result['to_date']) ? $result['to_date'] : "0000";
	                                                	
							?>
	                        <div class="col-sm-3">
	                            <?=dateselectyear("from_date", $from_date, 'class="form-control"');?>
	                        </div>
	                        <label for="selectDateTo" class="col-sm-3 control-label">Date To<span class="required">*</span>:</label>
	                        <div class="col-sm-3">
	                            <?=dateselectyear("to_date", $to_date, 'class="form-control"');?>
	                        </div>
	                    </div>
	                        
	                    <div class="form-group">
	                        <label for="inputSchool" class="col-sm-3 control-label">School<span class="required">*</span>:</label>
	                        <div class="col-sm-9">
	                            <input type="text" id="school" name="school" class="form-control" value="<?=isset($result['school']) ? $result['school'] : ''?>" placeholder="Name of School" />
	                        </div>
	                    </div>
	                        
	                    <div class="form-group">
	                        <label for="inputCourse" class="col-sm-3 control-label">Course<span class="required">*</span>:</label>
	                        <div class="col-sm-9">
	                            <input type="text" id="course" name="course" class="form-control" value="<?=isset($result['course']) ? $result['course'] : ''?>" placeholder="Name of Course" />
	                        </div>
	                    </div>
	                        
	                    <div class="form-group">
	                        <label for="inputLocation" class="col-sm-3 control-label">Location<span class="required">*</span>:</label>
	                        <div class="col-sm-9">
	                            <input type="text" id="location" name="location" class="form-control" value="<?=isset($result['location']) ? $result['location'] : ''?>" placeholder="Location" />
	                        </div>
	                    </div>
	                        
	                    <div class="form-group">
	                        <label for="inputFieldOfStudy" class="col-sm-3 control-label">Field of Study<span class="required">*</span>:</label>
	                        <div class="col-sm-9">
	                            <select id="f_study" name="f_study" class="form-control">
	                            	<?=$select_f_study?>
									<!-- <option value=""></option><option value="N/A">N/A</option><option value="Advertising / Media">Advertising / Media</option><option value="Agriculture">Agriculture</option><option value="Airline Transport">Airline Transport</option><option value="Architecture / Urban Studies">Architecture / Urban Studies</option><option value="Art &amp; Design">Art &amp; Design</option><option value="Biology">Biology</option><option value="BioTechnology">BioTechnology</option><option value="Business Studies/Administration/Management">Business Studies/Administration/Management</option><option value="Chemistry">Chemistry</option><option value="Commerce">Commerce</option><option value="Computer Science/Information technology">Computer Science/Information technology</option><option value="Dentistry">Dentistry</option><option value="Economics">Economics</option><option value="Editing and Publication">Editing and Publication</option><option value="Education/Teaching/Training">Education/Teaching/Training</option><option value="Engineering(Aviation/Aeronautics/Astronautics)">Engineering(Aviation/Aeronautics/Astronautics)</option><option value="Engineering(Chemical)">Engineering(Chemical)</option><option value="Engineering(Civil)">Engineering(Civil)</option><option value="Engineering(Computer/Telecommunication)">Engineering(Computer/Telecommunication)</option><option value="Engineering(Electrical/Electronic)">Engineering(Electrical/Electronic)</option><option value="Engineering(Environmental/Health/Safety)">Engineering(Environmental/Health/Safety)</option><option value="Engineering(Industrial)">Engineering(Industrial)</option><option value="Engineering(Marine)">Engineering(Marine)</option><option value="Engineering(Material Science)">Engineering(Material Science)</option><option value="Engineering(Mechanical)">Engineering(Mechanical)</option><option value="Engineering(Metal Fabrication/Tool &amp; Die/Welding)">Engineering(Metal Fabrication/Tool &amp; Die/Welding)</option><option value="Engineering(Metallurgical)">Engineering(Metallurgical)</option><option value="Engineering(Others)">Engineering(Others)</option><option value="Engineering(Petroleum/Oil/Gas)">Engineering(Petroleum/Oil/Gas)</option><option value="Finance/Accountancy/Banking">Finance/Accountancy/Banking</option><option value="Food and Beverage Preparation/Service Management">Food and Beverage Preparation/Service Management</option><option value="Geographical Science">Geographical Science</option><option value="Hospitality/Tourism Management">Hospitality/Tourism Management</option><option value="Human Resource Management">Human Resource Management</option><option value="Humanities/Liberal Arts">Humanities/Liberal Arts</option><option value="Land Transport">Land Transport</option><option value="Law">Law</option><option value="Library Management">Library Management</option><option value="Linguistics/Translation &amp; Interpretation">Linguistics/Translation &amp; Interpretation</option><option value="Mass Communications">Mass Communications</option><option value="Mathematics">Mathematics</option><option value="Medical Science">Medical Science</option><option value="Medicine">Medicine</option><option value="Merchant Marine">Merchant Marine</option><option value="Music/Performing Arts Studies">Music/Performing Arts Studies</option><option value="Nursing">Nursing</option><option value="Others">Others</option><option value="Personal Services &amp; Building/Ground Services">Personal Services &amp; Building/Ground Services</option><option value="Pharmacy/Pharmacology">Pharmacy/Pharmacology</option><option value="Physics">Physics</option><option value="Protective Services &amp; Management">Protective Services &amp; Management</option><option value="Quantity Survey">Quantity Survey</option><option value="Sales &amp; Marketing">Sales &amp; Marketing</option><option value="Science &amp; Technology">Science &amp; Technology</option><option value="Secretarial">Secretarial</option><option value="Textile/Fashion Design &amp; Production">Textile/Fashion Design &amp; Production</option><option value="Veterinary">Veterinary</option> -->
	                            </select>
	                        </div>
	                    </div>
	                        
	                    <!-- <div class="form-group">
	                        <label for="checkboxLicense" class="col-sm-3 control-label">License/Certificate:</label>
	                        <div class="col-sm-9">
	                            <label class="user-status"><input type="checkbox" class="flat-blue" name="" value="Yes" /> Yes </label>
	                        </div>
	                    </div> -->
	                        
	                </div>
                </form>
                
            </div>
            
            <div class="modal-footer">
                <div class="row">
                    <div class="col-sm-3 col-sm-offset-6">
                        <a href="#" type="button" id="btn_submit_asses" name="btn_submit_asses" class="btn btn-sm btn-flat btn-block btn-primary"onclick="assessment_update('education'); return false;" ><?=$btn_value?></a>
                    </div>
                    <div class="col-sm-3">
                        <a href="#" type="button" id="btn_close" name="btn_close" class="btn btn-sm btn-flat btn-block btn-default" onclick="jQuery(document).trigger('close.facebox'); return false;">Close</a>
                    </div>
		            <div class="modal-body" id="on_processing_display" style="display:none;">
		               <img src="<?=base_url()?>public/images/load.png" class="fa-spin">&nbsp; processing please wait ...
		 			</div>
                </div>
            </div>