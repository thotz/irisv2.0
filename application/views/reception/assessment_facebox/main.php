
<div id="modal-assessment" tabindex="-1" role="dialog" aria-labelledby="modalAssessmentLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" id="assessment_content">
            <div class="modal-body">
               <img src="<?=base_url()?>public/images/load.png" class="fa-spin">&nbsp; loading please wait ...
 			</div>
        </div>
    </div>
</div>


	<?php 

	
	switch ($action) {
		case 'prefered':
			$formname = "form_prefered";
			$inputname = array();
			$displayname = array();
			break;
		case 'position':
			$formname = "form_position";
			$inputname = array();
			$displayname = array();
			break;
		case 'education':
			$is_na_field = 'is_na_education';
	
			$formname = "form_education";
			$inputname = array("education","from_date","to_date","school","course","location","f_study");
			$displayname = array("Education","Date From","Date To","School","Course","Location","Field of Study");
			break;
		case 'licenses':
			$is_na_field = 'is_na_license';
	
			$formname = "form_licenses";
			$inputname = array("license","number");
			$displayname = array("Licence/Certification Taken","License/Certificate No.");
			break;
		case 'training':
			$is_na_field = 'is_na_training';
	
			$formname = "form_training";
			$inputname = array("title","center","duration");
			$displayname = array("Title","Training Center","Duration");
			break;
		case 'tradetest':
			$is_na_field = 'is_na_tradetest';
	
			$formname = "form_tradetest";
			$inputname = array("title","center","grade");
			$displayname = array("Title","TradeTest Center","Grade");
			break;
		case 'beneficiaries':
			$is_na_field = 'is_na_beneficiaries';
	
			$formname = "form_beneficiaries";
			$inputname = array("lname","fname","relationship","month","day","year");
			$displayname = array("Last Name","First Name","Relationship","BirthDate","BirthDate","BirthDate");
			break;
		case 'employment':
			$is_na_field = 'is_na_employment';
	
			$formname = "form_employment";
			//$inputname = array("company","department","position","salary","country_id","from_month","from_year","to_month","to_year","qualification");
			$inputname = array("company","position","country_id","from_month","from_year","to_month","to_year","qualification");
			$displayname = array("Company","Position","Country","Date from Month","Date from Year","Date to Month","Date to Year","Brief Job Description");
			break;
		case 'cvlocation':
			$formname = "form_cvlocation";
			$inputname = array("user_id","status_dcv","remarks");
			$displayname = array("Evaluator","Mode","Internal Advisory");
			break;
			
		case 'general':
			$formname = "form_general";
			$inputname = array("details","position1","natureofbusiness1");
			$displayname = array("Assessment Detail","Recommended Position","Nature of Project Experience");
			break;
	
		default:
	
	}
	
	function JS_CheckRequired($myform, $textarray,$displayname="0") {
		if($displayname=="0") $displayname=$textarray;
		?>
				<script type="text/javascript">
			       function PHP_hasValue(obj, obj_type) {
						if (obj_type == "TEXT" || obj_type == "PASSWORD")	{
							if (obj.value.length == 0) {
									return false;
							} else {
								return true;   	
							}
						}
					}
		
					function CheckRequired() {
						/* if N/A  is CHECK - escape the required fields */
						if (is_na_field) {
							if ($('#'+is_na_field).val() == 1) {
								return true;
							}
						}
		
						<?php for ($i=0;$i< count($textarray);$i++) {  ?>
								if  ( ! PHP_hasValue(document.<?=$myform?>.<?=$textarray[$i]?>, "TEXT" )) {
									alert("Notice: <?=$displayname[$i]?> is a required field.");
									document.<?=$myform?>.<?=$textarray[$i]?>.focus();
									return false;
								}
						<?php } ?>
		
						return true;
					}
			    </script>
		<?php 
	}
		
	JS_CheckRequired($formname, $inputname, $displayname);
	

?>

<script type="text/javascript">
	var referral = '<?=$referral?>';
	var is_na_field = '<?=$is_na_field?>';
	var is_na_value = '<?=$is_na_value?>';
	var action = '<?=$action?>';
	var applicant_id = '<?=$applicant_id?>';
	var id = '<?=$id?>';

	
	jQuery(document).ready(function() {
		
		/* Init - Load Facebox Main Form add/edit */
		load_assessment_facebox(action, applicant_id, id);


		/* Status of Interview - submit */
// 		$('#submit_distr_cv').click(function(){
// 		});
	});
	
	/* load facebox add/edit form */
	function load_assessment_facebox(item, applicant_id, id) {
		var action = "ajax_"+item;

		if (item) {
			$("#assessment_content").load(base_url_js()+"reception/assessment_facebox/"+action+"?action="+item+"&applicant_id="+applicant_id+"&id="+id+"&referral="+referral,function(response,status,xhr){
				if (status=="success") {
					var obj = jQuery.parseJSON(response);
					$(this).html(obj.html);
	
					/* onload License NA */
					if (is_na_value == 1) {
						$('#'+is_na_field).attr({'checked':'checked'});
						fn_for_na('#'+is_na_field);
					}
				} else {
					$(this).html("An error occured: <br/>" + xhr.status + " " + xhr.statusText)
				}
			});
		}
	}

	function assessment_update_cvlocation() {
		/* Edit the un-Reviewed Cv instead of Adding new */
		if ($('#cv_mode').val() == '') {
			alert('Applicant\'s CV is still unreview, please edit instead of adding new.');
			return false;
		}
		if ($("#status_dcv").val() == '') {
			alert("Status is required.");
			return false;
		}
		if ($("#remarks").val() == '') {
			alert("Remarks is required.");
			return false;
		}
		
		$.post(base_url_js()+'reception/assessment_facebox/ajax_check_applicant_initial_lineup', {what:'interview_status',applicant_id:'<?=$applicant_id?>'}, function(response){
			//var obj = jQuery.parseJSON(response);
			
			/* Check for General Assessment, restricted except Allowed Mode. */
			var arrayMode = ["PS","NA","TCB","WD"];	/* Allowed Mode */
			var resultMode = arrayMode.indexOf($("#status_dcv").val());
			
			$.post(base_url_js()+'reception/assessment_facebox/ajax_check_applicant_assessment', {table:'assessment_gen',applicant_id:'<?=$applicant_id?>'}, function(data){

				if (data || resultMode != -1) {
					$('#btn_close').hide();
					$('#btn_submit_asses').hide();
					$('#on_processing_display').show();


					$.post(base_url_js()+'reception/assessment_add/ajax_cvlocation?what=cvlocation', $("#form_cvlocation").serialize(), function(data) {
						if(data == 'error'){
							alert('an error occured.');
							return false;
						}else{
							/* refresh the list */
							load_assessments_lists(action, "add");

							alert("Success");
							
							/* Facebox Auto Close */
							jQuery(document).trigger('close.facebox');
						
							/*	POPUP ADVISORY */
							if ($("#status_dcv").val() == 'RC') {
							
							} else if ($("#status_dcv").val() == 'PS') {
								/* Show popup message here */
								//jQuery.facebox({ajax:'popup/rec_app_pop.php?app_id=<?=$applicant_id?>&remarks='+$("#textarea_dcv").val()+'&what=interview_status_ps'});
							} else if ($("#status_dcv").val() == 'NA') {
								
							} else if ($("#status_dcv").val() == 'TCB') {

							}
							/*	POPUP ADVISORY */
						}
					});

				} else {
					alert('GENERAL ASSESSMENT must be filled-up first.');
					popup_recruitment_assessment('general','<?=$applicant_id?>');
				}
			});
		});
	}
	
	/*	assessment_update form  add/edit	*/
	function assessment_update(action) {
		switch (action) {
			case 'prefered':
				var what = 'rec_prefered_position';
				break;
			case 'position':
				var what = 'rec_position_applied';
				break;
			case 'education':
				var what = 'rec_education';
				break;
			case 'licenses':
				var what = "rec_licenses";
				break;
			case 'training':
				var what = "rec_training";
				break;
			case 'tradetest':
				var what = "rec_tradetest";
				break;
			case 'beneficiaries':
				var what = "rec_beneficiaries";
				break;
			case 'employment':
				var what = "rec_employment";
				break;
			case 'cvlocation':
				var what = "rec_cvlocation";
				break;
			case 'general':
				var what = "rec_general";
				break;
			default:
				var what = '';
		}
		
		if (CheckRequired()) {
			
			var message_alert = "";
			if ($('#btn_submit_asses').html() == 'Save') {
				$('#btn_close').hide();
				$('#btn_submit_asses').hide();
				$('#on_processing_display').show();
				$.post(base_url_js()+'reception/assessment_update/ajax_'+action+'?what='+action, $("#form_"+action).serialize(), function(data) {
					var response = jQuery.parseJSON(data);
					if(response.status == 'Success') {
						load_assessments_lists(action, "edit");
					}

					alert(response.message);

					$('#btn_close').show();	
					$('#btn_submit_asses').show();
					$('#on_processing_display').hide();
					jQuery(document).trigger('close.facebox');
					return false;
				});
			} else {
				if (referral == 'reported_today') {
					/* Button do not Disable */
				} else if (referral == 'lineup_initial') {
					/* Button do not Disable */
				} else {
					$('#btn_close').hide();
					$('#btn_submit_asses').hide();
					$('#on_processing_display').show();
				}
				
				$.post(base_url_js()+'reception/assessment_add/ajax_'+action+'?what='+action, $("#form_"+action).serialize(), function(data) {
					var response = jQuery.parseJSON(data);
					message_alert = response.message;
					if (response.status == 'Success') {
						if (referral == '') {
							/* refresh the list */
							load_assessments_lists(action, "add");
							
							/* update the applicant status display */
							if (response.app_status) {
								$('#applicant_status').html(response.app_status);
							}
							
							/* processed in STATUS OF INTERVIEW [Add/Edit], if the applicant without GENERAL ASSESSMENT then auto save the STATUS OF INTERVIEW [Add/Edit] */
							if (action == 'general' && $('#status_dcv').val() != '' && $('#textarea_dcv').val() != '') {
								if ($('#edit_distr_cv').css('display') == 'inline') {	/* Edit STATUS OF INTERVIEW*/
									var do_action = "distr_cv_edt";
								} else {												/* Add STATUS OF INTERVIEW*/
									var do_action = "add_distr_cv_data";
								}
								
							}
						}
					}
					
					alert(message_alert);
					if (referral == 'reported_today') {
						/* Facebox do not Auto Close */
					} else {
						jQuery(document).trigger('close.facebox');
					}
					return false;
				});
			}
		}
	}
	
	function getRefnoBy(item) {
		$.get('ajax.php?action=getRefnoBy&type=position&position_id='+$(item).val()+'&applicant_id=<?=$applicant_id?>', function(data) {
			if (data != 'false') {
				$('#manpower_rid').html(data);
			} else {
				alert("Error.");
			}
			return false;
		});
	}
	
	function fn_for_na(item) {
		var count_active_assessment = 0;
		
		/* check the type of ITEM string/object */
		if (typeof(item) == 'object') {
			value = item.id;
		} else {
			value = item.replace("#", "");
		}

		switch (value) {
			case 'is_na_education':
				var array_fields = ['education','from_date','to_date','school','course','location','f_study'];
				count_active_assessment = count_education;
				break;
				
			case 'is_na_license':
				var array_fields = ['license','number','month','day','year','expiredate_month','expiredate_day','expiredate_year'];
				count_active_assessment = count_licenses;
				break;
				
			case 'is_na_training':
				var array_fields = ['center','title','month','day','year','to_month','to_day','to_year','duration'];
				count_active_assessment = count_training;
				break;
				
			case 'is_na_tradetest':
				var array_fields = ['title','center','grade','trade_month','trade_day','trade_year'];
				count_active_assessment = count_tradetest;
				break;
				
			case 'is_na_beneficiaries':
				var array_fields = ["lname","fname","mname","relationship","month","day","year"];
				count_active_assessment = count_beneficiaries;
				break;
				
			case 'is_na_employment':
				var array_fields = ['company','department','position','salary','country_id','natureofbusiness','qualification','from_month','from_year','to_month','to_year','qualification'];
				count_active_assessment = count_employment;
				break;
				
			default:
			
		
		}
		
		if ($(item).is(':checked')) {
			if (count_active_assessment > 0 && typeof(item) == 'object') {
				alert("Already have Entry, please delete to proceed.");
				$(item).val(0);
				$(item).attr({'checked':false});
			} else {
				$(item).val(1);
				$('#webaddmore').css({'display':'none'});
				
				if (id == 'undefined') {
					$('#btn_submit_asses').html(' Save ');
					$('#btn_close').hide();
				}
				
				$.each(array_fields, function(index, value) {
					$('#'+value).attr({'disabled':'disabled'});
				});
			}
		} else {
			$(item).val(0);
			$('#webaddmore').css({'display':''});
			
			if (id == 'undefined') {
				$('#btn_submit_asses').html('Add');
				$('#btn_close').show();
			}
			
			$.each(array_fields, function(index, value) {
				$('#'+value).attr({'disabled':false});
			});
		}
	}
	
	function relationship_others(item) {
		if ($(item).val() == 'Others') {
			$('#content_others').show();
		} else {
			$('#content_others').hide();
		}
	}
</script>