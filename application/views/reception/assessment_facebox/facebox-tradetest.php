            <div class="modal-body">
                
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modalAddLicenseCertificateLabel"><i class="fa fa-certificate"></i> Trade Test</h4>
                
                <form class="form-horizontal" method=post action='#' name='form_tradetest' id='form_tradetest'>
                	<input type="hidden" name="applicant_id" id="applicant_id" value="<?=$applicant_id?>">
                	<input type="hidden" name="id" id="id" value="<?=$id?>">

	                <div class="box-body">
	                        
	                    <div class="form-group">
	                        <label for="checkboxStatus" class="col-sm-4 control-label"></label>
	                        <div class="col-sm-8">
	                            <label class="user-status"><input type="checkbox" id="is_na_tradetest" name="is_na_tradetest" onclick="fn_for_na(this);" class="flat-blue" value="0" /> <strong>Not Applicable</strong> (No Training/Seminar)</label>
	                        </div>
	                    </div>
	                        
	                    <hr />
	                    
	                    <div class="form-group">
	                        <label for="inputLCTaken" class="col-sm-4 control-label">Title<span class="required">*</span>:</label>
	                        <div class="col-sm-8">
	                            <input type="text" id="title" name="title" class="form-control" value="<?=isset($result['title']) ? $result['title'] : ''?>" placeholder="" />
	                        </div>
	                    </div>
	                    
	                    <div class="form-group">
	                        <label for="inputLCNum" class="col-sm-4 control-label">TradeTest Center<span class="required">*</span>:</label>
	                        <div class="col-sm-8">
	                            <input type="text" id="center" name="center" class="form-control" value="<?=isset($result['center']) ? $result['center'] : ''?>" placeholder="" />
	                        </div>
	                    </div>
	                    
	                    <div class="form-group">
	                        <label for="selectDateExpireMonth" class="col-sm-4 control-label">Date Taken<span class="required">*</span>:</label>
							<?php
								if (isset($result["trade_date"]) && $result["trade_date"]){
									list ($trade_year, $trade_month, $trade_day) = @split ('[-]', $result['trade_date']);
								} else {
									$trade_year="";$trade_month="";$trade_day="";
								}
							?>

	                        <div class="col-sm-3">
	                            <?=dateselectmonth("trade_month", $trade_month, 'class="form-control"');?>
	                        </div>
	                        <div class="col-sm-2">
	                            <?=dateselectday("trade_day", $trade_day, 'class="form-control"');?>
	                        </div>
	                        <div class="col-sm-3">
	                            <?=dateselectyear("trade_year", $trade_year, 'class="form-control"');?>
	                        </div>
	                    </div>
	                    
	                    <div class="form-group">
	                        <label for="inputLCNum" class="col-sm-4 control-label">Grade<span class="required">*</span>:</label>
	                        <div class="col-sm-8">
	                            <input type="text" id="grade" name="grade" class="form-control" value="<?=isset($result['grade']) ? $result['grade'] : ''?>" placeholder="" />
	                        </div>
	                    </div>
	                    
	                </div>
                </form>
                
            </div>
            
            <div class="modal-footer">
                <div class="row">
                    <div class="col-sm-3 col-sm-offset-6">
                        <a href="#" type="button" id="btn_submit_asses" name="btn_submit_asses" class="btn btn-sm btn-flat btn-block btn-primary"onclick="assessment_update('tradetest'); return false;" ><?=$btn_value?></a>
                    </div>
                    <div class="col-sm-3">
                        <a href="#" type="button" id="btn_close" name="btn_close" class="btn btn-sm btn-flat btn-block btn-default" onclick="jQuery(document).trigger('close.facebox'); return false;">Close</a>
                    </div>
		            <div class="modal-body" id="on_processing_display" style="display:none;">
		               <img src="<?=base_url()?>public/images/load.png" class="fa-spin">&nbsp; processing please wait ...
		 			</div>
                </div>
            </div>