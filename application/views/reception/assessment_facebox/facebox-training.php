            <div class="modal-body">
                
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modalAddLicenseCertificateLabel"><i class="fa fa-certificate"></i> Training &amp; Seminar</h4>
                
                <form class="form-horizontal" method=post action='#' name='form_training' id='form_training'>
                	<input type="hidden" name="applicant_id" id="applicant_id" value="<?=$applicant_id?>">
                	<input type="hidden" name="id" id="id" value="<?=$id?>">

	                <div class="box-body">
	                        
	                    <div class="form-group">
	                        <label for="checkboxStatus" class="col-sm-4 control-label"></label>
	                        <div class="col-sm-8">
	                            <label class="user-status"><input type="checkbox" id="is_na_training" name="is_na_training" onclick="fn_for_na(this);" class="flat-blue" value="0" /> <strong>Not Applicable</strong> (No Training/Seminar)</label>
	                        </div>
	                    </div>
	                        
	                    <hr />
	                    
	                    <div class="form-group">
	                        <label for="inputLCTaken" class="col-sm-4 control-label">Title<span class="required">*</span>:</label>
	                        <div class="col-sm-8">
	                            <input type="text" id="title" name="title" class="form-control" value="<?=isset($result['title']) ? $result['title'] : ''?>" placeholder="" />
	                        </div>
	                    </div>
	                    
	                    <div class="form-group">
	                        <label for="inputLCNum" class="col-sm-4 control-label">Training Center<span class="required">*</span>:</label>
	                        <div class="col-sm-8">
	                            <input type="text" id="center" name="center" class="form-control" value="<?=isset($result['center']) ? $result['center'] : ''?>" placeholder="" />
	                        </div>
	                    </div>
	                    
	                    <div class="form-group">
	                        <label for="inputLCNum" class="col-sm-4 control-label">Duration<span class="required">*</span>:</label>
	                        <div class="col-sm-8">
	                            <input type="text" id="duration" name="duration" class="form-control" value="<?=isset($result['duration']) ? $result['duration'] : ''?>" placeholder="" />
	                        </div>
	                    </div>
	                    
	                </div>
                </form>
                
            </div>
            
            <div class="modal-footer">
                <div class="row">
                    <div class="col-sm-3 col-sm-offset-6">
                        <a href="#" type="button" id="btn_submit_asses" name="btn_submit_asses" class="btn btn-sm btn-flat btn-block btn-primary"onclick="assessment_update('training'); return false;" ><?=$btn_value?></a>
                    </div>
                    <div class="col-sm-3">
                        <a href="#" type="button" id="btn_close" name="btn_close" class="btn btn-sm btn-flat btn-block btn-default" onclick="jQuery(document).trigger('close.facebox'); return false;">Close</a>
                    </div>
		            <div class="modal-body" id="on_processing_display" style="display:none;">
		               <img src="<?=base_url()?>public/images/load.png" class="fa-spin">&nbsp; processing please wait ...
		 			</div>
                </div>
            </div>