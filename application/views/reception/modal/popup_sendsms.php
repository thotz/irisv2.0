<html>
	<head>
		<title></title>
		<script type="text/javascript" src="<?=base_url();?>public/js/ajax_functions.js"></script>
		<script type="text/javascript">
			jQuery(document).ready(function(){
				//check_gateway_connection();

				/*CHECK IF MOBILE NO IS VALID*/
				valid_mob = new RegExp('^0[0-9]{10}');	/*should start with 0, should be 11 characters long*/
				var check_mob = '<?=$check_mobile?>';
				var mobile_no = '<?=$mobile_no?>';

				if(check_mob == 1){
					if(mobile_no != ''){
						if(valid_mob.test(mobile_no) == false || mobile_no.length > 11 || mobile_no.length < 11){
							$('#err_msg').html('Primary Number should start with 0 and should be 11 characters long.');
							$('#err_msg').css('color','red');
							$('#err_msg').show();
							/*DISABLE SEND BUTTON*/
							$('#btn_send').attr('disabled', true);
							$('#btn_send').attr('title', 'please update applicant\'s Mobile Number to proceed');
						}
					}else{
						$('#err_msg').html('Mobile Number is Required.');
						$('#err_msg').css('color','red');
						$('#err_msg').show();
						/*DISABLE SEND BUTTON*/
						$('#btn_send').attr('disabled', true);
						$('#btn_send').attr('title', 'please update applicant\'s Mobile Number to proceed');
					}
				}

				$('#btn_send').click(function(){
					$('.err_cont').hide();
					if($('#txtA_message').val() == ''){
						$('#err_msg').html('Message is Required.');
						$('#err_msg').show();
						$('#txtA_message').focus();
					}else{
						$('#btn_send').hide();
						$('#wait_msg').show();
						$.post('<?=base_url();?>ajax/ajax_send_sms?action=<?=$ajax_action?>', $("#frm_send_ewsms").serialize(), function(data){
							if(data == 'error1'){
								$('#err_msg').html('SMS Gateway Failed.');
								$('#err_msg').show();
								$('#wait_msg').hide();
							}else if(data == 'error2'){
								$('#err_msg').html('An error occurred.');
								$('#err_msg').show();
								$('#wait_msg').hide();
							}else if(data == 'error2'){
								$('#err_msg').html('Sending Email Failed.');
								$('#err_msg').show();
								$('#wait_msg').hide();
							}else{
								/*$('#err_msg').html('Message Sent.');
								$('#err_msg').css('color','blue');
								$('#btn_send').attr('disabled', true);
								setTimeout("jQuery.facebox.close();", 3000);*/

								$('#sent_msg').show();
								$('#wait_msg').hide();
								//setTimeout(function(){$('#txtA_message').val(''); $('#btn_send').show(); $('#btn_clear').trigger('click');}, 2000);
								setTimeout("jQuery.facebox.close();", 3000);
							}
						});
					}
				});

				$('#txtA_message').bind('keyup paste', function(){
					var char_limit = '1200';
					var txt_msg = $(this).val();
					$('#msg_counter').css('color', '#000');
	
					if(txt_msg.length > (char_limit - 20)){
						$('#msg_counter').css('color', 'red');
					}
	
					if(txt_msg.length > char_limit){
						$(this).val(txt_msg.substring(0, char_limit));
					}
	
					$('#msg_counter').html($(this).val().length+'/'+char_limit);
				});
			});
		</script>
	</head>
	<body>
		<form name="frm_send_ewsms" id="frm_send_ewsms" method="POST">
			<input type="hidden" name="h_id" id="h_id" value="<?=$_GET['id']?>" />
			<input type="hidden" name="replied" value="<?=$replied?>" />
			<input type="hidden" name="app_info" id="app_info" value="<?=$sms_info?>" />
			<input type="hidden" name="parent_form" id="parent_form" value="<?=$_GET['parent_form']?>" />
			<table width="100%">
				<tr>
					<td><img src="<?=base_url();?>public/img/topleft-login.gif" border="0"></td>
					<td background="<?=base_url();?>public/img/toptile-login.gif"><img src="<?=base_url();?>public/img/toptile-login.gif" border="0"></td>
					<td><img src="<?=base_url();?>public/img/topright-login.gif" border="0"></td>
				</tr>
				<tr>
					<td background="<?=base_url();?>public/img/midleft-login.gif"><img src="<?=base_url();?>public/img/midleft-login.gif" border="0"></td>
					<td class="box" width="100%" align="center">
						<table width="95%" border="0" cellpadding="2" cellspacing="2">
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td colspan="2" align="center">
<?
								if($_GET['parent_form'] == 'deployed_databank'){
?>
									<h3>Compose SMS/Online Message</h3>
<?
								}else{
?>
									<h3>Compose SMS</h3>
<?
								}
?>
			   				</td>
							</tr>
							<tr>
								<td width="25%">&nbsp;</td>
								<td width="75%">
									<span class="err_cont" id="wait_msg" style="display:none;font-weight:bold;">Sending Message. Please wait <img src="<?=base_url();?>public/images/ajax-loader-dot.gif" width="15" height="15"></span>
									<span class="err_cont" id="sent_msg" style="display:none;color:blue;font-weight:bold;">Message Sent</span>
									<span class="err_cont" id="err_msg" style="display:none;color:red;font-weight:bold;"></span>
								</td>
							</tr>
							<tr style="height:25px;">
				   			<td><b>Name:</b></td>
				   			<td>
<?
								if($total_recipient > 1){
?>
									<textarea name="" id="" rows="3" cols="40" style="width:255px;"><?=$fullname?></textarea>
<?
								}else{
?>
									<input type="text" name="" id="" value="<?=$fullname?>" size="40" readonly="readonly" style="width:255px;" />
<?
								}
?>
			   				</td>
				     		</tr>
				     		<tr style="height:25px;">
				   			<td><b>Template:</b></td>
				   			<td>
									<select name="sms_tpl" id="sms_tpl" style="width:194px;" onchange="$('#txtA_message').val($(this).val()); $('#txtA_message').trigger('keyup');">
				   					<option value="">--</option>
<?
									foreach ($result_tpl as $val){
										if($_GET['parent_form'] == 'deployed_databank' && $val['label'] == 'DEPLOYED APPLICANT'){
?>
											<option value="<?=$val['message']?>" selected="selected"><?=$val['label']?></option>
<?
										}else{
?>
											<option value="<?=$val['message']?>"><?=$val['label']?></option>
<?
										}
									}
?>
				   				</select>
				   			</td>
				     		</tr>
				     		<tr style="height:25px;">
				     			<td><b>Message:</b></td>
				     			<td align="left">
				     				<textarea name="txtA_message" id="txtA_message" rows="12" cols="40" style="width:255px;"><?=$default_txta_val?></textarea>
				     			</td>
				     		</tr>
				     		<tr style="height:25px;">
								<td align="right" colspan="2">
									<div id="msg_counter" style="border:solid 0px;width:100px;float:left;text-align:left;font-size:11px;font-style:italic;padding-left:85px;">0/1200</div>
									<input type="button" value="Send" class="button" id="btn_send" name="btn_send">
									<input type="button" value="Cancel" class="button" onclick="jQuery.facebox.close();" />
								</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
						</table>
					</td>
					<td background="<?=base_url();?>public/img/midright-login.gif"><img src="<?=base_url();?>public/img/midright-login.gif" border="0"></td>
				</tr>
				<tr>
					<td><img src="<?=base_url();?>public/img/botleft-login.gif" border="0"></td>
					<td background="<?=base_url();?>public/img/bottile-login.gif"><img src="<?=base_url();?>public/img/bottile-login.gif" border="0"></td>
					<td><img src="<?=base_url();?>public/img/botright-login.gif" border="0"></td>
				</tr>
			</table>
		</form>
	</body>
</html>