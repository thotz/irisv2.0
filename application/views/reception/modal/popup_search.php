<?php
	
?>
<html>
	<head>
		<title>Search Applicant</title>
	</head>
	<script type="text/javascript">
		function validateNumber(event) {
			var key = window.event ? event.keyCode : event.which;
				
			if (event.keyCode == 8 || event.keyCode == 46
			 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 13) {
				return true;
			} else if ( key >= 48 && key <= 57 ) {	/*	charCode 0-9 */
				return true;
			} else if ( key >= 96 && key <= 105 ) {	/* numpad 0-9 */
				return true;
			} else if ( key == 86 || key == 17) {	/*	charCode v and CTRL*/
				return true;
			} else {
				return false;
			}
		};
		
		function process_search(e, cur_val) {
				var page_limit = 20												/* limit applicant per page */
				$('#load_img_txt').show();											/* show loading image */
				$('#dtl_cont').html('');										/* clear list container */
				$('#span_total_pages').html(0);
				$('#span_all_total').html(0);
				$('#span_pageno').html(0);

				if($('#reset_page').val()==1){
					$('#page').val('0');											/* reset page number to 1 */
				}

				idClicked = e.target.id;							/* chek for event : on button pagenate [pager_l/pager_r] no need to enter */
				
				/*	START KEYWORD FILTERING */
				var srch_keyword = $('#searchbox').val();							
				var $search_now = false;
					
					if ($("#srch_for").val() == 'applicant_computer') {		/*	search by computer number - allow 9 length */

						/*	START AUTO APPEND " - "	*/
							if ((srch_keyword.length > 2 && srch_keyword.length != 7) && e.keyCode != 8) {
							
								if (validateNumber(e) == false) {
									$('#searchbox').val(cur_val);
									return false;
								} else if (srch_keyword.length == 6) {
									$('#searchbox').val($('#searchbox').val() + '-');
								}
							} else if (srch_keyword.length == 1 && e.keyCode != 8) {		/*	Auto append " - " if length 1 and if not barcode reader */
								if (e.which != 16) {
									$('#searchbox').val($('#searchbox').val() + '-');
								}
							} else if (srch_keyword.length == 6 && e.keyCode != 8) {		/*	Auto append " - " if length 6 */
								$('#searchbox').val($('#searchbox').val() + '-');
							} else if ((srch_keyword.length == 2 || srch_keyword.length == 7) && e.keyCode != 8) {	/*	Force to replace any keyword to " - " if length 2 and 7 */
								$('#searchbox').val(cur_val + '-');
							}
						/*	END AUTO APPEND " - "	*/
						
						if (srch_keyword.length >= 9 && srch_keyword.length <= 10) {
							$search_now = true;
						}
					} else if ($("#srch_for").val() == 'applicant_name' && (e.keyCode == 13 || e.keyCode == 8 || idClicked == 'pager_l' || idClicked == 'pager_r') ) {	/*	search by name - allow 2 length and "Enter and Backspace" */
						if (srch_keyword.length < 2 && srch_keyword.length > 0) {
							alert("Please Input More Than 3 Letters and Press Enter.");
							$('#searchbox').val('');
							return false;
						}
						$search_now = true;					
					}
				/*	END KEYWORD FILTERING */
				
				if ($search_now) {
					$('#load_img_txt').hide();											/* hide loading txt */
					$('#load_img').show();											/* show loading image */
					search_object = $.get('<?=base_url();?>api/search_applicant/ajax_search?action=followup_popup', $("#frm_search_pop").serialize(), function(data){
					//alert(data);return false;
						var obj = jQuery.parseJSON(data);
	
						if(obj.pageno > 0 && obj.total_found == 0){
							$('#pager_l').trigger('click');						/* load previous page if query returns 0 */
						}
	
						$('#load_img').hide();									/* hide loading image */
						$('#dtl_cont').html(obj.html);							/* populate container with applicant list */
						$('#html_remarks').show();								/* show remarks for button */
						
						
	
						/* show/hide pagination button */
						if(obj.total_found != 0){
							$('#pager_l').show();
							$('#pager_r').show();
							$('#txt_page').show();
							$('#span_total_pages').html(obj.record_end);		/* end count */
							$('#span_all_total').html('<b>'+obj.all_total_found+'</b>');		/* all total count */
							$('#span_pageno').html(obj.record_start);			/* start count */
	
							if($('#page').val() == 0){
								$('#pager_l').attr('disabled', 'disabled');	/* disable back button if page=1 */
							}else{
								$('#pager_l').removeAttr('disabled');			/* enable back button */
							}
		
							if(obj.total_found < page_limit){
								$('#pager_r').attr('disabled', 'disabled');	/* disable next button if page=last */
							}else{
								$('#pager_r').removeAttr('disabled');			/* enable next button */
							}
						}
						/* end show/hide pagination button */
	
						$('#reset_page').val('1');									/* allow reseting of page no */
						
						$('#searchbox').keydown(function(e){
							search_object.abort();
						});
					});
				}
		}
		
		jQuery(document).ready(function(){
			//var def_val = "Lastname, Firstname or Comp No.";
			var def_val = "Lastname, Firstname";
			var cur_val = "";
			
			$("#searchbox").val(def_val);

			$("#frm_search_pop").bind("keypress", function(e){
				cur_val = $('#searchbox').val();
				if (e.keyCode == 13) return false;							/* disable enter key */
	        });

			$('#searchbox').click(function(){
				if($('#searchbox').val() == def_val){
					$('#searchbox').val('');
					$('#searchbox').css('color', '#000');
				}
			});

			$('#searchbox').blur(function(){
				if($('#searchbox').val() == ''){
					$('#searchbox').val(def_val);
					$('#searchbox').css('color', '#C1CDCD');
				}
			});

			$('#searchbox').keyup(function(e){
				process_search(e, cur_val);
			});

			/* IF COPY/PASTE */
				$('#searchbox').bind('paste', function(e){
					$('#searchbox').trigger('keyup');
				});

			/* PAGINATION */
				$('#pager_l').click(function(e){
					$('#page').val(parseInt($('#page').val()) - 1);
					$('#reset_page').val('0');
					//$('#searchbox').trigger('keyup');
					process_search(e, cur_val);
				});

				$('#pager_r').click(function(e){
					$('#page').val(parseInt($('#page').val()) + 1);
					$('#reset_page').val('0');
					//$('#searchbox').trigger('keyup');
					process_search(e, cur_val);
				});
			/* END PAGINATION */
			
			$("#btn_srch_switch").click(function() {
				if ($("#span_srch_label").html() != 'Search by Name :') {
					$("#span_srch_label").html("Search by Name :");
					$("#srch_for").val("applicant_name");
					
					$(this).css({'background-color':'#C0C0C0','color':'#A40101','cursor':'pointer'});
					
					def_val = "Lastname, Firstname";
				} else {
					$("#span_srch_label").html("Search by Computer No :");
					$("#srch_for").val("applicant_computer");
					
					$(this).css({'background-color':'green','color':'white','cursor':'pointer'});
					
					def_val = "Computer No.";
				}

				if($('#searchbox').val() == 'Lastname, Firstname' || $('#searchbox').val() == 'Computer No.'){
					$('#searchbox').val(def_val);
					$('#searchbox').css('color', '#C1CDCD');
				}else{
					//$('#searchbox').trigger('keyup');
					$('#searchbox').focus();
				}
				//$('#searchbox').val(def_val);
				//$('#searchbox').css('color', '#C1CDCD');
				
			});
		});
	</script>
	<body>
		<form name="frm_search_pop" id="frm_search_pop" method="POST">
			<input type="hidden" name="page" id="page" value="0" />
			<input type="hidden" name="pagename" id="pagename" value="<?=$pagename?>" />
			<input type="hidden" name="param" id="param" value="<?=$param?>" />
			<input type="hidden" name="additionalparam" id="additionalparam" value="<?=$additionalparam?>" />
			<input type="hidden" name="reset_page" id="reset_page" value="1" />
			<input type="hidden" name="srch_for" id="srch_for" value="applicant_name" />
			<input type="hidden" name="btn_pagenate" id="btn_pagenate" value="" />
			<table cellpadding="1" cellspacing="1" border="0" width="600">
				<tr>
					<td align="center" colspan="4">
						<table cellpadding="1" cellspacing="1" border="0" width="100%">
							<tr>
								<td width="20%">
									<input type="button" id="btn_srch_switch" name="btn_srch_switch" value="Switch" style="background-color: #C0C0C0; color: #A40101; cursor:pointer;">&nbsp;									
								</td>
								<td align="left">
									<b><i><span id="span_srch_label">Search by Name :</span></i></b>&nbsp;
									<input type="text" name="searchbox" id="searchbox" value="Lastname, Firstname or Comp No." size="40" style="color:#C1CDCD"; />
								</td>
							</tr>
						</table>

					</td>
					
<!--					<td align="center" colspan="4">
						<b><i>Search by Name or Computer No.</i></b>&nbsp;
						<input type="text" name="searchbox" id="searchbox" value="Lastname, Firstname or Comp No." size="40" style="color:#C1CDCD"; />
					</td>-->
				</tr>
			</table>
		</form>
		<span id="qry"></span>

		<!--LOADING IMAGE-->
		<div id="load_img" style="display:none;" align="center">
			<b>please wait</b><img src="<?=base_url();?>public/img/ajax-loader-dot.gif" width="12" height="12"><br><br>
		</div>
		<div id="load_img_txt" style="display:none;" align="center">
			<b>Press ENTER to start searching.<br><br>
		</div>

		<!--APPLICANT LIST CONTAINER-->
		<div id="dtl_cont" style="border:solid 0px;" align="center"></div>

		<!--CLOSE BUTTON-->
		<table cellpadding="1" cellspacing="1" width="600" border="0">
			<tr>
				<td>
					<span id="html_remarks" style="display:none;">Click <input type="button" value="New Applicant" title="Click here for new applicant" onClick="window.location='<?=base_url()?>reception/followup/new_applicant'"> if record is not found.<span>
				</td>
			</tr>	
			<tr>
				<td width="70%" align="left">&nbsp;
					<div>
						<input style="display:none;" type="button" id="pager_l" id="pager_l" value="<" />
						<span id="txt_page" style="display:none;">records <span id="span_pageno"></span> to <span id="span_total_pages"></span> of <span id="span_all_total"></span></span>
						<input style="display:none;" type="button" id="pager_r" id="pager_r" value=">" />
					</div>
				</td>
				<td width="30%" align="right">
					<!--<input type="button" style="display:none;" id="btn_new_applicant" name="btn_new_applicant" value="New Applicant" title="Click here for new applicant" onClick="window.location='activity_edit_reception2.php'">-->
					<input type=submit value="Close" name="user" class="button" onClick="jQuery(document).trigger('close.facebox'); return false;" style="cursor:pointer;">
				</td>
			</tr>
		</table>
	</body>
</html>