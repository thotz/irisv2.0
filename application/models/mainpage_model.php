<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mainpage_model extends CI_Model {
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function get_total_applicant()
	{
		$query = $this->db->query('select count(*) as total_app from personal');
		return $query->result_array();
	}
	
	public function get_total_principal()
	{
		$query = $this->db->query('select count(*) as total_prin from principals');
		return $query->result_array();
	}
	
	public function chart_sourceofapplication()
	{
		$start_date = date('Y-m-d',strtotime('-12 month'));
		$end_date = date('Y-m-d',strtotime('now'));
	
		$query = "select p.applicant_id as line_up_id,date(p.create_date) as date_create, p.create_by as creator, p.cv_source
		from personal as p
		where 1
		and p.create_date between '{$start_date}' and '{$end_date}'
		and p.create_by != 'admin'";
		$query = $this->db->query($query);
	
		$result = $query->result_array();
	
		$ol_applicant = array(); /*ONLINE*/
		$wa_applicant = array(); /*WORKABROAD*/
		$pra_applicant = array(); /*PRA*/
		$di_applicant = array(); /*DIRECT/WALKIN*/
		$re_applicant = array(); /*	REFERRED*/
		$key_column = array();
	
		if($result && count($result) > 0){
			foreach ($result as $val){
				if($val['creator'] == 'admin' && ($val['date_create'] == '2014-11-26' || $val['date_create'] == '2014-11-19')){
					/*DO NOT INCLUDE IN THE LIST*/
				}else{
					$labels = date('F Y', strtotime($val['date_create']));
					$temp = date('Y-m', strtotime($val['date_create']));
					$key = strtotime($temp);
					$key_column[$key] = $labels;
	
					if(trim($val['creator']) == '' and $val['cv_source'] == 'Online'){
						$ol_applicant[$key][$val['line_up_id']] = $val['line_up_id'];
					}else if (trim($val['creator']) != '') {
						if($val['cv_source'] == 'Direct'){
							$di_applicant[$key][$val['line_up_id']] = $val['line_up_id'];
						} else if($val['cv_source'] == 'PRA/Headhunting/Jobs Fair'){
							$pra_applicant[$key][$val['line_up_id']] = $val['line_up_id'];
						}else if($val['cv_source'] == 'Online'){
							$wa_applicant[$key][$val['line_up_id']] = $val['line_up_id'];
						}else if($val['cv_source'] == 'Referred'){
							$re_applicant[$key][$val['line_up_id']] = $val['line_up_id'];
						}
					}
				}
	
			}
		}
	
		$ol_applicant_total = array(); /*ONLINE*/
		$wa_applicant_total = array(); /*WORKABROAD*/
		$pra_applicant_total = array(); /*PRA*/
		$di_applicant_total = array(); /*DIRECT/WALKIN*/
		$re_applicant_total = array(); /*	REFERRED*/
		foreach ($key_column as $key=>$value) {
			if (isset($ol_applicant[$key])) $ol_applicant_total[$value] = count($ol_applicant[$key]);
			
			if (isset($wa_applicant[$key])) $wa_applicant_total[$value] = count($wa_applicant[$key]);
			
			if (isset($pra_applicant[$key])) $pra_applicant_total[$value] = count($pra_applicant[$key]);
			
			if (isset($di_applicant[$key])) $di_applicant_total[$value] = count($di_applicant[$key]);
			
			if (isset($re_applicant[$key])) $re_applicant_total[$value] = count($re_applicant[$key]);
		}
	
		return array(
				'online' => $ol_applicant_total,
				'workabroad' => $wa_applicant_total,
				'pra' => $pra_applicant_total,
				'direct' => $di_applicant_total,
				'reffered' => $re_applicant_total
		);
	}
}