<?php

Class File_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('upload');
		$this->load->helper('myfile');
	}
	
	public function upload_file($upload_folder, $file_element_name, $applicant_id, $replace_filename = true)
	{
		$status = "error";
		$message = "File Element Name Not Found.";
		$data = array();
		$database_filename = "";
		
		if (isset($_FILES[$file_element_name])) {
			/* if ($replace_filename equal to TRUE), replace the filename with the $applicant_id  */
			if ($replace_filename) {
				$file_ext = $this->upload->get_extension($_FILES[$file_element_name]['name']);
				$file_name = $applicant_id.$file_ext;
				
				/* Set the new filename */
				$config['file_name'] = $file_name;
			} else {
				$file_name = $_FILES[$file_element_name]['name'];
			}
			
			/* Check for allowed allowed_types */
			switch ($file_element_name) {
				case 'cv_applicant':
					$config['allowed_types'] = 'doc|docx';
					break;
			
				case 'cv_applicant_pdf':
					$config['allowed_types'] = 'pdf';
					break;
			
				case 'file_att1':
				case 'file_att2':
				case 'file_att3':
				case 'file_att4':
				case 'file_email_attach':
					$config['allowed_types'] = 'doc|docx|pdf|gif|jpg|png';
					break;
					
				default:
					$config['allowed_types'] = 'gif|jpg|png';
			}
			$config['overwrite'] = TRUE;
			$config['file_ext_tolower'] = TRUE;
			$config['max_size'] = 20000000;
	
			/* Create Directory Folder */
			$directory_local = create_directory($upload_folder, $applicant_id);
			$fullpath_filename = $directory_local["directory1"];
			
			if ($file_name) {
				$config['upload_path'] = $fullpath_filename;
		
				$this->upload->initialize($config);
				
				if (!$this->upload->do_upload($file_element_name)) {
					$status = 'error';
					$message = $this->upload->display_errors('', '');
				} else {
					$data = $this->upload->data();
					
					$new_file_name = $data['file_name'];
					$database_filename = $directory_local["directory2"].$new_file_name;
					
					/* Create Shared Folder and Copy the File */
					$directory_share = create_directory($upload_folder, $applicant_id, FILE_SHARED);
					copy($fullpath_filename.$new_file_name, $directory_share["directory1"].$new_file_name);
					
					$status = "success";
					$message = "File successfully uploaded";
				}
				@unlink($_FILES[$file_element_name]);
			}
		}
		
		return array(
				'status' => $status, 
				'message' => $message, 
				'data' => $data,
				'database_filename' => $database_filename, 
		);
	}
}