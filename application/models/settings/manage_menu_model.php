<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Manage_menu_model extends CI_Model {
	
	public function __construct(){
		parent::__construct();
	}
	
	public function add_menu(){
		$query = $this->db->query("select * from menu_high where name='".$_POST["name"]."'");
		$result = $query->result_array();
		if (count($result)) error_message('There is already an existing Menu like the one you entered.');

		$query = $this->db->query("select max(order_id) as order_id from menu_high");
		$order_id = $query->row_array();

		$_POST['order_id'] = $order_id['order_id'] + 1;
		
		$remove_data = array('clinic');
		$new_data = filter_before_add($_POST, $remove_data);
		
		return $this->db->insert('menu_high', $new_data);
	}
	
	public function edit_menu(){
		$query = $this->db->query("select * from menu_high where name='".$_POST["name"]."' and menu_high_id != ".$_POST["menu_high_id"]);
		$result = $query->result_array();
		if (count($result)) error_message('There is already an existing Menu like the one you entered.');

		$remove_data = array('clinic');
		$new_data = filter_before_add($_POST, $remove_data);

		return $this->db->update('menu_high', $new_data, array('menu_high_id' => $_POST['menu_high_id']));
	}
	
	public function add_submenu(){
		$result = $this->db->query("select * from menu_high where name='".$_POST["name"]."' and filename='".$_POST["filename"]."'")
				 ->result_array();
		if (count($result)) error_message('There is already an existing Menu like the one you entered.');

		$query = $this->db->query("select max(order_id) as order_id from menu_low where menu_high_id = " .$_POST["menu_high_id"]);
		$order_id = $query->row_array();
		
		/** Updated data */
		if (empty($_POST['menu_low_ids'])) $_POST['menu_low_ids'] = 0;
		
		/** Added data */
		$_POST['order_id'] = $order_id['order_id'] + 1;
		$_POST['mod_action'] = 0;
		$_POST['mod_delete'] = 0;
		$_POST['subs_menu'] = 0;
		
		$remove_data = array('clinic', 'menu_low_id');
		$new_data = filter_before_add($_POST, $remove_data);
		
		return $this->db->insert('menu_low', $new_data);
	}
	
	public function edit_submenu(){
		/** Updated data */
		if (empty($_POST['menu_low_ids'])) $_POST['menu_low_ids'] = 0;
				
		$remove_data = array('clinic');
		$new_data = filter_before_add($_POST, $remove_data);

		return $this->db->update('menu_low', $new_data, array('menu_low_id' => $_POST['menu_low_id']));
	}
	
	public function get_menu_data($menu_high_id){
		$alignment 	= array("Left", "Center", "Right");
		$menu 		= $this->db->query("Select * from menu_high where menu_high_id ='". $menu_high_id ."'")
					->row_array();
		
		$array_data = array(
						'menu_high_id' => $menu_high_id,
						'alignment' => $alignment,
						'menu' => $menu
						);
		return $array_data;
	}
	
	public function get_submenu_data($menu_high_id, $menu_low_id){
		$sql_withlowid = '';
		$menulow = array();
		if ($menu_low_id) {
			$menulow = $this->db->query("Select * from menu_low where menu_low_id ='". $menu_low_id ."'")
						->row_array();

			if(isset($menulow["menu_low_id"])) $sql_withlowid = "and menu_low_id != '".$menulow["menu_low_id"]."'";
		}
		
		$menu_lows = getdata("select * from menu_low where 1 and menu_high_id = '$menu_high_id' $sql_withlowid order by name asc"); 
		$array_data = array(
						'menu_high_id' => $menu_high_id,
						'menu_lows' => $menu_lows,
						'menu_low_id' => $menu_low_id,
						'menulow' => $menulow
						);
		return $array_data;
	}
	
	public function get_formated_side_menu()
	{
		$user_access_id = 1;
		$menu_high = $this->db->query("select menu_high.*
							from menu_high order by menu_high.order_id,menu_high.name asc");
		
		$formated_result = array();
		foreach ($menu_high->result_array() as $value) {
			$menu_high_id = $value['menu_high_id'];
			$menu_high_name = $value['name'];
			$menu_high_icon = $value['icon'];
		
			$menu_low = $this->db->query("select menu_low.*
					from menu_low where menu_low.menu_high_id = '$menu_high_id' and menu_low.access_id like '%\"$user_access_id\",%' and menu_low.menu_low_ids = '0'
					order by menu_low.order_id,menu_low.name asc");
		
			$formated_result_sub = array();
			if (count($menu_low->result_array())) {
				foreach ($menu_low->result_array() as $value) {
					$menu_low_id = $value['menu_low_id'];
		
					$menu_sub = $this->db->query("select menu_low.*
							from menu_low where menu_low.menu_high_id = '$menu_high_id' and menu_low.access_id like '%\"$user_access_id\",%' and menu_low.menu_low_ids = '$menu_low_id'
							order by menu_low.order_id,menu_low.name asc");
		
					$formated_result_sub[] = array(
							'menu_low_id' => $value['menu_low_id'],
							'menu_low_name' => $value['name'],
							'menu_low_filename' => $value['filename'],
							'menu_low_access_id' => $value['access_id'],
							'menu_sub' => $menu_sub->result_array()
					);
				}
			}
			$formated_result[] = array(
					'high_name' => $menu_high_name,
					'menu_high_icon' => $menu_high_icon,
					'menu_low' => $formated_result_sub
			);
		
		}
	}
}
