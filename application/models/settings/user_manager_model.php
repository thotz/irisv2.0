<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_manager_model extends CI_model {
	
	public function __construct(){
		parent::__construct();
	}
	
	public function get_all_user(){
		//$result = $this->db->query('SELECT * from users where 1 order by username asc');
		$result = $this->db->query('select u.user_id, u.username, u.fname, u.mname, u.lname, u.status, b.description as branch, a.name as access
									from users u
									left join branch b
									on u.branch_id = b.id
									left join access a
									on u.access_id = a.access_id
									where 1
									order by u.username asc');
		return $result->result();
	}

	public function get_user($user_id)
	{
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where('user_id', $user_id);
		$query = $this->db->get();
		return $query->row_array();
	}

	public function new_user(){
		$this->db->set('status', $this->input->post('radStatus', TRUE));
// 		$this->db->set('ew_id', $this->input->post('inputEWID', TRUE));
		(NULL!==$this->input->post('inputEWID') && $this->input->post('inputEWID')!='') ? $this->db->set('ew_id', $this->input->post('inputEWID', TRUE)):"";
		$this->db->set('username', $this->input->post('inputUsername', TRUE));
		$this->db->set('fname', $this->input->post('inputFirstName', TRUE));
		$this->db->set('mname', $this->input->post('inputMI', TRUE));
		$this->db->set('lname', $this->input->post('inputLastName', TRUE));
		$this->db->set('position', $this->input->post('inputPosition', TRUE));
		$this->db->set('pos_code', $this->input->post('inputCode', TRUE));
		$this->db->set('department', $this->input->post('inputDepartment', TRUE));
		$this->db->set('telno', $this->input->post('inputTel', TRUE));
		$this->db->set('loc_no', $this->input->post('inputLocal', TRUE));
		$this->db->set('ew_mob', $this->input->post('inputEWMobile', TRUE));
		$this->db->set('skype', $this->input->post('inputSkype', TRUE));
		$this->db->set('corp_email', $this->input->post('inputCorpEmail', TRUE));
		$this->db->set('app_email', $this->input->post('inputApplEmail', TRUE));
		$this->db->set('password', 'PASSWORD("'.$this->input->post('inputPassword', TRUE).'")', FALSE);
		$this->db->set('password2', 'PASSWORD("'.$this->input->post('inputPassword', TRUE).'")', FALSE);
		$this->db->set('branch_id', $this->input->post('selectBranch', TRUE));
		$this->db->set('access_id', $this->input->post('selectAccLvl', TRUE));
		$this->db->set('evaluator', $this->input->post('radEvaluator', TRUE));
		$this->db->set('add_by', $this->session->userdata['iris_user_id']);
		$this->db->set('add_date', 'NOW()', FALSE);

		$this->db->insert('users');
		return $this->db->insert_id();
	}

	public function edit($user_id)
	{
		$this->db->set('status', $this->input->post('radStatus', TRUE));
		(NULL!==$this->input->post('inputEWID') && $this->input->post('inputEWID')!='') ? $this->db->set('ew_id', $this->input->post('inputEWID', TRUE)):"";
		$this->db->set('fname', $this->input->post('inputFirstName', TRUE));
		$this->db->set('mname', $this->input->post('inputMI', TRUE));
		$this->db->set('lname', $this->input->post('inputLastName', TRUE));
		$this->db->set('position', $this->input->post('inputPosition', TRUE));
		$this->db->set('pos_code', $this->input->post('inputCode', TRUE));
		$this->db->set('department', $this->input->post('inputDepartment', TRUE));
		$this->db->set('telno', $this->input->post('inputTel', TRUE));
		$this->db->set('loc_no', $this->input->post('inputLocal', TRUE));
		$this->db->set('ew_mob', $this->input->post('inputEWMobile', TRUE));
		$this->db->set('skype', $this->input->post('inputSkype', TRUE));
		$this->db->set('corp_email', $this->input->post('inputCorpEmail', TRUE));
		$this->db->set('app_email', $this->input->post('inputApplEmail', TRUE));
		($this->input->post('inputPassword', TRUE)!='') ? $this->db->set('password', 'PASSWORD("'.$this->input->post('inputPassword', TRUE).'")', FALSE):"";
		($this->input->post('inputPassword', TRUE)!='') ? $this->db->set('password2', 'PASSWORD("'.$this->input->post('inputPassword', TRUE).'")', FALSE):"";
		$this->db->set('branch_id', $this->input->post('selectBranch', TRUE));
		$this->db->set('access_id', $this->input->post('selectAccLvl', TRUE));
		$this->db->set('evaluator', $this->input->post('radEvaluator', TRUE));
		$this->db->set('edit_by', $this->session->userdata['iris_user_id']);
		$this->db->set('edit_date', 'NOW()', FALSE);

		$this->db->where('user_id', $user_id);
		return $this->db->update('users');
	}
}