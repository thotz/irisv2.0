<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Manage_access_model extends CI_model {
	
	public function __construct(){
		parent::__construct();
	}

	public function get_menu(){
		$result = $this->db->query("select mh.menu_high_id, mh.name as 'main_menu', ml.subs_menu, ml.menu_low_id, ml.name as sub_menu, ml.menu_low_ids, ml.access_id, ml.mod_action, ml.mod_delete, ml.access_id_mod, ml.access_id_del
									from menu_high mh
									left join menu_low ml
									on mh.menu_high_id = ml.menu_high_id
									where 1
									and mh.name not in ('Pre-printed Forms', 'Help')
									order by mh.order_id asc, ml.order_id asc");
		
		$menu = array();
		if($result->result_array()){
			foreach ($result->result_array() as $val){
				/* MAIN MENU */
				$menu[$val['menu_high_id']]['desc'] = $val['main_menu'];
				$menu[$val['menu_high_id']]['menu_id'] = $val['menu_high_id'];

				if($val['menu_low_ids'] == 0){
					/* SUB MENU 1 */
					$menu[$val['menu_high_id']]['sub'][$val['menu_low_id']]['desc'] = $val['sub_menu'];
					$menu[$val['menu_high_id']]['sub'][$val['menu_low_id']]['menu_id'] = $val['menu_low_id'];
					$menu[$val['menu_high_id']]['sub'][$val['menu_low_id']]['access'] = $val['access_id'];
					$menu[$val['menu_high_id']]['sub'][$val['menu_low_id']]['del'] = $val['mod_delete'];
					$menu[$val['menu_high_id']]['sub'][$val['menu_low_id']]['act'] = $val['mod_action'];
					$menu[$val['menu_high_id']]['sub'][$val['menu_low_id']]['access_del'] = $val['access_id_del'];
					$menu[$val['menu_high_id']]['sub'][$val['menu_low_id']]['access_act'] = $val['access_id_mod'];
				}else{
					/* SUB MENU 2 */
					$menu[$val['menu_high_id']]['sub'][$val['menu_low_ids']]['sub'][$val['menu_low_id']]['desc'] = $val['sub_menu'];
					$menu[$val['menu_high_id']]['sub'][$val['menu_low_ids']]['sub'][$val['menu_low_id']]['menu_id'] = $val['menu_low_id'];
					$menu[$val['menu_high_id']]['sub'][$val['menu_low_ids']]['sub'][$val['menu_low_id']]['access'] = $val['access_id'];
					$menu[$val['menu_high_id']]['sub'][$val['menu_low_ids']]['sub'][$val['menu_low_id']]['del'] = $val['mod_delete'];
					$menu[$val['menu_high_id']]['sub'][$val['menu_low_ids']]['sub'][$val['menu_low_id']]['act'] = $val['mod_action'];
					$menu[$val['menu_high_id']]['sub'][$val['menu_low_ids']]['sub'][$val['menu_low_id']]['access_del'] = $val['access_id_del'];
					$menu[$val['menu_high_id']]['sub'][$val['menu_low_ids']]['sub'][$val['menu_low_id']]['access_act'] = $val['access_id_mod'];
				}
			}
		}

		return $menu;
	}

	public function get_access(){
		$query = $this->db->get('access');

		foreach($query->result_array() as $this_key => $this_data){
			$finalArray[$this_data['access_id']] = $this_data;
		}

		return $finalArray;
	}

	public function add(){
// 		$this->db->trans_begin();

		/* CREATE NEW ACCESS */
		$this->db->insert('access', array('name' => $this->input->post('inputAccessName'), 'codename' => $this->input->post('inputCodeName')));
		$new_access_id = '"'.$this->db->insert_id().'",';
		$raw_access_id = $this->db->insert_id();

		if(NULL!==$this->input->post('access_menu')){
			foreach ($this->input->post('access_menu') as $key => $val){
				/* ADD MENU ACCESS */
				$this->db->set('access_id', "concat(access_id,'".$new_access_id."')", FALSE);

				if(count($val) > 1){
					foreach ($val as $option){
						if($option == 'mod'){
							/* ADD ACCESS MODIFY */
							$this->db->set('access_id_mod', "concat(access_id_mod,'".$new_access_id."')", FALSE);
						}else{
							/* ADD ACCESS DELETE */
							$this->db->set('access_id_del', "concat(access_id_del,'".$new_access_id."')", FALSE);
						}
					}
				}

				$this->db->where('menu_low_id', $key);
				$this->db->update('menu_low');
			}
		}

// 		if ($this->db->trans_status() === FALSE){
// 			$this->db->trans_rollback();
// 			return FALSE;
// 		}else{
// 			$this->db->trans_commit();
// 			return $raw_access_id;
// 		}

		return $raw_access_id;
	}

	public function delete($access_id){
		$query = $this->db->query('SELECT * FROM users where access_id = "'.$access_id.'"');
		if($query->num_rows() > 0){
			$error = TRUE;
			$msg = "Access is being used by other users.";
		}else{
 			if($this->db->delete('access', array('access_id' => $access_id))){
				$query_chk_access = $this->db->query("select * from menu_low
													where access_id like '%".$access_id."%'
													or access_id_mod like '%".$access_id."%'
													or access_id_del like '%".$access_id."%'");
				if($query_chk_access->num_rows() > 0){
					foreach ($query_chk_access->result_array() as $val){
						/* REMOVE ACCESS FROM EACH MENU */
						$this->db->set('access_id', str_ireplace('"'.$access_id.'",', '', $val['access_id']));
						$this->db->set('access_id_mod', str_ireplace('"'.$access_id.'",', '', $val['access_id_mod']));
						$this->db->set('access_id_del', str_ireplace('"'.$access_id.'",', '', $val['access_id_del']));
						$this->db->where('menu_low_id', $val['menu_low_id']);
						$this->db->update('menu_low');
					}
				}

				$error = FALSE;
				$msg = TRUE;
			}else{
				$error = TRUE;
				$msg = "An error occurred.";
			}
		}
		
		return array('error' => $error, 'msg' => $msg);
	}
}