<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Create_applicant_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		
	}
	
	public function get_preferred_position($applicant_id="", $position_id1="", $position_id2="", $position_id3="")
	{
		if ( ! $this->cache->get('html_select_position') || $applicant_id) {
			$query = $this->db->query("select positions.* from positions where name not in('','-','-*','--','.') order by name");
			$positions = $query->result_array();
			
			$select_position1 = "";
			$select_position2 = "";
			$select_position3 = "";
			if (is_array($positions) && count($positions) > 0) {
				foreach($positions as $value) {
					$position_name = $value['name'];
					$position_value = $value['position_id'];
					
					if ($position_value == $position_id1) {
						$select_position1 .= "<option value='".$position_value."' selected='selected'>".$position_name."</option>";
					} else if ($position_value == $position_id2) {
						$select_position2 .= "<option value='".$position_value."' selected='selected'>".$position_name."</option>";
					} else if ($position_value == $position_id3) {
						$select_position3 .= "<option value='".$position_value."' selected='selected'>".$position_name."</option>";
					} else {
						$select_position1 .= "<option value='".$position_value."'>".$position_name."</option>";
						$select_position2 .= "<option value='".$position_value."'>".$position_name."</option>";
						$select_position3 .= "<option value='".$position_value."'>".$position_name."</option>";
					}
			
				}
				$data = array(
						'position1'=>$select_position1,
						'position2'=>$select_position2,
						'position3'=>$select_position3
				);
						
				$this->cache->save('html_select_position', $data, 43200);
			}
		}
		return $this->cache->get('html_select_position');
	}
	
	public function get_specialization($applicant_id="", $jobspec_id1="", $jobspec_id2="", $jobspec_id3="")
	{
		if ( ! $this->cache->get('html_select_natureofbusiness') || $applicant_id) {
			$query = $this->db->query("select * from job_spec where 1 order by name asc");
			$specializations = $query->result_array();
			
			$select_natureofbusiness1 = "";
			$select_natureofbusiness2 = "";
			$select_natureofbusiness3 = "";
			if (is_array($specializations) && count($specializations) > 0) {
				foreach($specializations as $value) {
					$position_name = $value['name'];
					$position_value = $value['jobspec_id'];
					
					if ($position_value == $jobspec_id1) {
						$select_natureofbusiness1 .= "<option value='".$position_value."' selected='selected'>".$position_name."</option>";
					} else if ($position_value == $jobspec_id2) {
						$select_natureofbusiness2 .= "<option value='".$position_value."' selected='selected'>".$position_name."</option>";
					} else if ($position_value == $jobspec_id3) {
						$select_natureofbusiness3 .= "<option value='".$position_value."' selected='selected'>".$position_name."</option>";
					} else {
						$select_natureofbusiness1 .= "<option value='".$position_value."'>".$position_name."</option>";
						$select_natureofbusiness2 .= "<option value='".$position_value."'>".$position_name."</option>";
						$select_natureofbusiness3 .= "<option value='".$position_value."'>".$position_name."</option>";
					}
				}
				$data = array(
						'natureofbusiness1'=>$select_natureofbusiness1,
						'natureofbusiness2'=>$select_natureofbusiness2,
						'natureofbusiness3'=>$select_natureofbusiness3
				
				);
				$this->cache->save('html_select_natureofbusiness', $data, 43200);
			}
		}
		return $this->cache->get('html_select_natureofbusiness');
	}
	
	public function get_applied_position()
	{
		if ( ! $this->cache->get('html_select_applied_position')) {
			/*	Select option on Agencys Ref No: */
			$sql_ref_no = "select mr.status,mp.status_mr,mp.mr_pos_id, p.position_id, mr.manpower_rid, p.name as position_name, mr.ref_no, pr.name as prin_name, pc.name as comp_name, concat(p.name, ' - ',mr.ref_no) as name
					from manpower_r as mr
					left join prin_company as pc ON pc.prin_company_id = mr.prin_company_id
					left join principals as pr ON pr.principal_id = mr.principal_id
					left join mr_position as mp on mp.manpower_rid = mr.manpower_rid
					left join positions as p on p.position_id = mp.position_id where 1
					and mr.manpower_rid not in (2445)
					and mr.ref_no != ''
					and mr.mrpriority in ('1')
					and mr.status = 'Active'
					and mp.status_mr = 'Active'
					order by mr.status,position_name,prin_name, trim(mr.ref_no) asc";
			
			$query = $this->db->query($sql_ref_no);
			$manpower = $query->result_array();
			
			$select_manpower_rid = "";
			if (is_array($manpower) && count($manpower) > 0) {
				foreach ($manpower as $value) {
					$style_active = ' style="background-color:white;"';
					if ($value["status"] == 'Active') {
						//$style_active = ' style="background-color:red;"';
					}
			
					if ($value["position_name"]) {
						$str_value = $value["position_name"].' &nbsp;|&nbsp; '. $value["prin_name"]. ' &nbsp;|&nbsp; ' .$value["ref_no"];
						$select_manpower_rid .= '<option value="'.$value["manpower_rid"].':'.$value["position_id"].'" '.$style_active.'> '.$str_value.' &nbsp; </option>';
					}
				}
				$data = array(
						'manpower_position' => $select_manpower_rid
				);
				$this->cache->save('html_select_applied_position', $data, 43200);
			}
		}
		return $this->cache->get('html_select_applied_position');
	}
	
	public function get_municipality($type="", $current_value="")
	{
		$query = $this->db->query("select * from city where 1 order by name");
		$result = $query->result_array();
		
		switch ($type) {
			case 'get_select_option':
				if (is_array($result)) {
					$str_option = "";
					foreach ($result as $value) {
						$my_value = $value['city_id'];
						$my_display = $value['name']. ' - ' .$value['province'];
						
						if ($my_value == $current_value) {
							$str_option .= '<option value="'.$my_value.'" selected>'.$my_display.'</option>';
						} else {
							$str_option .= '<option value="'.$my_value.'">'.$my_display.'</option>';
						}
					}
				}
				return $str_option;
				break;
				
			default:
				return $result;
		}
	}
	
	public function get_method_application()
	{
		$str_option = "";
		foreach (applicant_method_application() as $key=>$value) {
			$str_option .= '<option value="'.$value.'">'.$value.'</option>';
		}
		return $str_option;
	}
	
	public function get_source_application($method_application="")
	{
		$selected_value = "";
		switch ($method_application) {
			case 'Direct':
				$sqlsources = "select * from source where status = 1 order by name asc";
				break;
			case 'Email':
			case 'Mail':
				$sqlsources = "select * from source where code in ('BU','SS','FL','RD','TV','AG') order by name asc";
				break;
			case 'Online':
				$sqlsources = "select * from source where status = 1 order by name asc";
				break;
			case 'Online Overseas':	
				$sqlsources = "select * from source where code in ('WA','EW') order by name asc";
				break;
			case 'Referred':
				$sqlsources = "select * from source where code in ('RC','RW','RT','RP','RFP') order by name asc";
				break;
			case 'PRA':
			case 'PRA/Headhunting/Jobs Fair':
				$selected_value = 4;
				$sqlsources = "select * from source where status = 1 order by name asc";
				break;
			case 'EWTC':
				$sqlsources = "select * from source where code in ('WI','TESDA') order by name asc";
				break;
			default:
				$sqlsources = "select * from source where status = 1 order by name asc";
		}
		
		$query = $this->db->query($sqlsources);
		$result = $query->result_array();
		
		$str_option = '<option value=""></option>';
		if (is_array($result) && count($result) > 0) {
			foreach ($result as $value) {
				if ($value['source_id'] == $selected_value) {
					$str_option .= '<option value="'.$value['source_id'].'" selected="selected">'.$value['name'].'</option>';
				} else {
					$str_option .= '<option value="'.$value['source_id'].'">'.$value['name'].'</option>';
				}
			}
		}
		return $str_option;
	}
	
	public function get_personal_info($old_applicant_id)
	{
		$query = $this->db->query("select * 
					from personal as p
					left join personal_contact as pc on pc.applicant_id = p.applicant_id
					left join personal_address as pa on pa.applicant_id = p.applicant_id
					where p.applicant_id = '$old_applicant_id'
				");
		return $query->row_array();
	}
	
	public function get_option_nearest_branch($address_city="")
	{
		$result = applicant_nearest_branch($address_city);
		
		$str_option = '<option value=""></option>';
		if ($address_city) $str_option = '';
		
		if (is_array($result) && count($result) > 0) {
			foreach ($result as $value) {
				$str_option .= '<option value="'.$value['id'].'">'.$value['name'].'</option>';
			}
		}
		return $str_option;
	}
	
	public function get_option_agent()
	{
		$query = $this->db->query("select * from agent where 1 and status = 1 order by name asc, agent_code asc");
		$result = $query->result_array();
		
		$str_option = '<option value=""></option>';
		if (is_array($result) && count($result) > 0) {
			foreach ($result as $value) {
				$str_option .= '<option value="'.$value['agent_id'].'">'.$value['name'].' - '.$value['agent_code'].'</option>';
			}
		}
		return $str_option;
	}
	
	public function get_option_refferal($personal_info)
	{
		$temp_data = array('FP'=>'For Processing','NP'=>'Normal Procedure');
		
		$str_option = "";
		foreach ($temp_data as $key=>$value) {
			$str_style = "";
			if ($personal_info['referral'] == $key) {
				$str_style = ' selected="selected"';
			}
			$str_option .= '<option value="'.$key.'" '.$str_style.'>'.$value.'</option>';
		}
		return $str_option;
	}

	public function get_pra_leader()
	{
		$query = $this->db->query("select username, user_id from users where 1 and status = 1 order by username asc");
		$result = $query->result_array();
	
		$str_option = '<option value=""></option>';
		if (is_array($result) && count($result) > 0) {
			foreach ($result as $value) {
				$str_option .= '<option value="'.$value['user_id'].'">'.$value['username'].'</option>';
			}
		}
		return $str_option;
	}
	
	public function get_applicant_cv($old_applicant_id="")
	{
		$query = $this->db->query("select * from personal_cv where 1 and applicant_id ='".$old_applicant_id."'");
		$result = $query->row_array();
		return $result;
	}
	
	/* Insert Prefered Position and Job Specialization */
	function update_prefered_position($applicant_id, $position_ids, $jobspec_ids) 
	{
		$return_status = true;
		$sqltoday = date("Y-m-d H:i:s");
		if (isset($position_ids) && is_array($position_ids)) {
			foreach($position_ids as $key=>$value) {
				//if ($value) {
				$jobspec_id = $jobspec_ids[$key];
				$my_position_id = $value;
				$position_order = $key + 1;
					
				$query = $this->db->query("select * from prefered_position where applicant_id='".$applicant_id."' and position_order='".$position_order."'");
				$sql_check_order = $query->row_array();
				
				if (is_array($sql_check_order) && count($sql_check_order) > 0) {
					$data = array(
							'position_id' => $my_position_id,
							'jobspec_id' => $jobspec_id,
							'date_edit' => date("Y-m-d H:i:s")
					);
					$result = $this->db->update('prefered_position',$data, array('applicant_id'=>$applicant_id,'position_order'=>$position_order));
						
				} else {
					$data = array(
							'applicant_id' => $applicant_id,
							'position_id' => $my_position_id,
							'date_create' => date("Y-m-d H:i:s"),
							'position_order' => $position_order,
							'jobspec_id' => $jobspec_id
					);
					$result = $this->db->insert('prefered_position', $data);
						
				}
				//}
			}
			if ($result) {
				create_log($applicant_id,"prefered_position","add");
			} else {
				$return_status = false;
			}
		}
		return $return_status;
	
	}
	
	/* insert Prefered Position, Job Specialization and jobs opening */
	function add_multi_applied_position($applicant_id, $position_ids, $jobspec_ids, $mr_positions) 
	{
		$sqltoday = date("Y-m-d H:i:s");
	
		/* add Prefered Position and Job Specialization */
		$this->update_prefered_position($applicant_id, $position_ids, $jobspec_ids);
	
		$query = $this->db->query("select status,cv_source,source_id from personal where applicant_id = '".$applicant_id."'");
		$applicant_info = $query->row_array();

		/* add manpower or Job Opening  */
		if (isset($mr_positions) && is_array($mr_positions)) {
			foreach($mr_positions as $key=>$value) {
				if ($value) {
					$my_position_array = explode(':', $value);
					$manpower_rid = $my_position_array[0];
					$position_id = $my_position_array[1];
	
					$job1['manpower_rid'] = $manpower_rid;
					$job1['position_id'] = $position_id;
	
					$data = array(
							'applicant_id' => $applicant_id,
							'position_id' => $position_id,
							'manpower_rid' => $manpower_rid,
							'webjob_id' => 0,
							'salary' => ""
					);
					$result = $this->db->insert('apply_applicant', $data);
	
					if ($result) {
						create_log($applicant_id,"apply_applicant","add");
						
						if ($applicant_info['status'] == 'DEADFILE') {
							/* always restrict the medical UNFIT and DEADFILE */
							if (check_medical_unfit($applicant_id)) {
								//	unfit here
							} else {
								$data_method['status'] = "ACTIVE";
								$applicant_info['status'] = 'ACTIVE';
							}
						}
	
						/* Update personal */
						if ($applicant_info['cv_source'] == "Online" || $applicant_info['cv_source'] == "PRA/Headhunting/Jobs Fair") {
							/* Update Personal */
							$result = $this->db->update('personal',array('method_report' => 'Online','cv_location_status'=>''),array('applicant_id'=>$applicant_id));
							
							$data_method = array(
									'for_assessment' => 1,
									'for_assessment_date' => date("Y-m-d H:i:s")
							);
						} else {
							/* Update Personal */
							$result = $this->db->update('personal',array('method_report' => 'Direct'),array('applicant_id'=>$applicant_id));
							
							$data_method = array(
									'for_assessment' => 1,
									'for_assessment_date' => date("Y-m-d H:i:s")
							);
								
						}
						$this->db->where('applicant_id',$applicant_id);
						$result = $this->db->update('personal_assessment',$data_method);
	
						if ($applicant_info['status'] != 'DEADFILE') {
							/* Start Auto to Evaluator - Assessment */
							$this->add_auto_assessment($applicant_id, $job1);
	
							/* Start Auto to Evaluator - Lineup */
							$this->add_auto_lineup($applicant_id, $job1);
						}
					}
	
				}
			}
	
			/* If NO Applied Position or Job Openning, just add to assessment */
			$user_id2 = $this->session->userdata['iris_user_id'];
			
			$query = $this->db->query("select max(cv_location_id) as cv_location_id from cv_location where applicant_id ='".$applicant_id."' and status = ''");
			$check_cv = $query->row_array();
			if (is_array($check_cv) && $check_cv['cv_location_id'] == '') {
				$data = array(
						'applicant_id' => $applicant_id,
						'user_id' => 0,
						'user_id1' => $user_id2,
						'user_id2' => $user_id2,
						'cv_source' => $applicant_info['cv_source'],
						'source_id' => $applicant_info['source_id'],
						'remarks' => '',
						'principal_id' => '',
						'date_rc' => '',
						'deadfile' => '',
						'status' => '',
						'date_created' => date("Y-m-d H:i:s"),
						'date_edit' => date("Y-m-d H:i:s")
				);
				$this->db->insert('cv_location', $data);
			
				return true;
			}
		}
	}
	
	function add_auto_assessment($applicant_id, $job1) 
	{
		$manpower_rid= $job1['manpower_rid'];
		$position_id= $job1['position_id'];
	
		if ($manpower_rid) {
			/* get manpower RSO for auto distribution */
			$query = $this->db->query("select mr.pm,u.branch_id,u.user_id
							from manpower_r as mr
							join users as u on u.username = mr.pm
							where 1
							and u.branch_id in ('1')
							and mr.manpower_rid = '".$manpower_rid."'");
			$manpower_info = $query->row_array();
			$rsr_username = $manpower_info['pm'];
	
			/* check for manpower RSO add cv_location */
			$query = $this->db->query("select cv_source,source_id from personal where applicant_id = '".$applicant_id."'");
			$applicant_info = $query->row_array();
			if (count($manpower_info)) {
				/* If Direct, Do not Distribute the evaluator  */
				if ($applicant_info['cv_source'] != 'Direct') {
					/* if the user's branch is the same of RSO branch, then autdistribute. */
					if ($manpower_info['branch_id']) {
						$rsr_user_id = $manpower_info['user_id'];
					} else {
						$rsr_user_id = 0;
					}
				} else {
					$rsr_user_id = 0;
				}
			} else {
				$rsr_user_id = 0;
			}
			$user_id2 = $this->session->userdata['iris_user_id'];
			
			$query = $this->db->query("select max(cv_location_id) as cv_location_id from cv_location where applicant_id ='".$applicant_id."' and status = ''");
			$check_cv = $query->row_array();
			if (is_array($check_cv) && $check_cv['cv_location_id'] == '') {
				$data = array(
						'applicant_id' => $applicant_id,
						'user_id' => $rsr_user_id,
						'user_id1' => $user_id2,
						'user_id2' => $user_id2,
						'cv_source' => $applicant_info['cv_source'],
						'source_id' => $applicant_info['source_id'],
						'remarks' => '',
						'principal_id' => '',
						'date_rc' => '',
						'deadfile' => '',
						'status' => '',
						'date_created' => date("Y-m-d H:i:s"),
						'date_edit' => date("Y-m-d H:i:s"),
						'manpower_rid' => $manpower_rid,
						'position_id' => $position_id
				);
				$this->db->insert('cv_location', $data);
	
				return true;
			} else {
				$data = array(
						'user_id' => $rsr_user_id,
						'user_id2' => $user_id2,
						'date_edit' => date("Y-m-d H:i:s")
				);
				$this->db->where('cv_location_id', $check_cv['cv_location_id']);
				$this->db->update('cv_location', $data);
	
				return false;
			}
		}
	}
	
	function add_auto_lineup($applicant_id, $job1) 
	{
		$manpower_rid= $job1['manpower_rid'];
		$position_id= $job1['position_id'];
	
		$query = $this->db->query("select mr_pos_id from mr_position where manpower_rid = '$manpower_rid' and position_id = '".$position_id."'");
		$check_mr_category = $query->row_array();
		if (is_array($check_mr_category) && count($check_mr_category) > 0) {
			$query = $this->db->query("select mname,branch_id1 from personal where applicant_id = '".$applicant_id."'");
			$personal_info = $query->row_array();
			
			$mname = $personal_info['mname'];
			$branch_id = ($personal_info['branch_id1']) ? $personal_info['branch_id1'] : $personal_info['branch_id'];
			
			$mr_pos_id = $check_mr_category['mr_pos_id'];
			$query = $this->db->query("select p.name,mp.mr_pos_id,mp.manpower_rid,mp.position_id,mr.principal_id,mr.ref_no,p.country_id from mr_position as mp
							join manpower_r as mr on mr.manpower_rid = mp.manpower_rid
							join principals as p on p.principal_id = mr.principal_id
							where mr_pos_id = '".$mr_pos_id."'");
			$results = $query->row_array();
			if ($results["country_id"] == '231' && $mname == '') {
				/* */
			} else {
				/* check for duplicate mrref */
				$query = $this->db->query("select * from line_up l
						left join mr_position mp on mp.mr_pos_id = l.mr_pos_id
						where l.manpower_rid='".$manpower_rid."'
						and l.applicant_id='".$applicant_id."'
						and mp.status_mr = 'Active'");
				$duplicate_mr = $query->row_array();
				
					
				if(!empty($duplicate_mr)){
					/* */
				} else {
					/* check if selected/accepted	*/
					if( ! $this->chklineupcondition($applicant_id, NULL, NULL)) {
						$query = $this->db->query("select act1,act2,act3 from manpower_r where manpower_rid = '".$manpower_rid."'");
						$mr_activitys = $query->row_array();
						
						/* get manpower RSO for auto distribution */
						$query = $this->db->query("select mr.pm,u.branch_id,u.user_id
							from manpower_r as mr
							join users as u on u.username = mr.pm
							where 1
							and u.branch_id in ('1')
							and mr.manpower_rid = '".$manpower_rid."'");
						$manpower_info = $query->row_array();
						$rsr_username = $manpower_info['pm'];
	
						$rsr_user_id = 0;
						if (count($manpower_info)) $rsr_user_id = $manpower_info['user_id'];
	
						$new_data = array();
						$arr_data = array(
								'applicant_id' => $applicant_id,
								'manpower_rid' => $manpower_rid,
								'mr_pos_id' => $mr_pos_id,
								'evaluator' => $rsr_user_id,
								'mob_result' => 'AV',
								'branch_id' => $branch_id,
								'creator' => $this->session->userdata['iris_user_id'],
								'date_create' => date("Y-m-d H:i:s"),
								'for_confirm_initial' => 'yes',
								'fr' => $mr_activitys['act1'],
								'lu' => $mr_activitys['act2'],
								'cv' => $mr_activitys['act3']
						);
// 						foreach ($this->db->list_fields('line_up') as $key=>$value) {
// 							$new_data[$value] = isset($arr_data[$value]) ? $arr_data[$value] : '';
// 						}
						$this->db->insert('line_up', $arr_data);
						
// 						$sql="insert into line_up (applicant_id,manpower_rid,mr_pos_id,groups,mission_sched,evaluator,fr,lu,cv,ps,interview_date,venue_id,mob_result,sender,cv_status,branch_id,cm_date,mode,remark,mob_by,creator,date_create,fm,ra_assigned,psfm_date,app_feed,for_confirm_initial) 
// 						values ('$applicant_id','$manpower_rid','$mr_pos_id','$group','$mission_sched','$rsr_user_id','$fr','$lu','$cv','$ps','$interview_date','$venue_id','$mob_result','$sender','$cv_status','$branch_id','$cm_date','$mode','".addslashes($remark)."','$mob_by','".$iris_valid_user."','".$sqltoday."','$fm','$ra_assigned','$psfm_date','".addslashes($app_feed)."','yes')";
// 						$result = mysql_query($sql);
						
						
						/* new line_up_id */
						$line_up_id = $this->db->insert_id();
	
						create_log($applicant_id,"line_up","add","lineup_id-".$line_up_id);
	
					}
				}
	
			}
		}
	}
	
	function chklineupcondition($applicant_id, $cur_lineup=NULL, $app_status=NULL) 
	{
		$sql_lineupid = "";
		if(!is_null($cur_lineup)) $sql_lineupid = " and l.line_up_id = '{$cur_lineup}' ";
	
		/* ALLOW if reserved */
		$sQry_reserved = "";
		if($app_status == 'RESERVED' || $app_status == 'ACTIVE'){
			$sQry_reserved = " and p.status not in ('RESERVED', 'ACTIVE')";
		}
		
		$query = $this->db->query("select l.line_up_id
				from line_up l
				left join personal p on p.line_up_id = l.line_up_id
				where 1
				and l.interview_status IN ('Selected','Rejected')
				and l.acceptance not in ('Declined','Negotiate')
				and l.applicant_id = '{$applicant_id}'
				{$sql_lineupid}
				{$sQry_reserved}
				and p.status = 'OPERATIONS'");
		$data = $query->row_array();
		
		return $data["line_up_id"];
	}
	
}