<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Assessment_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		
	}
	
	public function check_for_prescreening_walkin($applicant_id)
	{
		if ($applicant_id) {
			$query = $this->db->query("select p.applicant_id
					FROM (select max(cv_location_id) as cv_location_id FROM cv_location where applicant_id in ('".$applicant_id."') group by applicant_id) as cvs 
					left join cv_location as x on x.cv_location_id = cvs.cv_location_id
					left join personal as p on p.applicant_id = x.applicant_id 
					left join users as u on u.user_id = x.user_id 
					where 1
					and x.status = '' 
					and p.status in ('ACTIVE','ON POOL') 
					and x.user_id not in (".$this->get_not_exist_user().")
					and p.date_reported = CURDATE() and p.reporting_status = 4 
					and p.method_report = 'Direct'
					and p.source_id not in ('28')
					and p.referral not in ('FP')
					and p.applicant_id in ('".$applicant_id."')
					group by p.applicant_id");
			$result = $query->row_array();
			
			if ($result) {
				return 'walkin';
			} else {
				return '';
			}
		} else {
			return '';
		}

	}
	
	public function get_for_prescreening_walkin()
	{
		
	}

	public function get_not_exist_user()
	{
		$query = $this->db->query("select cv.user_id 
				from cv_location as cv
				left join users as u on u.user_id = cv.user_id where 1
				and cv.user_id not in (select user_id from users) and cv.user_id <> 0 group by cv.user_id");
		$result = $query->result_array();
	
		$str_user = "";
		foreach($result as $value) {
			$str_user .= "'".$value['user_id']."',";
		}
		return substr($str_user, 0, -1);;
	}
	
	public function get_field_study($current_f_study = "")
	{
		$educ_field = array("N/A","Advertising / Media", "Agriculture", "Airline Transport", "Architecture / Urban Studies", "Art & Design", "Biology","BioTechnology","Business Studies/Administration/Management","Chemistry","Commerce","Computer Science/Information technology","Dentistry","Economics","Editing and Publication","Education/Teaching/Training","Engineering(Aviation/Aeronautics/Astronautics)","Engineering(Chemical)","Engineering(Civil)","Engineering(Computer/Telecommunication)","Engineering(Electrical/Electronic)","Engineering(Environmental/Health/Safety)","Engineering(Industrial)","Engineering(Marine)","Engineering(Material Science)","Engineering(Mechanical)","Engineering(Metal Fabrication/Tool & Die/Welding)","Engineering(Metallurgical)","Engineering(Others)","Engineering(Petroleum/Oil/Gas)","Finance/Accountancy/Banking","Food and Beverage Preparation/Service Management","Geographical Science","Hospitality/Tourism Management","Human Resource Management","Humanities/Liberal Arts","Land Transport","Law","Library Management","Linguistics/Translation & Interpretation","Mass Communications","Mathematics","Medical Science","Medicine","Merchant Marine","Music/Performing Arts Studies","Nursing","Others","Personal Services & Building/Ground Services","Pharmacy/Pharmacology","Physics","Protective Services & Management","Quantity Survey","Sales & Marketing","Science & Technology","Secretarial","Textile/Fashion Design & Production","Veterinary");
		
		$select_f_study = "";
		foreach($educ_field as $value) {
			if ($value == $current_f_study) {
				$select_f_study .= "<option value='".$value."' selected='selected'>".$value."</option>";
			} else {
				$select_f_study .= "<option value='".$value."'>".$value."</option>";
			}
		}
		return $select_f_study;
	}
	
	public function get_education_level($education = "")
	{
		$educ_level = array("","High School Diploma", "Vocational Diploma / Short Course Certificate", "College Level (Undergraduate)","Bachelor's / College Degree", "Post Graduate Diploma / Master's Degree", "Prof'l License(Passed Board/Bar/Prof'l License Exam)", "Doctorate Degree");
		
		$select_educ_level = "";
		foreach($educ_level as $value) {
			if ($value == $education) {
				$select_educ_level .= "<option value='".$value."' selected='selected'>".$value."</option>";
			} else {
				$select_educ_level .= "<option value='".$value."'>".$value."</option>";
			}
		}
		return $select_educ_level;
	}
	
	public function get_natureofproject_experience($current_nature = "")
	{
		$query = $this->db->query("select * from job_spec where status = 1 and jobspec_id not in (73) order by name asc");
		$result = $query->result_array();
		
		$select_nature = "<option value=''></option>";
		foreach($result as $value) {
			$position_name = $value['name'];
			$position_value = $value['jobspec_id'];
				
			if ($position_value == $current_nature) {
				$select_nature .= "<option value='".$position_value."' selected='selected'>".$position_name."</option>";
			} else {
				$select_nature .= "<option value='".$position_value."'>".$position_name."</option>";
			}
		}
		return $select_nature;
	}
	
	public function get_country($country_id="")
	{
		$query = $this->db->query("select country_id, name from country order by name");
		$result = $query->result_array();
		
		$select_country = "<option value=''></option>";
		foreach($result as $value) {
			$position_name = $value['name'];
			$position_value = $value['country_id'];
			if ($position_value == $country_id) {
				$select_country .= "<option value='".$position_value."' selected='selected'>".$position_name."</option>";
			} else {
				$select_country .= "<option value='".$position_value."'>".$position_name."</option>";
			}
		}
		return $select_country;
		
	}
	
	public function get_relationship($relationship="")
	{
		$relationships = array("","Spouse","Mother","Father","Daughter","Son","Sister","Brother","Others");

		$select_return = "";
		foreach($relationships as $value) {
			if ($value == $relationship) {
				$select_return .= "<option value='".$value."' selected='selected'>".$value."</option>";
			} else {
				$select_return .= "<option value='".$value."'>".$value."</option>";
			}
		}
		return $select_return;
	
	}
	
	public function get_option_evaluator($user_id="")
	{
		$query = $this->db->query("select username,user_id from users where 1 and status=1 and evaluator=1 order by username asc");
		$result = $query->result_array();
	
		$select_return = '<option value=""></option>';
		if (is_array($result) && count($result) > 0) {
			foreach ($result as $value) {
				if ($value['user_id'] == $user_id) {
					$select_return .= "<option value='".$value['user_id']."' selected='selected'>".$value['username']."</option>";
				} else {
					$select_return .= "<option value='".$value['user_id']."'>".$value['username']."</option>";
				}
				
			}
		}
		return $select_return;
	}
	
	public function get_option_mode($result, $status="")
	{
		
		$select_return = '<option value=""></option>';
		if (is_array($result) && count($result) > 0) {
			foreach ($result as $value) {
				if ($value == $status) {
					$select_return .= "<option value='".$value."' selected='selected'>".$value."</option>";
				} else {
					$select_return .= "<option value='".$value."'>".$value."</option>";
				}
		
			}
		}
		return $select_return;
	}
	
	public function get_lineup_final($applicant_id) 
	{
		$query = $this->db->query("select manpower_rid from line_up where applicant_id = '$applicant_id' and for_confirm_initial !='yes'");
		$oRes_lineup = $query->result_array();

		$str_manpower_ids = "";
		$manpower_ids = array();
		if (count($oRes_lineup)) {
			foreach ($oRes_lineup as $myrow_lineup) {
				array_push($manpower_ids, "'".$myrow_lineup['manpower_rid']."'");
			}
			$str_manpower_ids = implode(",", $manpower_ids);
		}
		
		return array(
				'data_string'=>$str_manpower_ids,
				'data_array'=>$manpower_ids
		);
	}
}