<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reported_model extends CI_Model {
	
	public function __construct()
	{
		parent::__construct();
	}

	public function get_lineup_history($applicant_id, $status="")
	{
		
		$str_pooling_mr_rec = "1104,1105,1106,1157";
		$query = $this->db->query("select l.line_up_id,l.manpower_rid,l.interview_date,l.interview_status,l.acceptance,l.select_date,l.approval_date,l.l_dropped,l.l_dropped_date, mro.ref_no, mro.status as mr_status, mpo.status_mr as cat_status, c.code, l.salary, l.currency_per,pos.name as position,
							prin.name as prin_name,prin_com.name as comp_name, IF(prin_com.name='', concat(prin.name, ' . ', prin_com.name),prin.name) as principal_company
							from line_up l
							left join manpower_r_ops mro on l.manpower_rid = mro.manpower_rid
							left join currency c on l.currency_id = c.currency_id
							left join mr_position_ops mpo on l.mr_pos_id = mpo.mr_pos_id
							left join positions pos on mpo.position_id = pos.position_id
							left join principals as prin on prin.principal_id = mro.principal_id
							left join prin_company as prin_com on prin_com.prin_company_id = mro.prin_company_id
							where l.applicant_id = '".$applicant_id."'
							and l.for_confirm_initial != 'yes'
							and l.manpower_rid not in ($str_pooling_mr_rec)
							order by l.line_up_id asc");
		return $query->result_array();
		
	}
		
}
