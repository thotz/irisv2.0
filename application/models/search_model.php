<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Search_model extends CI_Model {
	
	public function __construct(){
		parent::__construct();
	}
	
	public function search_applicant(){
		
		$search_tab      = $_GET['search_tab'];
 		$search_keyword  = $_GET['search_keyword'];
 		$search_link     = $_GET['search_link'];
 		$search_param    = $_GET['search_param'];
 		$additional_param = $_GET['search_param_additional'];
		$page            = $_GET['page'];
		$parameters		 = "";
		$page_limit      = 20;
		
		if ($search_tab == 'duplicate') {
			$aResult = $this->search_by_duplicate($search_keyword, $page, 20, $applicant_id);
		} else if ($search_tab == 'name') {
			$aResult = $this->search_by_name($search_keyword, $page);
		} else if ($search_tab == 'computer') {
			$aResult = $this->search_by_applicant($search_keyword, $page);
		} else if ($search_tab == 'mr') {
			$aResult = $this->search_by_mr($search_keyword, $page);
			
		} else {
			$aResult = array();
		}

        $html = "
                        <thead>
                            <tr>            
                                <th>COMPUTER NO.</th>
                                <th>NAME</th>
                                <th>BIRTHDATE</th>
                                <th>DB STATUS</th>
                                <th></th>
                            </tr>
                        </thead>";
		

		if(isset($aResult) && $aResult['total_found'] > 0){
			foreach ($aResult['details'] as $aVal_dtl){
				$name = utf8_encode($aVal_dtl['lname']).", ".utf8_encode($aVal_dtl['fname'])." ".utf8_encode($aVal_dtl['mname']);
                if ($additional_param) $additional_param = '&'.$additional_param;
				$html .= "<tr>
                                <td><a href=\"{$search_link}?{$search_param}={$aVal_dtl['applicant_id']}{$additional_param}\">{$aVal_dtl['applicant_id']}</a></td>
                                <td><a href=\"{$search_link}?{$search_param}={$aVal_dtl['applicant_id']}{$additional_param}\">".utf8_decode($name)."</a></td>
                                <td>{$aVal_dtl['birthdate']}</td>
                                <td>{$aVal_dtl['status']}</td>
                                <td class=\"text-right\"><a href=\"\" type=\"button\" class=\"btn btn-default btn-sm btn-flat\" data-toggle=\"tooltip\" data-placement=\"top\" data-original-title=\"View File\"><i class=\"fa fa-folder-open\"></i></a></td>
                            </tr>";
			}
			$rec_start = 1;
		}else{
			$html .= "<tr>
							<td colspan=\"5\" align=\"center\"><b>No match found.</b></td>
						</tr>";
			$rec_start = 0;
		}

		$html .= "<br>";
		return json_encode(array('html'=>$html,
									  'qry_str'=>$aResult['qry_str'],
									  'total_found'=>$aResult['total_found'],
									  'all_total_found'=>$aResult['all_total_found'],
									  'total_pages'=>0,
									  'record_start'=>($page==0)?$rec_start:($page*$page_limit),
									  'record_end'=>($page==0)?intval($aResult['total_found']):(($page*$page_limit) + $aResult['total_found']),
									  'pageno'=>$page));
		break;
	}
	
	public function search_by_applicant($needle, $page_number=NULL, $page_limit=10) {
		$aSearch_value = explode(',',$needle);
	
		if(isset($aSearch_value[0])){
			$sWhere_id = "  OR trim(p.applicant_id) = '{$aSearch_value[0]}'";
		}

		if(trim($needle) != '' && strlen($needle) == 9){
			$limit_start = $page_limit * $page_number;
			$sQry_search = "select p.lname, p.fname, p.mname, p.applicant_id, p.birthdate, p.status
								 from personal p
								 where p.applicant_id = '{$aSearch_value[0]}'";
			$oRes_search = search_getdata($sQry_search);

			return array('details'=>$oRes_search, 'total_found'=>count($oRes_search), 'all_total_found'=>count($oRes_search), 'qry_str'=>$sQry_search);
		}else {
			return false;
		}
	}
	
	public function search_by_name($needle, $page_number=NULL, $page_limit=10) {
		$aSearch_value = explode(',',$needle);
	
		$sWhere_fname = "";
		$sWhere_fname_only = "";
		if(isset($aSearch_value[0])){
			$sWhere_lname = " and trim(p.lname) like '{$aSearch_value[0]}%'";
			//$sWhere_fname_only = " OR trim(p.lname) like '%{$aSearch_value[0]}'";
		}

		if(isset($aSearch_value[1])){
			if ($aSearch_value[0]) $sWhere_lname = " and trim(p.lname) like '{$aSearch_value[0]}'";
			$sWhere_fname = " and trim(p.fname) like '".trim($aSearch_value[1])."%'";
			$sWhere_fname_only = '';
		}

		if(trim($needle) != ''){
			$limit_start = $page_limit * $page_number;
			$sQry_total = "select count(*) as all_total
								 from personal p
								 where 1
								 {$sWhere_lname}
								 {$sWhere_fname}
								 {$sWhere_fname_only}";
			$oRes_search_total = search_getdata($sQry_total);
			
			$sQry_search = "select p.lname, p.fname, p.mname, p.applicant_id, p.birthdate, p.status
								 from personal p
								 where 1
								 {$sWhere_lname}
								 {$sWhere_fname}
								 {$sWhere_fname_only}
								 order by apply_date desc 
								 limit {$limit_start}, {$page_limit}";
			$oRes_search_tmp = search_getdata($sQry_search);

			/*	START - GROUP ALL THE SAME (LASTNAME, FIRSTNAME) AND SORT BY LAST APPLICANT ID  */
			$result_temp = array();
			$oRes_search = array();
			if (is_array($oRes_search_tmp) && count($oRes_search_tmp)) {
				foreach ($oRes_search_tmp as $val) {
					$fullname = $val["lname"].'-'.$val["fname"].'-'.$val["mname"];
					if (!in_array($fullname, $result_temp)) {
						$result_temp[] = $fullname;

						
						$oRes_search[] = $val;
					}
				}
			}
			/*	END - GROUP ALL THE SAME (LASTNAME, FIRSTNAME) AND SORT BY LAST APPLICANT ID  */
			
			/*SHOW ALL MUNA */
			$oRes_search = $oRes_search_tmp;

			return array('details'=>$oRes_search, 'total_found'=>count($oRes_search), 'all_total_found'=>$oRes_search_total[0]["all_total"], 'qry_str'=>$sQry_search);
		}else {
			return false;
		}
	}
	
	public function search_by_mr($needle, $page_number=NULL, $page_limit=10, $manpower_rid="", $status="") 
	{
			$aSearch_value = explode(',',$needle);
	
		$sWhere_ref_no = "";
		if(isset($aSearch_value[0])){
			$sWhere_ref_no = " and trim(mr.ref_no) like '{$aSearch_value[0]}%'";
		}
		$sqlQry_status = "";
		if ($status) {
			$sqlQry_status = "and mr.status = '$status'";
		}

		if(trim($needle) != ''){
			$limit_start = $page_limit * $page_number;
			$sQry_total = "select count(*) as all_total
								 from manpower_r as mr
								 left join principals as p on p.principal_id = mr.principal_id 
								 where 1
								 {$sWhere_ref_no}
								 {$sqlQry_status}";
			$oRes_search_total = search_getdata($sQry_total);
			
			$sQry_search = "select mr.manpower_rid, mr.ref_no, mr.status, p.name, p.prin_code
								 from manpower_r as mr
								 left join principals as p on p.principal_id = mr.principal_id 
								 where 1
								 {$sWhere_ref_no}
								 {$sqlQry_status}
								 order by mr.ref_no asc 
								 limit {$limit_start}, {$page_limit}";
			$oRes_search = search_getdata($sQry_search);

			return array('details'=>$oRes_search, 
					'total_found'=>count($oRes_search), 
					'all_total_found'=>$oRes_search_total[0]["all_total"], 
					'qry_str'=>$sQry_search);
		}else {
			return false;
		}
	}
	
	public function search_by_duplicate($needle, $page_number=NULL, $page_limit=10, $applicant_id="") {
		if ($applicant_id) {
			$applicant_info = getdata_one("*","personal","applicant_id","$applicant_id");
			$needle = $applicant_info['lname'].','.$applicant_info['fname'].','.$applicant_info['mname'];
		}
		
		$aSearch_value = explode(',',$needle);
		
		if(isset($aSearch_value[0])){
			$sWhere_lname = " and trim(p.lname) like '{$aSearch_value[0]}%'";
		}

		if(isset($aSearch_value[1])){
			if ($aSearch_value[0]) $sWhere_lname = " and trim(p.lname) like '{$aSearch_value[0]}'";
			$sWhere_fname = " and trim(p.fname) like '".trim($aSearch_value[1])."'";
			$sWhere_fname_only = '';
		}
		
		if(isset($aSearch_value[2])){
			$sWhere_mname = " and trim(p.mname) like '{$aSearch_value[2]}%'";
		}

		if(trim($needle) != ''){
			$limit_start = $page_limit * $page_number;
			$sQry_total = "select count(*) as all_total
								 from personal p
								 where 1
								 {$sWhere_lname}
								 {$sWhere_fname}
								 {$sWhere_mname}
								 {$sWhere_fname_only}";
			$oRes_search_total = search_getdata($sQry_total);
			
			$sQry_search = "select p.lname, p.fname, p.mname, p.applicant_id, p.birthdate, p.status
								 from personal p
								 where 1
								 {$sWhere_lname}
								 {$sWhere_fname}
								 {$sWhere_mname}
								 {$sWhere_fname_only}
								 and status not in ('DEADFILE','DEPLOYED')
								 order by apply_date desc 
								 ";
								 //print $sQry_search;exit;	
			$oRes_search_tmp = search_getdata($sQry_search);
			
			/*	Start - Remove the Current APPLICANT ID  */
			$result_temp = array($applicant_id);
			$oRes_search = array();
			if (is_array($oRes_search_tmp) && count($oRes_search_tmp)) {
				foreach ($oRes_search_tmp as $val) {
					$applicant_id = $val["applicant_id"];
					if (!in_array($applicant_id, $result_temp)) {
						$result_temp[] = $applicant_id;

						$oRes_search[] = $val;
					}
				}
			}
			/*	END - Remove the Current APPLICANT ID  */

			return array('details'=>$oRes_search, 'total_found'=>count($oRes_search), 'all_total_found'=>$oRes_search_total[0]["all_total"], 'qry_str'=>$sQry_search);
		}else {
			return false;
		}
	}
	
}