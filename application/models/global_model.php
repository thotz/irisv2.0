<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Global_model extends CI_Model {
	
	public $user_access_id = "";
	
	public function __construct()
	{
		parent::__construct();
		
		/* Restore your connection, it must be in the My_Model (CORE)*/
		$this->db->reconnect();
		
		$this->user_access_id = isset($this->session->userdata['iris_user_access']) ? $this->session->userdata['iris_user_access'] : '';
	}
	
	public function set_side_menu()
	{
		if ( ! empty($this->user_access_id) && ! $this->cache->get(HTTP_HOST.'menu_user_access_'.$this->user_access_id)) {
			$formated_result = $this->get_side_menu_data();
			
			$access_menu = $this->load->view('layouts/main-sidebar', array('formated_result' => $formated_result), true);
			$this->cache->save(HTTP_HOST."menu_user_access_".$this->user_access_id, $access_menu, 0);
		}
	}
	
	public function get_side_menu()
	{
		if ( ! empty($this->user_access_id) ) {
			if (! $this->cache->get(HTTP_HOST.'menu_user_access_'.$this->user_access_id)) {
				$formated_result = $this->get_side_menu_data();
					
				$access_menu = $this->load->view('layouts/main-sidebar', array('formated_result' => $formated_result), true);
				$this->cache->save(HTTP_HOST."menu_user_access_".$this->user_access_id, $access_menu, 0);
			}
			return $this->cache->get(HTTP_HOST."menu_user_access_".$this->user_access_id);
		}
		return "";
	}

	public function get_side_menu_data()
	{
		$formated_result = array();
		
		if ( ! empty($this->user_access_id) ) {
			$menu_high = $this->db->query("select mh.*
						from menu_high as mh
						where mh.name not in('Pre-printed Forms') ## REMOVED MAIN MENU
						order by mh.order_id,mh.name asc");
				
			$formated_result = array();
			foreach ($menu_high->result_array() as $value) {
				$menu_high_id = $value['menu_high_id'];
				$menu_high_name = $value['name'];
				$menu_high_icon = $value['icon'];
		
				$menu_low = $this->db->query("select menu_low.*
						from menu_low where menu_low.menu_high_id = '$menu_high_id' and menu_low.access_id like '%\"$this->user_access_id\",%' and menu_low.menu_low_ids = '0'
						order by menu_low.order_id,menu_low.name asc");
		
				$formated_result_sub = array();
				if (count($menu_low->result_array())) {
					foreach ($menu_low->result_array() as $value) {
						$menu_low_id = $value['menu_low_id'];
		
						$menu_sub = $this->db->query("select menu_low.*
								from menu_low where menu_low.menu_high_id = '$menu_high_id' and menu_low.access_id like '%\"$this->user_access_id\",%' and menu_low.menu_low_ids = '$menu_low_id'
								and name not in('Status Manager')	## REMOVED SUB MENU
								order by menu_low.order_id,menu_low.name asc");
		
						$formated_result_sub[] = array(
								'menu_low_id' => $value['menu_low_id'],
								'menu_low_name' => $value['name'],
								'menu_low_filename' => $value['filename'],
								'menu_low_access_id' => $value['access_id'],
								'menu_sub' => $menu_sub->result_array()
						);
					}
				}
				$formated_result[] = array(
						'high_name' => $menu_high_name,
						'menu_high_icon' => $menu_high_icon,
						'menu_low' => $formated_result_sub
				);
		
			}
		}
		return $formated_result;
	}

	/**
	 * check duplicate entry when updating record
	 * @param string $field
	 * @return bool
	 */
	public function check_duplicate_in_tbl($field) {
		list($input, $table, $field, $exclude_field, $exclude_value)=explode('.', $field);

		if( $this->db->where($field, $input)->where($exclude_field.' !=', $exclude_value)
				->limit(1)->get($table)->num_rows())
			{
				return FALSE;
			}

		return TRUE;
	}

	/**
	 * simplified insert query
	 * @param string $table
	 * @param array $data
	 * @return int
	 */
	public function insert($table, $data=array())
	{
		$this->db->insert($table, $data);
		return $this->db->insert_id();
	}
	
	/**
	 * simplified update query
	 * @param string $table
	 * @param array $data
	 * @param array $where
	 * @return int
	 */
	public function update($table, $data=array(), $where=NULL){
		if($where == '' || is_null($where)){
			return FALSE;
		}else{
			return $this->db->update($table, $data, $where);
		}
	}

	public function get_applicant_information_data($applicant_id)
	{
		$query = $this->db->query("select p.applicant_id, p.lname, p.fname, p.mname, p.status, p.apply_date, p.date_reported,
				              p.reporting_status, p.religion, p.birthdate, p.sex, p.cv_source, p.source_id, p.agent_id, p.job_standard_id,p.work_availability,p.line_up_id, 
				              pc.email, pc.home_phone, pc.skypemail, pc.office_phone, pc.cellphone, pc.tel_postcode, 
				              pa.address1,pa.address_zip, pa.address_city,
							  pcv.cv_applicant as cv_word, pcv.picture, pcv.cv_applicant_pdf as cv_pdf, pcv.cv_applicant_other_doc, pcv.pictaken,  
				              a.mobile as agent_mobile, s.name as source,
				              c.name as city_name, c.province, a.name as agent, c.area_code,pcv.pic_request, 
							  br_a.name as app_br, br_n.name as near_br,
				
							  u.username as rso, u.user_id as rso_id, u_rm.username as rm, u_rm.user_id as rm_id
							  from personal p
				              left join personal_cv pcv on pcv.applicant_id = p.applicant_id
				              left join personal_contact pc on pc.applicant_id = p.applicant_id
				              left join personal_address pa on pa.applicant_id = p.applicant_id
				              left join agent a on p.agent_id = a.agent_id
				              left join source s on p.source_id = s.source_id
				              left join city c on pa.address_city = c.city_id
				              left join branch br_a on p.branch_id = br_a.id
				              left join branch br_n on p.branch_id1 = br_n.id
				
				              left join line_up l on l.line_up_id = p.line_up_id
				              left join manpower_r_ops mro on mro.manpower_rid = l.manpower_rid
				              
				              left join users u on mro.rso = u.user_id
							  left join users u_rm on mro.rmuser = u_rm.user_id
				
				              where 1
				              and p.applicant_id='".$applicant_id."'");
		
		$result = $query->row_array();

		$evaluation = array();
		$replacement = array();
		if ($result['status'] != 'OPERATIONS') {
			$query = $this->db->query("select u.username, u.user_id from cv_location cv
								left join users u
								on cv.user_id = u.user_id
								where 1
								and cv.applicant_id = '".$applicant_id."'
								order by cv.date_created desc
								limit 1");
			$evaluation = $query->row_array();
		}
		
		
		if ($result['rso'] != '') {
			$user_in_charge = $result['rso']; 
		} else {
			$user_in_charge = $evaluation['user_id'];
		}
		
		if ($user_in_charge) {
			$query = $this->db->query("select am.status, u.username as replacement
							from attendance_monitor am
							left join users u
							on am.replacement = u.user_id
							where 1
							and am.user_id = '".$user_in_charge."'
							and am.timestamp = '".date("Ymd")."'");
			$check_attendance = $query->row_array();
			
			if ($check_attendance['status'] == 'Absent' || $check_attendance['status'] == 'On Leave') {
				$user_replacement = $check_attendance['replacement'];
				$user_status = "[".$check_attendance['status']."]";
				
			} else {
				$user_replacement = "";
				$user_status = "";
			}
			
			$replacement = array(
					'user' => $user_replacement,
					'status' => $user_status
			);
		}
		
		return array(
				'app_info' => $result,
				'evaluation' => isset($evaluation['username']) ? $evaluation['username'] : '',
				'replacement' => $replacement
		);
		
	}
	
	public function get_applicant_information_content($applicant_id)
	{
		if ($applicant_id) {
			$this->load->helper("applicant");
			
			$app_data = $this->get_applicant_information_data($applicant_id);
			return $this->load->view('applicant_information', $app_data, true);
		} else {
			return "";
		}
	}
	
	public function get_applicant_link_menu($applicant_id)
	{
		if ($applicant_id) {
			$applicant_menu = $this->get_applicant_menu_data();
			
			$link_menu = '<div class="btn-group applicant-btn-group" role="group" aria-label="...">';
			foreach ($applicant_menu as $key=>$value) {
				$menu_class = "";
				if ($this->router->fetch_class() == $key && $this->router->fetch_method() == 'index') {
					$menu_class = " active";
					
				//} else if ($this->router->fetch_class().$this->router->fetch_method() == $key) {
				//	$menu_class = " active";
				
				}
				$label = $value['label'];
				$link_menu .= '<a href="'.$key.'?applicant_id='.$applicant_id.'" class="btn-link '.$menu_class.'">'.$label.'</a>';
			}
			$link_menu .= '</div>';
			return $link_menu;
		} else {
			return "";
		}
	}
	
	public function get_applicant_menu_data()
	{
		$data = array(
				'reported_today' => array('label'=>'Reported Today'),
				'overview' => array('label'=>'Overview'),
				'personal' => array('label'=>'Personal'),
				'education' => array('label'=>'Education'),
				'licenses' => array('label'=>'Licenses / Certifications'),
				'employment' => array('label'=>'Work History'),
				'reference' => array('label'=>'Reference'),
				'training' => array('label'=>'Training / Seminars'),
				'tradetest' => array('label'=>'Trade Test'),
				'edit_profile' => array('label'=>'Edit Profile'),
				'cvtracker' => array('label'=>'CV Trakcer'),
				'position' => array('label'=>'Position Applied'),
				'assessment' => array('label'=>'Assessment'),
				'lineup' => array('label'=>'Line-up'),
				'selections' => array('label'=>'Selections'),
				'advisory' => array('label'=>'Advisory'),
				'history' => array('label'=>'History'),
		);
		return $data;
	}
}
