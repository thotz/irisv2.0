<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sphinxrt_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('sphinxrt');
	}
	
	public function reindex_personal()
	{
		$data = $this->db->get('view_personal')->result_array();
	
		$this->sphinxrt->truncate('rt_personal');
		$this->sphinxrt->insert('rt_personal', $data);
	}
	
	public function search_by_name($needle, $page_number=0, $page_limit=10)
	{
		$aSearch_value = explode(' ',$needle);
		
		$str_search = "";
		foreach ($aSearch_value as $value) {
			$str_search .= $value . "* ";
		}
		
		$this->load->library('SphinxClient');
		$sphinx = new SphinxClient();
		
		//$sphinx->SetLimits($page_number, $page_limit);
		//$sphinx->SetSortMode(SPH_SORT_ATTR_ASC, '@lname ASC');
		$result = $sphinx->Query(".$str_search.", 'rt_personal');

		$oRes_search = array();
		if (count($result)) {
			$matches = isset($result['matches']) ? $result['matches'] : array();
			if (count($matches)) {
				$arr_matches = implode(',',array_keys($matches));
		
				$limit_start = $page_limit * $page_number;
				$query = $this->db->query("select applicant_id,lname,fname,mname,birthdate,status from personal where id in (".$arr_matches.") order by apply_date desc limit {$limit_start}, {$page_limit}");
				$oRes_search = $query->result_array();
				
				return array('details'=>$oRes_search, 'total_found'=>count($oRes_search), 'all_total_found'=>$result['total_found'], 'qry_str'=>'');
			}
			return array('details'=>$oRes_search, 'total_found'=>0, 'all_total_found'=>0, 'qry_str'=>'');
		}
		return array('details'=>$oRes_search, 'total_found'=>0, 'all_total_found'=>0, 'qry_str'=>'');
	}
}
