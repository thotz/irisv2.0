<?php

Class Assessment_facebox extends My_controller
{
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('reception/assessment_model');
		$this->load->helper('select_option','applicant');
	}
	
	public function ajax_main()
	{
		$id = isset($_GET['id']) ? $_GET['id'] : '';
		$action = isset($_GET['action']) ? $_GET['action'] : '';
		$referral = isset($_GET['referral']) ? $_GET['referral'] : '';
		$is_na_field = 'is_na_'.$action;
		$applicant_id = $_GET['applicant_id'];
		
		switch ($action) {
			case 'licenses':
				$is_na_field = 'is_na_license';
		}
		
		$query = $this->db->query("select * from personal_assessment where applicant_id='$applicant_id'");
		$applicant = $query->row_array();
		
		$is_na_value = isset($applicant[$is_na_field]) ? $applicant[$is_na_field] : '';
		
		$data = array(
			'id' => $id,
			'action' => $action,
			'referral' => $referral,
			'is_na_field' => $is_na_field,
			'is_na_value' => $is_na_value,
			'applicant_id' => $applicant_id,
		);
		
		$this->load->view('reception/assessment_facebox/main', $data);
	}

	public function ajax_education()
	{
		$id = isset($_GET['id']) ? $_GET['id'] : '';
		$referral = isset($_GET['referral']) ? $_GET['referral'] : '';
		$applicant_id = $_GET['applicant_id'];
		
		$education = "";
		$fstudy = "";
		$result = "";
		if ($id && $id != 'undefined') {
			$query = $this->db->query("select * from education where id='$id'");
			$result = $query->row_array();
				
			$fstudy = $result['f_study'];
			$education = $result['education'];
			
			$btn_value = "Save";
		} else {
			$btn_value = "Add";
		}
		
		$select_f_study = $this->assessment_model->get_field_study($fstudy);
		$select_educ_level = $this->assessment_model->get_education_level($education);
		
		$data = array(
				'id' => $id,
				'applicant_id' => $applicant_id,
				'result' => $result,
				'btn_value' => $btn_value,
				'select_f_study' => $select_f_study,
				'select_educ_level' => $select_educ_level
				
		);
		
		$html = $this->load->view('reception/assessment_facebox/facebox-education', $data, true);
			
		$html = iconv('UTF-8', 'UTF-8//IGNORE', $html);
		echo json_encode(array('html'=>$html));
	}

	public function ajax_employment()
	{
		$id = isset($_GET['id']) ? $_GET['id'] : '';
		$referral = isset($_GET['referral']) ? $_GET['referral'] : '';
		$applicant_id = $_GET['applicant_id'];
		
		$result = "";
		$country_id = "";
		$natureofbusiness = "";
		if ($id && $id != 'undefined') {
			$query = $this->db->query("select * from employment where id='$id'");
			$result = $query->row_array();
			
			$natureofbusiness = $result['natureofbusiness'];
			$country_id = $result['country_id'];
			$btn_value = "Save";
		} else {
			$btn_value = "Add";
		}

		$natureofproject = $this->assessment_model->get_natureofproject_experience($natureofbusiness);
		$country = $this->assessment_model->get_country($country_id);
		
		$data = array(
				'id' => $id,
				'applicant_id' => $applicant_id,
				'result' => $result,
				'btn_value' => $btn_value,
				'country' => $country,
				'natureofproject' => $natureofproject
		
		);
		
		$html = $this->load->view('reception/assessment_facebox/facebox-work', $data, true);
			
		$html = iconv('UTF-8', 'UTF-8//IGNORE', $html);
		echo json_encode(array('html'=>$html));
	}
	
	public function ajax_licenses()
	{
		$id = isset($_GET['id']) ? $_GET['id'] : '';
		$referral = isset($_GET['referral']) ? $_GET['referral'] : '';
		$applicant_id = $_GET['applicant_id'];
	
		$result = "";
		if ($id && $id != 'undefined') {
			$query = $this->db->query("select * from licenses where id='$id'");
			$result = $query->row_array();
				
			$btn_value = "Save";
		} else {
			$btn_value = "Add";
		}

		$data = array(
				'id' => $id,
				'applicant_id' => $applicant_id,
				'result' => $result,
				'btn_value' => $btn_value,
		);
	
		$html = $this->load->view('reception/assessment_facebox/facebox-licenses', $data, true);
			
		$html = iconv('UTF-8', 'UTF-8//IGNORE', $html);
		echo json_encode(array('html'=>$html));
	}
	
	public function ajax_training()
	{
		$id = isset($_GET['id']) ? $_GET['id'] : '';
		$referral = isset($_GET['referral']) ? $_GET['referral'] : '';
		$applicant_id = $_GET['applicant_id'];
	
		$result = "";
		if ($id && $id != 'undefined') {
			$query = $this->db->query("select * from training where training_id='$id'");
			$result = $query->row_array();
	
			$btn_value = "Save";
		} else {
			$btn_value = "Add";
		}
	
		$data = array(
				'id' => $id,
				'applicant_id' => $applicant_id,
				'result' => $result,
				'btn_value' => $btn_value,
		);
	
		$html = $this->load->view('reception/assessment_facebox/facebox-training', $data, true);
			
		$html = iconv('UTF-8', 'UTF-8//IGNORE', $html);
		echo json_encode(array('html'=>$html));
	}
	
	public function ajax_tradetest()
	{
		$id = isset($_GET['id']) ? $_GET['id'] : '';
		$referral = isset($_GET['referral']) ? $_GET['referral'] : '';
		$applicant_id = $_GET['applicant_id'];
	
		$result = "";
		if ($id && $id != 'undefined') {
			$query = $this->db->query("select * from tradetest where tradetest_id='$id'");
			$result = $query->row_array();
	
			$btn_value = "Save";
		} else {
			$btn_value = "Add";
		}
	
		$data = array(
				'id' => $id,
				'applicant_id' => $applicant_id,
				'result' => $result,
				'btn_value' => $btn_value,
		);
	
		$html = $this->load->view('reception/assessment_facebox/facebox-tradetest', $data, true);
			
		$html = iconv('UTF-8', 'UTF-8//IGNORE', $html);
		echo json_encode(array('html'=>$html));
	}
	
	public function ajax_beneficiaries()
	{
		$id = isset($_GET['id']) ? $_GET['id'] : '';
		$referral = isset($_GET['referral']) ? $_GET['referral'] : '';
		$applicant_id = $_GET['applicant_id'];
	
		$result = "";
		$relationship = "";
		if ($id && $id != 'undefined') {
			$query = $this->db->query("select * from beneficiaries where beneficiaries_id='$id'");
			$result = $query->row_array();
	
			$relationship = $result['relationship'];
			$btn_value = "Save";
		} else {
			$btn_value = "Add";
		}
		
		$select_relationship = $this->assessment_model->get_relationship($relationship);
	
		$data = array(
				'id' => $id,
				'applicant_id' => $applicant_id,
				'result' => $result,
				'btn_value' => $btn_value,
				'select_relationship' => $select_relationship
		);
	
		$html = $this->load->view('reception/assessment_facebox/facebox-beneficiaries', $data, true);
			
		$html = iconv('UTF-8', 'UTF-8//IGNORE', $html);
		echo json_encode(array('html'=>$html));
	}
	
	public function ajax_prefered()
	{
		$this->load->model('reception/create_applicant_model', 'create_applicant', true);
		
		$id = isset($_GET['id']) ? $_GET['id'] : '';
		$referral = isset($_GET['referral']) ? $_GET['referral'] : '';
		$applicant_id = $_GET['applicant_id'];
	
		$result = "";
		$position_id1 = "";
		$position_id2 = "";
		$position_id3 = "";
		
		$jobspec_id1 = "";
		$jobspec_id2 = "";
		$jobspec_id3 = "";
		if ($id && $id != 'undefined') {

			
			$btn_value = "Save";
		} else {
			$btn_value = "Add";
		}

		$query = $this->db->query("select * from prefered_position where 1 and applicant_id = '$applicant_id' order by position_order asc");
		$result = $query->result_array();
		
		if (is_array($result) && count($result) > 0) {
			foreach($result as $value) {
				$position_order = $value['position_order'];
				if ($position_order == 1) {
					$position_id1 = $value['position_id'];
					$jobspec_id1 = $value['jobspec_id'];
				} else if ($position_order == 2) {
					$position_id2 = $value['position_id'];
					$jobspec_id2 = $value['jobspec_id'];
				} else if ($position_order == 3) {
					$position_id3 = $value['position_id'];
					$jobspec_id3 = $value['jobspec_id'];
				}
			}
		}

		$res_preferred = $this->create_applicant->get_preferred_position($applicant_id, $position_id1, $position_id2, $position_id3);
		$res_specialization = $this->create_applicant->get_specialization($applicant_id, $jobspec_id2, $jobspec_id3);
	
	
		$data = array(
				'preferred' => $res_preferred,
				'specialization' => $res_specialization,
				
				'id' => $id,
				'applicant_id' => $applicant_id,
				'result' => $result,
				'btn_value' => $btn_value
		);

		$html = $this->load->view('reception/assessment_facebox/facebox-prefered', $data, true);
			
		$html = iconv('UTF-8', 'UTF-8//IGNORE', $html);
		echo json_encode(array('html'=>$html));
	}
	
	public function ajax_cvlocation()
	{
		$this->load->helper('applicant');
		
		$id = isset($_GET['id']) ? $_GET['id'] : '';
		$status = isset($_GET['status_dcv']) ? $_GET['status_dcv'] : '';
		$user_id = isset($_GET['user_id']) ? $_GET['user_id'] : '';
		$remarks = isset($_GET['remarks']) ? $_GET['remarks'] : '';
		$referral = isset($_GET['referral']) ? $_GET['referral'] : '';
		$applicant_id = $_GET['applicant_id'];
	
		$result = "";
		$user_id = $this->session->userdata['iris_user_id'];
		if ($id && $id != 'undefined') {
			$query = $this->db->query("select * from cv_location where cv_location_id='$id'");
			$result = $query->row_array();
	
			$user_id = $result['user_id'];
			$btn_value = "Save";
		} else {
			$btn_value = "Add";
		}
		
		if (direct_online($applicant_id)) {
			$mode = array("INT", "NA","TCB", "WD");
		} else {
			$mode = array("RC","PS");
		}
		

		$select_mode = $this->assessment_model->get_option_mode($mode, $status);
		$select_evaluator = $this->assessment_model->get_option_evaluator($user_id);
	
		$data = array(
				'id' => $id,
				'applicant_id' => $applicant_id,
				'result' => $result,
				'btn_value' => $btn_value,
				'mode' => $select_mode,
				'select_evaluator' => $select_evaluator
		);
	
		$html = $this->load->view('reception/assessment_facebox/facebox-cvlocation', $data, true);
			
		$html = iconv('UTF-8', 'UTF-8//IGNORE', $html);
		echo json_encode(array('html'=>$html));
	}
	
	public function ajax_check_applicant_initial_lineup()
	{
		$applicant_id = $_POST['applicant_id'];
		
		$count_lineup = 0;
		$message = "";
		$result_lineup = "";
		if ($applicant_id) {
			$sql_lineup = "select l.line_up_id
					from line_up as l
					left join personal as p on p.applicant_id = l.applicant_id
					left join manpower_r as mr on mr.manpower_rid = l.manpower_rid
					left join mr_position mr_pos on mr_pos.mr_pos_id = l.mr_pos_id
					where 1
					and l.applicant_id = '".$applicant_id."'
					and l.mob_result = 'AV'
					and l.cv_status = ''
					and l.for_confirm_initial = 'yes'
					and p.status not in('DEADFILE','DEPLOYED','OPERATIONS')
					and l.manpower_rid in (select manpower_rid from manpower_r where status = 'Active' and ref_no != '')
					group by l.line_up_id";
			$query = $this->db->query($sql_lineup);
			$result_lineup = $query->row_array();
			
			if (is_array($result_lineup) && count($result_lineup) > 0) {
				$message = "This applicant has Initial Line up (Online Position). Please check and confirm his initial line up.";
			}
		}
			
		echo json_encode(array(
				'data'=>$result_lineup,
				'count'=>count($result_lineup),
				'message'=>$message
				
		));
	}
	
	public function ajax_check_applicant_assessment()
	{
		$applicant_id = isset($_POST['applicant_id']) ? $_POST['applicant_id'] : '';
		$table = isset($_POST['table']) ? $_POST['table'] : '';

		if ($table != '' && $applicant_id) {
			$is_na = false;
		
			$applicant = $this->db->query("select * from personal where applicant_id = '".$applicant_id."'")->row_array();
		
			$result = $this->db->query("select * from ".$table." where applicant_id = '".$applicant_id."' limit 1")->row_array();
		
			switch ($table) {
				case 'apply_applicant':
					$is_na = true;
					break;
				case 'education':
					if ($applicant['is_na_education'] == 1) $is_na = true;
					break;
				case 'employment':
					if ($applicant['is_na_employment'] == 1) $is_na = true;
					break;
				case 'licenses':
					if ($applicant['is_na_license'] == 1) $is_na = true;
					break;
				case 'training':
					if ($applicant['is_na_training'] == 1) $is_na = true;
					break;
				case 'tradetest':
					if ($applicant['is_na_tradetest'] == 1) $is_na = true;
					break;
				case 'beneficiaries':
					$is_na = true;
					break;
		
				case 'allowed':	/* no restriction */
					$is_na = true;
					break;
				default:
					/**/
			}
		
			if ((is_array($result) && count($result) > 0) || $is_na) {
				echo true;
			} else {
				echo false;
			}
		}
	}

}