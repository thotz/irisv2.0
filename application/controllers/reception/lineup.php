<?php

Class Lineup extends My_controller
{
	public function __construct()
	{
		parent::__construct();
		
	}
	
	public function index()
	{
		set_header_title("Assessment");

		$applicant_id = "";
		if (isset($_GET['applicant_id']) && $_GET['applicant_id']) {
			$applicant_id = $_GET['applicant_id'];
		}

		$applicant_id = "";
		if (isset($_GET['applicant_id']) && $_GET['applicant_id']) {
			$applicant_id = $_GET['applicant_id'];
		}
		
		$data = array(
				'applicant_information' => $this->global_model->get_applicant_information_content($applicant_id),
				'applicant_link_menu' => $this->global_model->get_applicant_link_menu($applicant_id),
				'applicant_id' => $applicant_id
		);
		
		$this->template->view('reception/lineup/index', $data);
	}
}