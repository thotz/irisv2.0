<?php

Class Create_applicant extends My_controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('reception/create_applicant_model', 'create_applicant', true);
		
		$this->load->helper(array("select_option", "applicant"));
	}
	
	public function index()
	{
		set_header_title("Create New Applicant");
		
		$this->template->set('file_javascript', array(
				'js/plugins/iCheck/icheck.min.js',
				'js/reception/create_applicant.js'
		));
		$this->template->set('file_css', array(
				'js/plugins/iCheck/all.css'
				
		));
		

		
		$old_applicant_id = "A-0004-12";
		$action = "add_deadfile";
		
		$old_applicant_id = "";
		$action = "";
		
		$gender = array(
				'M' => 'Male',
				'F' => 'Female'
		);
		$personal_info = $this->create_applicant->get_personal_info($old_applicant_id);
		//if ($old_applicant_id && $action) {
			
			$res_preferred = $this->create_applicant->get_preferred_position();
			$res_specialization = $this->create_applicant->get_specialization();
			$res_applied = $this->create_applicant->get_applied_position();
			$res_municipality = $this->create_applicant->get_municipality('get_select_option', $personal_info['address_city']);
			$res_method_application = $this->create_applicant->get_method_application();
			$res_source = $this->create_applicant->get_source_application();

			$nearest_branch = $this->create_applicant->get_option_nearest_branch($personal_info['address_city']);
			$res_applicant_cv = $this->create_applicant->get_applicant_cv($old_applicant_id);
			
			/* Reffered Data */
				$style_reffered = ' style="display:none;"';
				if ($personal_info['source_id'] == 13) {
					$style_reffered = ' style=""';
				}
				
				$refferal_data = array(
						'data' => $this->create_applicant->get_option_refferal($personal_info),
						'style_reffered' => $style_reffered
				);
			
			/* Agent Data */
				$style_agent = ' style="display:none;"';
				if ($personal_info['agent_id'] && $personal_info['agent_id'] != 0) {
					$style_agent = ' style=""';
				}
				$agent_data = array(
						'data' => $this->create_applicant->get_option_agent(),
						'style_agent' => $style_agent
				);
				
			/* PRA Data */
				$style_pra = ' style="display:none;"';
				if (($personal_info['pra_date'] !='0000-00-00' && $personal_info['pra_date'] != '') && $personal_info['cv_source'] == 'PRA/Headhunting/Jobs Fair') {
					$style_pra = ' style=""';
				}
				$pra_data = array(
						'city' => $this->create_applicant->get_municipality('get_select_option'),
						'leader' => $this->create_applicant->get_pra_leader(),
						'style_pra' => $style_pra
				);
			
			/* Reporting Status */
				if ($old_applicant_id) {
					$style_walkin = ' checked="checked"';
					$style_issuance = '';
					if ($personal_info['reporting_status'] != 4) {
						$style_walkin = '';
						$style_issuance = ' checked="checked"';
					}
				} else {
					$style_walkin = '';
					$style_issuance = '';
				}
				$reporting_data = array(
						'style_walkin' => $style_walkin,
						'style_issuance' => $style_issuance
				);
			
			$data = array(
					'preferred' => $res_preferred,
					'specialization' => $res_specialization,
					'applied_position' => $res_applied,
					'municipality' => $res_municipality,
					'method_application' => $res_method_application,
					'res_source' => $res_source,
					'old_applicant_id' => $old_applicant_id,
					'action' => $action,
					'personal_info' => $personal_info,
					'branch' => get_user_branch(),
					'gender' => $gender,
					'nearest_branch' => $nearest_branch,
					'refferal_data' => $refferal_data,
					'agent_data' => $agent_data,
					'pra_data' => $pra_data,
					'applicant_cv' => $res_applicant_cv,
					'reporting_data' => $reporting_data
			);
			$this->template->view('reception/create_applicant/index_exist', $data);
	}
	
	public function ajax_popup_message()
	{
		
		$this->load->view('reception/create_applicant/popup_message', $_GET);
	}
	
	public function ajax_check_applicant_data()
	{
// 		print_r($_FILES);
// 		debug();
		
		$status = 'success';
		$message = 'Success';
		$applicant_id = "";
		$data = array();
		
		if((isset($_FILES['cv_applicant']) && $_FILES['cv_applicant']['size'] > 20000000) || (isset($_FILES['cv_applicant_pdf']) && $_FILES['cv_applicant_pdf']['size'] > 20000000)) {
			$status = 'error';
			$message = "Error When Uploading CV File  - Maximum File Size is 20MB Only.";
		} else {
			$lname = strtoupper(strtolower(trim($_POST['lname'])));
			$fname = strtoupper(strtolower(trim($_POST['fname'])));
			$mname = strtoupper(strtolower(trim(isset($_POST['mname']) ? $_POST['mname'] : '')));
			$source_id = $_POST['source_id'];
			
			$sqlbday = "";
			$sql_celno = "";
			if($_POST['bday_month'] != '' && $_POST['bday_year'] != '' && $_POST['bday_day'] != ''){
				$birthdate = "{$_POST['bday_year']}-{$_POST['bday_month']}-{$_POST['bday_day']}";
				$applicant_age = get_age($birthdate);
			
				if ($source_id != "28" && !isset($_POST['is_dependent'])) {
					if($applicant_age < MIN_AGE_LIMIT || $applicant_age > MAX_AGE_LIMIT) {
						$status = 'error';
						$message = "Error (Must be 21 - 59 years of age).";
						echo json_encode(array('status' => $status,'message' => $message,'data' => $data)); exit();
					}
				}
			
				$sqlbday = "and birthdate = '".$birthdate."'";
			}else{
				$sql_celno = "and cellphone like '%".$_POST['cellphone']."%'";
			}
			
			if(isset($_POST['lastext']) && $_POST['lastext']) $fname = $fname." JR.";
			
			$query = $this->db->query("select * from personal
					where UCASE(LTRIM(RTRIM(lname))) = '{$lname}'
					and UCASE(LTRIM(RTRIM(fname))) = '{$fname}'
					and UCASE(LTRIM(RTRIM(mname))) = '{$mname}'
					{$sqlbday}
					{$sql_celno}
					order by apply_date desc");
			$duplicate = $query->row_array();
						
			$duplicate_status = $duplicate["status"];
			if(!empty($duplicate) && $duplicate_status != 'DEPLOYED') {
				/* check for DEADFILE, if morethan 360-days create new computer number */
				$my_applicant_id = $duplicate["applicant_id"];
				if ($duplicate_status == 'DEADFILE') {
					/* always restrict the medical UNFIT and DEADFILE */
					if (check_medical_unfit($my_applicant_id)) {
						$status = 'error';
						$message = "This applicant was declared with UNFIT TO WORK medical result.\\n Please endorse the applicant to Operations / to concerned RSO or GH for clearance.\\n If you wish to activate this application, please refer to Ms. Ara.";
						echo json_encode(array('status' => $status,'message' => $message,'link'=>base_url().'reception/reported_today?applicant_id='.$my_applicant_id,'data' => $data)); exit();
					}
						
					if($_POST['action'] != 'add_deadfile'){
						if (is_deadfile_new_computer($my_applicant_id)) {
							/* Allow = "This applicant was DeadFile morethan a year, Please create a new computer."; */
						} else {
							$status = 'error';
							$message = "There is already an existing applicant, Please click the Last Reported to update.";
							echo json_encode(array('status' => $status,'message' => $message,'link'=>base_url().'reception/reported_today?applicant_id='.$my_applicant_id,'data' => $data)); exit();
						}
					}
				} else if ($duplicate_status == 'BLACKLISTED') {
					if(strtotime(date("Y-m-d")) != strtotime(substr($duplicate['date_reported'],0,10))){
						$status = 'error';
						$message = "This applicant was BLACKLISTED.\\n Please refer to Mr. Roggee or Mr. Patrick to get clearance.";
						echo json_encode(array('status' => $status,'message' => $message,'link'=>base_url().'reception/reported_today?applicant_id='.$my_applicant_id,'data' => $data)); exit();
					}
				} else if ($duplicate_status == 'DEPLOYED') {
					/* Allow */
				} else if ($duplicate_status == 'OPERATIONS') {
					$status = 'error';
					$message = "This applicant was OPERATIONS.\\n Please click the Last Reported to update.";
					echo json_encode(array('status' => $status,'message' => $message,'link'=>base_url().'reception/reported_today?applicant_id='.$my_applicant_id,'data' => $data)); exit();
				} else {
					if($_POST['action'] != 'add_deadfile'){
						$status = 'error';
						$message = "There is already an existing applicant, Try this Applicant ID = {$my_applicant_id}.";
						echo json_encode(array('status' => $status,'message' => $message,'link'=>base_url().'reception/reported_today?applicant_id='.$my_applicant_id,'data' => $data)); exit();
					}
				}
			}

			/* Create Applicant ID */
			$purpose_id = isset($_POST['purpose_id']) ? $_POST['purpose_id'] : '';
			$applicant_id = get_applicant_id($purpose_id);
			
			/* Upload Files */
			$this->load->model('file/file_model', 'file_model', true);
			/* Upload WORD */
			if (isset($_FILES['cv_applicant'])) {
				$res_doc = $this->file_model->upload_file("iriscvwordpdf", "cv_applicant", $applicant_id, true);
				
				if ($res_doc['status'] == 'error') {
					$status = 'error';
					$message = $res_doc['message'];
					echo json_encode(array('status' => $status,'message' => $message,'data' => $data)); exit();
				}
			}
			/* Upload PDF */
			if (isset($_FILES['cv_applicant_pdf'])) {
				$res_pdf = $this->file_model->upload_file("iriscvwordpdf", "cv_applicant_pdf", $applicant_id, true);
				
				if ($res_pdf['status'] == 'error') {
					$status = 'error';
					$message = $res_pdf['message'];
					echo json_encode(array('status' => $status,'message' => $message,'data' => $data)); exit();
				}
			}
			
			$referral = "NP";
			if($_POST['cv_source'] == 'Referred' && $_POST['source_id'] == '13'){
				$referral = $_POST['referral'];
			}
				
			if($_POST['is_walkin'] == 1){
				$rptstatus_val = 4; //REPORTED
			}else{
				$rptstatus_val = 0;
			}
				
			/* IF APPLICANT IS DEPENDENT */
			if(isset($_POST['is_dependent']) && $_POST['is_dependent']){
				$dependent_val = 'Y';
			}else{
				$dependent_val = 'N';
			}
				
			$date_reported = "0000-00-00";
			$method_report = "Online";
			if ($_POST['cv_source'] == "Direct" || $_POST['cv_source'] == "Referred") {
				$date_reported = date("Y-m-d");
				$method_report = "Direct";
			}
				
			$pra_day = isset($_POST['pra_date_day'])? $_POST['pra_date_day'] : "00";
			$pra_month = isset($_POST['pra_date_month'])? $_POST['pra_date_month'] : "00";
			$pra_year = isset($_POST['pra_date_year'])? $_POST['pra_date_year'] : "0000";
			$pra_date = "$pra_year-$pra_month-$pra_day";
			
			$bday_day = isset($_POST['bday_day'])? $_POST['bday_day'] : "00";
			$bday_month = isset($_POST['bday_month'])? $_POST['bday_month'] : "00";
			$bday_year = isset($_POST['bday_year'])? $_POST['bday_year'] : "0000";
			$birthdate = "$bday_year-$bday_month-$bday_day";
			
			/* Add Personal */
			$personal = array(
					'applicant_id' => $applicant_id,
					'lname' => $lname,
					'fname' => $fname,
					'mname' => $mname,
					'sex' => strtoupper(strtolower(trim($_POST['sex']))),
					'birthdate' => $birthdate,
						
					'branch_id' => $_POST['branch_id'],
					'branch_id1' => $_POST['branch_id1'],
					'cv_source' => $_POST['cv_source'],
					'source_id' => $_POST['source_id'],
					'referral' => $referral,
					'agent_id' => $_POST['agent_id'],
					'work_availability' => $_POST['work_availability'],
						
					'pra_city_id' => $_POST['pra_city_id'],
					'pra_user_id' => $_POST['pra_user_id'],
					'pra_date' => $pra_date,
						
					'method_report' => $method_report,
					'is_dependent' => $dependent_val,
					'reporting_status' => $rptstatus_val,
						
					'date_reported' => $date_reported,
					'apply_date' => date("Y-m-d h:i:s"),
					'create_date' => date("Y-m-d h:i:s"),
					'date_edit' => date("Y-m-d h:i:s"),
					'create_by' => $this->session->userdata['iris_user_name']
			);
			$new_data = filter_before_add($personal);
			$result_personal = $this->db->insert('personal', $new_data);
			$personal_id = $this->db->insert_id();
			
			/* Insert Personal to RT_PERSONAL */
			$this->load->library('SphinxRT');
			if ($personal_id) {
				$rt_data = array(
						'id' => $personal_id,
						'status' => 'ON POOL',
						'applicant_id' => $applicant_id,
						'lname' => $lname,
						'fname' => $fname,
						'mname' => $mname
				);
				$this->sphinxrt->insert('rt_personal', $rt_data);
			}
			
			/* Add Personal CV */
			$personal = array(
					'applicant_id' => $applicant_id,
					'cv_applicant' => $res_doc['database_filename'],
					'cv_applicant_pdf' => $res_pdf['database_filename'],
			);
			$result_cv = $this->db->insert('personal_cv', $personal);
			
			/* Add Personal Contact */
			$personal = array(
					'applicant_id' => $applicant_id,
					'cellphone' => $_POST['cellphone'],
					'office_phone' => $_POST['office_phone'],
					'email' => strtolower(trim($_POST['email'])),
			);
			$new_data = filter_before_add($personal);
			$result_contact = $this->db->insert('personal_contact', $new_data);
				
			/* Add Personal Address */
			$personal = array(
					'applicant_id' => $applicant_id,
					'branch_id' => $_POST['branch_id'],
					'branch_id1' => $_POST['branch_id1'],
					'branch_id1' => $_POST['branch_id1'],
					'address1' => $_POST['address1'],
					'address_city' => $_POST['address_city'],
			);
			$new_data = filter_before_add($personal);
			$result_address = $this->db->insert('personal_address', $new_data);
			
			if ($applicant_id && ($result_contact || $result_address) ) {
				/* Logs Create New applicant */
				create_log($applicant_id,"new_applicant","add");
			
				/* Logs Create Agent */
				if(isset($_POST['agent_id']) && $_POST['agent_id']){
					create_log($applicant_id, "add agent id:{$_POST['agent_id']}","add");
				}
			
				/* Insert General/Company Position and Initial Lineup */
				$this->create_applicant->add_multi_applied_position($applicant_id, $_POST['position_ids'], $_POST['jobspec_ids'], $_POST['mr_positions']);
			
			
				/* ADD CONTACTS TO SMS CONTACTS */
				$sms_data = array(
						'id' => $applicant_id,
						'fname' => ucwords($_POST['fname']),
						'mname' => ucwords($_POST['mname']),
						'lname' => ucwords($_POST['lname']),
						'mobile' => $_POST['cellphone'],
						'other_mob' => isset($_POST['office_phone']) ? $_POST['office_phone'] : '',
						'added_by' => $_SESSION['iris_user_name']
				);
				//add_sms_contacts($sms_data);
				
				/* ADD EMAIL TO DATABASE */
				
			
				/* Insert to process table the tin, philhealth, passport number */
				$query = $this->db->query("select * from process where applicant_id='$applicant_id'");
				$result_process = $query->row_array();
				if(empty($result_process)){
					$this->db->insert('process', array('applicant_id'=>$applicant_id));
				}
			}
		}
		
		$response = array(
				'status' => $status,
				'message' => $message,
				'applicant_id' => $applicant_id,
				'data' => $data,
		);
		echo json_encode($response);
	}
	
	public function ajax_nearest_branch($address_city="")
	{
		echo $this->create_applicant->get_option_nearest_branch($address_city);
	}
	
	public function ajax_source_application($method_application)
	{
		echo $this->create_applicant->get_source_application($method_application);
	}
}