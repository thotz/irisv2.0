<?php

Class Assessment_delete extends My_controller
{
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('reception/assessment_model');
		$this->load->helper('select_option','applicant');
	}

	public function ajax_education()
	{
		$status = "Success";
		$message = "Education Deleted.";

		$id = isset($_GET['id']) ? $_GET['id'] : '';
		$applicant_id = $_GET['applicant_id'];
		
		if ($id) {
			$result = $this->db->delete('education', array('id' => $id)); 
			
			if($result){
				create_log($applicant_id,"education","delete");
			} else {
				$status = "Error";$message = "Error please try again.";
			}
		}

		echo json_encode(array('status'=>$status,'message'=>$message));
	}

	public function ajax_employment()
	{
		$status = "Success";
		$message = "Work History Deleted.";

		$id = isset($_GET['id']) ? $_GET['id'] : '';
		$applicant_id = $_GET['applicant_id'];
		
		if ($id) {
			$result = $this->db->delete('employment', array('id' => $id));
			
			if($result){
				create_log($applicant_id,"employment","delete");
			} else {
				$status = "Error";$message = "Error please try again.";
			}
		}

		echo json_encode(array('status'=>$status,'message'=>$message));
	}
	
	public function ajax_licenses()
	{
		$status = "Success";
		$message = "Licenses Deleted.";
	
		$id = isset($_GET['id']) ? $_GET['id'] : '';
		$applicant_id = $_GET['applicant_id'];
	
		if ($id) {
			$result = $this->db->delete('licenses', array('id' => $id));
				
			if($result){
				create_log($applicant_id,"licenses","delete");
			} else {
				$status = "Error";$message = "Error please try again.";
			}
		}
	
		echo json_encode(array('status'=>$status,'message'=>$message));
	}
	
	public function ajax_training()
	{
		$status = "Success";
		$message = "Training Deleted.";
	
		$id = isset($_GET['id']) ? $_GET['id'] : '';
		$applicant_id = $_GET['applicant_id'];
	
		if ($id) {
			$result = $this->db->delete('training', array('training_id' => $id));
	
			if($result){
				create_log($applicant_id,"training","delete");
			} else {
				$status = "Error";$message = "Error please try again.";
			}
		}
	
		echo json_encode(array('status'=>$status,'message'=>$message));
	}
	
	public function ajax_tradetest()
	{
		$status = "Success";
		$message = "Tradetest Deleted.";
	
		$id = isset($_GET['id']) ? $_GET['id'] : '';
		$applicant_id = $_GET['applicant_id'];
	
		if ($id) {
			$result = $this->db->delete('tradetest', array('tradetest_id' => $id));
	
			if($result){
				create_log($applicant_id,"tradetest","delete");
			} else {
				$status = "Error";$message = "Error please try again.";
			}
		}
	
		echo json_encode(array('status'=>$status,'message'=>$message));
	}
	
	public function ajax_beneficiaries()
	{
		$status = "Success";
		$message = "Beneficiaries Deleted.";
	
		$id = isset($_GET['id']) ? $_GET['id'] : '';
		$applicant_id = $_GET['applicant_id'];
	
		if ($id) {
			$result = $this->db->delete('beneficiaries', array('beneficiaries_id' => $id));
	
			if($result){
				create_log($applicant_id,"beneficiaries","delete");
			} else {
				$status = "Error";$message = "Error please try again.";
			}
		}
	
		echo json_encode(array('status'=>$status,'message'=>$message));
	}

}