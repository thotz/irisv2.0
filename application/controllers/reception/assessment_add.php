<?php

Class Assessment_add extends My_controller
{
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('reception/assessment_model');
		$this->load->helper('select_option','applicant');
	}

	public function ajax_education()
	{
		$status = "Success";
		$message = "Education Save.";
		
		$id = isset($_POST['id']) ? $_POST['id'] : '';
		$applicant_id = $_POST['applicant_id'];
		$referral = isset($_POST['referral']) ? $_POST['referral'] : '';
		$is_na_education = isset($_POST['is_na_education']) ? $_POST['is_na_education'] : '';
		
		if ($is_na_education) {
			$new_data = array(
				'is_na_education' => $is_na_education
			);
			
			$this->db->where('applicant_id',$applicant_id);
			$result = $this->db->update('personal_assessment',$new_data);
			if($result){ create_log($applicant_id,"education","add","NA"); }
		} else {

			$remove_data = array('id');
			$new_data = filter_before_add($_POST, $remove_data);
			
			$result = $this->db->insert('education', $new_data);
			
			if($result){ 
				create_log($applicant_id,"education","add"); 
			} else { 
				$status = "Error";$message = "Error please try again.";
			}
		}

		echo json_encode(array('status'=>$status,'message'=>$message));
	}

	public function ajax_employment()
	{
		$status = "Success";
		$message = "Work History Save.";
		
		$id = isset($_POST['id']) ? $_POST['id'] : '';
		$applicant_id = $_POST['applicant_id'];
		$referral = isset($_POST['referral']) ? $_POST['referral'] : '';
		$is_na_employment = isset($_POST['is_na_employment']) ? $_POST['is_na_employment'] : '';
		
		if ($is_na_employment) {
			$new_data = array(
					'is_na_employment' => $is_na_employment
			);
				
			$this->db->where('applicant_id',$applicant_id);
			$result = $this->db->update('personal_assessment',$new_data);
			if($result){ create_log($applicant_id,"employment","add","NA"); }
		} else {
			
			$from_day = "01";
			$from_month = isset($_POST['from_month'])? $_POST['from_month'] : "01";
			$from_year = isset($_POST['from_year'])? $_POST['from_year'] : "0001";
				
			$to_day = "01";
			$to_month = isset($_POST['to_month'])? $_POST['to_month'] : "01";
			$to_year = isset($_POST['to_year'])? $_POST['to_year'] : "0001";
				
			$_POST['from_date'] = "$from_year-$from_month-$from_day";
			$_POST['to_date'] = "$to_year-$to_month-$to_day";

			$remove_data = array('id','from_year','from_month','to_year','to_month');
			$new_data = filter_before_add($_POST, $remove_data);
			
			$result = $this->db->insert('employment', $new_data);
			
			if($result){ 
				create_log($applicant_id,"employment","add"); 
			} else { 
				$status = "Error";$message = "Error please try again.";
			}
		}

		echo json_encode(array('status'=>$status,'message'=>$message));
	}
	
	public function ajax_licenses()
	{
		$status = "Success";
		$message = "Licenses Save.";
	
		$id = isset($_POST['id']) ? $_POST['id'] : '';
		$applicant_id = $_POST['applicant_id'];
		$referral = isset($_POST['referral']) ? $_POST['referral'] : '';
		$is_na_licenses = isset($_POST['is_na_licenses']) ? $_POST['is_na_licenses'] : '';
	
		if ($is_na_licenses) {
			$sql = "UPDATE personal
			SET is_na_licenses='$is_na_licenses'
			where applicant_id='$applicant_id'";
			$result = mysql_query($sql);
			if($result){ create_log($applicant_id,"licenses","add","NA"); }
		} else {
				
			$day = isset($_POST['day'])? $_POST['day'] : "01";
			$month = isset($_POST['month'])? $_POST['month'] : "00";
			$year = isset($_POST['year'])? $_POST['year'] : "0000";
			
			$exday = isset($_POST['expiredate_day'])? $_POST['expiredate_day'] : "01";
			$exmonth = isset($_POST['expiredate_month'])? $_POST['expiredate_month'] : "00";
			$exyear = isset($_POST['expiredate_year'])? $_POST['expiredate_year'] : "0000";
				
			$_POST['datetaken'] = "$year-$month-$day";
			$_POST['expiredate'] = "$exyear-$exmonth-$exday";
	
			$remove_data = array('id','month','year','day','expiredate_month','expiredate_day','expiredate_year');
			$new_data = filter_before_add($_POST, $remove_data);

			$result = $this->db->insert('licenses', $new_data);
				
			if($result){
				create_log($applicant_id,"licenses","add");
			} else {
				$status = "Error";$message = "Error please try again.";
			}
		}
	
		echo json_encode(array('status'=>$status,'message'=>$message));
	}
	
	public function ajax_training()
	{
		$status = "Success";
		$message = "Training Save.";
	
		$id = isset($_POST['id']) ? $_POST['id'] : '';
		$applicant_id = $_POST['applicant_id'];
		$referral = isset($_POST['referral']) ? $_POST['referral'] : '';
		$is_na_training = isset($_POST['is_na_training']) ? $_POST['is_na_training'] : '';
	
		if ($is_na_training) {
			$sql = "UPDATE personal
			SET is_na_training='$is_na_training'
			where applicant_id='$applicant_id'";
			$result = mysql_query($sql);
			if($result){ create_log($applicant_id,"training","add","NA"); }
		} else {
	
			$remove_data = array('id');
			$new_data = filter_before_add($_POST, $remove_data);
	
			$result = $this->db->insert('training', $new_data);
	
			if($result){
				create_log($applicant_id,"training","add");
			} else {
				$status = "Error";$message = "Error please try again.";
			}
		}
	
		echo json_encode(array('status'=>$status,'message'=>$message));
	}
	
	public function ajax_tradetest()
	{
		$status = "Success";
		$message = "Tradetest Save.";
	
		$id = isset($_POST['id']) ? $_POST['id'] : '';
		$applicant_id = $_POST['applicant_id'];
		$referral = isset($_POST['referral']) ? $_POST['referral'] : '';
		$is_na_tradetest = isset($_POST['is_na_tradetest']) ? $_POST['is_na_tradetest'] : '';
	
		if ($is_na_tradetest) {
			/* NA set to - 1 */
			$this->db->where('applicant_id', $applicant_id);
			$result = $this->db->update('personal_assessment', array('is_na_tradetest'=>1));
			if($result){ create_log($applicant_id,"tradetest","add","NA"); }
		} else {
			/* NA set to - 0 */
			$this->db->where('applicant_id', $applicant_id);
			$this->db->update('personal_assessment', array('is_na_tradetest'=>0));
			
			$trade_day = isset($_POST['trade_day'])? $_POST['trade_day'] : "01";
			$trade_month = isset($_POST['trade_month'])? $_POST['trade_month'] : "00";
			$trade_year = isset($_POST['trade_year'])? $_POST['trade_year'] : "0000";
				
			$_POST['trade_date'] = "$trade_year-$trade_month-$trade_day";
			
			$remove_data = array('id','trade_day','trade_month','trade_year');
			$new_data = filter_before_add($_POST, $remove_data);
	
			$result = $this->db->insert('tradetest', $new_data);
	
			if($result){
				create_log($applicant_id,"tradetest","add");
			} else {
				$status = "Error";$message = "Error please try again.";
			}
		}
	
		echo json_encode(array('status'=>$status,'message'=>$message));
	}
	
	public function ajax_beneficiaries()
	{
		$status = "Success";
		$message = "Beneficiaries Save.";
	
		$id = isset($_POST['id']) ? $_POST['id'] : '';
		$applicant_id = $_POST['applicant_id'];
		$referral = isset($_POST['referral']) ? $_POST['referral'] : '';
		$is_na_beneficiaries = isset($_POST['is_na_beneficiaries']) ? $_POST['is_na_beneficiaries'] : '';
	
		if ($is_na_beneficiaries) {
			/* NA set to - 1 */
			$this->db->where('applicant_id', $applicant_id);
			$result = $this->db->update('personal_assessment', array('is_na_beneficiaries'=>1));
			if($result){ create_log($applicant_id,"tradetest","add","NA"); }
		} else {
			/* NA set to - 0 */
			$this->db->where('applicant_id', $applicant_id);
			$this->db->update('personal_assessment', array('is_na_beneficiaries'=>0));
			
			$day = isset($_POST['day'])? $_POST['day'] : "01";
			$month = isset($_POST['month'])? $_POST['month'] : "00";
			$year = isset($_POST['year'])? $_POST['year'] : "0000";
				
			$_POST['bday'] = "$year-$month-$day";
			
			$remove_data = array('id','day','month','year');
			$new_data = filter_before_add($_POST, $remove_data);
	
			$result = $this->db->insert('beneficiaries', $new_data);
	
			if($result){
				create_log($applicant_id,"beneficiaries","add");
			} else {
				$status = "Error";$message = "Error please try again.";
			}
		}
	
		echo json_encode(array('status'=>$status,'message'=>$message));
	}
	
	public function ajax_cvlocation()
	{
		$status = "Success";
		$message = "Status of Interview Save.";
	
		$applicant_id = $_POST['applicant_id'];
		$status_dcv = isset($_POST['status_dcv']) ? $_POST['status_dcv'] : '';
		
		if ($applicant_id) {
			$user_id2 = $this->session->userdata['iris_user_id'];
			$applicant = $this->db->query("select cv_source,source_id from personal where applicant_id = '".$applicant_id."'")->row_array();
			
			
			$_POST['cv_source'] = $applicant['cv_source'];
			$_POST['source_id'] = $applicant['source_id'];
			$_POST['date_created'] = date('Y-m-d h:i:s');
			$_POST['date_rc'] = date('Y-m-d');
			$_POST['date_edit'] = date('Y-m-d h:i:s');
			$_POST['user_id1'] = $this->session->userdata['iris_user_id'];
			$_POST['user_id2'] = $this->session->userdata['iris_user_id'];
			$_POST['status'] = $status_dcv;
			
			$remove_data = array('id','status_dcv');
			$new_data = filter_before_add($_POST, $remove_data);
	
			$result = $this->db->insert('cv_location', $new_data);
	
			if($result){
				create_log($applicant_id,"cvlocation","add");
			} else {
				$status = "Error";$message = "Error please try again.";
			}
		}
	
		echo json_encode(array('status'=>$status,'message'=>$message));
	}

}