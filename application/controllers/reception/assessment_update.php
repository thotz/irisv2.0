<?php

Class Assessment_update extends My_controller
{
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('reception/assessment_model');
		$this->load->helper('select_option','applicant');
	}

	public function ajax_education()
	{
		$status = "Success";
		$message = "Education Updated.";

		$id = isset($_POST['id']) ? $_POST['id'] : '';
		$applicant_id = $_POST['applicant_id'];
		$referral = isset($_POST['referral']) ? $_POST['referral'] : '';
		$is_na_education = isset($_POST['is_na_education']) ? $_POST['is_na_education'] : '';
		
		if ($id) {
			$remove_data = array('id','applicant_id');
			$new_data = filter_before_add($_POST, $remove_data);
			
			$this->db->where('id', $id);
			$result = $this->db->update('education', $new_data);
			
			if($result){
				create_log($applicant_id,"education","edit");
			} else {
				$status = "Error";$message = "Error please try again.";
			}
		}

		echo json_encode(array('status'=>$status,'message'=>$message));
	}

	public function ajax_employment()
	{
		$status = "Success";
		$message = "Work History Updated.";

		$id = isset($_POST['id']) ? $_POST['id'] : '';
		$applicant_id = $_POST['applicant_id'];
		$referral = isset($_POST['referral']) ? $_POST['referral'] : '';
		$is_na_employment = isset($_POST['is_na_employment']) ? $_POST['is_na_employment'] : '';
		
		if ($id) {
			$from_day = "01";
			$from_month = isset($_POST['from_month'])? $_POST['from_month'] : "00";
			$from_year = isset($_POST['from_year'])? $_POST['from_year'] : "0000";
			
			$to_day = "01";
			$to_month = isset($_POST['to_month'])? $_POST['to_month'] : "01";
			$to_year = isset($_POST['to_year'])? $_POST['to_year'] : "0000";
			
			$_POST['from_date'] = "$from_year-$from_month-$from_day";
			$_POST['to_date'] = "$to_year-$to_month-$to_day";
				
			$remove_data = array('id','from_year','from_month','to_year','to_month');
			$new_data = filter_before_add($_POST, $remove_data);
			
			$this->db->where('id', $id);
			$result = $this->db->update('employment', $new_data);
			
			if($result){
				create_log($applicant_id,"employment","edit");
			} else {
				$status = "Error";$message = "Error please try again.";
			}
		}

		echo json_encode(array('status'=>$status,'message'=>$message));
	}
	
	public function ajax_licenses()
	{
		$status = "Success";
		$message = "Licenses Updated.";
	
		$id = isset($_POST['id']) ? $_POST['id'] : '';
		$applicant_id = $_POST['applicant_id'];
		$referral = isset($_POST['referral']) ? $_POST['referral'] : '';
		$is_na_licenses = isset($_POST['is_na_licenses']) ? $_POST['is_na_licenses'] : '';
	
		if ($id) {
			$day = isset($_POST['day'])? $_POST['day'] : "01";
			$month = isset($_POST['month'])? $_POST['month'] : "00";
			$year = isset($_POST['year'])? $_POST['year'] : "0000";
			
			$exday = isset($_POST['expiredate_day'])? $_POST['expiredate_day'] : "01";
			$exmonth = isset($_POST['expiredate_month'])? $_POST['expiredate_month'] : "00";
			$exyear = isset($_POST['expiredate_year'])? $_POST['expiredate_year'] : "0000";
				
			$_POST['datetaken'] = "$year-$month-$day";
			$_POST['expiredate'] = "$exyear-$exmonth-$exday";
			
			$remove_data = array('id','applicant_id','month','year','day','expiredate_month','expiredate_day','expiredate_year');
			$new_data = filter_before_add($_POST, $remove_data);

			$this->db->where('id', $id);
			$result = $this->db->update('licenses', $new_data);
				
			if($result){
				create_log($applicant_id,"licenses","edit");
			} else {
				$status = "Error";$message = "Error please try again.";
			}
		}
	
		echo json_encode(array('status'=>$status,'message'=>$message));
	}
	
	public function ajax_training()
	{
		$status = "Success";
		$message = "Training Updated.";
	
		$id = isset($_POST['id']) ? $_POST['id'] : '';
		$applicant_id = $_POST['applicant_id'];
		$referral = isset($_POST['referral']) ? $_POST['referral'] : '';
		$is_na_training = isset($_POST['is_na_training']) ? $_POST['is_na_training'] : '';
	
		if ($id) {
			$remove_data = array('id','applicant_id');
			$new_data = filter_before_add($_POST, $remove_data);
	
			$this->db->where('training_id', $id);
			$result = $this->db->update('training', $new_data);
	
			if($result){
				create_log($applicant_id,"training","edit");
			} else {
				$status = "Error";$message = "Error please try again.";
			}
		}
	
		echo json_encode(array('status'=>$status,'message'=>$message));
	}
	
	public function ajax_tradetest()
	{
		$status = "Success";
		$message = "TradeTest Updated.";
	
		$id = isset($_POST['id']) ? $_POST['id'] : '';
		$applicant_id = $_POST['applicant_id'];
		$referral = isset($_POST['referral']) ? $_POST['referral'] : '';
		$is_na_tradetest = isset($_POST['is_na_tradetest']) ? $_POST['is_na_tradetest'] : '';
	
		if ($id) {
			$trade_day = isset($_POST['trade_day'])? $_POST['trade_day'] : "01";
			$trade_month = isset($_POST['trade_month'])? $_POST['trade_month'] : "00";
			$trade_year = isset($_POST['trade_year'])? $_POST['trade_year'] : "0000";
			
			$_POST['trade_date'] = "$trade_year-$trade_month-$trade_day";
			
			$remove_data = array('id','applicant_id','applicant_id','trade_day','trade_month','trade_year');
			$new_data = filter_before_add($_POST, $remove_data);
	
			$this->db->where('tradetest_id', $id);
			$result = $this->db->update('tradetest', $new_data);
	
			if($result){
				create_log($applicant_id,"tradetest","edit");
			} else {
				$status = "Error";$message = "Error please try again.";
			}
		}
	
		echo json_encode(array('status'=>$status,'message'=>$message));
	}
	
	public function ajax_beneficiaries()
	{
		$status = "Success";
		$message = "Beneficiaries Updated.";
	
		$id = isset($_POST['id']) ? $_POST['id'] : '';
		$applicant_id = $_POST['applicant_id'];
		$referral = isset($_POST['referral']) ? $_POST['referral'] : '';
		$is_na_beneficiaries = isset($_POST['is_na_beneficiaries']) ? $_POST['is_na_beneficiaries'] : '';
	
		if ($id) {
			$day = isset($_POST['day'])? $_POST['day'] : "01";
			$month = isset($_POST['month'])? $_POST['month'] : "00";
			$year = isset($_POST['year'])? $_POST['year'] : "0000";
				
			$_POST['bday'] = "$year-$month-$day";
				
			$remove_data = array('id','applicant_id','day','month','year');
			$new_data = filter_before_add($_POST, $remove_data);
	
			$this->db->where('beneficiaries_id', $id);
			$result = $this->db->update('beneficiaries', $new_data);
	
			if($result){
				create_log($applicant_id,"beneficiaries","edit");
			} else {
				$status = "Error";$message = "Error please try again.";
			}
		}
	
		echo json_encode(array('status'=>$status,'message'=>$message));
	}

}