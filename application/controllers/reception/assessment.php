<?php

Class Assessment extends My_controller
{
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('reception/assessment_model');
		$this->load->helper('select_option','applicant');
	}
	
	public function index()
	{
		set_header_title("Assessment");
		
		$this->template->set('file_javascript', array(
				'js/plugins/iCheck/icheck.min.js',
				'js/reception/assessment.js'
		));
		$this->template->set('file_css', array(
				'js/plugins/iCheck/all.css'
		
		));

		$applicant_id = "";
		if (isset($_GET['applicant_id']) && $_GET['applicant_id']) {
			$applicant_id = $_GET['applicant_id'];
		}
		
		$assessment_from = $this->assessment_model->check_for_prescreening_walkin($applicant_id);
		$my_javascript_data = '
			<script>
				var my_javascript_data = {
					applicant_id:"'.$applicant_id.'",
					assessment_from :"'.$assessment_from.'"
				};
			</script>';

		$data = array(
				'applicant_information' => $this->global_model->get_applicant_information_content($applicant_id),
				'applicant_link_menu' => $this->global_model->get_applicant_link_menu($applicant_id),
				'applicant_id' => $applicant_id,
				'my_javascript_data' => $my_javascript_data
		);
		
		$this->template->view('reception/assessment/index', $data);
	}
	
	public function ajax_assessment_modal_show()
	{
		$id = isset($_GET['id']) ? $_GET['id'] : '';
		$action = isset($_GET['action']) ? $_GET['action'] : '';
		$referral = isset($_GET['referral']) ? $_GET['referral'] : '';
		$is_na_field = 'is_na_'.$action;
		$applicant_id = $_GET['applicant_id'];
		
		$query = $this->db->query("select * from personal_assessment where applicant_id='$applicant_id'");
		$applicant = $query->row_array();
		
		$is_na_value = $applicant[$is_na_field];
		
		$data = array(
			'id' => $id,
			'action' => $action,
			'referral' => $referral,
			'is_na_field' => $is_na_field,
			'is_na_value' => $is_na_value,
			'applicant_id' => $applicant_id,
		);
		
		$this->load->view('reception/modal/modal-assessment', $data);
	}
	
	public function ajax_assessment_education_view()
	{
		$applicant_id = $_GET['applicant_id'];
		$query = $this->db->query("select * from education where applicant_id='$applicant_id' order by to_date desc");
		$education = $query->result_array();

		$query = $this->db->query("select is_na_education from personal_assessment where applicant_id='$applicant_id'");
		$applicant = $query->row_array();
		
		$is_na_value = $applicant['is_na_education'];
		if($applicant_id && count($education) > 0){
			$html = $this->load->view('reception/assessment/list_education', array('education' => $education), true);
		}else{
			if ($is_na_value) {
				$html = "<li style=\"color:red;\">N/A</li>";
			} else {
				$html = "<li style=\"color:green;\">No record found.</li>";
			}
		}
		$show_logs = show_log_ajax("education",$applicant_id);
			
		$html = iconv('UTF-8', 'UTF-8//IGNORE', $html);
		echo json_encode(array('html'=>$html, 'count'=>count($education), 'logs'=>$show_logs));
	}
	
	public function ajax_assessment_education_update()
	{
		$educ_field = array("N/A","Advertising / Media", "Agriculture", "Airline Transport", "Architecture / Urban Studies", "Art & Design", "Biology","BioTechnology","Business Studies/Administration/Management","Chemistry","Commerce","Computer Science/Information technology","Dentistry","Economics","Editing and Publication","Education/Teaching/Training","Engineering(Aviation/Aeronautics/Astronautics)","Engineering(Chemical)","Engineering(Civil)","Engineering(Computer/Telecommunication)","Engineering(Electrical/Electronic)","Engineering(Environmental/Health/Safety)","Engineering(Industrial)","Engineering(Marine)","Engineering(Material Science)","Engineering(Mechanical)","Engineering(Metal Fabrication/Tool & Die/Welding)","Engineering(Metallurgical)","Engineering(Others)","Engineering(Petroleum/Oil/Gas)","Finance/Accountancy/Banking","Food and Beverage Preparation/Service Management","Geographical Science","Hospitality/Tourism Management","Human Resource Management","Humanities/Liberal Arts","Land Transport","Law","Library Management","Linguistics/Translation & Interpretation","Mass Communications","Mathematics","Medical Science","Medicine","Merchant Marine","Music/Performing Arts Studies","Nursing","Others","Personal Services & Building/Ground Services","Pharmacy/Pharmacology","Physics","Protective Services & Management","Quantity Survey","Sales & Marketing","Science & Technology","Secretarial","Textile/Fashion Design & Production","Veterinary");
		$educ_level = array("","High School Diploma", "Vocational Diploma / Short Course Certificate", "College Level (Undergraduate)","Bachelor's / College Degree", "Post Graduate Diploma / Master's Degree", "Prof'l License(Passed Board/Bar/Prof'l License Exam)", "Doctorate Degree");
		
		$id = isset($_GET['id']) ? $_GET['id'] : '';
		$referral = 'education';
		$referral = isset($_GET['referral']) ? $_GET['referral'] : '';
		$applicant_id = $_GET['applicant_id'];
		
		$education = "";
		$fstudy = "";
		$result = "";
		if ($id && $id != 'undefined') {
			$query = $this->db->query("select * from education where id='$id'");
			$result = $query->result_array();
				
			$fstudy = $result['f_study'];
			$education = $result['education'];
			
// 			$from_date = $result['from_date'];
// 			$to_date = $result['to_date'];
// 			$school = $result['school'];
			$btn_value = "Save";
		} else {
			$btn_value = "Add";
		}
		
		$select_f_study = "";
		foreach($educ_field as $value) {
			if ($value == $fstudy) {
				$select_f_study .= "<option value='".$value."' selected='selected'>".$value."</option>";
			} else {
				$select_f_study .= "<option value='".$value."'>".$value."</option>";
			}
		}
		
		$select_educ_level = "";
		foreach($educ_level as $value) {
			if ($value == $education) {
				$select_educ_level .= "<option value='".$value."' selected='selected'>".$value."</option>";
			} else {
				$select_educ_level .= "<option value='".$value."'>".$value."</option>";
			}
		}
		
		$data = array(
				'id' => $id,
				'result' => $result,
				'btn_value' => $btn_value,
				'select_f_study' => $select_f_study,
				'select_educ_level' => $select_educ_level
				
		);
		
		$html = $this->load->view('reception/modal/modal-add-education', $data, true);
			
		$html = iconv('UTF-8', 'UTF-8//IGNORE', $html);
		echo json_encode(array('html'=>$html));
	}
	
	public function ajax_assessment_employment_view()
	{
		$applicant_id = $_GET['applicant_id'];
		$query = $this->db->query("select * from employment where applicant_id='$applicant_id' order by from_date desc");
		$employment = $query->result_array();
	
		$query = $this->db->query("select is_na_employment from personal_assessment where applicant_id='$applicant_id'");
		$applicant = $query->row_array();
	
		$is_na_value = $applicant['is_na_employment'];
		if($applicant_id && count($employment) > 0){
			$html = $this->load->view('reception/assessment/list_employment', array('employment' => $employment), true);
		}else{
			if ($is_na_value) {
				$html = "<li style=\"color:red;\">N/A</li>";
			} else {
				$html = "<li style=\"color:green;\">No record found.</li>";
			}
		}
		$show_logs = show_log_ajax("employment",$applicant_id);
		
		$html = iconv('UTF-8', 'UTF-8//IGNORE', $html);
		echo json_encode(array('html'=>$html, 'count'=>count($employment), 'logs'=>$show_logs));
	}
	
	public function ajax_assessment_employment_update()
	{
		$applicant_id = $_GET['applicant_id'];
		$query = $this->db->query("select * from employment where applicant_id='$applicant_id' order by from_date desc");
		$employment = $query->result_array();
	
		$query = $this->db->query("select is_na_employment from personal_assessment where applicant_id='$applicant_id'");
		$applicant = $query->row_array();
	
		$is_na_value = $applicant['is_na_employment'];
		if($applicant_id && count($employment) > 0){
			$html = $this->load->view('reception/modal/modal-add-work', array('employment' => $employment), true);
		}else{
			if ($is_na_value) {
				$html = "<li style=\"color:red;\">N/A</li>";
			} else {
				$html = "<li style=\"color:green;\">No record found.</li>";
			}
		}
		$show_logs = show_log_ajax("employment",$applicant_id);
	
		$html = iconv('UTF-8', 'UTF-8//IGNORE', $html);
		echo json_encode(array('html'=>$html, 'count'=>count($employment), 'logs'=>$show_logs));
	}
	
	public function ajax_assessment_licenses_view()
	{
		$applicant_id = $_GET['applicant_id'];
		$query = $this->db->query("select * from licenses where applicant_id='$applicant_id' order by datetaken desc");
		$licenses = $query->result_array();
	
		$query = $this->db->query("select is_na_license from personal_assessment where applicant_id='$applicant_id'");
		$applicant = $query->row_array();
	
		$is_na_value = $applicant['is_na_license'];
		if($applicant_id && count($licenses) > 0){
			$html = $this->load->view('reception/assessment/list_licenses', array('licenses' => $licenses), true);
		}else{
			if ($is_na_value) {
				$html = "<li style=\"color:red;\">N/A</li>";
			} else {
				$html = "<li style=\"color:green;\">No record found.</li>";
			}
		}
		$show_logs = show_log_ajax("licenses",$applicant_id);
	
		$html = iconv('UTF-8', 'UTF-8//IGNORE', $html);
		echo json_encode(array('html'=>$html, 'count'=>count($licenses), 'logs'=>$show_logs));
	}
	
	public function ajax_assessment_training_view()
	{
		$applicant_id = $_GET['applicant_id'];
		$query = $this->db->query("select * from training where applicant_id='$applicant_id' order by date desc");
		$training = $query->result_array();
	
		$query = $this->db->query("select is_na_training from personal_assessment where applicant_id='$applicant_id'");
		$applicant = $query->row_array();
	
		$is_na_value = $applicant['is_na_training'];
		if($applicant_id && count($training) > 0){
			$html = $this->load->view('reception/assessment/list_training', array('training' => $training), true);
		}else{
			if ($is_na_value) {
				$html = "<li style=\"color:red;\">N/A</li>";
			} else {
				$html = "<li style=\"color:green;\">No record found.</li>";
			}
		}
		$show_logs = show_log_ajax("training",$applicant_id);
	
		$html = iconv('UTF-8', 'UTF-8//IGNORE', $html);
		echo json_encode(array('html'=>$html, 'count'=>count($training), 'logs'=>$show_logs));
	}
	
	public function ajax_assessment_tradetest_view()
	{
		$applicant_id = $_GET['applicant_id'];
		$query = $this->db->query("select * from tradetest where applicant_id='$applicant_id' order by trade_date desc");
		$tradetest = $query->result_array();
	
		$query = $this->db->query("select is_na_tradetest from personal_assessment where applicant_id='$applicant_id'");
		$applicant = $query->row_array();
	
		$is_na_value = $applicant['is_na_tradetest'];
		if($applicant_id && count($tradetest) > 0){
			$html = $this->load->view('reception/assessment/list_tradetest', array('tradetest' => $tradetest), true);
		}else{
			if ($is_na_value) {
				$html = "<li style=\"color:red;\">N/A</li>";
			} else {
				$html = "<li style=\"color:green;\">No record found.</li>";
			}
		}
		$show_logs = show_log_ajax("tradetest",$applicant_id);
	
		$html = iconv('UTF-8', 'UTF-8//IGNORE', $html);
		echo json_encode(array('html'=>$html, 'count'=>count($tradetest), 'logs'=>$show_logs));
	}
	
	public function ajax_assessment_beneficiaries_view()
	{
		$applicant_id = $_GET['applicant_id'];
		$query = $this->db->query("select * from beneficiaries where applicant_id='$applicant_id'");
		$beneficiaries = $query->result_array();
	
		$query = $this->db->query("select is_na_beneficiaries from personal_assessment where applicant_id='$applicant_id'");
		$applicant = $query->row_array();
	
		$is_na_value = $applicant['is_na_beneficiaries'];
		if($applicant_id && count($beneficiaries) > 0){
			$html = $this->load->view('reception/assessment/list_beneficiaries', array('beneficiaries' => $beneficiaries), true);
		}else{
			if ($is_na_value) {
				$html = "<li style=\"color:red;\">N/A</li>";
			} else {
				$html = "<li style=\"color:green;\">No record found.</li>";
			}
		}
		$show_logs = show_log_ajax("beneficiaries",$applicant_id);
	
		$html = iconv('UTF-8', 'UTF-8//IGNORE', $html);
		echo json_encode(array('html'=>$html, 'count'=>count($beneficiaries), 'logs'=>$show_logs));
	}
	
	public function ajax_assessment_prefered_view()
	{
		$applicant_id = $_GET['applicant_id'];
		$query = $this->db->query("select * from prefered_position where applicant_id='$applicant_id' order by position_order asc");
		$prefered = $query->result_array();
	
		if($applicant_id && count($prefered) > 0){
			$html = $this->load->view('reception/assessment/list_prefered', array('prefered' => $prefered), true);
		}else{
			$html = "<li style=\"color:green;\">No record found.</li>";
		}
		$show_logs = show_log_ajax("prefered_position",$applicant_id);
	
		$html = iconv('UTF-8', 'UTF-8//IGNORE', $html);
		echo json_encode(array('html'=>$html, 'count'=>count($prefered), 'logs'=>$show_logs));
	}
	
	public function ajax_assessment_position_view()
	{
		$applicant_id = $_GET['applicant_id'];
		$result_lineup = $this->assessment_model->get_lineup_final($applicant_id);
		$query = $this->db->query("select l.line_up_id,l.applicant_id,l.manpower_rid,l.mr_pos_id,po.position_id,po.name as position_name,mr.ref_no,mr.principal_id,mr.prin_company_id,pr.name as prin_name, pc.name as company_name
									,mr_pos.status_mr,l.for_confirm_initial,l.for_confirm_mode
									from line_up as l
									left join personal as p on p.applicant_id = l.applicant_id 
									left join mr_position mr_pos on mr_pos.mr_pos_id = l.mr_pos_id 
									left join positions as po on po.position_id = mr_pos.position_id 
									left join manpower_r as mr on mr.manpower_rid = l.manpower_rid
									left join principals as pr ON pr.principal_id = mr.principal_id
									left join prin_company as pc ON pc.prin_company_id = mr.prin_company_id
									where 1 
									and mr.mrpriority = 1
									and l.for_confirm_initial != ''
									and l.applicant_id = '".$applicant_id."'
									and l.manpower_rid in (select manpower_rid from manpower_r where status = 'Active' and ref_no != '')
									group by l.line_up_id
									order by mr.ref_no,po.name,p.lname asc");
		$position = $query->result_array();
		
		$data = array(
				'position' => $position,
				'result_lineup' => $result_lineup
		);
	
		if($applicant_id && count($position) > 0){
			$html = $this->load->view('reception/assessment/list_position', $data, true);
		}else{
			$html = "<li style=\"color:green;\">No record found.</li>";
		}
		$show_logs = show_log_ajax("apply_applicant",$applicant_id);
	
		$html = iconv('UTF-8', 'UTF-8//IGNORE', $html);
		echo json_encode(array('html'=>$html, 'count'=>count($position), 'logs'=>$show_logs));
	}
	
	public function ajax_assessment_general_view()
	{
		$applicant_id = $_GET['applicant_id'];
		$query = $this->db->query("select * from assessment_gen where applicant_id='$applicant_id' order by date_created desc");
		$general = $query->result_array();
	
		if($applicant_id && count($general) > 0){
			$html = $this->load->view('reception/assessment/list_general', array('general' => $general), true);
		}else{
			$html = "<tr><td colspan=\"7\" style=\"color:green;text-align:center;padding:10px;\">No record found.</td></tr>";
		}
		$show_logs = show_log_ajax("assessment_gen",$applicant_id);
	
		$html = iconv('UTF-8', 'UTF-8//IGNORE', $html);
		echo json_encode(array('html'=>$html, 'count'=>count($general), 'logs'=>$show_logs));
	}
	
	public function ajax_assessment_cvlocation_view()
	{
		$applicant_id = $_GET['applicant_id'];
		$cv_location_id = isset($_GET['cv_location_id']) ? $_GET['cv_location_id'] : "";
		
		$sqlnocvdistribution = "";
		if($cv_location_id) $sqlnocvdistribution = "and cv_location_id != '$cv_location_id'";
		
		$query = $this->db->query("select cv.*, p.apply_date, u1.username as evaluator, u2.username as creator, u3.username as edit_by
					from cv_location as cv
					left join personal as p on p.applicant_id = cv.applicant_id
					left join users as u1 on u1.user_id = cv.user_id
					left join users as u2 on u2.user_id = cv.user_id1
					left join users as u3 on u3.user_id = cv.user_id2
					where 1 and cv.applicant_id = '".$applicant_id."' 
					order by cv.date_created desc, cv.cv_location_id desc");
		$cvlocation = $query->result_array();
	
		if($applicant_id && count($cvlocation) > 0){
			$html = $this->load->view('reception/assessment/list_cvlocation', array('cvlocation' => $cvlocation), true);
		}else{
			$html = "<tr><td colspan=\"7\" style=\"color:green;text-align:center;padding:10px;\">No record found.</td></tr>";
		}
		$show_logs = show_log_ajax("assessment_gen",$applicant_id);
	
		$html = iconv('UTF-8', 'UTF-8//IGNORE', $html);
		echo json_encode(array('html'=>$html, 'count'=>count($cvlocation), 'logs'=>$show_logs));
	}
	
	public function ajax_check_applicant_assessment()
	{
		$table = $_POST['table'];
		$applicant_id = $_POST['applicant_id'];
		
		
		if ($table != '' && $applicant_id !='' ) {
			$is_na = false;
			
			$applicant = getdata_row("select * from personal_assessment where applicant_id='".$applicant_id."'");
		
			$query = "select * from $table where applicant_id='".$applicant_id."' limit 1";
			$result = getdata($query);
		
			switch ($table) {
				case 'apply_applicant':
					$is_na = true;
					break;
				case 'education':
					if ($applicant['is_na_education'] == 1) $is_na = true;
					break;
				case 'employment':
					if ($applicant['is_na_employment'] == 1) $is_na = true;
					break;
				case 'licenses':
					if ($applicant['is_na_license'] == 1) $is_na = true;
					break;
				case 'training':
					if ($applicant['is_na_training'] == 1) $is_na = true;
					break;
				case 'tradetest':
					if ($applicant['is_na_tradetest'] == 1) $is_na = true;
					break;
				case 'beneficiaries':
					$is_na = true;
					break;
		
				case 'allowed':	/* no restriction */
					$is_na = true;
					break;
				default:
					/**/
			}
		
			if((is_array($result) && count($result) > 0) || $is_na){
				echo true;
			}else{
				echo false;
			}
		}
	}
}