<?php

Class Reported_today extends My_controller
{
	public function __construct()
	{
		parent::__construct();
		
	}
	
	public function index()
	{
		set_header_title("Reported Today");
		
		$this->load->model('reception/reported_model', 'reported', true);

		$applicant_id = "";
		if (isset($_GET['applicant_id']) && $_GET['applicant_id']) {
			$applicant_id = $_GET['applicant_id'];
		}
		
		$lineup_history = $this->reported->get_lineup_history($applicant_id);
				
		$data = array(
				'applicant_information' => $this->global_model->get_applicant_information_content($applicant_id),
				'applicant_link_menu' => $this->global_model->get_applicant_link_menu($applicant_id),
				'applicant_id' => $applicant_id,
				'lineup_history' => $lineup_history
		);
		
		$this->template->view('reception/reported_today/index', $data);
	}
}