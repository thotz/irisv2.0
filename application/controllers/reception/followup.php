<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Followup extends My_Controller {
	
	public function __construct(){
		parent::__construct();
		
	}
	
	public function index(){
		set_header_title("Follow-up");
		
		$this->template->view('reception/followup/index');
	}
	
	public function applicant_info(){
		$this->load->model("reception/reception_model", "reception", true);
		
		$applicant_id = $_GET['applicant_id'];
		
		$view_data['applicant_id']   = $applicant_id;
		$view_data['applicant_info'] = $this->reception->get_applicant_info($applicant_id);
		$view_data['all_line_up']    = $this->reception->get_applicant_lineup($applicant_id);

		$this->template->view('reception/followup/applicant_info', $view_data);
	}
	
	public function new_applicant(){
		$this->template->view('reception/followup/new_applicant');
	}
	
	// public function ajax_search(){
		// $this->load->model("reception/reception_model", "reception", true);
		
		// $action = $_GET['action'];
		// switch ($action) {
			// case 'search_applicant':
				// echo $this->reception->search_applicant();
				// break;
				
			// default:
		// }
	// }
	
	public function popup_search(){
		$view_data['pagename'] = base_url()."reception/followup/applicant_info";
		$view_data['param'] = "applicant_id";
		$view_data['additionalparam'] = "";
	
		$this->load->view('reception/followup/popup_search', $view_data);
	}
	
	public function popup_sendsms(){
		$this->load->model("reception/reception_model", "reception", true);
		$applicant_id 	= $_GET['applicant_id'];
		$action 		= $_GET['action'];
	
	
		$check_mobile = false;
		$ajax_action = 'ajax_send_sms';
	
		switch ($action) {
			case 'sms_single':
				if($applicant_id) {
					$applicant_info = $this->reception->get_applicant_info($applicant_id);
	
					$mobile_no = $applicant_info['cellphone'];
					$other_mob = $applicant_info['office_phone'];
					$fullname = utf8_encode($applicant_info['lname']).", ".utf8_encode($applicant_info['fname'])." ".utf8_encode($applicant_info['mname']);
					$check_mobile = true;
					$total_recipient = 1;
					$sms_info = $applicant_id."|".ucwords(strtolower(utf8_encode($applicant_info['fname'])))." ".ucwords(strtolower(utf8_encode($applicant_info['lname'])))."|".$mobile_no."|".$applicant_info['office_phone'];
				}
				break;
			case 'sms_multiple':
				/*SEND TO MULTIPLE RECIPIENT*/
				$sms_info = $_GET['sms_info'];
				$recipient = explode('@', $_GET['sms_info']);
				$recipient_name = array();
	
				foreach ($recipient as $val){
					$recipient_info = explode('|', $val);
					array_push($recipient_name, utf8_encode(ucwords(strtolower($recipient_info[1]))));
				}
	
				$fullname = implode(';', $recipient_name);
				$total_recipient = count($recipient_name);
				break;
			case 'sms_setting':
				/*SEND TO MULTIPLE RECIPIENT*/
				$sms_info = $_GET['sms_setting'];
				$recipient = explode('@', $_GET['sms_setting']);
				$recipient_name = array();
	
				foreach ($recipient as $val){
					$recipient_info = explode('|', $val);
					array_push($recipient_name, utf8_encode(ucwords(strtolower($recipient_info[1]))));
				}
	
				$fullname = implode(';', $recipient_name);
				$total_recipient = count($recipient_name);
	
				$ajax_action = 'ajax_send_sms_setting';
				break;
			default:
					
					
					
		}
	
		// $query_tpl = "select * from templates";
		// $result_tpl = getdata($query_tpl);
	
		// if($_GET['delay_med'] == 'a'){
		// $default_txta_val = "1st reminder! ".$result_tpl[7]['message'];
		// }else if($_GET['delay_med'] == 'b'){
		// $default_txta_val = "2nd reminder! ".$result_tpl[7]['message'];
		// }else if($_GET['delay_med'] == 'c'){
		// $default_txta_val = "3rd reminder! ".$result_tpl[7]['message'];
		// }else if($_GET['delay_med'] == 'd'){
		// $default_txta_val = "4th reminder! ".$result_tpl[7]['message'];
		// }else if($_GET['parent_form'] == 'deployed_databank'){
		// $default_txta_val = $result_tpl[10]['message'];
		// $default_tpl_label = "DEPLOYED APPLICANT";
		// }else if($_GET['parent_form'] == 'sourcing_databank'){
		// $default_txta_val = $result_tpl[10]['message'];
		// $default_tpl_label = "";
		// }else{
		// $default_txta_val = "";
		// $default_tpl_label = "";
		// }
	
		$replied 			 = "";
		$default_txta_val    = "";
		$default_tpl_label   = "";
		$_GET['id']			 = "";
		$_GET['delay_med']   = "";
		$_GET['parent_form'] = "";
	
		$view_data['replied']         = $replied;
		$view_data['fullname']        = $fullname;
		$view_data['mobile_no']        = $mobile_no;
		$view_data['ajax_action']        = $ajax_action;
	
	
		$view_data['sms_info']        = $sms_info;
		$view_data['check_mobile']    = $check_mobile;
		$view_data['total_recipient'] = $total_recipient;
		$view_data['default_txta_val']= $default_txta_val;
		$view_data['default_tpl_label']= $default_tpl_label;
		$view_data['delay_med'] 	  = $_GET['delay_med'];
		$view_data['parent_form'] 	  = $_GET['parent_form'];
		//debug($view_data);
		$this->load->view('reception/followup/popup_sendsms', $view_data);
	}
}