<?php

Class Edit_profile extends My_controller
{
	public function __construct()
	{
		parent::__construct();
		
	}
	
	public function index()
	{
		set_header_title("Edit Profile");
		
		$this->template->set('file_javascript', array(
				'js/plugins/iCheck/icheck.min.js',
				'js/reception/edit_profile.js'
		));
		$this->template->set('file_css', array(
				'js/plugins/iCheck/all.css'
		
		));
		
		$applicant_id = "";
		if (isset($_GET['applicant_id']) && $_GET['applicant_id']) {
			$applicant_id = $_GET['applicant_id'];
		}
		
		$data = array(
				'applicant_information' => $this->global_model->get_applicant_information_content($applicant_id),
				'applicant_link_menu' => $this->global_model->get_applicant_link_menu($applicant_id),
				'applicant_id' => $applicant_id
		);
		
		$this->template->view('reception/edit_profile/index', $data);
	}
}