<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Photoshoot_restriction extends My_controller
{
	public function __construct(){
		parent::__construct();
		
		/** Set title for all function */
		set_header_title("Photo Shoot Restriction");
	}
	
	public function index(){
		$this->template->set('file_javascript', array('js/settings/photoshoot/datatable.js', 'js/settings/photoshoot/photoshoot.js'));
		$this->template->set('file_css', array('js/plugins/datatables/dataTables.bootstrap.css'));

		$query = "select p.applicant_id, p.status, p.fname, p.mname, p.lname, p.branch_id, p.cv_source, p.date_reported, pcv.picture, pcv.pictaken, mr.ref_no, br.description as branch, v.from_date, v.to_date
				from personal p
				left join personal_cv pcv
				on p.applicant_id = pcv.applicant_id
				left join line_up l
				on p.applicant_id = l.applicant_id
				left join manpower_r mr
				on l.manpower_rid = mr.manpower_rid
				left join branch br
				on p.branch_id = br.id
				left join venue v
				on l.venue_id = v.venue_id
				where 1
				and ( ( pcv.picture = '' and pcv.pictaken <> 'webcam') or ( pcv.picture <> '' and pcv.pictaken in ('upload','Allowed') ) )
				and p.status in ('ACTIVE','ON POOL','OPERATIONS','POOLING')
				and date(p.date_reported) = CURDATE()";
		$ps_result = $this->db->query($query);

		$data['ps_data'] = $ps_result->result_array();
		$this->template->view('settings/photoshoot/index', $data);
	}

	public function ajax_update_ps(){
		if($_GET['allow_ps'] == 'true'){
			$this->global_model->update('personal_cv', array('pictaken' => 'Allowed', 'picaction'=>'1' ), array('applicant_id' => $_GET['app_id']));

			/* CREATE LOG */
			create_log($_GET['app_id'], 'photoshoot restriction', 'allow');
		}else{
			if($_GET['pic'] != ''){
				$pic_taken = "upload";
			}else{
				$pic_taken = "";
			}

			$this->global_model->update('personal_cv', array('pictaken' => $pic_taken, 'picaction'=>'0' ), array('applicant_id' => $_GET['app_id']));

			/* CREATE LOG */
			create_log($_GET['app_id'], 'photoshoot restriction', 'restrict');
		}
		
		return TRUE;
	}
}