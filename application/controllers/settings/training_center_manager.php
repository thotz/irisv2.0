<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Training_center_manager extends My_controller
{
	public function __construct(){
		parent::__construct();

		/** Set title for all function */
		set_header_title("Training Center");

		$this->load->helper('form');
		$this->load->library('form_validation');
	}

	public function index(){
		$this->template->set('file_javascript', array('js/settings/training_center_manager/datatable.js'));
		$this->template->set('file_css', array('js/plugins/datatables/dataTables.bootstrap.css'));

		$query = $this->db->get('trainingcenter');
		$data['trainingcenter'] = $query->result_array();
		$data['msg'] = "";
		$this->template->view('settings/training_center_manager/index', $data);
	}

	public function add(){
		set_header_title("Training Center - Add");

		$this->form_validation->set_rules('inputName', 'NAME', 'trim|required|is_unique[trainingcenter.name]|min_length[3]');
		$this->form_validation->set_rules('inputCode', 'CODE', 'trim|required|is_unique[trainingcenter.trID]|min_length[2]');
		$this->form_validation->set_message('is_unique', '{field} already exists.');

		$msg = "";
		$error = FALSE;
		if (isset($_POST['btnAdd'])){
			if ($this->form_validation->run() == FALSE){
				$aError = $this->form_validation->error_array();
				$msg = reset($aError);
				$error = TRUE;
			}else{
				/* insert into db */
				if($new_id = $this->global_model->insert('trainingcenter', array('name' => $_POST['inputName'], 'trID' => $_POST['inputCode']))){
					/* CREATE LOG */
					create_log($new_id, 'training center', 'add');

					$this->session->set_flashdata('add_trctr_success', 'Training Center is successfully added.');
					redirect('/settings/training_center_manager/edit/'.$new_id);
				}else{
					$msg = "an error occured";
					$error = TRUE;
				}
			}
		}

		$data = array('msg' => $msg,
					'error' => $error,
		);

		$this->template->view('settings/training_center_manager/add', $data);
	}

	public function edit($id){
		$query = "select * from trainingcenter where trainingcenter_id = '{$id}'";
		$trainingcenter = getdata($query);

		if(empty($trainingcenter)){
			redirect('/settings/training_center_manager/index');
		}

		set_header_title("Training Center - Edit");

		$this->form_validation->set_rules('inputName', 'NAME', 'trim|required|edit_unique[trainingcenter.name.trainingcenter_id.'.$id.']|min_length[3]');
		$this->form_validation->set_rules('inputCode', 'CODE', 'trim|required|edit_unique[trainingcenter.trID.trainingcenter_id.'.$id.']|min_length[2]');
		$this->form_validation->set_message('edit_unique', '{field} already exists.');

		$msg = "";
		$error = FALSE;
		if (isset($_POST['btnEdit'])){
			if ($this->form_validation->run() == FALSE){
				$aError = $this->form_validation->error_array();
				$msg = reset($aError);
				$error = true;
			}else{
				/* update tbl */
				if($this->global_model->update('trainingcenter', array('name' => $_POST['inputName'], 'trID' => $_POST['inputCode']), array('trainingcenter_id' => $id))){
					$msg = TRUE;

					/* CREATE LOG */
					create_log($id, 'training center', 'edit');
				}else{
					$msg = "an error occured";
					$error = TRUE;
				}
			}
		}

		$data = array('msg' => $msg,
					'error' => $error,
					'trainingcenter' => $trainingcenter,
		);

		$this->template->view('settings/training_center_manager/edit', $data);
	}


	public function delete($id){
		$query = "select * from trainingcenter where trainingcenter_id = '{$id}'";
		$trainingcenter = getdata($query);

		if(empty($trainingcenter)){
			redirect('/settings/training_center_manager/index');
		}else{
			/* delete from db */
			if($this->db->delete('trainingcenter', array('trainingcenter_id' => $id))){
				/* CREATE LOG */
				create_log($id, 'training center', 'delete', $trainingcenter[0]['name']);

				$error = FALSE;
				$msg = strtoupper($trainingcenter[0]['name'])." is successfully deleted.";
			}else{
				$error = TRUE;
				$msg = "An error occurred.";
			}

			$this->session->set_flashdata('trctr_del_error', $error);
			$this->session->set_flashdata('trctr_del_msg', $msg);
			redirect('/settings/training_center_manager');
		}
	}
}