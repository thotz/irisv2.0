<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Agent_manager extends My_controller
{
	public $branch_list = array();

	public function __construct(){
		parent::__construct();
		
		/** Set title for all function */
		set_header_title("Agent Manager");

		$this->load->helper('form');
		$this->load->library('form_validation');

		$this->branch_list = get_options_from_cache('branch');
	}

	public function index(){
		$this->template->set('file_javascript', array('js/settings/agent_manager/datatable.js'));
		$this->template->set('file_css', array('js/plugins/datatables/dataTables.bootstrap.css'));

		$query = "select a.agent_id, a.agent_code, b.description as branch, a.lastname, a.firstname, a.middlename, a.mobile, a.telephone, a.address, a.status, a.remarks
					from agent a
					left join branch b
					on a.branch_id = b.id
					where 1";
		$result = $this->db->query($query);

		$data['agent_status'] = array(1 => 'Active', 2 => 'Inactive', 3 => 'Cancelled');
		$data['agent_list'] = $result->result_array();
		$this->template->view('settings/agent_manager/index', $data);
	}

	public function add(){
		set_header_title("Agent Manager - Add");

		$this->form_validation->set_rules('radStatus', '', 'trim');
		$this->form_validation->set_rules('inputLastName', 'LAST NAME', 'trim|required|min_length[2]');
		$this->form_validation->set_rules('inputFirstName', 'FIRST NAME', 'trim|required|min_length[3]');
		$this->form_validation->set_rules('selectBranch', 'BRANCH', 'required');
		$this->form_validation->set_rules('inputCode', 'AGENT CODE', 'required|is_unique[agent.agent_code]|min_length[3]');
		$this->form_validation->set_message('is_unique', '{field} already exists.');
		$this->form_validation->set_rules('inputMobile', 'MOBILE NO.', 'required');
		$this->form_validation->set_rules('txtAddress', 'ADDRESS', 'trim|required');
		
		$msg = "";
		$error = FALSE;
		if (isset($_POST['btnAdd'])){
			if ($this->form_validation->run() == FALSE){
				$aError = $this->form_validation->error_array();
				$msg = reset($aError);
				$error = TRUE;
			}else{
				/* insert into db */
				$insert_data = array('status' => $_POST['radStatus'],
									'lastname' => $_POST['inputLastName'],
									'firstname' => $_POST['inputFirstName'],
									'middlename' => $_POST['inputMI'],
									'branch_id' => $_POST['selectBranch'],
									'agent_code' => $_POST['inputCode'],
									'mobile' => $_POST['inputMobile'],
									'telephone' => $_POST['inputPhone'],
									'address' => $_POST['txtAddress'],
									'remarks' => $_POST['txtRemarks'],
									'name' => $_POST['inputFirstName']." ".($_POST['inputMI']!='') ? $_POST['inputMI'].". ":" ".$_POST['inputLastName'],
				);

				if($new_access_id = $this->global_model->insert('agent', $insert_data)){
					/* CREATE LOG */
					create_log($new_access_id, 'agent manager', 'add');

					$this->session->set_flashdata('add_agent_success', 'Agent is successfully added.');
					redirect('/settings/agent_manager/edit/'.$new_access_id);
				}else{
					$msg = "an error occured";
					$error = TRUE;
				}
			}
		}

		$data = array('msg' => $msg,
					'error' => $error,
					'branches' => $this->branch_list,
		);

		$this->template->view('settings/agent_manager/add', $data);
	}

	public function edit($agent_id){
		$query = "select * from agent where agent_id = '{$agent_id}'";
		$agent_details = getdata($query);

		if(empty($agent_details)){
			redirect('/settings/agent_manager');
		}

		set_header_title("Agent Manager - Edit");

		$this->form_validation->set_rules('radStatus', '', 'trim');
		$this->form_validation->set_rules('inputLastName', 'LAST NAME', 'trim|required|min_length[2]');
		$this->form_validation->set_rules('inputFirstName', 'FIRST NAME', 'trim|required|min_length[3]');
		$this->form_validation->set_rules('selectBranch', 'BRANCH', 'required');
		$this->form_validation->set_rules('inputCode', 'AGENT CODE', 'trim|required|edit_unique[agent.agent_code.agent_id.'.$agent_id.']');
		$this->form_validation->set_message('edit_unique', '{field} already exists.');
		$this->form_validation->set_rules('inputMobile', 'MOBILE NO.', 'required');
		$this->form_validation->set_rules('txtAddress', 'ADDRESS', 'trim|required');

		$msg = "";
		$error = FALSE;
		if (isset($_POST['btnEdit'])){
			if ($this->form_validation->run() == FALSE){
				$aError = $this->form_validation->error_array();
				$msg = reset($aError);
				$error = true;
			}else{
				/* update tbl */
				$update_data = array('status' => $_POST['radStatus'],
									'lastname' => $_POST['inputLastName'],
									'firstname' => $_POST['inputFirstName'],
									'middlename' => $_POST['inputMI'],
									'branch_id' => $_POST['selectBranch'],
									'agent_code' => $_POST['inputCode'],
									'mobile' => $_POST['inputMobile'],
									'telephone' => $_POST['inputPhone'],
									'address' => $_POST['txtAddress'],
									'remarks' => $_POST['txtRemarks'],
									'name' => $_POST['inputFirstName']." ".(($_POST['inputMI']!='') ? $_POST['inputMI'].". ":" ").$_POST['inputLastName'],
				);
				
				if($this->global_model->update('agent', $update_data, array('agent_id' => $agent_id))){
					$msg = TRUE;

					/* CREATE LOG */
					create_log($agent_id, 'agent manager', 'edit');
				}else{
					$msg = "an error occured";
					$error = TRUE;
				}
			}
		}

		$data = array('msg' => $msg,
					'error' => $error,
					'branches' => $this->branch_list,
					'agent_details' => $agent_details
		);

		$this->template->view('settings/agent_manager/edit', $data);
	}

}