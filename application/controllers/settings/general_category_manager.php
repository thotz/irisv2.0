<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class General_category_manager extends My_controller
{
	public $category = array(''=>'Select Category', 'Professional'=>'Professional', 'Non-Professional'=>'Non-Professional');

	public function __construct(){
		parent::__construct();

		$this->load->helper('form');
		$this->load->library('form_validation');
	}

	public function index(){
		$this->template->set('file_javascript', array('js/settings/general_category_manager/datatable.js'));
		$this->template->set('file_css', array('js/plugins/datatables/dataTables.bootstrap.css'));

		$query = $this->db->get('gen_category');
		$data['gen_category'] = $query->result_array();
		$data['msg'] = "";
		$this->template->view('settings/general_category_manager/index', $data);
	}

	public function add(){
		set_header_title("General Category Manager - Add");

		$this->form_validation->set_rules('inputName', 'NAME', 'trim|required|is_unique[gen_category.name]|min_length[3]');
		$this->form_validation->set_message('is_unique', '{field} already exists.');
		$this->form_validation->set_rules('selectCat1', '', 'trim');
		$this->form_validation->set_rules('selectCat2', '', 'trim');
		
		$msg = "";
		$error = FALSE;
		if (isset($_POST['btnAdd'])){
			if ($this->form_validation->run() == FALSE){
				$aError = $this->form_validation->error_array();
				$msg = reset($aError);
				$error = TRUE;
			}else{
				/* insert into db */
				$insert_data = array('name' => $_POST['inputName'], 'type1' => $_POST['selectCat1'], 'type2' => $_POST['selectCat2'],);
				if($new_id = $this->global_model->insert('gen_category', $insert_data)){
					/* CREATE LOG */
					create_log($new_id, 'gen_category', 'add');

					$this->session->set_flashdata('add_gencat_success', 'General Category is successfully added.');
					redirect('/settings/general_category_manager/edit/'.$new_id);
				}else{
					$msg = "an error occured";
					$error = TRUE;
				}
			}
		}

		$data = array('msg' => $msg,
					'error' => $error,
					'category' => $this->category,
		);

		$this->template->view('settings/general_category_manager/add', $data);
	}

	public function edit($id){
		$query = "select * from gen_category where gen_category_id = '{$id}'";
		$gen_category = getdata($query);

		if(empty($gen_category)){
			redirect('/settings/general_category_manager/index');
		}

		set_header_title("General Category Manager - Edit");

		$this->form_validation->set_rules('inputName', 'NAME', 'trim|required|edit_unique[gen_category.name.gen_category_id.'.$id.']|min_length[3]');
		$this->form_validation->set_message('edit_unique', '{field} already exists.');
		$this->form_validation->set_rules('selectCat1', '', 'trim');
		$this->form_validation->set_rules('selectCat2', '', 'trim');

		$msg = "";
		$error = FALSE;
		if (isset($_POST['btnEdit'])){
			if ($this->form_validation->run() == FALSE){
				$aError = $this->form_validation->error_array();
				$msg = reset($aError);
				$error = true;
			}else{
				/* update tbl */
				$update_data = array('name' => $_POST['inputName'], 'type1' => $_POST['selectCat1'], 'type2' => $_POST['selectCat2'],);
				if($this->global_model->update('gen_category', $update_data, array('gen_category_id' => $id))){
					$msg = TRUE;

					/* CREATE LOG */
					create_log($id, 'gen_category', 'edit');
				}else{
					$msg = "an error occured";
					$error = TRUE;
				}
			}
		}

		$data = array('msg' => $msg,
					'error' => $error,
					'category' => $this->category,
					'gen_category' => $gen_category,
		);

		$this->template->view('settings/general_category_manager/edit', $data);
	}


	public function delete($id){
		$query = "select * from gen_category where gen_category_id = '{$id}'";
		$gen_category = getdata($query);

		if(empty($gen_category)){
			redirect('/settings/general_category_manager/index');
		}else{
			/* delete from db */
			if($this->db->delete('gen_category', array('gen_category_id' => $id))){
				/* CREATE LOG */
				create_log($id, 'gen_category', 'delete', $gen_category[0]['name']);

				$error = FALSE;
				$msg = strtoupper($gen_category[0]['name'])." is successfully deleted.";
			}else{
				$error = TRUE;
				$msg = "An error occurred.";
			}

			$this->session->set_flashdata('gencat_del_error', $error);
			$this->session->set_flashdata('gencat_del_msg', $msg);
			redirect('/settings/general_category_manager');
		}
	}
}