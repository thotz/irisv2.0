<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Travel_agent_manager extends My_controller
{
	public function __construct(){
		parent::__construct();

		/** Set title for all function */
		set_header_title("Travel Agent Manager");

		$this->load->helper('form');
		$this->load->library('form_validation');
	}

	public function index(){
		$this->template->set('file_javascript', array('js/settings/travel_agent_manager/datatable.js'));
		$this->template->set('file_css', array('js/plugins/datatables/dataTables.bootstrap.css'));

		$query = $this->db->get('travelagent');
		$data['travelagent_list'] = $query->result_array();
		$data['msg'] = "";
		$this->template->view('settings/travel_agent_manager/index', $data);
	}

	public function add(){
		set_header_title("Travel Agent Manager - Add");

		$this->form_validation->set_rules('inputName', 'TRAVEL AGENT NAME', 'trim|required|is_unique[travelagent.name]|min_length[2]');
		$this->form_validation->set_rules('inputCode', 'TRAVEL AGENT CODE', 'required|is_unique[travelagent.code]|min_length[2]');
		$this->form_validation->set_message('is_unique', '{field} already exists.');
		$this->form_validation->set_rules('txtAddress', 'ADDRESS', 'trim|required');
		$this->form_validation->set_rules('inputPhone', 'CONTACT NO.', 'trim|required|min_length[7]');
		$this->form_validation->set_rules('inputContact', 'CONTACT PERSON', 'trim|required');

		$msg = "";
		$error = FALSE;
		if (isset($_POST['btnAdd'])){
			if ($this->form_validation->run() == FALSE){
				$aError = $this->form_validation->error_array();
				$msg = reset($aError);
				$error = TRUE;
			}else{
				/* insert into db */
				$insert_data = array('name' => $_POST['inputName'],
									'code' => strtoupper($_POST['inputCode']),
									'fax' => $_POST['inputFax'],
									'contact' => $_POST['inputContact'],
									'address' => $_POST['txtAddress'],
									'tel' => $_POST['inputPhone'],
				);

				if($new_id = $this->global_model->insert('travelagent', $insert_data)){
					/* CREATE LOG */
					create_log($new_id, 'travel agent manager', 'add');

					$this->session->set_flashdata('add_tagent_success', 'Travel Agent is successfully added.');
					redirect('/settings/travel_agent_manager/edit/'.$new_id);
				}else{
					$msg = "an error occured";
					$error = TRUE;
				}
			}
		}

		$data = array('msg' => $msg,
						'error' => $error,
		);

		$this->template->view('settings/travel_agent_manager/add', $data);
	}

	public function edit($id){
		$query = "select * from travelagent where travelagent_id = '{$id}'";
		$travel_agent = getdata($query);

		if(empty($travel_agent)){
			redirect('/settings/travel_agent_manager/index');
		}

		set_header_title("Travel Agent Manager - Edit");

		$this->form_validation->set_rules('inputName', 'TRAVEL AGENT NAME', 'trim|required|edit_unique[travelagent.name.travelagent_id.'.$id.']|min_length[2]');
		$this->form_validation->set_rules('inputCode', 'TRAVEL AGENT CODE', 'trim|required|edit_unique[travelagent.code.travelagent_id.'.$id.']|min_length[2]');
		$this->form_validation->set_message('edit_unique', '{field} already exists.');
		$this->form_validation->set_rules('txtAddress', 'ADDRESS', 'trim|required');
		$this->form_validation->set_rules('inputPhone', 'CONTACT NO.', 'trim|required|min_length[7]');
		$this->form_validation->set_rules('inputContact', 'CONTACT PERSON', 'trim|required');

		$msg = "";
		$error = FALSE;
		if (isset($_POST['btnEdit'])){
			if ($this->form_validation->run() == FALSE){
				$aError = $this->form_validation->error_array();
				$msg = reset($aError);
				$error = true;
			}else{
				/* update tbl */
				$update_data = array('name' => $_POST['inputName'],
									'code' => strtoupper($_POST['inputCode']),
									'fax' => $_POST['inputFax'],
									'contact' => $_POST['inputContact'],
									'address' => $_POST['txtAddress'],
									'tel' => $_POST['inputPhone'],
				);

				if($this->global_model->update('travelagent', $update_data, array('travelagent_id' => $id))){
					$msg = TRUE;

					/* CREATE LOG */
					create_log($id, 'travel agent manager', 'edit');
				}else{
					$msg = "an error occured";
					$error = TRUE;
				}
			}
		}

		$data = array('msg' => $msg,
					'error' => $error,
					'travel_agent' => $travel_agent,
		);

		$this->template->view('settings/travel_agent_manager/edit', $data);
	}


	public function delete($id){
		$query = "select * from travelagent where travelagent_id = '{$id}'";
		$travelagent_details = getdata($query);

		if(empty($travelagent_details)){
			redirect('/settings/travel_agent_manager');
		}else{
			/* delete from db */
			if($this->db->delete('travelagent', array('travelagent_id' => $id))){
				/* CREATE LOG */
				create_log($id, 'travel agent manager', 'delete', $travelagent_details[0]['name']);

				$error = FALSE;
				$msg = strtoupper($travelagent_details[0]['name'])." is successfully deleted.";
			}else{
				$error = TRUE;
				$msg = "An error occurred.";
			}

			$this->session->set_flashdata('tagent_del_error', $error);
			$this->session->set_flashdata('tagent_del_msg', $msg);
			redirect('/settings/travel_agent_manager');
		}
	}
}