<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tesda_manager extends My_controller
{
	public $city = "";
	
	public function __construct(){
		parent::__construct();
		
		/** Set title for all function */
		set_header_title("TESDA Manager");

		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$this->city = get_options_from_cache('city');
	}

	public function index(){
		$this->template->set('file_javascript', array('js/settings/tesda_manager/datatable.js'));
		$this->template->set('file_css', array('js/plugins/datatables/dataTables.bootstrap.css'));

		$query = $this->db->get('tesda');
		$data['tesda_list'] = $query->result_array();
		$data['msg'] = "";
		$this->template->view('settings/tesda_manager/index', $data);
	}
	
	public function add(){
		set_header_title("TESDA Manager - Add");

		$this->form_validation->set_rules('inputTESDAName', 'TESDA NAME', 'required|is_unique[tesda.name]|min_length[2]');
		$this->form_validation->set_message('is_unique', '{field} already exists.');
		$this->form_validation->set_rules('inputTESDAHead', 'HEAD', 'trim|required');
		$this->form_validation->set_rules('inputPosition', 'POSITION', 'trim|required');
		$this->form_validation->set_rules('inputEmail', 'EMAIL', 'trim|valid_email');
		$this->form_validation->set_rules('txtAddress', 'ADDRESS', 'trim|required');
		$this->form_validation->set_rules('selectCity', 'CITY', 'required');
		$this->form_validation->set_rules('inputPhone', 'PHONE NO.', 'trim|min_length[7]');

		$msg = "";
		$error = FALSE;
		if (isset($_POST['btnAdd'])){
			if ($this->form_validation->run() == FALSE){
				$aError = $this->form_validation->error_array();
				$msg = reset($aError);
				$error = TRUE;
			}else{
				/* insert into db */
				$insert_data = array('name' => $_POST['inputTESDAName'],
									'head' => $_POST['inputTESDAHead'],
									'position' => $_POST['inputPosition'],
									'email' => $_POST['inputEmail'],
									'address' => $_POST['txtAddress'],
									'tel' => $_POST['inputPhone'],
									'city_id' => $_POST['selectCity'],
				);
		
				if($new_id = $this->global_model->insert('tesda', $insert_data)){
					/* CREATE LOG */
					create_log($new_id, 'tesda manager', 'add');

					$this->session->set_flashdata('add_tesda_success', 'TESDA Office is successfully added.');
					redirect('/settings/tesda_manager/edit/'.$new_id);
				}else{
					$msg = "an error occured";
					$error = TRUE;
				}
			}
		}

		$data = array('msg' => $msg,
						'error' => $error,
						'city' => $this->city,
		);
		
		$this->template->view('settings/tesda_manager/add', $data);
	}
	
	public function edit($id){
		$query = "select * from tesda where tesda_id = '{$id}'";
		$tesda_details = getdata($query);
		
		if(empty($tesda_details)){
			redirect('/settings/tesda_manager/index');
		}
		
		set_header_title("TESDA Office Manager - Edit");

		$this->form_validation->set_rules('inputTESDAName', 'TESDA NAME', 'required|edit_unique[tesda.name.tesda_id.'.$id.']|min_length[2]');
		$this->form_validation->set_message('edit_unique', '{field} already exists.');
		$this->form_validation->set_rules('inputTESDAHead', 'HEAD', 'trim|required');
		$this->form_validation->set_rules('inputPosition', 'POSITION', 'trim|required');
		$this->form_validation->set_rules('inputEmail', 'EMAIL', 'trim|valid_email');
		$this->form_validation->set_rules('txtAddress', 'ADDRESS', 'trim|required');
		$this->form_validation->set_rules('selectCity', 'CITY', 'required');
		$this->form_validation->set_rules('inputPhone', 'PHONE NO.', 'trim|min_length[7]');

		$msg = "";
		$error = FALSE;
		if (isset($_POST['btnEdit'])){
			if ($this->form_validation->run() == FALSE){
				$aError = $this->form_validation->error_array();
				$msg = reset($aError);
				$error = true;
			}else{
				/* update tbl */
				$update_data = array('name' => $_POST['inputTESDAName'],
									'head' => $_POST['inputTESDAHead'],
									'position' => $_POST['inputPosition'],
									'email' => $_POST['inputEmail'],
									'address' => $_POST['txtAddress'],
									'tel' => $_POST['inputPhone'],
									'city_id' => $_POST['selectCity'],
				);
		
				if($this->global_model->update('tesda', $update_data, array('tesda_id' => $id))){
					$msg = TRUE;
		
					/* CREATE LOG */
					create_log($id, 'tesda manager', 'edit');
				}else{
					$msg = "an error occured";
					$error = TRUE;
				}
			}
		}
		
		$data = array('msg' => $msg,
						'error' => $error,
						'tesda_details' => $tesda_details,
						'city' => $this->city,
		);
		
		$this->template->view('settings/tesda_manager/edit', $data);
	}

	
	public function delete($id){
		$query = "select * from tesda where tesda_id = '{$id}'";
		$tesda_details = getdata($query);

		if(empty($tesda_details)){
			redirect('/settings/tesda_manager');
		}else{
			/* delete from db */
			if($this->db->delete('tesda', array('tesda_id' => $id))){
				/* CREATE LOG */
				create_log($id, 'tesda manager', 'delete', $tesda_details[0]['name']);

				$error = FALSE;
				$msg = strtoupper($tesda_details[0]['name'])." is successfully deleted.";
			}else{
				$error = TRUE;
				$msg = "An error occurred.";
			}

			$this->session->set_flashdata('tesda_del_error', $error);
			$this->session->set_flashdata('tesda_del_msg', $msg);
			redirect('/settings/tesda_manager');
		}
	}
}