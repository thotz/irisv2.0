<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Venue_manager extends My_controller
{
	public $branch = array();
	public function __construct(){
		parent::__construct();

		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$this->branch = get_options_from_cache('branch');
	}

	public function index(){
		$this->template->set('file_javascript', array('js/settings/venue_manager/datatable.js'));
		$this->template->set('file_css', array('js/plugins/datatables/dataTables.bootstrap.css'));

		$query = $this->db->get('venue_sub');
		$data['venue_sub'] = $query->result_array();
		$data['msg'] = "";
		$data['branch'] = $this->branch;
		$this->template->view('settings/venue_manager/index', $data);
	}

	public function add(){
		set_header_title("Venue Manager - Add");

		$this->form_validation->set_rules('inputName', 'NAME', 'trim|required|is_unique[venue_sub.name]|min_length[3]');
		$this->form_validation->set_message('is_unique', '{field} already exists.');
		$this->form_validation->set_rules('selectBranch', 'BRANCH', 'trim|required');

		$msg = "";
		$error = FALSE;
		if (isset($_POST['btnAdd'])){
			if ($this->form_validation->run() == FALSE){
				$aError = $this->form_validation->error_array();
				$msg = reset($aError);
				$error = TRUE;
			}else{
				/* insert into db */
				$insert_data = array('name' => $_POST['inputName'], 'branch_id' => $_POST['selectBranch'],);
				if($new_id = $this->global_model->insert('venue_sub', $insert_data)){
					/* CREATE LOG */
					create_log($new_id, 'venue_sub', 'add');

					$this->session->set_flashdata('add_venue_success', 'Venue is successfully added.');
					redirect('/settings/venue_manager/edit/'.$new_id);
				}else{
					$msg = "an error occured";
					$error = TRUE;
				}
			}
		}

		$data = array('msg' => $msg,
					'error' => $error,
					'branch' => $this->branch,
		);

		$this->template->view('settings/venue_manager/add', $data);
	}

	public function edit($id){
		$query = "select * from venue_sub where venue_sub_id = '{$id}'";
		$venue_sub = getdata($query);

		if(empty($venue_sub)){
			redirect('/settings/venue_manager/index');
		}

		set_header_title("Venue Manager - Edit");

		$this->form_validation->set_rules('inputName', 'NAME', 'trim|required|edit_unique[venue_sub.name.venue_sub_id.'.$id.']|min_length[3]');
		$this->form_validation->set_message('edit_unique', '{field} already exists.');
		$this->form_validation->set_rules('selectBranch', 'CODE', 'trim|required');

		$msg = "";
		$error = FALSE;
		if (isset($_POST['btnEdit'])){
			if ($this->form_validation->run() == FALSE){
				$aError = $this->form_validation->error_array();
				$msg = reset($aError);
				$error = true;
			}else{
				/* update tbl */
				$update_data = array('name' => $_POST['inputName'], 'branch_id' => $_POST['selectBranch']);
				if($this->global_model->update('venue_sub', $update_data, array('venue_sub_id' => $id))){
					$msg = TRUE;

					/* CREATE LOG */
					create_log($id, 'venue_sub', 'edit');
				}else{
					$msg = "an error occured";
					$error = TRUE;
				}
			}
		}

		$data = array('msg' => $msg,
					'error' => $error,
					'branch' => $this->branch,
					'venue_sub' => $venue_sub,
		);

		$this->template->view('settings/venue_manager/edit', $data);
	}


	public function delete($id){
		$query = "select * from venue_sub where venue_sub_id = '{$id}'";
		$venue_sub = getdata($query);

		if(empty($venue_sub)){
			redirect('/settings/venue_manager/index');
		}else{
			/* delete from db */
			if($this->db->delete('venue_sub', array('venue_sub_id' => $id))){
				/* CREATE LOG */
				create_log($id, 'venue_sub', 'delete', $venue_sub[0]['name']);

				$error = FALSE;
				$msg = strtoupper($venue_sub[0]['name'])." is successfully deleted.";
			}else{
				$error = TRUE;
				$msg = "An error occurred.";
			}

			$this->session->set_flashdata('venue_del_error', $error);
			$this->session->set_flashdata('venue_del_msg', $msg);
			redirect('/settings/venue_manager');
		}
	}
}