<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Industry_manager extends My_controller
{
	public $branches = array();

	public function __construct(){
		parent::__construct();

		/** Set title for all function */
		set_header_title("Industry Manager");

		$this->load->helper('form');
		$this->load->library('form_validation');
	}

	public function index(){
		$this->template->set('file_javascript', array('js/settings/industry_manager/datatable.js'));
		$this->template->set('file_css', array('js/plugins/datatables/dataTables.bootstrap.css'));

		$query = $this->db->get('industry');
		$data['industry'] = $query->result_array();
		$data['msg'] = "";
		$this->template->view('settings/industry_manager/index', $data);
	}

	public function add(){
		set_header_title("Industry Manager - Add");

		$this->form_validation->set_rules('inputName', 'NAME', 'trim|required|is_unique[industry.name]|min_length[3]');
		$this->form_validation->set_message('is_unique', '{field} already exists.');

		$msg = "";
		$error = FALSE;
		if (isset($_POST['btnAdd'])){
			if ($this->form_validation->run() == FALSE){
				$aError = $this->form_validation->error_array();
				$msg = reset($aError);
				$error = TRUE;
			}else{
				/* insert into db */
				$insert_data = array('name' => $_POST['inputName'],);
				if($new_id = $this->global_model->insert('industry', $insert_data)){
					/* CREATE LOG */
					create_log($new_id, 'industry', 'add');

					$this->session->set_flashdata('add_ind_success', 'Industry is successfully added.');
					redirect('/settings/industry_manager/edit/'.$new_id);
				}else{
					$msg = "an error occured";
					$error = TRUE;
				}
			}
		}

		$data = array('msg' => $msg,
					'error' => $error,
		);

		$this->template->view('settings/industry_manager/add', $data);
	}

	public function edit($id){
		$query = "select * from industry where industry_id = '{$id}'";
		$industry = getdata($query);

		if(empty($industry)){
			redirect('/settings/industry_manager/index');
		}

		set_header_title("Industry Manager - Edit");

		$this->form_validation->set_rules('inputName', 'NAME', 'trim|required|edit_unique[industry.name.industry_id.'.$id.']|min_length[3]');
		$this->form_validation->set_message('edit_unique', '{field} already exists.');

		$msg = "";
		$error = FALSE;
		if (isset($_POST['btnEdit'])){
			if ($this->form_validation->run() == FALSE){
				$aError = $this->form_validation->error_array();
				$msg = reset($aError);
				$error = true;
			}else{
				/* update tbl */
				$update_data = array('name' => $_POST['inputName'],);
				if($this->global_model->update('industry', $update_data, array('industry_id' => $id))){
					$msg = TRUE;

					/* CREATE LOG */
					create_log($id, 'industry', 'edit');
				}else{
					$msg = "an error occured";
					$error = TRUE;
				}
			}
		}

		$data = array('msg' => $msg,
					'error' => $error,
					'industry' => $industry,
		);

		$this->template->view('settings/industry_manager/edit', $data);
	}


	public function delete($id){
		$query = "select * from industry where industry_id = '{$id}'";
		$industry = getdata($query);

		if(empty($industry)){
			redirect('/settings/industry_manager/index');
		}else{
			/* delete from db */
			if($this->db->delete('industry', array('industry_id' => $id))){
				/* CREATE LOG */
				create_log($id, 'industry', 'delete', $industry[0]['name']);

				$error = FALSE;
				$msg = strtoupper($industry[0]['name'])." is successfully deleted.";
			}else{
				$error = TRUE;
				$msg = "An error occurred.";
			}

			$this->session->set_flashdata('ind_del_error', $error);
			$this->session->set_flashdata('ind_del_msg', $msg);
			redirect('/settings/industry_manager');
		}
	}
}