<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Account_description extends My_controller
{
	public function __construct(){
		parent::__construct();

		/** Set title for all function */
		set_header_title("Account Description");

		$this->load->helper('form');
		$this->load->library('form_validation');
	}

	public function index(){
		$this->template->set('file_javascript', array('js/settings/account_description/datatable.js'));
		$this->template->set('file_css', array('js/plugins/datatables/dataTables.bootstrap.css'));

		$query = $this->db->get('acc_description');
		$data['acc_description'] = $query->result_array();
		$data['msg'] = "";
		$this->template->view('settings/account_description/index', $data);
	}

	public function add(){
		set_header_title("Account Description - Add");

		$this->form_validation->set_rules('inputAccountDescription', 'ACCOUNT DESCRIPTION', 'trim|required|is_unique[acc_description.mode]|min_length[4]');
		$this->form_validation->set_message('is_unique', '{field} already exists.');

		$msg = "";
		$error = FALSE;
		if (isset($_POST['btnAdd'])){
			if ($this->form_validation->run() == FALSE){
				$aError = $this->form_validation->error_array();
				$msg = reset($aError);
				$error = TRUE;
			}else{
				/* insert into db */
				if($new_id = $this->global_model->insert('acc_description', array('mode' => $_POST['inputAccountDescription']))){
					/* CREATE LOG */
					create_log($new_id, 'account description', 'add');

					$this->session->set_flashdata('add_accdesc_success', 'Account Description is successfully added.');
					redirect('/settings/account_description/edit/'.$new_id);
				}else{
					$msg = "an error occured";
					$error = TRUE;
				}
			}
		}

		$data = array('msg' => $msg,
					'error' => $error,
		);

		$this->template->view('settings/account_description/add', $data);
	}

	public function edit($id){
		$query = "select * from acc_description where id = '{$id}'";
		$acc_description = getdata($query);

		if(empty($acc_description)){
			redirect('/settings/account_description/index');
		}

		set_header_title("Account Description - Edit");

		$this->form_validation->set_rules('inputAccountDescription', 'ACCOUNT DESCRIPTION', 'trim|required|edit_unique[acc_description.mode.id.'.$id.']|min_length[4]');
		$this->form_validation->set_message('edit_unique', '{field} already exists.');

		$msg = "";
		$error = FALSE;
		if (isset($_POST['btnEdit'])){
			if ($this->form_validation->run() == FALSE){
				$aError = $this->form_validation->error_array();
				$msg = reset($aError);
				$error = true;
			}else{
				/* update tbl */
				if($this->global_model->update('acc_description', array('mode' => $_POST['inputAccountDescription']), array('id' => $id))){
					$msg = TRUE;

					/* CREATE LOG */
					create_log($id, 'account description', 'edit');
				}else{
					$msg = "an error occured";
					$error = TRUE;
				}
			}
		}

		$data = array('msg' => $msg,
					'error' => $error,
					'acc_description' => $acc_description,
		);

		$this->template->view('settings/account_description/edit', $data);
	}


	public function delete($id){
		$query = "select * from acc_description where id = '{$id}'";
		$acc_description = getdata($query);

		if(empty($acc_description)){
			redirect('/settings/account_description/index');
		}else{
			/* delete from db */
			if($this->db->delete('acc_description', array('id' => $id))){
				/* CREATE LOG */
				create_log($id, 'account description', 'delete', $acc_description[0]['mode']);

				$error = FALSE;
				$msg = strtoupper($acc_description[0]['mode'])." is successfully deleted.";
			}else{
				$error = TRUE;
				$msg = "An error occurred.";
			}

			$this->session->set_flashdata('accdesc_del_error', $error);
			$this->session->set_flashdata('accdesc_del_msg', $msg);
			redirect('/settings/account_description');
		}
	}
}