<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Broker_manager extends My_controller
{
	public $country = "";
	public $principals = "";

	public function __construct(){
		parent::__construct();
		
		/** Set title for all function */
		set_header_title("Broker Manager");

		$this->load->helper(array('form', 'select_option'));
		$this->load->library('form_validation');
		
		$this->country = get_options_from_cache('country');
		$this->principals = get_options_from_cache('principals');
	}

	public function index(){
		$this->template->set('file_javascript', array('js/settings/broker_manager/datatable.js'));
		$this->template->set('file_css', array('js/plugins/datatables/dataTables.bootstrap.css'));

		$query = $this->db->get('brokers');
		$data['broker_list'] = $query->result_array();
		$data['msg'] = "";
		$data['country'] = $this->country;
		$data['principals'] = $this->principals;

		$this->template->view('settings/broker_manager/index', $data);
	}
	
	public function add(){
		set_header_title("Broker Manager - Add");

		$this->form_validation->set_rules('inputBrokerName', 'BROKER NAME', 'required|is_unique[brokers.name]|min_length[2]');
		$this->form_validation->set_message('is_unique', '{field} already exists.');
		$this->form_validation->set_rules('txtAddress', '', 'trim');
		$this->form_validation->set_rules('selectCountry', '', 'trim');
		$this->form_validation->set_rules('selectPrincipal', '', 'trim');
		$this->form_validation->set_rules('inputFax', 'FAX NO.', 'trim|min_length[7]');
		$this->form_validation->set_rules('inputTel', 'TEL NO.', 'trim|min_length[7]');
		$this->form_validation->set_rules('inputContact', 'CONTACT PERSON', 'trim|required|min_length[2]');
		$this->form_validation->set_rules('inputContactPos', '', 'trim');
		$this->form_validation->set_rules('txtContactDtl', '', 'trim');
		$this->form_validation->set_rules('inputAccr', '', 'trim');
		$this->form_validation->set_rules('valid_year', '', 'trim');
		$this->form_validation->set_rules('valid_month', '', 'trim');
		$this->form_validation->set_rules('valid_day', '', 'trim');
		$this->form_validation->set_rules('inputRecOffcr', '', 'trim');

		$msg = "";
		$error = FALSE;
		if (isset($_POST['btnAdd'])){
			if ($this->form_validation->run() == FALSE){
				$aError = $this->form_validation->error_array();
				$msg = reset($aError);
				$error = TRUE;
			}else{
				/* insert into db */
				$insert_data = array('name' => $_POST['inputBrokerName'],
									'address' => $_POST['txtAddress'],
									'country_id' => $_POST['selectCountry'],
									'principal_id' => $_POST['selectPrincipal'],
									'fax' => $_POST['inputFax'],
									'telephone' => $_POST['inputTel'],
									'contact_person' => $_POST['inputContact'],
									'contact_position' => $_POST['inputContactPos'],
									'contact_details' => $_POST['txtContactDtl'],
									'acc_no' => $_POST['inputAccr'],
									'RO' => $_POST['inputRecOffcr'],
									'acc_date' => ($_POST['valid_month'] != '' && $_POST['valid_day'] != '' && $_POST['valid_year'] != '') ? $_POST['valid_year']."-".$_POST['valid_month']."-".$_POST['valid_day']:"0000-00-00",
				);
		
				if($new_id = $this->global_model->insert('brokers', $insert_data)){
					/* CREATE LOG */
					create_log($new_id, 'broker manager', 'add');

					$this->session->set_flashdata('add_broker_success', 'Broker is successfully added.');
					redirect('/settings/broker_manager/edit/'.$new_id);
				}else{
					$msg = "an error occured";
					$error = TRUE;
				}
			}
		}

		$data = array('msg' => $msg,
						'error' => $error,
						'country' => $this->country,
						'principals' => $this->principals,
		);
		
		$this->template->view('settings/broker_manager/add', $data);
	}
	
	public function edit($id){
		$query = "select * from brokers where broker_id = '{$id}'";
		$broker_details = getdata($query);
		
		if(empty($broker_details)){
			redirect('/settings/broker_manager');
		}
		
		set_header_title("Broker Manager - Edit");

		$this->form_validation->set_rules('inputBrokerName', 'BROKER NAME', 'required|edit_unique[brokers.name.broker_id.'.$id.']|min_length[2]');
		$this->form_validation->set_message('edit_unique', '{field} already exists.');
		$this->form_validation->set_rules('txtAddress', '', 'trim');
		$this->form_validation->set_rules('selectCountry', '', 'trim');
		$this->form_validation->set_rules('selectPrincipal', '', 'trim');
		$this->form_validation->set_rules('inputFax', 'FAX NO.', 'trim|min_length[7]');
		$this->form_validation->set_rules('inputTel', 'TEL NO.', 'trim|min_length[7]');
		$this->form_validation->set_rules('inputContact', 'CONTACT PERSON', 'trim|required|min_length[2]');
		$this->form_validation->set_rules('inputContactPos', '', 'trim');
		$this->form_validation->set_rules('txtContactDtl', '', 'trim');
		$this->form_validation->set_rules('inputAccr', '', 'trim');
		$this->form_validation->set_rules('valid_year', '', 'trim');
		$this->form_validation->set_rules('valid_month', '', 'trim');
		$this->form_validation->set_rules('valid_day', '', 'trim');
		$this->form_validation->set_rules('inputRecOffcr', '', 'trim');
		
		$msg = "";
		$error = FALSE;
		if (isset($_POST['btnEdit'])){
			if ($this->form_validation->run() == FALSE){
				$aError = $this->form_validation->error_array();
				$msg = reset($aError);
				$error = true;
			}else{
				$msg = "save";
				/* update tbl */
				$update_data = array('name' => $_POST['inputBrokerName'],
									'address' => $_POST['txtAddress'],
									'country_id' => $_POST['selectCountry'],
									'principal_id' => $_POST['selectPrincipal'],
									'fax' => $_POST['inputFax'],
									'telephone' => $_POST['inputTel'],
									'contact_person' => $_POST['inputContact'],
									'contact_position' => $_POST['inputContactPos'],
									'contact_details' => $_POST['txtContactDtl'],
									'acc_no' => $_POST['inputAccr'],
									'RO' => $_POST['inputRecOffcr'],
									'acc_date' => ($_POST['valid_month'] != '' && $_POST['valid_day'] != '' && $_POST['valid_year'] != '') ? $_POST['valid_year']."-".$_POST['valid_month']."-".$_POST['valid_day']:"0000-00-00",
				);
		
				if($this->global_model->update('brokers', $update_data, array('broker_id' => $id))){
					$msg = TRUE;
		
					/* CREATE LOG */
					create_log($id, 'broker manager', 'edit');
				}else{
					$msg = "an error occured";
					$error = TRUE;
				}
			}
		}
		
		$data = array('msg' => $msg,
				'error' => $error,
				'broker_details' => $broker_details,
				'country' => $this->country,
				'principals' => $this->principals,
		);
		
		$this->template->view('settings/broker_manager/edit', $data);
	}

	
	public function delete($id){
		$query = "select * from brokers where broker_id = '{$id}'";
		$broker_details = getdata($query);

		if(empty($broker_details)){
			redirect('/settings/broker_manager');
		}else{
			/* delete from db */
			if($this->db->delete('brokers', array('broker_id' => $id))){
				/* CREATE LOG */
				create_log($id, 'broker manager', 'delete', $broker_details[0]['name']);

				$error = FALSE;
				$msg = strtoupper($broker_details[0]['name'])." is successfully deleted.";
			}else{
				$error = TRUE;
				$msg = "An error occurred.";
			}

			$this->session->set_flashdata('broker_del_error', $error);
			$this->session->set_flashdata('broker_del_msg', $msg);
			redirect('/settings/broker_manager');
		}
	}
}