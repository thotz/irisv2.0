<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Poea_manager extends My_controller
{
	public $city = "";

	public function __construct(){
		parent::__construct();
		
		/** Set title for all function */
		set_header_title("POEA Manager");

		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$this->city = get_options_from_cache('city');
	}

	public function index(){
		$this->template->set('file_javascript', array('js/settings/poea_manager/datatable.js'));
		$this->template->set('file_css', array('js/plugins/datatables/dataTables.bootstrap.css'));

		$query = $this->db->get('poea');
		$data['poea_list'] = $query->result_array();
		$data['msg'] = "";
		$data['city'] = $this->city;

		$this->template->view('settings/poea_manager/index', $data);
	}
	
	public function add(){
		set_header_title("POEA Manager - Add");

		$this->form_validation->set_rules('inputPOEAName', 'PESO NAME', 'required|is_unique[poea.name]|min_length[2]');
		$this->form_validation->set_message('is_unique', '{field} already exists.');
		$this->form_validation->set_rules('inputHead', '', 'trim');
		$this->form_validation->set_rules('inputPosition', '', 'trim');
		$this->form_validation->set_rules('inputPhone', '', 'trim|min_length[7]');
		$this->form_validation->set_rules('inputEmail', 'EMAIL ADDRESS', 'trim|valid_email');
		$this->form_validation->set_rules('txtAddress', '', 'trim');

		$msg = "";
		$error = FALSE;
		if (isset($_POST['btnAdd'])){
			if ($this->form_validation->run() == FALSE){
				$aError = $this->form_validation->error_array();
				$msg = reset($aError);
				$error = TRUE;
			}else{
				/* insert into db */
				$insert_data = array('name' => $_POST['inputPOEAName'],
									'head' => $_POST['inputHead'],
									'position' => $_POST['inputPosition'],
									'address' => $_POST['txtAddress'],
									'city_id' => $_POST['selectCity'],
									'tel' => $_POST['inputPhone'],
									'email' => $_POST['inputEmail'],
				);
		
				if($new_id = $this->global_model->insert('poea', $insert_data)){
					/* CREATE LOG */
					create_log($new_id, 'poea manager', 'add');

					$this->session->set_flashdata('add_poea_success', 'POEA is successfully added.');
					redirect('/settings/poea_manager/edit/'.$new_id);
				}else{
					$msg = "an error occured";
					$error = TRUE;
				}
			}
		}

		$data = array('msg' => $msg,
						'error' => $error,
						'city' => $this->city,
		);
		
		$this->template->view('settings/poea_manager/add', $data);
	}
	
	public function edit($id){
		$query = "select * from poea where poea_id = '{$id}'";
		$poea_details = getdata($query);
		
		if(empty($poea_details)){
			redirect('/settings/poea_manager');
		}
		
		set_header_title("POEA Manager - Edit");

		$this->form_validation->set_rules('inputPOEAName', 'PESO NAME', 'required|edit_unique[poea.name.poea_id.'.$id.']|min_length[2]');
		$this->form_validation->set_message('edit_unique', '{field} already exists.');
		$this->form_validation->set_rules('inputHead', '', 'trim');
		$this->form_validation->set_rules('inputPosition', '', 'trim');
		$this->form_validation->set_rules('inputPhone', '', 'trim|min_length[7]');
		$this->form_validation->set_rules('inputEmail', 'EMAIL ADDRESS', 'trim|valid_email');
		$this->form_validation->set_rules('txtAddress', '', 'trim');
		
		$msg = "";
		$error = FALSE;
		if (isset($_POST['btnEdit'])){
			if ($this->form_validation->run() == FALSE){
				$aError = $this->form_validation->error_array();
				$msg = reset($aError);
				$error = true;
			}else{
				$msg = "save";
				/* update tbl */
				$update_data = array('name' => $_POST['inputPOEAName'],
									'head' => $_POST['inputHead'],
									'position' => $_POST['inputPosition'],
									'address' => $_POST['txtAddress'],
									'city_id' => $_POST['selectCity'],
									'tel' => $_POST['inputPhone'],
									'email' => $_POST['inputEmail'],
				);
		
				if($this->global_model->update('poea', $update_data, array('poea_id' => $id))){
					$msg = TRUE;
		
					/* CREATE LOG */
					create_log($id, 'poea manager', 'edit');
				}else{
					$msg = "an error occured";
					$error = TRUE;
				}
			}
		}
		
		$data = array('msg' => $msg,
				'error' => $error,
				'poea_details' => $poea_details,
				'city' => $this->city,
		);
		
		$this->template->view('settings/poea_manager/edit', $data);
	}

	
	public function delete($id){
		$query = "select * from poea where poea_id = '{$id}'";
		$poea_details = getdata($query);

		if(empty($poea_details)){
			redirect('/settings/poea_manager');
		}else{
			/* delete from db */
			if($this->db->delete('poea', array('poea_id' => $id))){
				/* CREATE LOG */
				create_log($id, 'poea manager', 'delete', $poea_details[0]['name']);

				$error = FALSE;
				$msg = strtoupper($poea_details[0]['name'])." is successfully deleted.";
			}else{
				$error = TRUE;
				$msg = "An error occurred.";
			}

			$this->session->set_flashdata('poea_del_error', $error);
			$this->session->set_flashdata('poea_del_msg', $msg);
			redirect('/settings/poea_manager');
		}
	}
}