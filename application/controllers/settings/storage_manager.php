<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Storage_manager extends My_controller
{
	public function __construct(){
		parent::__construct();

		/** Set title for all function */
		set_header_title("Storage Manager");

		$this->load->helper('form');
		$this->load->library('form_validation');
	}

	public function index(){
		$this->template->set('file_javascript', array('js/settings/storage_manager/datatable.js'));
		$this->template->set('file_css', array('js/plugins/datatables/dataTables.bootstrap.css'));

		$query = $this->db->get('storage_users');
		$data['storage_users'] = $query->result_array();
		$data['msg'] = "";
		$this->template->view('settings/storage_manager/index', $data);
	}

	public function add(){
		set_header_title("Storage Manager - Add");

		$this->form_validation->set_rules('inputUserName', 'USERNAME', 'trim|required|is_unique[storage_users.username]|min_length[3]|alpha_numeric');
		$this->form_validation->set_rules('inputName', 'STORAGE NAME', 'trim|required|is_unique[storage_users.name]|min_length[3]|alpha_numeric');
		$this->form_validation->set_message('is_unique', '{field} already exists.');

		$msg = "";
		$error = FALSE;
		if (isset($_POST['btnAdd'])){
			if ($this->form_validation->run() == FALSE){
				$aError = $this->form_validation->error_array();
				$msg = reset($aError);
				$error = TRUE;
			}else{
				/* insert into db */
				if($new_id = $this->global_model->insert('storage_users', array('name' => $_POST['inputName'], 'username' => $_POST['inputUserName']))){
					/* CREATE LOG */
					create_log($new_id, 'storage manager', 'add');

					$this->session->set_flashdata('add_st_success', 'Storage is successfully added.');
					redirect('/settings/storage_manager/edit/'.$new_id);
				}else{
					$msg = "an error occured";
					$error = TRUE;
				}
				$msg=TRUE;
			}
		}

		$data = array('msg' => $msg,
					'error' => $error,
		);

		$this->template->view('settings/storage_manager/add', $data);
	}

	public function edit($id){
		$query = "select * from storage_users where user_id = '{$id}'";
		$storage_users = getdata($query);

		if(empty($storage_users)){
			redirect('/settings/storage_manager/index');
		}

		set_header_title("Storage Manager - Edit");

		$this->form_validation->set_rules('inputUserName', 'USERNAME', 'trim|required|edit_unique[storage_users.username.user_id.'.$id.']|min_length[3]|alpha_numeric');
		$this->form_validation->set_rules('inputName', 'STORAGE NAME', 'trim|required|edit_unique[storage_users.name.user_id.'.$id.']|min_length[3]|alpha_numeric');
		$this->form_validation->set_message('edit_unique', '{field} already exists.');

		$msg = "";
		$error = FALSE;
		if (isset($_POST['btnEdit'])){
			if ($this->form_validation->run() == FALSE){
				$aError = $this->form_validation->error_array();
				$msg = reset($aError);
				$error = true;
			}else{
				/* update tbl */
				if($this->global_model->update('storage_users', array('name' => $_POST['inputName'], 'username' => $_POST['inputUserName'],), array('user_id' => $id))){
					$msg = TRUE;

					/* CREATE LOG */
					create_log($id, 'storage manager', 'edit');
				}else{
					$msg = "an error occured";
					$error = TRUE;
				}
			}
		}

		$data = array('msg' => $msg,
					'error' => $error,
					'storage_users' => $storage_users,
		);

		$this->template->view('settings/storage_manager/edit', $data);
	}


	public function delete($id){
		$query = "select * from storage_users where user_id = '{$id}'";
		$storage_users = getdata($query);

		if(empty($storage_users)){
			redirect('/settings/storage_manager/index');
		}else{
			/* delete from db */
			if($this->db->delete('storage_users', array('user_id' => $id))){
				/* CREATE LOG */
				create_log($id, 'storage manager', 'delete', $storage_users[0]['name']);

				$error = FALSE;
				$msg = strtoupper($storage_users[0]['name'])." is successfully deleted.";
			}else{
				$error = TRUE;
				$msg = "An error occurred.";
			}

			$this->session->set_flashdata('st_del_error', $error);
			$this->session->set_flashdata('st_del_msg', $msg);
			redirect('/settings/storage_manager');
		}
	}
}