<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class City_manager extends My_controller
{
	public $branches = array();

	public function __construct(){
		parent::__construct();

		/** Set title for all function */
		set_header_title("City Manager");

		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$this->branches = get_options_from_cache('branch');
	}

	public function index(){
		$this->template->set('file_javascript', array('js/settings/city_manager/datatable.js'));
		$this->template->set('file_css', array('js/plugins/datatables/dataTables.bootstrap.css'));

		$query = $this->db->get('city');
		$data['city'] = $query->result_array();
		$data['branches'] = $this->branches;
		$data['msg'] = "";
		$this->template->view('settings/city_manager/index', $data);
	}

	public function add(){
		set_header_title("City Manager - Add");

		$this->form_validation->set_rules('inputName', 'NAME', 'trim|required|is_unique[city.name]|min_length[3]');
		$this->form_validation->set_message('is_unique', '{field} already exists.');
		$this->form_validation->set_rules('inputProvince', '', 'trim');
		$this->form_validation->set_rules('inputZip', 'ZIP CODE', 'trim|numeric');
		$this->form_validation->set_rules('inputRegion', '', 'trim');
		$this->form_validation->set_rules('inputACode', 'AREA CODE', 'trim|numeric');
		$this->form_validation->set_rules('selectBranch', '', 'trim');

		$msg = "";
		$error = FALSE;
		if (isset($_POST['btnAdd'])){
			if ($this->form_validation->run() == FALSE){
				$aError = $this->form_validation->error_array();
				$msg = reset($aError);
				$error = TRUE;
			}else{
				/* insert into db */
				$insert_data = array('name' => $_POST['inputName'],
									'province' => $_POST['inputProvince'],
									'zipcode' => $_POST['inputZip'],
									'region' => $_POST['inputRegion'],
									'area_code' => $_POST['inputACode'],
									'city_branch' => $_POST['selectBranch'],
				);
				if($new_id = $this->global_model->insert('city', $insert_data)){
					/* CREATE LOG */
					create_log($new_id, 'city', 'add');

					$this->session->set_flashdata('add_city_success', 'City is successfully added.');
					redirect('/settings/city_manager/edit/'.$new_id);
				}else{
					$msg = "an error occured";
					$error = TRUE;
				}
			}
		}

		$data = array('msg' => $msg,
					'error' => $error,
					'branches' => $this->branches,
		);

		$this->template->view('settings/city_manager/add', $data);
	}

	public function edit($id){
		$query = "select * from city where city_id = '{$id}'";
		$city = getdata($query);

		if(empty($city)){
			redirect('/settings/city_manager/index');
		}

		set_header_title("City Manager - Edit");

		$this->form_validation->set_rules('inputName', 'NAME', 'trim|required|edit_unique[city.name.city_id.'.$id.']|min_length[3]');
		$this->form_validation->set_message('edit_unique', '{field} already exists.');
		$this->form_validation->set_rules('inputProvince', '', 'trim');
		$this->form_validation->set_rules('inputZip', 'ZIP CODE', 'trim|numeric');
		$this->form_validation->set_rules('inputRegion', '', 'trim');
		$this->form_validation->set_rules('inputACode', 'AREA CODE', 'trim|numeric');
		$this->form_validation->set_rules('selectBranch', '', 'trim');

		$msg = "";
		$error = FALSE;
		if (isset($_POST['btnEdit'])){
			if ($this->form_validation->run() == FALSE){
				$aError = $this->form_validation->error_array();
				$msg = reset($aError);
				$error = true;
			}else{
				/* update tbl */
				$update_data = array('name' => $_POST['inputName'],
									'province' => $_POST['inputProvince'],
									'zipcode' => $_POST['inputZip'],
									'region' => $_POST['inputRegion'],
									'area_code' => $_POST['inputACode'],
									'city_branch' => $_POST['selectBranch'],
						);
				if($this->global_model->update('city', $update_data, array('city_id' => $id))){
					$msg = TRUE;

					/* CREATE LOG */
					create_log($id, 'city', 'edit');
				}else{
					$msg = "an error occured";
					$error = TRUE;
				}
			}
		}

		$data = array('msg' => $msg,
					'error' => $error,
					'city' => $city,
					'branches' => $this->branches,
		);

		$this->template->view('settings/city_manager/edit', $data);
	}


	public function delete($id){
		$query = "select * from city where city_id = '{$id}'";
		$city = getdata($query);

		if(empty($city)){
			redirect('/settings/city_manager/index');
		}else{
			/* delete from db */
			if($this->db->delete('city', array('city_id' => $id))){
				/* CREATE LOG */
				create_log($id, 'city', 'delete', $city[0]['name']);

				$error = FALSE;
				$msg = strtoupper($city[0]['name'])." is successfully deleted.";
			}else{
				$error = TRUE;
				$msg = "An error occurred.";
			}

			$this->session->set_flashdata('city_del_error', $error);
			$this->session->set_flashdata('city_del_msg', $msg);
			redirect('/settings/city_manager');
		}
	}
}