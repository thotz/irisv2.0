<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Standard_category_manager extends My_controller
{
	public $gen_category_sub = array();
	public $gen_category = array();
	public $default_category_sub = array(''=>'Select Category', 'Professional'=>'Professional', 'Non-Professional'=>'Non-Professional');

	public function __construct(){
		parent::__construct();

		$this->load->helper('form');
		$this->load->library('form_validation');

		/*GET GENERAL CATEGORY/GENERAL SUB CATEGORY*/
		$gen_category_main[''] = "Please Select"; /*DEFAULT*/
		$query = $this->db->get('gen_category');
		foreach ($query->result_array() as $gc_val){
			$gen_category_main[$gc_val['gen_category_id']] = $gc_val['name'];
			$gen_category_sub[$gc_val['gen_category_id']][''] = "Please Select"; /*DEFAULT*/

			if($gc_val['type1'] != ''){
				$gen_category_sub[$gc_val['gen_category_id']][$gc_val['type1']] = $gc_val['type1'];
			}
			
			if($gc_val['type2'] != ''){
				$gen_category_sub[$gc_val['gen_category_id']][$gc_val['type2']] = $gc_val['type2'];
			}
		}

		$this->gen_category_sub = $gen_category_sub;
		$this->gen_category = $gen_category_main;
		/*END GET GENERAL CATEGORY/GENERAL SUB CATEGORY*/
	}

	public function index(){
		$this->template->set('file_javascript', array('js/settings/standard_category_manager/datatable.js'));
		$this->template->set('file_css', array('js/plugins/datatables/dataTables.bootstrap.css'));

		$query = $this->db->get('job_standard');
		$data['job_standard'] = $query->result_array();
		$data['msg'] = "";
		$data['gen_category'] = $this->gen_category;
		$this->template->view('settings/standard_category_manager/index', $data);
	}

	public function add(){
		$this->template->set('file_javascript', array('js/settings/standard_category_manager/ajax.js'));
		set_header_title("Standard Category Manager - Add");

		$this->form_validation->set_rules('inputName', 'NAME', 'trim|required|is_unique[job_standard.name]|min_length[3]');
		$this->form_validation->set_message('is_unique', '{field} already exists.');
		$this->form_validation->set_rules('selectGenCat', 'GENERAL CATEGORY', 'trim|required');
		
		$msg = "";
		$error = FALSE;
		if (isset($_POST['btnAdd'])){
			if ($this->form_validation->run() == FALSE){
				$aError = $this->form_validation->error_array();
				$msg = reset($aError);
				$error = TRUE;
			}else{
				/* insert into db */
				$insert_data = array('name' => $_POST['inputName'], 'genfield' => $_POST['selectGenCat'], 'postype' => $_POST['selectCat2'],);
				if($new_id = $this->global_model->insert('job_standard', $insert_data)){
					/* CREATE LOG */
					create_log($new_id, 'job_standard', 'add');

					$this->session->set_flashdata('add_stdcat_success', 'Standard Category is successfully added.');
					redirect('/settings/standard_category_manager/edit/'.$new_id);
				}else{
					$msg = "an error occured";
					$error = TRUE;
				}
			}
		}

		$data = array('msg' => $msg,
					'error' => $error,
					'gen_category' => $this->gen_category,
		);

		$this->template->view('settings/standard_category_manager/add', $data);
	}

	public function edit($id){
		$query = "select * from job_standard where job_standard_id = '{$id}'";
		$job_standard = getdata($query);

		if(empty($job_standard)){
			redirect('/settings/standard_category_manager/index');
		}

		set_header_title("Standard Category Manager - Edit");

		$this->form_validation->set_rules('inputName', 'NAME', 'trim|required|edit_unique[job_standard.name.job_standard_id.'.$id.']|min_length[3]');
		$this->form_validation->set_message('edit_unique', '{field} already exists.');
		$this->form_validation->set_rules('selectGenCat', 'GENERAL CATEGORY', 'trim|required');

		$msg = "";
		$error = FALSE;
		if (isset($_POST['btnEdit'])){
			if ($this->form_validation->run() == FALSE){
				$aError = $this->form_validation->error_array();
				$msg = reset($aError);
				$error = true;
			}else{
				/* update tbl */
				$update_data = array('name' => $_POST['inputName'], 'genfield' => $_POST['selectGenCat'], 'postype' => $_POST['selectCat2'],);
				if($this->global_model->update('job_standard', $update_data, array('job_standard_id' => $id))){
					$msg = TRUE;

					/* CREATE LOG */
					create_log($id, 'job_standard', 'edit');
				}else{
					$msg = "an error occured";
					$error = TRUE;
				}
			}
		}

		$data = array('msg' => $msg,
					'error' => $error,
					'gen_category' => $this->gen_category,
					'default_category_sub' => $this->default_category_sub,
					'job_standard' => $job_standard
		);

		$this->template->view('settings/standard_category_manager/edit', $data);
	}


	public function delete($id){
		$query = "select * from job_standard where job_standard_id = '{$id}'";
		$job_standard = getdata($query);

		if(empty($job_standard)){
			redirect('/settings/standard_category_manager/index');
		}else{
			/* delete from db */
			if($this->db->delete('job_standard', array('job_standard_id' => $id))){
				/* CREATE LOG */
				create_log($id, 'job_standard', 'delete', $job_standard[0]['name']);

				$error = FALSE;
				$msg = strtoupper($job_standard[0]['name'])." is successfully deleted.";
			}else{
				$error = TRUE;
				$msg = "An error occurred.";
			}

			$this->session->set_flashdata('stdcat_del_error', $error);
			$this->session->set_flashdata('stdcat_del_msg', $msg);
			redirect('/settings/standard_category_manager');
		}
	}

	/**
	 * get subcategory of gen category;
	 */
	public function ajax_get_sub_cat(){
// 		if(isset($this->gen_category_sub[$_GET['gen_cat']]['sub_cat'])){
// // 			echo json_encode($this->gen_category_sub[$_GET['gen_cat']]['sub_cat']);
// 			$aSubcat = array();
// 			foreach ($this->gen_category_sub[$_GET['gen_cat']]['sub_cat'] as $val){
// 				if($val != ''){
// 					array_push($aSubcat, $val);					
// 				}
// 			}
// 		}
		
// 		echo json_encode($aSubcat);
		$html = "";
		if(isset($this->gen_category_sub[$_GET['gen_cat']])){
			foreach ($this->gen_category_sub[$_GET['gen_cat']] as $val){
				$html .= "<option value=\"".$val."\">".$val."</option>";				
			}
		}
		
		echo $html;
		exit;
	}
}