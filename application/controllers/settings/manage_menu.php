<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Manage_menu extends My_controller {

	public function __construct(){
		parent::__construct();
		
		/** Load model	*/
		$this->load->model('settings/manage_menu_model', 'manage_menu', true);
		
		/** Set title for all function */
		set_header_title("Manage Menu");
	}
	
	public function index(){
		$this->template->view('settings/manage_menu/index');
	}
	
	public function edit_menu(){
		if (isset($_POST['clinic'])) {	
			// Add to database
			$result = $this->manage_menu->edit_menu();
			
			// Redirect
			if ($result) {
				$msg = "Updated";
			} else {
				$msg = "Error Updating";
			}
			redirect(base_url().'settings/manage_menu/?msg='.$msg);		
		} else {
			$menu_high_id = isset($_GET['menu_high_id']) ? $_GET['menu_high_id'] : 0;
			$view_data = $this->manage_menu->get_menu_data($menu_high_id);
			$this->template->view('settings/manage_menu/edit_menu', $view_data);
		}
	}
	
	public function add_menu(){
		if (isset($_POST['clinic'])) {
			/** Add to database */
			$result = $this->manage_menu->add_menu();
			
			/** Redirect */
			if ($result) redirect(base_url().'settings/manage_menu/');
			
		} else {
			$menu_high_id = isset($_GET['menu_high_id']) ? $_GET['menu_high_id'] : 0;
			$view_data = $this->manage_menu->get_menu_data($menu_high_id);
			$this->template->view('settings/manage_menu/add_menu', $view_data);
		}
	}
	
	public function edit_submenu(){
		if (isset($_POST['clinic'])) {
			/** Add to database */
			$result = $this->manage_menu->edit_submenu();
			
			/** Redirect */
			if ($result) {
				redirect(base_url().'settings/manage_menu/add_submenu?msg=Updated&menu_high_id='.$_POST['menu_high_id']);
			} else {
				redirect(base_url().'settings/manage_menu/add_submenu?msg=Error Updating&menu_high_id='.$_POST['menu_high_id']);
			}
		} else {
			$menu_high_id = isset($_GET['menu_high_id']) ? $_GET['menu_high_id'] : 0;
			$menu_low_id = isset($_GET['menu_low_id']) ? $_GET['menu_low_id'] : '';
	
			if ($menu_high_id) {
				$view_data = $this->manage_menu->get_submenu_data($menu_high_id, $menu_low_id);
								
				$this->template->view('settings/manage_menu/edit_submenu', $view_data);
			}
		}
	}
	
	public function add_submenu($menu_high_id=""){
		/** Show if the msg has a value. */
		show_message_alert();

		if (isset($_POST['clinic'])) {		
			/** Add to database */
			$result = $this->manage_menu->add_submenu();
			
			/** Redirect */
			if ($result) redirect(base_url().'settings/manage_menu/add_submenu?menu_high_id='.$_POST['menu_high_id']);
			
		} else {
			$menu_high_id = isset($_GET['menu_high_id']) ? $_GET['menu_high_id'] : 0;
			$menu_low_id = isset($_GET['menu_low_id']) ? $_GET['menu_low_id'] : '';

			if ($menu_high_id) {
				$view_data = $this->manage_menu->get_submenu_data($menu_high_id, $menu_low_id);
								
				$this->template->view('settings/manage_menu/add_submenu', $view_data);
			}
		}
	}
}

/* End of file manage_menu.php */
/* Location: ./application/controllers/manage_menu.php */