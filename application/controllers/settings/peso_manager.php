<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Peso_manager extends My_controller
{
	public $city = "";

	public function __construct(){
		parent::__construct();
		
		/** Set title for all function */
		set_header_title("PESO Office Manager");

		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$this->city = get_options_from_cache('city');
	}

	public function index(){
		$this->template->set('file_javascript', array('js/settings/peso_manager/datatable.js'));
		$this->template->set('file_css', array('js/plugins/datatables/dataTables.bootstrap.css'));

		$query = $this->db->get('peso');
		$data['peso_list'] = $query->result_array();
		$data['msg'] = "";
		$data['city'] = $this->city;

		$this->template->view('settings/peso_manager/index', $data);
	}
	
	public function add(){
		set_header_title("PESO Office Manager - Add");
// 		$this->load->model('reception/create_applicant_model', 'create_applicant', true);

		$this->form_validation->set_rules('radStatus', 'STATUS', 'trim');
		$this->form_validation->set_rules('inputPESOName', 'PESO NAME', 'required|is_unique[peso.name]|min_length[2]');
		$this->form_validation->set_message('is_unique', '{field} already exists.');
		$this->form_validation->set_rules('txtAddress', 'ADDRESS', 'trim|required');
		$this->form_validation->set_rules('selectCity', 'CITY', 'required');
		$this->form_validation->set_rules('inputPhone', 'PHONE NO.', 'trim|min_length[7]');

		$msg = "";
		$error = FALSE;
		if (isset($_POST['btnAdd'])){
			if ($this->form_validation->run() == FALSE){
				$aError = $this->form_validation->error_array();
				$msg = reset($aError);
				$error = TRUE;
			}else{
				/* insert into db */
				$insert_data = array('name' => $_POST['inputPESOName'],
									'streetaddress' => $_POST['txtAddress'],
									'city_id' => $_POST['selectCity'],
									'tel' => $_POST['inputPhone'],
									'active' => $_POST['radStatus'],
				);
		
				if($new_id = $this->global_model->insert('peso', $insert_data)){
					/* CREATE LOG */
					create_log($new_id, 'peso manager', 'add');

					$this->session->set_flashdata('add_peso_success', 'PESO Office is successfully added.');
					redirect('/settings/peso_manager/edit/'.$new_id);
				}else{
					$msg = "an error occured";
					$error = TRUE;
				}
			}
		}

// 		$res_municipality = $this->create_applicant->get_municipality('get_select_option', (isset($_POST['selectCity']))?$_POST['selectCity']:"");
		$data = array('msg' => $msg,
						'error' => $error,
						'city' => $this->city,
		);
		
		$this->template->view('settings/peso_manager/add', $data);
	}
	
	public function edit($id){
		$query = "select * from peso where peso_id = '{$id}'";
		$peso_details = getdata($query);
		
		if(empty($peso_details)){
			redirect('/settings/peso_manager');
		}
		
		set_header_title("PESO Office Manager - Edit");
// 		$this->load->model('reception/create_applicant_model', 'create_applicant', true);

		$this->form_validation->set_rules('radStatus', 'STATUS', 'trim');
		$this->form_validation->set_rules('inputPESOName', 'PESO NAME', 'required|edit_unique[peso.name.peso_id.'.$id.']|min_length[2]');
		$this->form_validation->set_message('edit_unique', '{field} already exists.');
		$this->form_validation->set_rules('txtAddress', 'ADDRESS', 'trim|required');
		$this->form_validation->set_rules('selectCity', 'CITY', 'required');
		$this->form_validation->set_rules('inputPhone', 'PHONE NO.', 'trim|min_length[7]');
		
		$msg = "";
		$error = FALSE;
		if (isset($_POST['btnEdit'])){
			if ($this->form_validation->run() == FALSE){
				$aError = $this->form_validation->error_array();
				$msg = reset($aError);
				$error = true;
			}else{
				$msg = "save";
				/* update tbl */
				$update_data = array('name' => $_POST['inputPESOName'],
									'streetaddress' => $_POST['txtAddress'],
									'city_id' => $_POST['selectCity'],
									'tel' => $_POST['inputPhone'],
									'active' => $_POST['radStatus'],
				);
		
				if($this->global_model->update('peso', $update_data, array('peso_id' => $id))){
					$msg = TRUE;
		
					/* CREATE LOG */
					create_log($id, 'peso manager', 'edit');
				}else{
					$msg = "an error occured";
					$error = TRUE;
				}
			}
		}
		
// 		$res_municipality = $this->create_applicant->get_municipality('get_select_option', $peso_details[0]['city_id']);
		$data = array('msg' => $msg,
				'error' => $error,
				'peso_details' => $peso_details,
				'city' => $this->city,
		);
		
		$this->template->view('settings/peso_manager/edit', $data);
	}

	
	public function delete($id){
		$query = "select * from peso where peso_id = '{$id}'";
		$peso_details = getdata($query);

		if(empty($peso_details)){
			redirect('/settings/peso_manager');
		}else{
			/* delete from db */
			if($this->db->delete('peso', array('peso_id' => $id))){
				/* CREATE LOG */
				create_log($id, 'peso manager', 'delete', $peso_details[0]['name']);

				$error = FALSE;
				$msg = strtoupper($peso_details[0]['name'])." is successfully deleted.";
			}else{
				$error = TRUE;
				$msg = "An error occurred.";
			}

			$this->session->set_flashdata('peso_del_error', $error);
			$this->session->set_flashdata('peso_del_msg', $msg);
			redirect('/settings/peso_manager');
		}
	}
}