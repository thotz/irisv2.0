<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Job_specialization_manager extends My_controller
{
	public function __construct(){
		parent::__construct();

		/** Set title for all function */
		set_header_title("Job Specialization");

		$this->load->helper('form');
		$this->load->library('form_validation');
	}

	public function index(){
		$this->template->set('file_javascript', array('js/settings/job_specialization_manager/datatable.js'));
		$this->template->set('file_css', array('js/plugins/datatables/dataTables.bootstrap.css'));

		$query = $this->db->get('job_spec');
		$data['job_spec'] = $query->result_array();
		$data['msg'] = "";
		$this->template->view('settings/job_specialization_manager/index', $data);
	}

	public function add(){
		set_header_title("Job Specialization - Add");

		$this->form_validation->set_rules('inputName', 'NAME', 'trim|required|is_unique[job_spec.name]|min_length[3]');
		$this->form_validation->set_message('is_unique', '{field} already exists.');

		$msg = "";
		$error = FALSE;
		if (isset($_POST['btnAdd'])){
			if ($this->form_validation->run() == FALSE){
				$aError = $this->form_validation->error_array();
				$msg = reset($aError);
				$error = TRUE;
			}else{
				/* insert into db */
				if($new_id = $this->global_model->insert('job_spec', array('name' => $_POST['inputName']))){
					/* CREATE LOG */
					create_log($new_id, 'job specialization', 'add');

					$this->session->set_flashdata('add_job_success', 'Job Specialization is successfully added.');
					redirect('/settings/job_specialization_manager/edit/'.$new_id);
				}else{
					$msg = "an error occured";
					$error = TRUE;
				}
			}
		}

		$data = array('msg' => $msg,
					'error' => $error,
		);

		$this->template->view('settings/job_specialization_manager/add', $data);
	}

	public function edit($id){
		$query = "select * from job_spec where jobspec_id = '{$id}'";
		$job_spec = getdata($query);

		if(empty($job_spec)){
			redirect('/settings/job_specialization_manager/index');
		}

		set_header_title("Job Specialization - Edit");

		$this->form_validation->set_rules('inputName', 'NAME', 'trim|required|edit_unique[job_spec.name.jobspec_id.'.$id.']|min_length[3]');
		$this->form_validation->set_message('edit_unique', '{field} already exists.');

		$msg = "";
		$error = FALSE;
		if (isset($_POST['btnEdit'])){
			if ($this->form_validation->run() == FALSE){
				$aError = $this->form_validation->error_array();
				$msg = reset($aError);
				$error = true;
			}else{
				/* update tbl */
				if($this->global_model->update('job_spec', array('name' => $_POST['inputName']), array('jobspec_id' => $id))){
					$msg = TRUE;

					/* CREATE LOG */
					create_log($id, 'job specialization', 'edit');
				}else{
					$msg = "an error occured";
					$error = TRUE;
				}
			}
		}

		$data = array('msg' => $msg,
					'error' => $error,
					'job_spec' => $job_spec,
		);

		$this->template->view('settings/job_specialization_manager/edit', $data);
	}


	public function delete($id){
		$query = "select * from job_spec where jobspec_id = '{$id}'";
		$job_spec = getdata($query);

		if(empty($job_spec)){
			redirect('/settings/job_specialization_manager/index');
		}else{
			/* delete from db */
			if($this->db->delete('job_spec', array('jobspec_id' => $id))){
				/* CREATE LOG */
				create_log($id, 'job specialization', 'delete', $job_spec[0]['name']);

				$error = FALSE;
				$msg = strtoupper($job_spec[0]['name'])." is successfully deleted.";
			}else{
				$error = TRUE;
				$msg = "An error occurred.";
			}

			$this->session->set_flashdata('job_del_error', $error);
			$this->session->set_flashdata('job_del_msg', $msg);
			redirect('/settings/job_specialization_manager');
		}
	}
}