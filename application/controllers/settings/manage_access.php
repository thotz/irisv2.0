<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Manage_access extends CI_controller {

	public function __construct(){
		parent::__construct();
		
		/** Load model	*/
		$this->load->model('settings/manage_access_model', 'manage_access', true);
		
		/** Set title for all function */
		set_header_title("Access Level Manager");
	}
	
	public function index(){
		$this->template->view('settings/manage_access/index');
	}
	

}