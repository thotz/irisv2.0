<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Airline_manager extends My_controller
{
// 	public $city = "";

	public function __construct(){
		parent::__construct();
		
		/** Set title for all function */
		set_header_title("Airline Manager");

		$this->load->helper('form');
		$this->load->library('form_validation');
		
// 		$this->city = get_options_from_cache('city');
	}

	public function index(){
		$this->template->set('file_javascript', array('js/settings/airline_manager/datatable.js'));
		$this->template->set('file_css', array('js/plugins/datatables/dataTables.bootstrap.css'));

		$query = $this->db->get('airline');
		$data['airline_list'] = $query->result_array();
		$data['msg'] = "";

		$this->template->view('settings/airline_manager/index', $data);
	}
	
	public function add(){
		set_header_title("Airline Manager - Add");

		$this->form_validation->set_rules('inputAirName', 'AIRLINE NAME', 'trim|required|is_unique[airline.name]|min_length[2]');
		$this->form_validation->set_rules('inputCode', 'CODE', 'trim|required|is_unique[airline.code]|min_length[2]');
		$this->form_validation->set_message('is_unique', '{field} already exists.');
		$this->form_validation->set_rules('txtAddress', 'ADDRESS', 'trim|required');

		$msg = "";
		$error = FALSE;
		if (isset($_POST['btnAdd'])){
			if ($this->form_validation->run() == FALSE){
				$aError = $this->form_validation->error_array();
				$msg = reset($aError);
				$error = TRUE;
			}else{
				/* insert into db */
				$insert_data = array('name' => $_POST['inputAirName'],
									'address' => $_POST['txtAddress'],
									'code' => strtoupper($_POST['inputCode']),
				);
		
				if($new_id = $this->global_model->insert('airline', $insert_data)){
					/* CREATE LOG */
					create_log($new_id, 'airline manager', 'add');

					$this->session->set_flashdata('add_air_success', 'Airline is successfully added.');
					redirect('/settings/airline_manager/edit/'.$new_id);
				}else{
					$msg = "an error occured";
					$error = TRUE;
				}
				$msg = true;
			}
		}

		$data = array('msg' => $msg,
						'error' => $error,
		);
		
		$this->template->view('settings/airline_manager/add', $data);
	}

	public function edit($id){
		$query = "select * from airline where airline_id = '{$id}'";
		$airline_details = getdata($query);
		
		if(empty($airline_details)){
			redirect('/settings/airline_manager');
		}
		
		set_header_title("Airline Manager - Edit");

		$this->form_validation->set_rules('inputAirName', 'AIRLINE NAME', 'trim|required|edit_unique[airline.name.airline_id.'.$id.']|min_length[2]');
		$this->form_validation->set_rules('inputCode', 'CODE', 'trim|required|edit_unique[airline.code.airline_id.'.$id.']|min_length[2]');
		$this->form_validation->set_message('edit_unique', '{field} already exists.');
		$this->form_validation->set_rules('txtAddress', 'ADDRESS', 'trim|required');
		
		$msg = "";
		$error = FALSE;
		if (isset($_POST['btnEdit'])){
			if ($this->form_validation->run() == FALSE){
				$aError = $this->form_validation->error_array();
				$msg = reset($aError);
				$error = true;
			}else{
				/* update tbl */
				$update_data = array('name' => $_POST['inputAirName'],
									'address' => $_POST['txtAddress'],
									'code' => strtoupper($_POST['inputCode']),
				);
		
				if($this->global_model->update('airline', $update_data, array('airline_id' => $id))){
					$msg = TRUE;
		
					/* CREATE LOG */
					create_log($id, 'airline manager', 'edit');
				}else{
					$msg = "an error occured";
					$error = TRUE;
				}
			}
		}
		
		$data = array('msg' => $msg,
				'error' => $error,
				'airline_details' => $airline_details,
		);
		
		$this->template->view('settings/airline_manager/edit', $data);
	}

	
	public function delete($id){
		$query = "select * from airline where airline_id = '{$id}'";
		$airline_details = getdata($query);

		if(empty($airline_details)){
			redirect('/settings/airline_manager');
		}else{
			/* delete from db */
			if($this->db->delete('airline', array('airline_id' => $id))){
				/* CREATE LOG */
				create_log($id, 'airline manager', 'delete', $airline_details[0]['name']);

				$error = FALSE;
				$msg = strtoupper($airline_details[0]['name'])." is successfully deleted.";
			}else{
				$error = TRUE;
				$msg = "An error occurred.";
			}

			$this->session->set_flashdata('air_del_error', $error);
			$this->session->set_flashdata('air_del_msg', $msg);
			redirect('/settings/airline_manager');
		}
	}
}