<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Access_level_manager extends My_controller
{
	public $html_breaks = array("<br />","<br>","<br/>","<br />","&lt;br /&gt;","&lt;br/&gt;","&lt;br&gt;");
	public $default_msg_update = "Access is successfully updated.";

	public function __construct(){
		parent::__construct();

		$this->load->model('settings/manage_access_model', 'access', TRUE);
		$this->load->helper('string');
		$this->load->helper('form');
		$this->load->library('form_validation');

		/** Set title for all function */
		set_header_title("Access Level Manager");
	}

	public function index(){
		$this->template->set('file_javascript', array('js/settings/access_level_manager/datatable.js'));
		$this->template->set('file_css', array('js/plugins/datatables/dataTables.bootstrap.css'));

		$data['all_access'] = $this->access->get_access();
		$this->template->view('settings/access_level_manager/index', $data);
	}
	
	public function add(){
		/** Set title for all function */
		set_header_title("Access Level Manager - Add");

		$this->form_validation->set_rules('inputAccessName', 'Access Name', 'trim|required|is_unique[access.name]');
		$this->form_validation->set_rules('inputCodeName', 'Access Code', 'trim|required|is_unique[access.codename]');
		$this->form_validation->set_message('is_unique', '{field} already exists.');

		$msg = "";
		$error = FALSE;
		if (isset($_POST['btnAdd'])){
			if ($this->form_validation->run() == FALSE){
				$aError = $this->form_validation->error_array();
				$msg = reset($aError);
				$error = TRUE;
			}else{
				$insert_data = array('name' => $_POST['inputAccessName'],
									'codename' => $_POST['inputCodeName']);
				if($new_access_id = $this->global_model->insert('access', $insert_data)){
					/* DELETE ACCESS CACHE */
					$this->cache->delete('access');

					/* CREATE LOG */
					create_log($new_access_id, 'access level manager', 'add');

 					$this->session->set_flashdata('add_acc_success', 'Access is successfully added.');
 					redirect('/settings/access_level_manager/edit/'.$new_access_id);
				}else{
 					$msg = "an error occured";
 					$error = TRUE;
				}
			}
		}

		$data['msg'] = $msg;
		$data['error'] = $error;
// 		$data['breaks'] = $this->html_breaks;
// 		$data['menu_all'] = $this->access->get_menu();
		$this->template->view('settings/access_level_manager/add', $data);
	}

	public function edit($access_id){
		/** Set title for all function */
		set_header_title("Access Level Manager - Edit");

		$this->template->set('file_javascript', array('js/settings/access_level_manager/access.js'));

		$this->form_validation->set_rules('inputAccessName', 'Access Name', 'trim|required|callback__is_unique2[access.name.access_id.'.$access_id.']');
		$this->form_validation->set_rules('inputCodeName', 'Access Code', 'trim|required|callback__is_unique2[access.codename.access_id.'.$access_id.']');
		$this->form_validation->set_message('_is_unique2', '{field} already exists.');

		$msg = "";
		$error = FALSE;
		if (isset($_POST['btnEdit'])){
			if ($this->form_validation->run() == FALSE){
				$aError = $this->form_validation->error_array();
				$msg = reset($aError);
				$error = TRUE;
			}else{
				/* update record */
				$update_data = array('name' => $_POST['inputAccessName'],
									'codename' => $_POST['inputCodeName']);
				if($this->global_model->update('access', $update_data, array('access_id' => $access_id))){
					$msg = TRUE;

					/* CREATE LOG */
					create_log($access_id, 'access level manager', 'edit');
				}else{
 					$msg = "an error occured";
 					$error = TRUE;					
				}
			}
		}

		$data['all_access'] = $this->access->get_access();
		$data['access_id'] = $access_id;
		$data['menu_all'] = $this->access->get_menu();
		$data['msg'] = $msg;
		$data['error'] = $error;
		$data['breaks'] = $this->html_breaks;
		$data['default_msg'] = $this->default_msg_update;

		$this->template->view('settings/access_level_manager/edit', $data);
	}

	public function _is_unique2($input, $field) {
		return $this->global_model->check_duplicate_in_tbl($input.".".$field);
	}

	public function ajax_update_access(){
		list($access_id, $menu_id, $access) = explode("|", urldecode($_GET['menu_id']));

		if($_GET['what'] == 'mod'){
			$field = "access_id_mod";
		}else if($_GET['what'] == 'del'){
			$field = "access_id_del";
		}

		if($_GET['action'] == 'add'){
			/* ADD ACCESS */
			$this->global_model->update('menu_low', array('access_id' => $access.'"'.$access_id.'",'), array('menu_low_id' => $menu_id));
		}else if($_GET['action'] == 'remove'){
			/* REMOVE ACCESS */
			$new_access = str_ireplace('"'.$access_id.'",', '', $access);
			$this->global_model->update('menu_low', array('access_id' => $new_access), array('menu_low_id' => $menu_id));
		}else if($_GET['action'] == 'opt_add'){
			/* ADD ACCESS - MODIFY/DELETE OPTION */
			$this->global_model->update('menu_low', array($field => $access.'"'.$access_id.'",'), array('menu_low_id' => $menu_id));
		}else if($_GET['action'] == 'opt_remove'){
			/* REMOVE ACCESS - MODIFY/DELETE OPTION */
			$new_access = str_ireplace('"'.$access_id.'",', '', $access);
			$this->global_model->update('menu_low', array($field => $new_access), array('menu_low_id' => $menu_id));
		}

		/* CLEAR ACCESS MENU CACHE */
		$this->cache->delete(HTTP_HOST.'menu_user_access_'.$access_id);

		return TRUE;
	}

	public function delete($access_id){
		$del_result = $this->access->delete($access_id);

		if($del_result['error'] == FALSE){
			/* CREATE LOG */
			create_log($access_id, 'access level manager', 'delete');
		}

		$this->session->set_flashdata('del_error', $del_result['error']);
		$this->session->set_flashdata('del_msg', $del_result['msg']);
		redirect('/settings/access_level_manager');
	}
}