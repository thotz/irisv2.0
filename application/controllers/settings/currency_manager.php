<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Currency_manager extends My_controller
{
	public function __construct(){
		parent::__construct();

		/** Set title for all function */
		set_header_title("Currency Manager");

		$this->load->helper('form');
		$this->load->library('form_validation');
	}

	public function index(){
		$this->template->set('file_javascript', array('js/settings/currency_manager/datatable.js'));
		$this->template->set('file_css', array('js/plugins/datatables/dataTables.bootstrap.css'));

		$query = $this->db->get('currency');
		$data['currency'] = $query->result_array();
		$data['msg'] = "";
		$this->template->view('settings/currency_manager/index', $data);
	}

	public function add(){
		set_header_title("Currency Manager - Add");

		$this->form_validation->set_rules('inputName', 'CURRENCY NAME', 'trim|required|is_unique[currency.name]|min_length[3]');
		$this->form_validation->set_rules('inputCode', 'CURRENCY CODE', 'trim|required|is_unique[currency.code]|min_length[2]|max_length[10]');
		$this->form_validation->set_message('is_unique', '{field} already exists.');

		$msg = "";
		$error = FALSE;
		if (isset($_POST['btnAdd'])){
			if ($this->form_validation->run() == FALSE){
				$aError = $this->form_validation->error_array();
				$msg = reset($aError);
				$error = TRUE;
			}else{
				/* insert into db */
				if($new_id = $this->global_model->insert('currency', array('name' => $_POST['inputName'], 'code' => strtoupper($_POST['inputCode'])))){
					/* CREATE LOG */
					create_log($new_id, 'currency', 'add');

					/* DELETE CURRENCY CACHE */
					$this->cache->delete('currency');

					$this->session->set_flashdata('add_cu_success', 'Currency is successfully added.');
					redirect('/settings/currency_manager/edit/'.$new_id);
				}else{
					$msg = "an error occured";
					$error = TRUE;
				}
			}
		}

		$data = array('msg' => $msg,
					'error' => $error,
		);

		$this->template->view('settings/currency_manager/add', $data);
	}

	public function edit($id){
		$query = "select * from currency where currency_id = '{$id}'";
		$currency = getdata($query);

		if(empty($currency)){
			redirect('/settings/currency_manager/index');
		}

		set_header_title("Currency Manager - Edit");

		$this->form_validation->set_rules('inputName', 'CURRENCY NAME', 'trim|required|edit_unique[currency.name.currency_id.'.$id.']|min_length[3]');
		$this->form_validation->set_rules('inputCode', 'CURRENCY CODE', 'trim|required|edit_unique[currency.code.currency_id.'.$id.']|min_length[2]|max_length[10]');
		$this->form_validation->set_message('edit_unique', '{field} already exists.');

		$msg = "";
		$error = FALSE;
		if (isset($_POST['btnEdit'])){
			if ($this->form_validation->run() == FALSE){
				$aError = $this->form_validation->error_array();
				$msg = reset($aError);
				$error = true;
			}else{
				/* update tbl */
				if($this->global_model->update('currency', array('name' => $_POST['inputName'], 'code' => strtoupper($_POST['inputCode'])), array('currency_id' => $id))){
					$msg = TRUE;

					/* CREATE LOG */
					create_log($id, 'currency', 'edit');

					/* DELETE CURRENCY CACHE */
					$this->cache->delete('currency');
				}else{
					$msg = "an error occured";
					$error = TRUE;
				}
			}
		}

		$data = array('msg' => $msg,
					'error' => $error,
					'currency' => $currency,
		);

		$this->template->view('settings/currency_manager/edit', $data);
	}


	public function delete($id){
		$query = "select * from currency where currency_id = '{$id}'";
		$currency = getdata($query);

		if(empty($currency)){
			redirect('/settings/currency_manager/index');
		}else{
			/* delete from db */
			if($this->db->delete('currency', array('currency_id' => $id))){
				/* CREATE LOG */
				create_log($id, 'currency', 'delete', $currency[0]['name']);

				/* DELETE CURRENCY CACHE */
				$this->cache->delete('currency');

				$error = FALSE;
				$msg = strtoupper($currency[0]['name'])." is successfully deleted.";
			}else{
				$error = TRUE;
				$msg = "An error occurred.";
			}

			$this->session->set_flashdata('cu_del_error', $error);
			$this->session->set_flashdata('cu_del_msg', $msg);
			redirect('/settings/currency_manager');
		}
	}
}