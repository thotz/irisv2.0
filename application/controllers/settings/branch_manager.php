<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Branch_manager extends My_controller
{
	public function __construct(){
		parent::__construct();

		/** Set title for all function */
		set_header_title("Branch Manager");

		$this->load->helper('form');
		$this->load->library('form_validation');
	}

	public function index(){
		$this->template->set('file_javascript', array('js/settings/branch_manager/datatable.js'));
		$this->template->set('file_css', array('js/plugins/datatables/dataTables.bootstrap.css'));

		$query = $this->db->get('branch');
		$data['branch'] = $query->result_array();
		$data['msg'] = "";
		$this->template->view('settings/branch_manager/index', $data);
	}

	public function add(){
		set_header_title("Branch Manager - Add");

		$this->form_validation->set_rules('inputBrName', 'BRANCH NAME', 'trim|required|is_unique[branch.description]|min_length[4]');
		$this->form_validation->set_rules('inputCode', 'BRANCH CODE', 'trim|required|is_unique[branch.description]|min_length[3]');
		$this->form_validation->set_message('is_unique', '{field} already exists.');

		$msg = "";
		$error = FALSE;
		if (isset($_POST['btnAdd'])){
			if ($this->form_validation->run() == FALSE){
				$aError = $this->form_validation->error_array();
				$msg = reset($aError);
				$error = TRUE;
			}else{
				/* insert into db */
				if($new_id = $this->global_model->insert('branch', array('description' => strtoupper($_POST['inputBrName']), 'name' => strtoupper($_POST['inputCode'])))){
					/* CREATE LOG */
					create_log($new_id, 'branch', 'add');

					/* DELETE BRANCH CACHE */
					$this->cache->delete('branch');

					$this->session->set_flashdata('add_branch_success', 'Branch is successfully added.');
					redirect('/settings/branch_manager/edit/'.$new_id);
				}else{
					$msg = "an error occured";
					$error = TRUE;
				}
			}
		}

		$data = array('msg' => $msg,
					'error' => $error,
		);

		$this->template->view('settings/branch_manager/add', $data);
	}

	public function edit($id){
		$query = "select * from branch where id = '{$id}'";
		$branch = getdata($query);

		if(empty($branch)){
			redirect('/settings/branch_manager/index');
		}

		set_header_title("Branch Manager - Edit");

		$this->form_validation->set_rules('inputBrName', 'BRANCH NAME', 'trim|required|edit_unique[branch.description.id.'.$id.']|min_length[4]');
		$this->form_validation->set_rules('inputCode', 'BRANCH CODE', 'trim|required|edit_unique[branch.name.id.'.$id.']|min_length[3]');
		$this->form_validation->set_message('edit_unique', '{field} already exists.');

		$msg = "";
		$error = FALSE;
		if (isset($_POST['btnEdit'])){
			if ($this->form_validation->run() == FALSE){
				$aError = $this->form_validation->error_array();
				$msg = reset($aError);
				$error = true;
			}else{
				/* update tbl */
				if($this->global_model->update('branch', array('description' => strtoupper($_POST['inputBrName']), 'name' => strtoupper($_POST['inputCode'])), array('id' => $id))){
					$msg = TRUE;

					/* CREATE LOG */
					create_log($id, 'branch', 'edit');

					/* DELETE BRANCH CACHE */
					$this->cache->delete('branch');
				}else{
					$msg = "an error occured";
					$error = TRUE;
				}
			}
		}

		$data = array('msg' => $msg,
					'error' => $error,
					'branch' => $branch,
		);

		$this->template->view('settings/branch_manager/edit', $data);
	}


	public function delete($id){
		$query = "select * from branch where id = '{$id}'";
		$branch = getdata($query);

		if(empty($branch)){
			redirect('/settings/branch_manager/index');
		}else{
			/* delete from db */
			if($this->db->delete('branch', array('id' => $id))){
				/* CREATE LOG */
				create_log($id, 'branch', 'delete', $branch[0]['description']);

				/* DELETE BRANCH CACHE */
				$this->cache->delete('branch');

				$error = FALSE;
				$msg = strtoupper($branch[0]['description'])." is successfully deleted.";
			}else{
				$error = TRUE;
				$msg = "An error occurred.";
			}

			$this->session->set_flashdata('br_del_error', $error);
			$this->session->set_flashdata('br_del_msg', $msg);
			redirect('/settings/branch_manager');
		}
	}
}