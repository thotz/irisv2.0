<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Country_manager extends My_controller
{
	public function __construct(){
		parent::__construct();

		/** Set title for all function */
		set_header_title("Country Manager");

		$this->load->helper('form');
		$this->load->library('form_validation');
	}

	public function index(){
		$this->template->set('file_javascript', array('js/settings/country_manager/datatable.js'));
		$this->template->set('file_css', array('js/plugins/datatables/dataTables.bootstrap.css'));

		$query = $this->db->get('country');
		$data['country'] = $query->result_array();
		$data['msg'] = "";
		$this->template->view('settings/country_manager/index', $data);
	}

	public function add(){
		set_header_title("Country Manager - Add");

		$this->form_validation->set_rules('inputName', 'COUNTRY NAME', 'trim|required|is_unique[country.name]|min_length[3]');
		$this->form_validation->set_rules('inputCode', 'COUNTRY CODE', 'trim|required|is_unique[country.countrycode]|min_length[1]|max_length[4]');
		$this->form_validation->set_message('is_unique', '{field} already exists.');

		$msg = "";
		$error = FALSE;
		if (isset($_POST['btnAdd'])){
			if ($this->form_validation->run() == FALSE){
				$aError = $this->form_validation->error_array();
				$msg = reset($aError);
				$error = TRUE;
			}else{
				/* insert into db */
				if($new_id = $this->global_model->insert('country', array('name' => strtoupper($_POST['inputName']), 'countrycode' => strtoupper($_POST['inputCode'])))){
					/* CREATE LOG */
					create_log($new_id, 'country', 'add');

					/* DELETE COUNTRY CACHE */
					$this->cache->delete('country');

					$this->session->set_flashdata('add_cn_success', 'Country is successfully added.');
					redirect('/settings/country_manager/edit/'.$new_id);
				}else{
					$msg = "an error occured";
					$error = TRUE;
				}
			}
		}

		$data = array('msg' => $msg,
					'error' => $error,
		);

		$this->template->view('settings/country_manager/add', $data);
	}

	public function edit($id){
		$query = "select * from country where country_id = '{$id}'";
		$country = getdata($query);

		if(empty($country)){
			redirect('/settings/country_manager/index');
		}

		set_header_title("Country Manager - Edit");

		$this->form_validation->set_rules('inputName', 'COUNTRY NAME', 'trim|required|edit_unique[country.name.country_id.'.$id.']|min_length[3]');
		$this->form_validation->set_rules('inputCode', 'COUNTRY CODE', 'trim|required|edit_unique[country.countrycode.country_id.'.$id.']|min_length[1]|max_length[4]');
		$this->form_validation->set_message('edit_unique', '{field} already exists.');

		$msg = "";
		$error = FALSE;
		if (isset($_POST['btnEdit'])){
			if ($this->form_validation->run() == FALSE){
				$aError = $this->form_validation->error_array();
				$msg = reset($aError);
				$error = true;
			}else{
				/* update tbl */
				if($this->global_model->update('country', array('name' => strtoupper($_POST['inputName']), 'countrycode' => strtoupper($_POST['inputCode'])), array('country_id' => $id))){
					$msg = TRUE;

					/* CREATE LOG */
					create_log($id, 'country', 'edit');

					/* DELETE COUNTRY CACHE */
					$this->cache->delete('country');
				}else{
					$msg = "an error occured";
					$error = TRUE;
				}
			}
		}

		$data = array('msg' => $msg,
					'error' => $error,
					'country' => $country,
		);

		$this->template->view('settings/country_manager/edit', $data);
	}


	public function delete($id){
		$query = "select * from country where country_id = '{$id}'";
		$country = getdata($query);

		if(empty($country)){
			redirect('/settings/country_manager/index');
		}else{
			/* delete from db */
			if($this->db->delete('country', array('country_id' => $id))){
				/* CREATE LOG */
				create_log($id, 'country', 'delete', $country[0]['name']);

				/* DELETE COUNTRY CACHE */
				$this->cache->delete('country');

				$error = FALSE;
				$msg = strtoupper($country[0]['name'])." is successfully deleted.";
			}else{
				$error = TRUE;
				$msg = "An error occurred.";
			}

			$this->session->set_flashdata('cn_del_error', $error);
			$this->session->set_flashdata('cn_del_msg', $msg);
			redirect('/settings/country_manager');
		}
	}
}