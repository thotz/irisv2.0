<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Nature_manager extends My_controller
{
	public function __construct(){
		parent::__construct();

		/** Set title for all function */
		set_header_title("Nature Manager");

		$this->load->helper('form');
		$this->load->library('form_validation');
	}

	public function index(){
		$this->template->set('file_javascript', array('js/settings/nature_manager/datatable.js'));
		$this->template->set('file_css', array('js/plugins/datatables/dataTables.bootstrap.css'));

		$query = $this->db->get('nature');
		$data['nature'] = $query->result_array();
		$data['msg'] = "";
		$this->template->view('settings/nature_manager/index', $data);
	}

	public function add(){
		set_header_title("Nature Manager - Add");

		$this->form_validation->set_rules('inputName', 'NAME', 'trim|required|is_unique[nature.name]|min_length[3]');
		$this->form_validation->set_message('is_unique', '{field} already exists.');

		$msg = "";
		$error = FALSE;
		if (isset($_POST['btnAdd'])){
			if ($this->form_validation->run() == FALSE){
				$aError = $this->form_validation->error_array();
				$msg = reset($aError);
				$error = TRUE;
			}else{
				/* insert into db */
				$insert_data = array('name' => $_POST['inputName'],);
				if($new_id = $this->global_model->insert('nature', $insert_data)){
					/* CREATE LOG */
					create_log($new_id, 'nature', 'add');

					$this->session->set_flashdata('add_nature_success', 'Nature is successfully added.');
					redirect('/settings/nature_manager/edit/'.$new_id);
				}else{
					$msg = "an error occured";
					$error = TRUE;
				}
			}
		}

		$data = array('msg' => $msg,
					'error' => $error,
		);

		$this->template->view('settings/nature_manager/add', $data);
	}

	public function edit($id){
		$query = "select * from nature where nature_id = '{$id}'";
		$nature = getdata($query);

		if(empty($nature)){
			redirect('/settings/nature_manager/index');
		}

		set_header_title("Nature Manager - Edit");

		$this->form_validation->set_rules('inputName', 'NAME', 'trim|required|edit_unique[nature.name.nature_id.'.$id.']|min_length[3]');
		$this->form_validation->set_message('edit_unique', '{field} already exists.');

		$msg = "";
		$error = FALSE;
		if (isset($_POST['btnEdit'])){
			if ($this->form_validation->run() == FALSE){
				$aError = $this->form_validation->error_array();
				$msg = reset($aError);
				$error = true;
			}else{
				/* update tbl */
				$update_data = array('name' => $_POST['inputName'],);
				if($this->global_model->update('nature', $update_data, array('nature_id' => $id))){
					$msg = TRUE;

					/* CREATE LOG */
					create_log($id, 'nature', 'edit');
				}else{
					$msg = "an error occured";
					$error = TRUE;
				}
			}
		}

		$data = array('msg' => $msg,
					'error' => $error,
					'nature' => $nature,
		);

		$this->template->view('settings/nature_manager/edit', $data);
	}


	public function delete($id){
		$query = "select * from nature where nature_id = '{$id}'";
		$nature = getdata($query);

		if(empty($nature)){
			redirect('/settings/nature_manager/index');
		}else{
			/* delete from db */
			if($this->db->delete('nature', array('nature_id' => $id))){
				/* CREATE LOG */
				create_log($id, 'nature', 'delete', $nature[0]['name']);

				$error = FALSE;
				$msg = strtoupper($nature[0]['name'])." is successfully deleted.";
			}else{
				$error = TRUE;
				$msg = "An error occurred.";
			}

			$this->session->set_flashdata('nature_del_error', $error);
			$this->session->set_flashdata('nature_del_msg', $msg);
			redirect('/settings/nature_manager');
		}
	}
}