<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Doctype_manager extends My_controller
{
	public function __construct(){
		parent::__construct();

		/** Set title for all function */
		set_header_title("Document type Manager");

		$this->load->helper('form');
		$this->load->library('form_validation');
	}

	public function index(){
		$this->template->set('file_javascript', array('js/settings/doctype_manager/datatable.js'));
		$this->template->set('file_css', array('js/plugins/datatables/dataTables.bootstrap.css'));

		$query = $this->db->get('doc_type');
		$data['doc_type'] = $query->result_array();
		$data['msg'] = "";
		$this->template->view('settings/doctype_manager/index', $data);
	}

	public function add(){
		set_header_title("Document type Manager - Add");

		$this->form_validation->set_rules('inputName', 'CURRENCY NAME', 'trim|required|is_unique[doc_type.name]|min_length[3]');
		$this->form_validation->set_message('is_unique', '{field} already exists.');

		$msg = "";
		$error = FALSE;
		if (isset($_POST['btnAdd'])){
			if ($this->form_validation->run() == FALSE){
				$aError = $this->form_validation->error_array();
				$msg = reset($aError);
				$error = TRUE;
			}else{
				/* insert into db */
				if($new_id = $this->global_model->insert('doc_type', array('name' => $_POST['inputName']))){
					/* CREATE LOG */
					create_log($new_id, 'doc_type', 'add');

					$this->session->set_flashdata('add_dt_success', 'Document type is successfully added.');
					redirect('/settings/doctype_manager/edit/'.$new_id);
				}else{
					$msg = "an error occured";
					$error = TRUE;
				}
			}
		}

		$data = array('msg' => $msg,
					'error' => $error,
		);

		$this->template->view('settings/doctype_manager/add', $data);
	}

	public function edit($id){
		$query = "select * from doc_type where type_id = '{$id}'";
		$doc_type = getdata($query);

		if(empty($doc_type)){
			redirect('/settings/doctype_manager/index');
		}

		set_header_title("Document type Manager - Edit");

		$this->form_validation->set_rules('inputName', 'CURRENCY NAME', 'trim|required|edit_unique[doc_type.name.type_id.'.$id.']|min_length[3]');
		$this->form_validation->set_message('edit_unique', '{field} already exists.');

		$msg = "";
		$error = FALSE;
		if (isset($_POST['btnEdit'])){
			if ($this->form_validation->run() == FALSE){
				$aError = $this->form_validation->error_array();
				$msg = reset($aError);
				$error = true;
			}else{
				/* update tbl */
				if($this->global_model->update('doc_type', array('name' => $_POST['inputName']), array('type_id' => $id))){
					$msg = TRUE;

					/* CREATE LOG */
					create_log($id, 'doc_type', 'edit');
				}else{
					$msg = "an error occured";
					$error = TRUE;
				}
			}
		}

		$data = array('msg' => $msg,
					'error' => $error,
					'doc_type' => $doc_type,
		);

		$this->template->view('settings/doctype_manager/edit', $data);
	}


	public function delete($id){
		$query = "select * from doc_type where type_id = '{$id}'";
		$doc_type = getdata($query);

		if(empty($doc_type)){
			redirect('/settings/doctype_manager/index');
		}else{
			/* delete from db */
			if($this->db->delete('doc_type', array('type_id' => $id))){
				/* CREATE LOG */
				create_log($id, 'doc_type', 'delete', $doc_type[0]['name']);

				$error = FALSE;
				$msg = strtoupper($doc_type[0]['name'])." is successfully deleted.";
			}else{
				$error = TRUE;
				$msg = "An error occurred.";
			}

			$this->session->set_flashdata('dt_del_error', $error);
			$this->session->set_flashdata('dt_del_msg', $msg);
			redirect('/settings/doctype_manager');
		}
	}
}