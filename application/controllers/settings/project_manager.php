<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Project_manager extends My_controller
{
	public $users = array();

	public function __construct(){
		parent::__construct();

		/** Set title for all function */
		set_header_title("Project Manager");

		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$this->users = get_options_from_cache('users');
	}

	public function index(){
		$this->template->set('file_javascript', array('js/settings/project_manager/datatable.js'));
		$this->template->set('file_css', array('js/plugins/datatables/dataTables.bootstrap.css'));

		$query = $this->db->get('project_nature');
		$data['project_nature'] = $query->result_array();
		$data['users'] = $this->users;
		$data['msg'] = "";
		$this->template->view('settings/project_manager/index', $data);
	}

	public function add(){
		set_header_title("Project Manager - Add");

		$this->form_validation->set_rules('inputName', 'PROJECT NAME', 'trim|required|is_unique[project_nature.name]|min_length[3]|alpha_numeric');
		$this->form_validation->set_message('is_unique', '{field} already exists.');
		$this->form_validation->set_rules('inputUser1', '', 'trim');
		$this->form_validation->set_rules('inputUser2', '', 'trim');

		$msg = "";
		$error = FALSE;
		if (isset($_POST['btnAdd'])){
			if ($this->form_validation->run() == FALSE){
				$aError = $this->form_validation->error_array();
				$msg = reset($aError);
				$error = TRUE;
			}else{
				/* insert into db */
				if($new_id = $this->global_model->insert('project_nature', array('name' => $_POST['inputName'], 'user_id' => $_POST['inputUser1'], 'user_id1' => $_POST['inputUser2']))){
					/* CREATE LOG */
					create_log($new_id, 'project_nature', 'add');

					$this->session->set_flashdata('add_pn_success', 'Project is successfully added.');
					redirect('/settings/project_manager/edit/'.$new_id);
				}else{
					$msg = "an error occured";
					$error = TRUE;
				}
				$msg=TRUE;
			}
		}

		$data = array('msg' => $msg,
					'error' => $error,
					'users' => $this->users,
		);

		$this->template->view('settings/project_manager/add', $data);
	}

	public function edit($id){
		$query = "select * from project_nature where project_nature_id = '{$id}'";
		$project_nature = getdata($query);

		if(empty($project_nature)){
			redirect('/settings/project_manager/index');
		}

		set_header_title("Project Manager - Edit");

		$this->form_validation->set_rules('inputName', 'PROJECT NAME', 'trim|required|edit_unique[project_nature.name.project_nature_id.'.$id.']|min_length[3]|alpha_numeric');
		$this->form_validation->set_message('edit_unique', '{field} already exists.');
		$this->form_validation->set_rules('inputUser1', '', 'trim');
		$this->form_validation->set_rules('inputUser2', '', 'trim');

		$msg = "";
		$error = FALSE;
		if (isset($_POST['btnEdit'])){
			if ($this->form_validation->run() == FALSE){
				$aError = $this->form_validation->error_array();
				$msg = reset($aError);
				$error = true;
			}else{
				/* update tbl */
				if($this->global_model->update('project_nature', array('name' => $_POST['inputName'], 'user_id' => $_POST['inputUser1'], 'user_id1' => $_POST['inputUser2']), array('project_nature_id' => $id))){
					$msg = TRUE;

					/* CREATE LOG */
					create_log($id, 'project_nature', 'edit');
				}else{
					$msg = "an error occured";
					$error = TRUE;
				}
			}
		}

		$data = array('msg' => $msg,
					'error' => $error,
					'project_nature' => $project_nature,
					'users' => $this->users,
		);

		$this->template->view('settings/project_manager/edit', $data);
	}


	public function delete($id){
		$query = "select * from project_nature where project_nature_id = '{$id}'";
		$project_nature = getdata($query);

		if(empty($project_nature)){
			redirect('/settings/project_manager/index');
		}else{
			/* delete from db */
			if($this->db->delete('project_nature', array('project_nature_id' => $id))){
				/* CREATE LOG */
				create_log($id, 'project_nature', 'delete', $project_nature[0]['name']);

				$error = FALSE;
				$msg = strtoupper($project_nature[0]['name'])." is successfully deleted.";
			}else{
				$error = TRUE;
				$msg = "An error occurred.";
			}

			$this->session->set_flashdata('pn_del_error', $error);
			$this->session->set_flashdata('pn_del_msg', $msg);
			redirect('/settings/project_manager');
		}
	}
}