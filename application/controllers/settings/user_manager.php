<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_manager extends My_controller
{
	public $branch_list = array();
	public $access_list = array();	

	public function __construct()
	{
		parent::__construct();
		$this->load->model('settings/user_manager_model', 'users', true);

		$this->load->helper('form');
		$this->load->library('form_validation');
		
		/** Set title for all function */
		set_header_title("User Manager");
		
		$this->branch_list = get_options_from_cache('branch');
		$this->access_list = get_options_from_cache('access');
	}
	
	public function index()
	{
		$this->template->set('file_javascript', array('js/settings/user_manager/datatable.js'));
		$this->template->set('file_css', array('js/plugins/datatables/dataTables.bootstrap.css'));

		$data['all_user'] = $this->users->get_all_user();
		$this->template->view('settings/user_manager/index', $data);
	}
	
	public function add()
	{
		/** Set title for all function */
		set_header_title("User Manager - Add");

		$this->form_validation->set_rules('radStatus', '', 'trim');
		$this->form_validation->set_rules('inputEWID', 'EW ID NO.', 'numeric|is_unique[users.ew_id]');
		$this->form_validation->set_rules('inputUsername', 'USER NAME', 'required|is_unique[users.username]|min_length[3]|alpha');
		$this->form_validation->set_message('is_unique', '{field} already exists.');
		$this->form_validation->set_rules('inputLastName', 'LAST NAME', 'trim|required|min_length[2]');
		$this->form_validation->set_rules('inputFirstName', 'FIRST NAME', 'trim|required|min_length[3]');
		$this->form_validation->set_rules('inputCorpEmail', 'CORPORATE EMAIL', 'valid_email');
		$this->form_validation->set_rules('inputApplEmail', 'APPLICANT EMAIL', 'valid_email');
		$this->form_validation->set_rules('inputPassword', 'PASSWORD', 'required|min_length[5]');
		$this->form_validation->set_rules('inputConfirmPassword', 'CONFIRM PASSWORD', 'required|matches[inputPassword]');
		$this->form_validation->set_rules('selectBranch', 'BRANCH', 'required');
		$this->form_validation->set_rules('selectAccLvl', 'ACCESS LEVEL', 'required');
		$this->form_validation->set_rules('radEvaluator', '', 'trim');

		$msg = "";
		$error = false;
		if (isset($_POST['btnAdd'])){
			if ($this->form_validation->run() == FALSE){
				$aError = $this->form_validation->error_array();
				$msg = reset($aError);
				$error = true;
			}else{
				/* insert into db */
				if($new_user_id = $this->users->new_user()){
					/* CREATE LOG */
					create_log($new_user_id, 'access level manager', 'add');

					$this->session->set_flashdata('add_success', 'User is successfully added.');
					redirect('/settings/user_manager/edit/'.$new_user_id);
				}else{
					$msg = "an error occurred";
					$error = true;
				}
			}
		}

		$data = array('msg' => $msg,
					  'error' => $error,
					  'branches' => $this->branch_list,
					  'access' => $this->access_list
		);

		$this->template->view('settings/user_manager/add', $data);
		
	}

	public function edit($user_id=NULL)
	{
		set_header_title("User Manager - Edit");

		$this->form_validation->set_rules('radStatus', '', 'trim');
		$this->form_validation->set_rules('inputEWID', 'EW ID NO.', 'is_unique[users.ew_id]|numeric');
		$this->form_validation->set_message('is_unique', '{field} already exists.');
		$this->form_validation->set_rules('inputLastName', 'LAST NAME', 'trim|required|min_length[2]');
		$this->form_validation->set_rules('inputFirstName', 'FIRST NAME', 'trim|required|min_length[3]');
		$this->form_validation->set_rules('inputCorpEmail', 'CORPORATE EMAIL', 'valid_email');
		$this->form_validation->set_rules('inputApplEmail', 'APPLICANT EMAIL', 'valid_email');
		$cpasswd_required_if = (isset($_POST['inputPassword']) && $_POST['inputPassword']!='') ? '|required|matches[inputPassword]' : '' ;
		$passwd_required_if = (isset($_POST['inputConfirmPassword']) && $_POST['inputConfirmPassword']!='') ? '|required' : '' ;
		$this->form_validation->set_rules('inputPassword', 'PASSWORD', 'trim|min_length[5]'.$passwd_required_if);
		$this->form_validation->set_rules('inputConfirmPassword', 'CONFIRM PASSWORD', 'trim'.$cpasswd_required_if);
		$this->form_validation->set_rules('selectBranch', 'BRANCH', 'required');
		$this->form_validation->set_rules('selectAccLvl', 'ACCESS LEVEL', 'required');
		$this->form_validation->set_rules('radEvaluator', '', 'trim');

		$msg = "";
		$error = false;
		if (isset($_POST['btnEdit'])){
			if ($this->form_validation->run() == FALSE){
				$aError = $this->form_validation->error_array();
				$msg = reset($aError);
				$error = true;
			}else{
				/* update tbl */
				if($this->users->edit($user_id)){
					$msg = "SUCESS";
					
					/* CREATE LOG */
					create_log($user_id, 'user manager', 'edit');
				}else{
					$msg = "an error occurred";
					$error = true;
				}
			}
		}

		$user_details = $this->users->get_user($user_id);
		$data = array('msg' => $msg,
					  'error' => $error,
					  'branches' => $this->branch_list,
					  'access' => $this->access_list,
					  'user_details' => $user_details,
		);

		$this->template->view('settings/user_manager/edit', $data);
	}
}