<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Clinic_manager extends My_controller
{
	public function __construct(){
		parent::__construct();
		
		/** Set title for all function */
		set_header_title("Clinic Manager");

		$this->load->helper('form');
		$this->load->library('form_validation');
	}

	public function index(){
		$this->template->set('file_javascript', array('js/settings/clinic_manager/datatable.js'));
		$this->template->set('file_css', array('js/plugins/datatables/dataTables.bootstrap.css'));

		$query = $this->db->get('clinics');
		$data['clinic_list'] = $query->result_array();
		$this->template->view('settings/clinic_manager/index', $data);
	}
	
	public function add(){
		set_header_title("Clinic Manager - Add");

		$this->form_validation->set_rules('inputClinicName', 'CLINIC NAME', 'trim|required|min_length[2]');
		$this->form_validation->set_rules('inputContact', 'CONTACT PERSON', 'trim|required|min_length[3]');
		$this->form_validation->set_rules('inputPhone', 'CONTACT NO.', 'trim|required|min_length[7]');
		$this->form_validation->set_rules('inputCode', 'CLINIC CODE', 'required|is_unique[clinics.code]|min_length[2]');
		$this->form_validation->set_message('is_unique', '{field} already exists.');
		$this->form_validation->set_rules('inputEmailAddress', 'EMAIL', 'trim|valid_email');
		$this->form_validation->set_rules('txtAddress', 'ADDRESS', 'trim|required');

		$msg = "";
		$error = FALSE;
		if (isset($_POST['btnAdd'])){
			if ($this->form_validation->run() == FALSE){
				$aError = $this->form_validation->error_array();
				$msg = reset($aError);
				$error = TRUE;
			}else{
				/* insert into db */
				$insert_data = array('name' => $_POST['inputClinicName'],
						'address' => $_POST['txtAddress'],
						'email' => $_POST['inputEmailAddress'],
						'code' => strtoupper($_POST['inputCode']),
						'fax_no' => $_POST['inputFax'],
						'telephone' => $_POST['inputPhone'],
						'address' => $_POST['txtAddress'],
						'remarks' => $_POST['txtRemarks'],
						'contact' => $_POST['inputContact'],
				);
		
				if($new_id = $this->global_model->insert('clinics', $insert_data)){
					/* CREATE LOG */
					create_log($new_id, 'clinic manager', 'add');
		
					$this->session->set_flashdata('add_clinic_success', 'Clinic is successfully added.');
					redirect('/settings/clinic_manager/edit/'.$new_id);
				}else{
					$msg = "an error occured";
					$error = TRUE;
				}
			}
		}
		
		$data = array('msg' => $msg,
						'error' => $error,
		);
		
		$this->template->view('settings/clinic_manager/add', $data);
	}
	
	public function edit($id){
		$query = "select * from clinics where clinic_id = '{$id}'";
		$clinic_details = getdata($query);
		
		if(empty($clinic_details)){
			redirect('/settings/clinic_manager');
		}
		
		set_header_title("Clinic Manager - Edit");
		
		$this->form_validation->set_rules('inputClinicName', 'CLINIC NAME', 'trim|required|min_length[2]');
		$this->form_validation->set_rules('inputContact', 'CONTACT PERSON', 'trim|required|min_length[3]');
		$this->form_validation->set_rules('inputPhone', 'CONTACT NO.', 'trim|required|min_length[7]');
		$this->form_validation->set_rules('inputCode', 'CLINIC CODE', 'trim|required|edit_unique[clinics.code.clinic_id.'.$id.']');
		$this->form_validation->set_message('edit_unique', '{field} already exists.');
		$this->form_validation->set_rules('inputEmailAddress', 'EMAIL', 'trim|valid_email');
		$this->form_validation->set_rules('txtAddress', 'ADDRESS', 'trim|required');
		
		$msg = "";
		$error = FALSE;
		if (isset($_POST['btnEdit'])){
			if ($this->form_validation->run() == FALSE){
				$aError = $this->form_validation->error_array();
				$msg = reset($aError);
				$error = true;
			}else{
				/* update tbl */
				$update_data = array('name' => $_POST['inputClinicName'],
						'address' => $_POST['txtAddress'],
						'email' => $_POST['inputEmailAddress'],
						'code' => strtoupper($_POST['inputCode']),
						'fax_no' => $_POST['inputFax'],
						'telephone' => $_POST['inputPhone'],
						'address' => $_POST['txtAddress'],
						'remarks' => $_POST['txtRemarks'],
						'contact' => $_POST['inputContact'],
				);
		
				if($this->global_model->update('clinics', $update_data, array('clinic_id' => $id))){
					$msg = TRUE;
		
					/* CREATE LOG */
					create_log($id, 'clinic manager', 'edit');
				}else{
					$msg = "an error occured";
					$error = TRUE;
				}
			}
		}
		
		$data = array('msg' => $msg,
				'error' => $error,
				'clinic_details' => $clinic_details
		);
		
		$this->template->view('settings/clinic_manager/edit', $data);
	}

	
	public function delete($id){
		$query = "select * from clinics where clinic_id = '{$id}'";
		$clinic_details = getdata($query);

		if(empty($clinic_details)){
			redirect('/settings/clinic_manager');
		}else{
			/* delete from db */
			if($this->db->delete('clinics', array('clinic_id' => $id))){
				/* CREATE LOG */
				create_log($id, 'clinic manager', 'delete', $clinic_details[0]['name']);

				$error = FALSE;
				$msg = strtoupper($clinic_details[0]['name'])." is successfully deleted.";
			}else{
				$error = TRUE;
				$msg = "An error occurred.";
			}

			$this->session->set_flashdata('clinic_del_error', $error);
			$this->session->set_flashdata('clinic_del_msg', $msg);
			redirect('/settings/clinic_manager');
		}
	}
}