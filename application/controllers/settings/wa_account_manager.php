<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Wa_account_manager extends My_controller
{
	public function __construct(){
		parent::__construct();

		$this->load->helper('form');
		$this->load->library('form_validation');
	}

	public function index(){
		$this->template->set('file_javascript', array('js/settings/wa_account_manager/datatable.js'));
		$this->template->set('file_css', array('js/plugins/datatables/dataTables.bootstrap.css'));

		$query = $this->db->get('wa_users');
		$data['wa_users'] = $query->result_array();
		$data['msg'] = "";
		$this->template->view('settings/wa_account_manager/index', $data);
	}

	public function add(){
		set_header_title("WorkAbroad Account Manager - Add");

		$this->form_validation->set_rules('inputName', 'NAME', 'trim|required|is_unique[wa_users.username]|min_length[2]');
		$this->form_validation->set_message('is_unique', '{field} already exists.');
		$this->form_validation->set_rules('inputPass', 'PASSWORD', 'trim|required');

		$msg = "";
		$error = FALSE;
		if (isset($_POST['btnAdd'])){
			if ($this->form_validation->run() == FALSE){
				$aError = $this->form_validation->error_array();
				$msg = reset($aError);
				$error = TRUE;
			}else{
				/* insert into db */
				$insert_data = array('username' => $_POST['inputName'], 'password' => $_POST['inputPass'],);
				if($new_id = $this->global_model->insert('wa_users', $insert_data)){
					/* CREATE LOG */
					create_log($new_id, 'wa_users', 'add');

					$this->session->set_flashdata('add_wa_success', 'WorkAbroad Account is successfully added.');
					redirect('/settings/wa_account_manager/edit/'.$new_id);
				}else{
					$msg = "an error occured";
					$error = TRUE;
				}
			}
		}

		$data = array('msg' => $msg,
					'error' => $error,
		);

		$this->template->view('settings/wa_account_manager/add', $data);
	}

	public function edit($id){
		$query = "select * from wa_users where wa_users_id = '{$id}'";
		$wa_users = getdata($query);

		if(empty($wa_users)){
			redirect('/settings/wa_account_manager/index');
		}

		set_header_title("WorkAbroad Account Manager - Edit");

		$this->form_validation->set_rules('inputName', 'NAME', 'trim|required|edit_unique[wa_users.username.wa_users_id.'.$id.']|min_length[3]');
		$this->form_validation->set_message('edit_unique', '{field} already exists.');
		$this->form_validation->set_rules('inputPass', 'PASSWORD', 'trim|required');

		$msg = "";
		$error = FALSE;
		if (isset($_POST['btnEdit'])){
			if ($this->form_validation->run() == FALSE){
				$aError = $this->form_validation->error_array();
				$msg = reset($aError);
				$error = true;
			}else{
				/* update tbl */
				$update_data = array('username' => $_POST['inputName'], 'password' => $_POST['inputPass']);
				if($this->global_model->update('wa_users', $update_data, array('wa_users_id' => $id))){
					$msg = TRUE;

					/* CREATE LOG */
					create_log($id, 'wa_users', 'edit');
				}else{
					$msg = "an error occured";
					$error = TRUE;
				}
			}
		}

		$data = array('msg' => $msg,
					'error' => $error,
					'wa_users' => $wa_users,
		);

		$this->template->view('settings/wa_account_manager/edit', $data);
	}


	public function delete($id){
		$query = "select * from wa_users where wa_users_id = '{$id}'";
		$wa_users = getdata($query);

		if(empty($wa_users)){
			redirect('/settings/wa_account_manager/index');
		}else{
			/* delete from db */
			if($this->db->delete('wa_users', array('wa_users_id' => $id))){
				/* CREATE LOG */
				create_log($id, 'wa_users', 'delete', $wa_users[0]['username']);

				$error = FALSE;
				$msg = strtoupper($wa_users[0]['username'])." is successfully deleted.";
			}else{
				$error = TRUE;
				$msg = "An error occurred.";
			}

			$this->session->set_flashdata('wa_del_error', $error);
			$this->session->set_flashdata('wa_del_msg', $msg);
			redirect('/settings/wa_account_manager');
		}
	}
}