<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Position_manager extends My_controller
{
	public $std_category = array();
// 	public $positions = array();

	public function __construct(){
		parent::__construct();

		/** Set title for all function */
		set_header_title("Position Manager");

		$this->load->helper('form');
		$this->load->library('form_validation');

		$this->std_category = get_options_from_cache('standard category');
// 		$this->positions = get_options_from_cache('positions');
	}

	public function index(){
		$this->template->set('file_javascript', array('js/settings/position_manager/datatable.js'));
		$this->template->set('file_css', array('js/plugins/datatables/dataTables.bootstrap.css'));

		$query = $this->db->get('positions');
		$data['positions'] = $query->result_array();
		$data['std_category'] = $this->std_category;
		$data['msg'] = "";
		$this->template->view('settings/position_manager/index', $data);
	}

	public function add(){
		set_header_title("Position Manager - Add");

		$this->form_validation->set_rules('inputName', 'POSITION NAME', 'trim|required|is_unique[positions.name]|min_length[3]|regex_match[/^[a-z 0-9 ().,\/\&\\-]+$/i]');
		$this->form_validation->set_message('is_unique', '{field} already exists.');
		$this->form_validation->set_message('regex_match', 'Invalid characters are not allowed.');

		$msg = "";
		$error = FALSE;
		if (isset($_POST['btnAdd'])){
			if ($this->form_validation->run() == FALSE){
				$aError = $this->form_validation->error_array();
				$msg = reset($aError);
				$error = TRUE;
			}else{
				/* insert into db */
				if($new_id = $this->global_model->insert('positions', array('name' => $_POST['inputName'], 'job_standard_id' => $_POST['selectJobStd']))){
					/* CREATE LOG */
					create_log($new_id, 'positions', 'add');

					$this->session->set_flashdata('add_pos_success', 'Position is successfully added.');
					redirect('/settings/position_manager/edit/'.$new_id);
				}else{
					$msg = "an error occured";
					$error = TRUE;
				}
			}
		}

		$data = array('msg' => $msg,
					'error' => $error,
					'std_category' => $this->std_category
		);

		$this->template->view('settings/position_manager/add', $data);
	}

	public function edit($id){
		$query = "select * from positions where position_id = '{$id}'";
		$positions = getdata($query);

		if(empty($positions)){
			redirect('/settings/position_manager/index');
		}

		set_header_title("Position Manager - Edit");

		$this->form_validation->set_rules('inputName', 'POSITION NAME', 'trim|required|edit_unique[positions.name.position_id.'.$id.']|min_length[3]|regex_match[/^[a-z 0-9 ().,\/\&\\-]+$/i]');
		$this->form_validation->set_message('edit_unique', '{field} already exists.');
		$this->form_validation->set_message('regex_match', 'Invalid characters are not allowed.');

		$msg = "";
		$error = FALSE;
		if (isset($_POST['btnEdit'])){
			if ($this->form_validation->run() == FALSE){
				$aError = $this->form_validation->error_array();
				$msg = reset($aError);
				$error = true;
			}else{
				/* update tbl */
				if($this->global_model->update('positions', array('name' => $_POST['inputName'], 'job_standard_id' => $_POST['selectJobStd']), array('position_id' => $id))){
					$msg = TRUE;

					/* CREATE LOG */
					create_log($id, 'positions', 'edit');
				}else{
					$msg = "an error occured";
					$error = TRUE;
				}
			}
		}

		$data = array('msg' => $msg,
					'error' => $error,
					'positions' => $positions,
					'std_category' => $this->std_category
		);

		$this->template->view('settings/position_manager/edit', $data);
	}


	public function delete($id){
		$query = "select * from positions where position_id = '{$id}'";
		$positions = getdata($query);

		if(empty($positions)){
			redirect('/settings/position_manager/index');
		}else{
			/* delete from db */
			if($this->db->delete('positions', array('position_id' => $id))){
				/* CREATE LOG */
				create_log($id, 'positions', 'delete', $positions[0]['name']);

				$error = FALSE;
				$msg = strtoupper($positions[0]['name'])." is successfully deleted.";
			}else{
				$error = TRUE;
				$msg = "An error occurred.";
			}

			$this->session->set_flashdata('pos_del_error', $error);
			$this->session->set_flashdata('pos_del_msg', $msg);
			redirect('/settings/position_manager');
		}
	}
}