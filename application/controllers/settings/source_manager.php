<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Source_manager extends My_controller
{
	public function __construct(){
		parent::__construct();

		/** Set title for all function */
// 		set_header_title("Source Manager");

		$this->load->helper('form');
		$this->load->library('form_validation');
	}

	public function index(){
		$this->template->set('file_javascript', array('js/settings/source_manager/datatable.js'));
		$this->template->set('file_css', array('js/plugins/datatables/dataTables.bootstrap.css'));

		$query = $this->db->get('source');
		$data['source'] = $query->result_array();
		$data['msg'] = "";
		$this->template->view('settings/source_manager/index', $data);
	}

	public function add(){
		set_header_title("Source Manager - Add");

		$this->form_validation->set_rules('inputName', 'NAME', 'trim|required|is_unique[source.name]|min_length[3]');
		$this->form_validation->set_rules('inputCode', 'CODE', 'trim|required|is_unique[source.code]|min_length[2]');
		$this->form_validation->set_message('is_unique', '{field} already exists.');

		$msg = "";
		$error = FALSE;
		if (isset($_POST['btnAdd'])){
			if ($this->form_validation->run() == FALSE){
				$aError = $this->form_validation->error_array();
				$msg = reset($aError);
				$error = TRUE;
			}else{
				/* insert into db */
				$insert_data = array('name' => $_POST['inputName'], 'code' => $_POST['inputCode'],);
				if($new_id = $this->global_model->insert('source', $insert_data)){
					/* CREATE LOG */
					create_log($new_id, 'source', 'add');

					$this->session->set_flashdata('add_source_success', 'Source is successfully added.');
					redirect('/settings/source_manager/edit/'.$new_id);
				}else{
					$msg = "an error occured";
					$error = TRUE;
				}
			}
		}

		$data = array('msg' => $msg,
					'error' => $error,
		);

		$this->template->view('settings/source_manager/add', $data);
	}

	public function edit($id){
		$query = "select * from source where source_id = '{$id}'";
		$source = getdata($query);

		if(empty($source)){
			redirect('/settings/source_manager/index');
		}

		set_header_title("Source Manager - Edit");

		$this->form_validation->set_rules('inputName', 'NAME', 'trim|required|edit_unique[source.name.source_id.'.$id.']|min_length[3]');
		$this->form_validation->set_rules('inputCode', 'CODE', 'trim|required|edit_unique[source.code.source_id.'.$id.']|min_length[2]');
		$this->form_validation->set_message('edit_unique', '{field} already exists.');

		$msg = "";
		$error = FALSE;
		if (isset($_POST['btnEdit'])){
			if ($this->form_validation->run() == FALSE){
				$aError = $this->form_validation->error_array();
				$msg = reset($aError);
				$error = true;
			}else{
				/* update tbl */
				$update_data = array('name' => $_POST['inputName'], 'code' => $_POST['inputCode']);
				if($this->global_model->update('source', $update_data, array('source_id' => $id))){
					$msg = TRUE;

					/* CREATE LOG */
					create_log($id, 'source', 'edit');
				}else{
					$msg = "an error occured";
					$error = TRUE;
				}
			}
		}

		$data = array('msg' => $msg,
					'error' => $error,
					'source' => $source,
		);

		$this->template->view('settings/source_manager/edit', $data);
	}


	public function delete($id){
		$query = "select * from source where source_id = '{$id}'";
		$source = getdata($query);

		if(empty($source)){
			redirect('/settings/source_manager/index');
		}else{
			/* delete from db */
			if($this->db->delete('source', array('source_id' => $id))){
				/* CREATE LOG */
				create_log($id, 'source', 'delete', $source[0]['name']);

				$error = FALSE;
				$msg = strtoupper($source[0]['name'])." is successfully deleted.";
			}else{
				$error = TRUE;
				$msg = "An error occurred.";
			}

			$this->session->set_flashdata('source_del_error', $error);
			$this->session->set_flashdata('source_del_msg', $msg);
			redirect('/settings/source_manager');
		}
	}
}