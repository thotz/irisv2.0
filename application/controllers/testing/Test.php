<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends MY_Controller 
{

	public function __construct()
	{
		parent::__construct();
		ini_set('max_execution_time', 300); //300 seconds = 5 minutes
		ini_set('memory_limit','1512M');
		
		
	}
	
	public function index($param1="default param")
	{
		print $param1;
		$this->load->view('testing/test');
		
		
	}
	
	public function newfunction($param1="default param")
	{
		print $param1;
	}
	
	public function testsphinx() 
	{
		$this->load->library('sphinxclient');
		$sphinx = new SphinxClient();
		
		//$sphinx->SetFilter("applicant_id", array('L-0186-12'));
		//$result = $sphinx->Query('dimaculangan ric*', 'main_personal_index delta_personal_index');
		$result = $sphinx->Query('dimaculanga*');
		//$result = $sphinx->Query('A-0001-13*');
		debug($result);
	}
	
	public function testsphinxapi() 
	{
		$this->load->library('SphinxClient');
		$sphinx = new SphinxClient();
		
		//$sphinx->SetFieldWeights(array('lname' => 10, 'fname' => 40));
		//$sphinx->SetSortMode(SPH_SORT_EXTENDED2, '@lname DESC, fname DESC');
		$sphinx->SetLimits(0,10);
		
		//$sphinx->SetFilter("applicant_id", array('L-0186-12'));
		//$result = $sphinx->Query('dimaculangan ric*', 'main_personal_index delta_personal_index');
		//$result = $sphinx->Query('tes* ','rt_personal');
		//$sphinx->SetFieldWeights(array('lname' => 10, 'fname' => 40, 'mname' => 60));
		//$sphinx->SetSortMode(SPH_SORT_ATTR_ASC, '@lname ASC');
		
		$result = $sphinx->Query('rt_lname1*');
		debug($result);
		if (count($result)) {
			if (count($result['matches'])) {
				$arr_matches = implode(',',array_keys($result['matches']));
				
				$query = $this->db->query("select applicant_id,lname,fname,mname,birthdate,status from personal where id in (".$arr_matches.")");
				$query_result = $query->result_array();
				//debug($query_result);
			}
			
			debug($result);
		}
	}
	
	public function testsphinxrt()
	{
		$this->load->library('SphinxRT');
		$sphinx = new SphinxRT();
		
		debug($sphinx);
	}
	
	public function rt_insert_personal()
	{
		$this->load->library('SphinxRT');
		
		$data = array(
				'id' => 308198,
				'status' => 'ACTIVE',
				'applicant_id' => 'Z-0001-16',
				'lname' => 'rt_lname1',
				'fname' => 'rt_fname1',
				'mname' => 'rt_mname1'
		);
		
		$data = array(
				'id' => 308199,
				'status' => 'ACTIVE',
				'applicant_id' => 'Z-0001-16',
				'lname' => 'rt lname1',
				'fname' => 'rt fname1',
				'mname' => 'rt mname1'
		);
		
		$this->sphinxrt->insert('rt_personal', $data);
		
	}
	
	public function testnewsrt()
	{
        $this->load->model('sphinx/sphinxrt_model');
        $this->load->library('SphinxRT');
		
		$news = $this->sphinxrt_model->reindex();
		

	}
	
	public function rt_index_personal()
	{
		$this->load->model('sphinx/sphinxrt_model');
		$this->load->library('SphinxRT');
		
		$news = $this->sphinxrt_model->reindex_personal();
		
	}
	
	public function connect_sms()
	{
		print "<pre>";
		
		debug(dbSMS());
		
		echo dbSMS()->count_all('outbox')."<br>";
		echo $this->db->platform()."<br>";
		echo $this->db->version()."<br>";
		
		print_r($this->db->list_tables());
		print_r($this->db->table_exists('personal_status'));
		print_r($this->db->list_fields('personal_status'));
		print_r($this->db->field_exists('name','personal_status'));
		
		$query = dbSMS()->query("select * from outbox where sender='373'");
		$result = $query->num_fields();
		debug($result);
	}
	
	public function tbl_field_list()
	{
		print "<pre>";
		print_r($this->db->list_fields('line_up'));
		
		$applicant_id = 'A-0709-13';
		$manpower_rid = '2323';
		$mr_pos_id = '3434';
		$evaluator = '340';
		
		$new_data = array();
		$arr_data = array(
				'applicant_id' => $applicant_id,
				'manpower_rid' => $manpower_rid,
				'mr_pos_id' => $mr_pos_id,
				'evaluator' => $evaluator,
		);
		foreach ($this->db->list_fields('line_up') as $key=>$value) {
			$new_data[$value] = isset($arr_data[$value]) ? $arr_data[$value] : '';
		}
		debug($new_data);
	}
	
	public function dbconnect()
	{
		//$this->load->library('dbConnect');
		//$db_sms = new dbConnect('SMSDB');
		//debug($db_sms);
	}
}
