<?php
class Cache extends My_controller
{
	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		print "<pre>";
		
		$tmp_object = new stdClass;
		$tmp_object->str_attr = 'test';
		$tmp_object->int_attr = 123;
		
// 		$memcache = new Memcache;
// 		$memcache->connect('localhost', 11211) or die ("Could not connect");
		
// 		$version = $memcache->getVersion();
// 		echo "Server's version: ".$version."<br/>\n";
		
// 		$memcache->set('key', $tmp_object, false, 10) or die ("Failed to save data at the server");
// 		echo "Store data in the cache (data will expire in 10 seconds)<br/>\n";
// 		$get_result = $memcache->get('key');
// 		echo "Data from the cache:<br/>\n";
		
// 		var_dump($get_result);
		
		$this->cache->save('key', $tmp_object, 30);
		$get_result = $this->cache->get('key');
		//var_dump($this->cache->memcached->get_metadata('key'));
		var_dump($get_result);
		
	}
	
}