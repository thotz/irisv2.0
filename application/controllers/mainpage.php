<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mainpage extends My_Controller {

	public function __construct(){
		parent::__construct();

		$this->load->model('mainpage_model');
	}
	
	public function index()
	{
		$result_app = $this->mainpage_model->get_total_applicant();
		$result_prin = $this->mainpage_model->get_total_principal();
		
		if ( ! $this->cache->get('chart_source')) {
			$result = $this->mainpage_model->chart_sourceofapplication();
			$this->cache->save('chart_source', $result, 43200);	/* Expires every 12 Hours */
		}
		$result = $this->cache->get('chart_source');
		
		$from_month = "";
		$to_month = "";
		$value_online = '[]';
		$value_workabroad = '[]';
		$value_direct = '[]';
		$mylinechart = "";
		if ($result) {
			/* Get the Label Date */
			$str_labels_online = "";
			if (isset($result['online'])) {
				foreach(array_keys($result['online']) as $key => $val) {
					$str_labels_online .= '"'.$val.'",';
				}
			
				$labels = '['.substr($str_labels_online, 0, -1).']';
				$from_month = current(array_keys($result['online']));
				$to_month = key( array_slice( $result['online'], -1, 1, TRUE ) );
		
				/* Online Value */
				$value_online = '['.implode(",",$result['online']).']';
				
				/* Workabroad Value */
				$value_workabroad = '['.implode(",",$result['workabroad']).']';
				
				/* Direct Value */
				$value_direct = '['.implode(",",$result['direct']).']';
				
				//$labels_online = '["January yeeh", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]';
				//$value = '[2500,2500,2500,2500,2500,2500,2500,2500,2500,2250,2500,2500]';
			}
			$mylinechart = '
			<script>
				var mylinechart = {
					labels:'.$labels.',
					value_online :'.$value_online.',
					value_workabroad :'.$value_workabroad.',
					value_direct :'.$value_direct.'
				};
			</script>';
		}
		
		$this->template->set('file_javascript', array(
				'js/plugins/chartjs/Chart.min.js', 
				'js/plugins/morris/raphael-min.js', 
				'js/plugins/morris/morris.min.js', 
				'js/dashboard.js'
		));
		$this->template->set('file_css', array(
				'js/plugins/morris/morris.css', 
				'css/dashboard.css', 
				'js/plugins/datatables/dataTables.bootstrap.css'
		));
		$data = array(
				'total_app' => substr_replace($result_app[0]['total_app'], ',', -3).substr($result_app[0]['total_app'], -3),
				'total_prin' => substr_replace($result_prin[0]['total_prin'], ',', -3) . substr($result_prin[0]['total_prin'], -3),
				'from_month'=> $from_month,
				'to_month'	=> $to_month,
				'mylinechart' => $mylinechart
		);
		
		$this->template->view('mainpage/dashboard', $data);
	}
	
	public function test(){
		$this->template->view('mainpage/test', array('what'=>$_GET));
	}
}

/* End of file Mainpage.php */
/* Location: ./application/controllers/mainpage.php */