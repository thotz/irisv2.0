<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends My_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('auth_model', 'auth', true);

	}
	
	public function index(){
		/** Redirect to Mainpage if already Logged */
		if ( is_login() ) redirect(base_url().'mainpage/');
		
		redirect('auth/login');
	}
	
	public function login(){
		if (isset($_POST['submit']) && strtolower($_POST['submit']) == 'submit') {
			$result = $this->auth->check_login();

			if (count($result) > 0) {
				/** Set user information session */
				$user_info = array(
					'iris_user_id' => $result['user_id'],
					'iris_user_name' => $result['username'],
					'iris_user_branch_id' => $result['branch_id'],
					'iris_user_access' => $result['access_id'],
				);
				$this->session->set_userdata($user_info);
				
				/** Create menu */
				if($result['access_id'] != '5') {
					//$current_menu = APPPATH."libraries/menu/custom_".$result['access_id'].".php";
					//if (!file_exists($current_menu)) {
						//@unlink($current_menu);
						//require_once(APPPATH."libraries/menu/custom_writer.php");
					//}
					
					/** Temporary - Delete Menu every login, it must be in the Manage Menu */
					$this->cache->delete(HTTP_HOST.'menu_user_access_'.$result['access_id']);
					
				}
				
				redirect(base_url().'mainpage/');
			}
			
			$msg = 'Invalid Username/Password';
			if ($_POST['username'] == '' or $_POST['password'] == '') $msg = 'You did not fill in all the fields!';
			
			$array_data = array(
				'msg' => $msg,
			);
			$this->template->view('auth/login', $array_data);
		} else {
			/** Redirect to Mainpage if already Logged */
			if ( is_login() ) redirect(base_url().'mainpage/');
			
			$this->template->view('auth/login');
		}
	}
	
	public function logout(){
		$this->session->sess_destroy();

		redirect('auth/login');
	}
}

/* End of file auth.php */
/* Location: ./application/controllers/auth.php */