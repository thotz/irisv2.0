<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Emergency_cases extends My_controller
{
	public function __construct()
	{
		parent::__construct();
		
		/** Set title for all function */
		set_header_title("Emergency Alert");
	}
	
	public function index()
	{
		
		$this->template->view('alert/emergency_cases/index');
	}
}