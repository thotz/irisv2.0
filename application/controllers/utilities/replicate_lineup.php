<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Replicate_lineup extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
		ini_set('max_execution_time', 300); //300 seconds = 5 minutes
		
	}
	
	public function lineup_training() 
	{
		$query = $this->db->query("select applicant_id,cellphone,office_phone,tel_postcode,home_phone,skypemail,email
				from personal where 1 
				#limit 0, 100000
				");
		
		$result = $query->result_array();

		$return = $this->db->insert_batch('personal_contact', $result);
		debug($return);
		/**
		 ALTER TABLE personal_copy
		 DROP COLUMN cellphone,
		 DROP COLUMN office_phone,
		 DROP COLUMN tel_postcode,
		 DROP COLUMN home_phone,
		 DROP COLUMN skypemail,
		 DROP COLUMN email;
		 */
	}
	
	public function lineup_employmentoffer()
	{
		$query = $this->db->query("select 
				applicant_id,address1,address2,address_city,address_city2,address_zip,branch_id,branch_id1
				from personal where 1
				#limit 0, 100000
				");
		$result = $query->result_array();

		$return = $this->db->insert_batch('personal_address', $result);
		debug($return);
		/**
		 ALTER TABLE personal_copy
		 DROP COLUMN address1,
		 DROP COLUMN address2,
		 DROP COLUMN address_city,
		 DROP COLUMN address_city2,
		 DROP COLUMN address_zip,
		 DROP COLUMN branch_id,
		 DROP COLUMN branch_id1;
		 */
	}
	
	public function lineup_clientcv()
	{
		$query = $this->db->query("select
				applicant_id,cv_applicant,cv_applicant_pdf,cv_applicant_other_doc,picture,pictaken,picaction,pic_request,pic_request_date
				from personal where 1
				#limit 0, 100000
				");
		$result = $query->result_array();
	
		$return = $this->db->insert_batch('personal_cv', $result);
		debug($return);
		/**
		 ALTER TABLE personal_copy
		 DROP COLUMN cv_applicant,
		 DROP COLUMN cv_applicant_pdf,
		 DROP COLUMN cv_applicant_other_doc,
		 DROP COLUMN picture,
		 DROP COLUMN pictaken,
		 DROP COLUMN picaction,
		 DROP COLUMN pic_request,
		 DROP COLUMN pic_request_date;
		 */
	}
	
	
}