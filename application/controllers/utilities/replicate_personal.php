<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Replicate_personal extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
		ini_set('max_execution_time', 300); //300 seconds = 5 minutes
		ini_set('memory_limit','1512M');
		
		/** Clear the Tables
		delete from personal_contact;
		delete from personal_address;
		delete from personal_cv;
		delete from personal_assessment;
		delete from assessment_otherinfo;
		

		 */
	}
	
	public function personal_contact() 
	{
		$query = $this->db->query("select applicant_id,cellphone,office_phone,tel_postcode,home_phone,skypemail,email
				#,address1,address2,address_city,address_city2,address_zip,branch_id,branch_id1
				#,cv_applicant,cv_applicant_pdf,cv_applicant_other_doc,picture,pictaken,picaction,pic_request,pic_request_date
				#,is_na_education,is_na_license,is_na_training,is_na_tradetest,is_na_employment,is_na_beneficiaries,for_assessment,for_assessment_date,full_encoded_sms,full_encoded_by,full_encoded_date,full_encoded_priority,full_encoded_remarks
				#,driver_local,driver_local_option,driver_int,driver_type,languagespeak,languageread,driver_code,driver_country,driver_validity,skill_computer,skill_computer_level,language_option,language_country
				from personal where 1 
				#limit 0, 100000
				");
		
		$result = $query->result_array();

		$return = $this->db->insert_batch('personal_contact', $result);
		debug($return);
		
		
		/**
		 * 
			CREATE TABLE `personal_contact` (
			  `applicant_id` varchar(11) NOT NULL,
			  `cellphone` varchar(60) DEFAULT NULL,
			  `office_phone` varchar(60) DEFAULT NULL,
			  `tel_postcode` varchar(60) DEFAULT NULL,
			  `home_phone` varchar(60) DEFAULT NULL,
			  `skypemail` varchar(100) DEFAULT NULL,
			  `email` varchar(100) DEFAULT NULL,
			  PRIMARY KEY (`applicant_id`),
			  KEY `applicant_id` (`applicant_id`) USING BTREE
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;
			
		 */
		
		/**
		 ALTER TABLE personal
		 DROP COLUMN cellphone,
		 DROP COLUMN office_phone,
		 DROP COLUMN tel_postcode,
		 DROP COLUMN home_phone,
		 DROP COLUMN skypemail,
		 DROP COLUMN email;
		 */
	}
	
	public function personal_address()
	{
		$query = $this->db->query("select 
				applicant_id,address1,address2,address_city,address_city2,address_zip,perm_no,perm_st,perm_city,perm_prov,perm_zip
				from personal where 1
				#limit 0, 100000
				");
		$result = $query->result_array();

		$return = $this->db->insert_batch('personal_address', $result);
		debug($return);
		
		
		/**
		 CREATE TABLE `personal_address` (
		 `applicant_id` varchar(11) NOT NULL,
		 `address1` varchar(255) DEFAULT NULL,
		 `address2` varchar(255) DEFAULT NULL,
		 `address_city` int(10) DEFAULT NULL,
		 `address_city2` int(10) DEFAULT NULL,
		 `address_zip` int(10) DEFAULT NULL,
		 `branch_id` int(10) DEFAULT NULL,
		 `branch_id1` int(10) DEFAULT NULL,
		 `perm_no` varchar(150) DEFAULT NULL,
		 `perm_st` varchar(200) DEFAULT NULL,
		 `perm_city` varchar(200) DEFAULT NULL,
		 `perm_prov` varchar(200) DEFAULT NULL,
		 `perm_zip` varchar(10) DEFAULT NULL,
		 PRIMARY KEY (`applicant_id`),
		 KEY `applicant_id` (`applicant_id`) USING BTREE
		 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
		 */
		
		/**
		 ALTER TABLE personal_copy
		 DROP COLUMN address1,
		 DROP COLUMN address2,
		 DROP COLUMN address_city,
		 DROP COLUMN address_city2,
		 DROP COLUMN address_zip,
		 
		 DROP COLUMN perm_no,
		 DROP COLUMN perm_st,
		 DROP COLUMN perm_city,
		 DROP COLUMN perm_prov,
		 DROP COLUMN perm_zip
		 ;
		 */
	}
	
	public function personal_cv()
	{
		$query = $this->db->query("select
				applicant_id,cv_applicant,cv_applicant_pdf,cv_applicant_other_doc,picture,pictaken,picaction,pic_request,pic_request_date
				from personal where 1
				#limit 0, 100000
				");
		$result = $query->result_array();
	
		$return = $this->db->insert_batch('personal_cv', $result);
		debug($return);
		
		/**
		 * 
			CREATE TABLE `personal_cv` (
			  `applicant_id` varchar(11) NOT NULL,
			  `cv_applicant` varchar(100) DEFAULT NULL,
			  `cv_applicant_pdf` varchar(100) DEFAULT NULL,
			  `cv_applicant_other_doc` varchar(100) DEFAULT NULL,
			  `picture` varchar(100) DEFAULT NULL,
			  `pictaken` varchar(50) DEFAULT NULL,
			  `picaction` int(10) DEFAULT NULL,
			  `pic_request` int(10) DEFAULT NULL,
			  `pic_request_date` datetime DEFAULT NULL,
			  PRIMARY KEY (`applicant_id`),
			  KEY `applicant_id` (`applicant_id`) USING BTREE
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;		 * 
		 */
		
		/**
		 ALTER TABLE personal
		 DROP COLUMN cv_applicant,
		 DROP COLUMN cv_applicant_pdf,
		 DROP COLUMN cv_applicant_other_doc,
		 DROP COLUMN picture,
		 DROP COLUMN pictaken,
		 DROP COLUMN picaction,
		 DROP COLUMN pic_request,
		 DROP COLUMN pic_request_date;
		 */
	}
	
	public function personal_assessment()
	{
		$query = $this->db->query("select
				applicant_id,is_na_education,is_na_license,is_na_training,is_na_tradetest,is_na_employment,is_na_beneficiaries,for_assessment,for_assessment_date,full_encoded_sms,full_encoded_by,full_encoded_date,full_encoded_priority,full_encoded_remarks
				,qc,qc_by,qc_date
				from personal where 1
				#limit 0, 100000
				");
		$result = $query->result_array();
	
		$return = $this->db->insert_batch('personal_assessment', $result);
		debug($return);
		/**
		 * 
		CREATE TABLE `personal_assessment` (
		  `applicant_id` varchar(11) NOT NULL,
		  `is_na_education` int(11) DEFAULT NULL,
		  `is_na_license` int(11) DEFAULT NULL,
		  `is_na_training` int(11) DEFAULT NULL,
		  `is_na_tradetest` int(11) DEFAULT NULL,
		  `is_na_employment` int(11) DEFAULT NULL,
		  `is_na_beneficiaries` int(11) DEFAULT NULL,
		  `for_assessment` int(11) DEFAULT NULL,
		  `for_assessment_date` datetime DEFAULT NULL,
		  `full_encoded_sms` int(11) DEFAULT NULL,
		  `full_encoded_by` int(11) DEFAULT NULL,
		  `full_encoded_date` datetime DEFAULT NULL,
		  `full_encoded_priority` int(11) DEFAULT NULL,
		  `full_encoded_remarks` text,
		  `qc` enum('N','Y') DEFAULT 'N',
		  `qc_by` varchar(50) DEFAULT NULL,
		  `qc_date` datetime DEFAULT NULL,
		  PRIMARY KEY (`applicant_id`),
		  KEY `applicant_id` (`applicant_id`) USING BTREE
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;
		 * 
		 */
		
		/**
		 ALTER TABLE personal
		 DROP COLUMN is_na_education,
		 DROP COLUMN is_na_license,
		 DROP COLUMN is_na_training,
		 DROP COLUMN is_na_tradetest,
		 DROP COLUMN is_na_employment,
		 DROP COLUMN is_na_beneficiaries,
		 
		 DROP COLUMN for_assessment,
		 DROP COLUMN for_assessment_date,
		 DROP COLUMN full_encoded_sms,
		 DROP COLUMN full_encoded_by,
		 DROP COLUMN full_encoded_date,
		 DROP COLUMN full_encoded_priority,
		 DROP COLUMN full_encoded_remarks,
		 
		 DROP COLUMN qc,
		 DROP COLUMN qc_by,
		 DROP COLUMN qc_date
		 ;
		 */
	}
	
	public function assessment_otherinfo()
	{
		$query = $this->db->query("select
				applicant_id,driver_local,driver_local_option,driver_int,driver_type,languagespeak,languageread,driver_code,driver_country,driver_validity,skill_computer,skill_computer_level,language_option,language_country
				from personal where 1
				#limit 0, 100000
				");
		$result = $query->result_array();
	
		$return = $this->db->insert_batch('assessment_otherinfo', $result);
		debug($return);
		
		/**
		 * 
		CREATE TABLE `assessment_otherinfo` (
		  `applicant_id` varchar(11) NOT NULL,
		  `driver_local` int(11) DEFAULT NULL,
		  `driver_local_option` varchar(100) DEFAULT NULL,
		  `driver_int` int(11) DEFAULT NULL,
		  `driver_type` varchar(50) DEFAULT NULL,
		  `languagespeak` varchar(100) DEFAULT NULL,
		  `languageread` varchar(100) DEFAULT NULL,
		  `driver_code` varchar(50) DEFAULT NULL,
		  `driver_country` varchar(100) DEFAULT NULL,
		  `driver_validity` date DEFAULT NULL,
		  `skill_computer` text,
		  `skill_computer_level` varchar(100) DEFAULT NULL,
		  `language_option` varchar(255) DEFAULT NULL,
		  `language_country` varchar(100) DEFAULT NULL,
		  PRIMARY KEY (`applicant_id`),
		  KEY `applicant_id` (`applicant_id`) USING BTREE
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;		 */
		
		
		/**
		 ALTER TABLE personal_copy
		 DROP COLUMN driver_local,
		 DROP COLUMN driver_local_option,
		 DROP COLUMN driver_int,
		 DROP COLUMN driver_type,
		 DROP COLUMN languagespeak,
		 DROP COLUMN languageread,
		 DROP COLUMN driver_code,
		 DROP COLUMN driver_country,

		 DROP COLUMN driver_validity,
		 DROP COLUMN skill_computer,
		 DROP COLUMN skill_computer_level,
		 DROP COLUMN language_option,
		 DROP COLUMN language_country;
		 */
	}
	
	/**	All column to drop 
	 * 
		 ALTER TABLE personal
		 DROP COLUMN cellphone,
		 DROP COLUMN office_phone,
		 DROP COLUMN tel_postcode,
		 DROP COLUMN home_phone,
		 DROP COLUMN skypemail,
		 DROP COLUMN email,
		 DROP COLUMN address1,
		 DROP COLUMN address2,
		 DROP COLUMN address_city,
		 DROP COLUMN address_city2,
		 DROP COLUMN address_zip,
		 
		 DROP COLUMN perm_no,
		 DROP COLUMN perm_st,
		 DROP COLUMN perm_city,
		 DROP COLUMN perm_prov,
		 DROP COLUMN perm_zip,
		 DROP COLUMN cv_applicant,
		 DROP COLUMN cv_applicant_pdf,
		 DROP COLUMN cv_applicant_other_doc,
		 DROP COLUMN picture,
		 DROP COLUMN pictaken,
		 DROP COLUMN picaction,
		 DROP COLUMN pic_request,
		 DROP COLUMN pic_request_date,
		 DROP COLUMN is_na_education,
		 DROP COLUMN is_na_license,
		 DROP COLUMN is_na_training,
		 DROP COLUMN is_na_tradetest,
		 DROP COLUMN is_na_employment,
		 DROP COLUMN is_na_beneficiaries,
		 
		 DROP COLUMN for_assessment,
		 DROP COLUMN for_assessment_date,
		 DROP COLUMN full_encoded_sms,
		 DROP COLUMN full_encoded_by,
		 DROP COLUMN full_encoded_date,
		 DROP COLUMN full_encoded_priority,
		 DROP COLUMN full_encoded_remarks,
		 DROP COLUMN qc,
		 DROP COLUMN qc_by,
		 DROP COLUMN qc_date,
		 
		 DROP COLUMN driver_local,
		 DROP COLUMN driver_local_option,
		 DROP COLUMN driver_int,
		 DROP COLUMN driver_type,
		 DROP COLUMN languagespeak,
		 DROP COLUMN languageread,
		 DROP COLUMN driver_code,
		 DROP COLUMN driver_country,

		 DROP COLUMN driver_validity,
		 DROP COLUMN skill_computer,
		 DROP COLUMN skill_computer_level,
		 DROP COLUMN language_option,
		 DROP COLUMN language_country;
	 */
	
}