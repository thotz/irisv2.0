<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed.');

class Sphinx extends My_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('sphinx/sphinxrt_model', 'sphinxrt', true);
	}
	
	public function index()
	{
		$this->sphinxrt->reindex('testing_index');
	}
	
	
}
