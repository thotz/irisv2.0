<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Search_applicant extends CI_Controller {

	public function __construct(){
		parent::__construct();
	}
	
	public function ajax_search(){
		$this->load->model('search_model');
		$this->load->model('sphinx/sphinxrt_model');
		
		$search_tab      	= $_GET['search_tab'];
		$search_keyword  	= $_GET['search_keyword'];
		$mr_status        	= isset($_GET['search_mr_status']) ? $_GET['search_mr_status'] : '';
		$search_link     	= $_GET['search_link'];
		$search_param    	= $_GET['search_param'];
		$additional_param 	= $_GET['search_param_additional'];
		$page           	 = $_GET['page'];

		$parameters		 = "";
		$page_limit      = 10;
		
		$action = $_GET['search_tab'];
		switch ($action) {
			case 'computer':
				
				$aResult = $this->search_model->search_by_applicant($search_keyword, $page, $page_limit);
				
				$html = "
                        <thead>
                            <tr>
                                <th>COMPUTER NO.</th>
                                <th>NAME</th>
                                <th>BIRTHDATE</th>
                                <th>DB STATUS</th>
                                <th></th>
                            </tr>
                        </thead>";
				
				//"<td class=\"text-right\"><a href=\"".base_url()."reception/reported_today?applicant_id={$aVal_dtl['applicant_id']}\" type=\"button\" class=\"btn btn-default btn-sm btn-flat\" data-toggle=\"tooltip\" data-placement=\"top\" data-original-title=\"View File\"><i class=\"fa fa-folder-open\"></i></a></td>"
				if(isset($aResult) && $aResult['total_found'] > 0){
					foreach ($aResult['details'] as $aVal_dtl){
						$name = utf8_encode($aVal_dtl['lname']).", ".utf8_encode($aVal_dtl['fname'])." ".utf8_encode($aVal_dtl['mname']);
						if ($additional_param) $additional_param = '&'.$additional_param;
						$html .= "<tr>
						<td>{$aVal_dtl['applicant_id']}</td>
						<td>".utf8_decode($name)."</td>
						<td>{$aVal_dtl['birthdate']}</td>
						<td>{$aVal_dtl['status']}</td>
						
			            <td class=\"text-right\" style=\"width:117px;\">
			                
			                <div class=\"btn-group\">
			                    <a href=\"".base_url()."reception/reported_today?applicant_id={$aVal_dtl['applicant_id']}\" type=\"button\" class=\"btn btn-sm btn-default btn-flat\" data-toggle=\"tooltip\" data-placement=\"top\" data-original-title=\"Follow up\"><i class=\"fa fa-arrow-circle-up\"></i></a>
			                    <a href=\"../applicant/applicant-assessment.php\" type=\"button\" class=\"btn btn-sm btn-default btn-flat\" data-toggle=\"tooltip\" data-placement=\"top\" data-original-title=\"Assessment\"><i class=\"fa fa-paste\"></i></a>
			                    <a href=\"../applicant/applicant-lineup.php\" type=\"button\" class=\"btn btn-sm btn-default btn-flat\" data-toggle=\"tooltip\" data-placement=\"top\" data-original-title=\"Lineup\"><i class=\"fa fa-th-list\"></i></a>
			                </div>
			                
			            </td>
						</tr>";
					}
					$rec_start = 1;
				}else{
					$html .= "<tr>
							<td colspan=\"5\" align=\"center\"><b>No match found.</b></td>
						</tr>";
					$rec_start = 0;
				}
				
				$html .= "<br>";
				echo json_encode(array('html'=>$html,
						'qry_str'=>$aResult['qry_str'],
						'total_found'=>$aResult['total_found'],
						'all_total_found'=>$aResult['all_total_found'],
						'total_pages'=>0,
						'page_limit'=>$page_limit,
						'record_start'=>($page==0)?$rec_start:($page*$page_limit),
						'record_end'=>($page==0)?intval($aResult['total_found']):(($page*$page_limit) + $aResult['total_found']),
						'pageno'=>$page));
				break;
				
			case 'name':
				$aResult = $this->sphinxrt_model->search_by_name($search_keyword, $page, $page_limit);
				//$aResult = $this->search_model->search_by_name($search_keyword, $page, $page_limit);

				$html = "
                        <thead>
                            <tr>
                                <th>COMPUTER NO.</th>
                                <th>NAME</th>
                                <th>BIRTHDATE</th>
                                <th>DB STATUS</th>
                                <th></th>
                            </tr>
                        </thead>";
				
				
				if(isset($aResult) && $aResult['total_found'] > 0){
					foreach ($aResult['details'] as $aVal_dtl){
						$name = utf8_encode($aVal_dtl['lname']).", ".utf8_encode($aVal_dtl['fname'])." ".utf8_encode($aVal_dtl['mname']);
						if ($additional_param) $additional_param = '&'.$additional_param;
						$html .= "<tr>
						<td>{$aVal_dtl['applicant_id']}</td>
						<td>".utf8_decode($name)."</td>
						<td>{$aVal_dtl['birthdate']}</td>
						<td>{$aVal_dtl['status']}</td>
			            <td class=\"text-right\" style=\"width:117px;\">
			                
			                <div class=\"btn-group\">
			                    <a href=\"".base_url()."reception/reported_today?applicant_id={$aVal_dtl['applicant_id']}\" type=\"button\" class=\"btn btn-sm btn-default btn-flat\" data-toggle=\"tooltip\" data-placement=\"top\" data-original-title=\"Follow up\"><i class=\"fa fa-arrow-circle-up\"></i></a>
			                    <a href=\"../applicant/applicant-assessment.php\" type=\"button\" class=\"btn btn-sm btn-default btn-flat\" data-toggle=\"tooltip\" data-placement=\"top\" data-original-title=\"Assessment\"><i class=\"fa fa-paste\"></i></a>
			                    <a href=\"../applicant/applicant-lineup.php\" type=\"button\" class=\"btn btn-sm btn-default btn-flat\" data-toggle=\"tooltip\" data-placement=\"top\" data-original-title=\"Lineup\"><i class=\"fa fa-th-list\"></i></a>
			                </div>
			                
			            </td>
						
						</tr>";
					}
					$rec_start = 1;
				}else{
					$html .= "<tr>
							<td colspan=\"5\" align=\"center\"><b>No match found.</b></td>
						</tr>";
					$rec_start = 0;
				}
				
				$html .= "<br>";
				echo json_encode(array('html'=>$html,
						'qry_str'=>$aResult['qry_str'],
						'total_found'=>$aResult['total_found'],
						'all_total_found'=>$aResult['all_total_found'],
						'total_pages'=>0,
						'page_limit'=>$page_limit,
						'record_start'=>($page==0)?$rec_start:($page*$page_limit),
						'record_end'=>($page==0)?intval($aResult['total_found']):(($page*$page_limit) + $aResult['total_found']),
						'pageno'=>$page));
				break;
				
			case 'mr':
				//debug($this->search_model->search_applicant());
				//echo $this->search_model->search_applicant();
				
				$aResult = $this->search_model->search_by_mr($search_keyword, $page, $page_limit, "", $mr_status);
				
				$html = "
                        <thead>
                            <tr>
                                <th>REREFENCE NO.</th>
                                <th>PRINCIPAL</th>
                                <th>PRINCIPAL CODE</th>
                                <th>STATUS</th>
                                <th></th>
                            </tr>
                        </thead>";
				
				
				if(isset($aResult) && $aResult['total_found'] > 0){
					foreach ($aResult['details'] as $aVal_dtl){
						if ($additional_param) $additional_param = '&'.$additional_param;
						$html .= "<tr>
						<td>{$aVal_dtl['ref_no']}</td>
						<td>{$aVal_dtl['name']}</td>
						<td>{$aVal_dtl['prin_code']}</td>
						<td>{$aVal_dtl['status']}</td>
						<td class=\"text-right\"><a href=\"\" type=\"button\" class=\"btn btn-default btn-sm btn-flat\" data-toggle=\"tooltip\" data-placement=\"top\" data-original-title=\"View File\"><i class=\"fa fa-folder-open\"></i></a></td>
						</tr>";
					}
					$rec_start = 1;
				}else{
					$html .= "<tr>
							<td colspan=\"5\" align=\"center\"><b>No match found.</b></td>
						</tr>";
					$rec_start = 0;
				}
				
				$html .= "<br>";
				echo json_encode(array('html'=>$html,
						'qry_str'=>$aResult['qry_str'],
						'total_found'=>$aResult['total_found'],
						'all_total_found'=>$aResult['all_total_found'],
						'total_pages'=>0,
						'page_limit'=>$page_limit,
						'record_start'=>($page==0)?$rec_start:($page*$page_limit),
						'record_end'=>($page==0)?intval($aResult['total_found']):(($page*$page_limit) + $aResult['total_found']),
						'pageno'=>$page));
				break;
				break;
				
			default:
		}
	}
	
	public function lists(){
		//debug($this->db);
		//$CI =& get_instance();

		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
		 * Easy set variables
		 */
		
		/* Array of database columns which should be read and sent back to DataTables. Use a space where
		 * you want to insert a non-database field (for example a counter or static image)
		 */
		 
	//	$smsid = getid("menu_high_id","menu_high","name","SMS");
	//	$sms_feature=getfeature("status","feature_module","id","".$smsid."","tablename","menu_high");
		$sms_feature = false;
	 	 
		$aColumns = array( 'applicant_id', 'applicant_name', 'last_updated', 'status', 'smsdel' );
		
		//$aWhereColumns = array( 'applicant_id', 'lname', 'fname' );
		$aWhereColumns = array( "applicant_id", "(concat(lname,', ',fname,' ',left(mname,1),'.'))", "lname", "fname" );
		
		/* Indexed column (used for fast and accurate table cardinality) */
		$sIndexColumn = "applicant_id";
		
		/* DB table to use */
		$sTable = "personal";
		
		/* Database connection information */
		$gaSql['user']       = $this->db->username;
		$gaSql['password']   = $this->db->password;;
		$gaSql['db']         = $this->db->database;;
		$gaSql['server']     = $this->db->hostname;;	
		
		/* REMOVE THIS LINE (it just includes my SQL connection user/pass) */
		//include( $_SERVER['DOCUMENT_ROOT']."/datatables/mysql.php" );		
		
		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
		 * If you just want to use the basic configuration for DataTables with PHP server-side, there is
		 * no need to edit below this line
		 */
		
		/* 
		 * MySQL connection
		 */
		$gaSql['link'] =  mysqli_connect( $gaSql['server'], $gaSql['user'], $gaSql['password']  ) or
			die( 'Could not open connection to server' );
		
		$temp = mysqli_select_db($gaSql['link'], $gaSql['db'] ) or 
			die( 'Could not select database '. $gaSql['db'] );

		/* 
		 * Paging
		 */
		$sLimit = "";
		if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
		{
			$sLimit = "LIMIT ". $_GET['iDisplayStart'] .", ".
				 $_GET['iDisplayLength'] ;
		}
		
		
		/*
		 * Ordering
		 */
		$sOrder = "";
		if ( isset( $_GET['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
			{
				if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]."
					 	". $_GET['sSortDir_'.$i]  .", ";
				}
			}
			
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		
		
		/* 
		 * Filtering
		 * NOTE this does not match the built-in DataTables filtering which does it
		 * word by word on any field. It's possible to do here, but concerned about efficiency
		 * on very large tables, and MySQL's regex functionality is very limited
		 */
		$sWhere = "";
		if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
		{
			$sWhere = "WHERE (";
			for ( $i=0 ; $i<count($aWhereColumns) ; $i++ )
			{
				$sWhere .= $aWhereColumns[$i]." LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ";
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		
		/* Individual column filtering */
		for ( $i=0 ; $i<count($aWhereColumns) ; $i++ )
		{
			if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
			{
				if ( $sWhere == "" )
				{
					$sWhere = "WHERE ";
				}
				else
				{
					$sWhere .= " AND ";
				}
				$sWhere .= $aWhereColumns[$i]." LIKE '%".mysql_real_escape_string($_GET['sSearch_'.$i])."%' ";
			}
		}
		
		
		/*
		 * SQL queries
		 * Get data to display
		 */
		/*
		$sQuery = "
			SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
			FROM   $sTable
			$sWhere
			$sOrder
			$sLimit
		";
		*/
		$sQuery = "SELECT SQL_CALC_FOUND_ROWS `applicant_id`, `birthdate`, `status`, 
			(concat(`lname`,', ',`fname`,' ',left(`mname`,1),'.')) as `applicant_name`
			FROM   $sTable
			$sWhere
			$sOrder
			$sLimit";
		$rResult = $this->db->query($sQuery);
	
		/* Data set length after filtering */
		$sQuery = "
			SELECT FOUND_ROWS()
		";
		$rResultFilterTotal = $this->db->query( $sQuery);
		$aResultFilterTotal = $rResultFilterTotal->result_array();
		$iFilteredTotal = $aResultFilterTotal[0];
		
		/* Total data set length */
		$sQuery = "
			SELECT COUNT(".$sIndexColumn.")
			FROM   $sTable
		";
		$rResultTotal = $this->db->query( $sQuery);
		$aResultTotal = $rResultTotal->result_array();
		$iTotal = $aResultTotal[0];
		
		
		/*
		 * Output
		 */
		$output = array(
			"sEcho" => intval($_GET['sEcho']),
			"iTotalRecords" => $iTotal,
			"iTotalDisplayRecords" => $iFilteredTotal,
			"aaData" => array()
		);

		foreach ($rResult->result_array() as $aRow)
		{
			$row = array();
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				if ( $aColumns[$i] == "last_updated" )
				{
					/* Special output formatting for 'birthdate' column */
					//$row[] = show_log_latest_search("personal",$aRow['applicant_id']);
				}
				elseif ( $aColumns[$i] == "smsdel" )
				{
					$sms = "";
					$del = "";
					if($sms_feature) {
						$sms = "<a onclick=\"javascript: jQuery.facebox({ajax:'sms_write_message_live_solo.php?boxes=".$aRow['applicant_id']."'});\" style=\"cursor:pointer;\" title=\"Send SMS\"><font color=\"#0000FF\"><img src=\"../img/cp.png\" /></a>";
						$sms .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
					}
					if(in_array($this->session->userdata('iris_user_access'),array())){
						$del = "<a href=\"sqldelete.php?what=applicant&applicant_id=".$aRow['applicant_id']."\" onclick=\"return confirm('Are you sure you want to delete this applicant?')\"><font color=\"maroon\">Delete</font></a>";
					}
	
					$row[] = $sms.$del;
				}
				else if ( $aColumns[$i] != ' ' )
				{
					if ( $aColumns[$i] == "applicant_id" or $aColumns[$i] == "applicant_name" )
					{
						$row[] = '<a href="'.$_GET['pagename'].'?'.$_GET['param'].'='.$aRow['applicant_id'].'">'.str_replace("Ñ","&Ntilde;",$aRow[ $aColumns[$i] ]).'</a>';
					} else {
						/* General output */
						$row[] = $aRow[ $aColumns[$i] ];
					}
				}
			}
			$output['aaData'][] = $aRow;
		}
		echo json_encode( $output );
	}

}
