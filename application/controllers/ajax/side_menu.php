<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Side_menu extends My_controller
{
	public function __construct()
	{
		parent::__construct();
		
	}
	
	public function index()
	{
		echo $this->global_model->get_side_menu();
		
		create_log("thotz", "side_menue", "testing", date('Y-m-d h:i:s'));
	}
}