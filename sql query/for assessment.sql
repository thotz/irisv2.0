select x.cv_location_id,x.applicant_id,p.date_reported,p.reporting_status
FROM (select max(cv_location_id) as cv_location_id FROM cv_location group by applicant_id) as cvs 
left join cv_location as x on x.cv_location_id = cvs.cv_location_id
left join personal as p on p.applicant_id = x.applicant_id 
where 1
and x.user_id not in ('234','235','237','247','253','255','257','267','274','281','286','288','299','303','309','317','320','322','331','332','343','345','368','418','437')	
and p.status in ('ACTIVE','ON POOL') 
and x.status = '' 
and p.source_id not in ('28')
and p.referral not in ('FP')
group by p.applicant_id

select x.cv_location_id,x.applicant_id,p.date_reported,p.reporting_status
FROM (select max(cv_location_id) as cv_location_id FROM cv_location group by applicant_id) as cvs 
left join cv_location as x on x.cv_location_id = cvs.cv_location_id
left join personal as p on p.applicant_id = x.applicant_id 
where 1
and x.user_id not in ('234','235','237','247','253','255','257','267','274','281','286','288','299','303','309','317','320','322','331','332','343','345','368','418','437')	
and p.status in ('ACTIVE','ON POOL') 
and x.status = '' 
and p.date_reported = CURDATE()
and p.reporting_status = 4 
and p.source_id not in ('28')
and p.referral not in ('FP')
group by p.applicant_id

select x.cv_location_id,x.applicant_id,p.date_reported,p.reporting_status
FROM (select max(cv_location_id) as cv_location_id FROM cv_location group by applicant_id) as cvs 
left join cv_location as x on x.cv_location_id = cvs.cv_location_id
left join personal as p on p.applicant_id = x.applicant_id 
where 1
and x.user_id not in ('234','235','237','247','253','255','257','267','274','281','286','288','299','303','309','317','320','322','331','332','343','345','368','418','437')	
and p.status in ('ACTIVE','ON POOL') 
and x.status = '' 
and p.applicant_id not in (select applicant_id from view_initial_lineup )
and p.date_reported <> CURDATE()
    #and p.method_report = 'Direct'
  #and p.method_report in ('Online','Direct')
and p.source_id not in ('28')
and p.referral not in ('FP')
group by p.applicant_id