select l.line_up_id,l.applicant_id,l.manpower_rid,l.mr_pos_id,p.branch_id,
IF(p.branch_id = 0, c.city_branch, p.branch_id) AS my_branch,c.city_branch,mr.ref_no,mr.pm,po.position_id,po.name as position_name, p.fname,p.lname, 
CONCAT(p.lname,', ',p.fname,' ', p.mname) as name,p.birthdate, p.sex,p.cv_applicant,p.cv_applicant_pdf,p.status,p.date_reported,p.cv_source,
				u.user_id,u.username,p.apply_date,ed.id as count_education,em.id as count_employment,p.is_na_education,p.is_na_employment, mr.email_attachment
				from line_up as l
				left join personal as p on p.applicant_id = l.applicant_id 
				left join mr_position mr_pos on mr_pos.mr_pos_id = l.mr_pos_id 
				left join positions as po on po.position_id = mr_pos.position_id 
				left join manpower_r as mr on mr.manpower_rid = l.manpower_rid
				left join users as u on u.user_id = mr_pos.mr_pos_rso
				left join branch as b on b.id = p.branch_id 
				left join city as c on c.city_id = p.address_city 
				left join education as ed on ed.applicant_id = p.applicant_id
				left join employment as em on em.applicant_id = p.applicant_id
				where 1 
				and l.line_up_id in (select line_up_id from view_initial_lineup where for_confirm_mode not in ('PS','NQ'))
				group by l.line_up_id
				order by mr.ref_no,po.name,p.lname asc
