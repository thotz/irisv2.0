
	$(function() {

            /* Line Chart */
            var lineChartData = {
                //labels: testchart(labels),
                labels: mylinechart.labels,
                datasets: [{
                    label: "Online",
                    fillColor: "rgba(60,141,188,0.9)",
                    strokeColor: "#3c8dbc",
                    pointColor: "#3c8dbc",
                    pointStrokeColor: "#3c8dbc",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(60,141,188,1)",
                    data: mylinechart.value_online
                },{
                    label: "Commercial",
                    fillColor: "rgba(60,141,188,0.9)",
                    strokeColor: "#dd4b39",
                    pointColor: "#dd4b39",
                    pointStrokeColor: "#dd4b39",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(60,141,188,1)",
                    data: mylinechart.value_workabroad
                },{
                    label: "Direct",
                    fillColor: "rgba(210, 214, 222, 1)",
                    strokeColor: "#00a65a",
                    pointColor: "#00a65a",
                    pointStrokeColor: "#00a65a",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(220,220,220,1)",
                    data: mylinechart.value_direct
                }]
            };

            var lineChartOptions = {
                //Boolean - If we should show the scale at all
                showScale: true,
                //Boolean - Whether grid lines are shown across the chart
                scaleShowGridLines: false,
                //String - Colour of the grid lines
                scaleGridLineColor: "rgba(0,0,0,.05)",
                //Number - Width of the grid lines
                scaleGridLineWidth: 1,
                //Boolean - Whether to show horizontal lines (except X axis)
                scaleShowHorizontalLines: true,
                //Boolean - Whether to show vertical lines (except Y axis)
                scaleShowVerticalLines: true,
                //Boolean - Whether the line is curved between points
                bezierCurve: true,
                //Number - Tension of the bezier curve between points
                bezierCurveTension: 0.3,
                //Boolean - Whether to show a dot for each point
                pointDot: false,
                //Number - Radius of each point dot in pixels
                pointDotRadius: 4,
                //Number - Pixel width of point dot stroke
                pointDotStrokeWidth: 1,
                //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
                pointHitDetectionRadius: 20,
                //Boolean - Whether to show a stroke for datasets
                datasetStroke: true,
                //Number - Pixel width of dataset stroke
                datasetStrokeWidth: 2,
                //Boolean - Whether to fill the dataset with a color
                datasetFill: true,
                //String - A legend template
                legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
                //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                maintainAspectRatio: true,
                //Boolean - whether to make the chart responsive to window resizing
                responsive: true
            };

            var lineChartCanvas = $("#lineChart").get(0).getContext("2d");
            var lineChart = new Chart(lineChartCanvas);
            lineChartOptions.datasetFill = false;
            lineChart.Line(lineChartData, lineChartOptions);
    
            //DONUT CHART
            var donutOption = {
                scaleFontFamily: "OpenSansRegular",
                scaleFontSize: 14,
            }
            var donut = new Morris.Donut({
                element: 'chartApplicantStatus',
                resize: true,
                colors: ["#e3337e", "#0b326b", "#827775", "#f5bd42", "#f05129", "#966eac", "#7bcbc0", "#b7cc94", "#f1c7dd", "#b09977"],
                style: { fontFamily: '\'OpenSansRegular\', sans-serif', lineHeight: '16px', fontSize: '14px' },
                data: [
                    {label: "Active", value: 59},
                    {label: "Black Listed", value: 30},
                    {label: "Dead File", value: 20},
                    {label: "Deployed", value: 35},
                    {label: "Excess", value: 15},
                    {label: "On Pool", value: 86},
                    {label: "Operations", value: 46},
                    {label: "Pooling", value: 99},
                    {label: "Reserved", value: 12},
                    {label: "Worked in Abroad", value: 60}
                ],
                hideHover: 'auto'
            },donutOption);
          
            //BAR CHART
            var bar = new Morris.Bar({
                element: 'bar-chart',
                resize: true,
                data: [
                    {y: '2006', a: 100, b: 90},
                    {y: '2007', a: 75, b: 65},
                    {y: '2008', a: 50, b: 40},
                    {y: '2009', a: 75, b: 65},
                    {y: '2010', a: 50, b: 40},
                    {y: '2011', a: 75, b: 65},
                    {y: '2012', a: 100, b: 90}
                ],
                barColors: ['#3c8dbc', '#75bbe4'],
                xkey: 'y',
                ykeys: ['a', 'b'],
                labels: ['Applicant 1', 'Applicant 2'],
                hideHover: 'auto'
            });
            
            // LINE CHART
            var line = new Morris.Line({
                element: 'line-chart',
                resize: true,
                data: [
                    {y: '2011 Q1', item1: 2666},
                    {y: '2011 Q2', item1: 2778},
                    {y: '2011 Q3', item1: 4912},
                    {y: '2011 Q4', item1: 3767},
                    {y: '2012 Q1', item1: 6810},
                    {y: '2012 Q2', item1: 5670},
                    {y: '2012 Q3', item1: 4820},
                    {y: '2012 Q4', item1: 15073},
                    {y: '2013 Q1', item1: 10687},
                    {y: '2013 Q2', item1: 8432}
                ],
                xkey: 'y',
                ykeys: ['item1'],
                labels: ['Applicant'],
                lineColors: ['#3c8dbc'],
                hideHover: 'auto'
            });
        });