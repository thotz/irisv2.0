	$(function() {
        /* Show and Hide for Logs */
        $('.show-logs').click(function(){
        	/* karl script 
            var parentID = $(this).parent().parent().attr('id');
            $('ul#'+parentID+' li').show();
            $('ul#'+parentID+' li button.hide-logs').show();
            $(this).hide(); */
            
//        	$('ul#datalogseducation li').show();
//            $('.hide-logs').show();
//            $(this).hide();
            
        });
        $('.hide-logs').click(function(){
        	/* karl script
            var parentID = $(this).parent().parent().attr('id');
            $('ul#'+parentID+' li').hide();
            $('ul#'+parentID+' li button.show-logs').show();
            $(this).hide(); */
        	
//        	$('ul#datalogseducation li').hide();
//            $('.show-logs').show();
//            $(this).hide();
        });
	});

	function isNumberKey(evt){
	  var charCode = (evt.which) ? evt.which : event.keyCode
	  if (charCode > 31 && (charCode < 48 || charCode > 57))
		  return false;
	  
	  return true;
	}

	function text_uppercase(item){
		document.getElementById(item.id).style.textTransform="uppercase";
	}
	
	function text_lowercase(item){
		document.getElementById(item.id).style.textTransform="lowercase";
	}
	
	function CheckExtension(fld) {
		var valid_extensions = /(.doc|.docx|.pdf|.xls)$/i;
		if (valid_extensions.test(fld.value)) return true;
			alert('The selected file is of the wrong type.');
			fld.value='';
			fld.select();
			fld.focus();
			return false;
	}

	function CheckExtensionword(fld) {
		var valid_extensions = /(.doc|.docx)$/i;
		if (valid_extensions.test(fld.value)) return true;
			alert('The selected file is of the wrong type.');
			fld.value='';
			fld.select();
			fld.focus();
			return false;
	}

	function CheckExtensionpdf(fld) {
		var valid_extensions = /(.pdf|.fdp)$/i;
		if (valid_extensions.test(fld.value)) return true;
			alert('The selected file is of the wrong type.');
			fld.value='';
			fld.select();
			fld.focus();
			return false;
	}