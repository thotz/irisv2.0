var search_tab_active = "computer";
var search_keyword = "";
var current_value = "";
var search_length = 0;
var search_now = false;

$(document).ready(function() {
//	$('#btn_modalsearch').click(function() {
//		if ($('#tab_modalsearch_mr').attr('class')) {
//			search_tab_active = "mr";
//		} else if ($('#tab_modalsearch_name').attr('class')) {
//			search_tab_active = "name";
//		} else if ($('#tab_modalsearch_computer').attr('class')) {
//			search_tab_active = "computer";
//		}
//		
//	});
//	
//	$('#tab_modalsearch_computer, #tab_modalsearch_name').click(function() {
//		if ($('#tab_modalsearch_mr').attr('class') == 'active') {
//			search_tab_active = "mr";
//		} else if ($('#tab_modalsearch_name').attr('class') == 'active') {
//			search_tab_active = "name";
//		} else if ($('#tab_modalsearch_computer').attr('class') == 'active') {
//			search_tab_active = "computer";
//		}
//	});

	if ( ! $('#modalSearch').hasClass('in')) {
		//alert("loaded");
	}

	/* IF ENTER KEY */
	$('#form_search_computer').bind("keypress", function(e){
		current_value = $('#search_computer').val();
		if (e.keyCode == 13) return false;							/* if enter key - do not redirect */
    });
	$('#form_search_name').bind("keypress", function(e){
		current_value = $('#search_name').val();
		if (e.keyCode == 13) return false;							/* if enter key - do not redirect */
    });
	$('#form_search_mr').bind("keypress", function(e){
		current_value = $('#search_mr').val();
		if (e.keyCode == 13) return false;							/* if enter key - do not redirect */
    });


	/* IF COPY/PASTE */
	$('#search_computer').bind('paste', function(e){
		$(this).trigger('keyup');
	});
	$('#search_name').bind('paste', function(e){
		$(this).trigger('keyup');
	});
	$('#search_mr').bind('paste', function(e){
		$(this).trigger('keyup');
	});
	
	/* IF KEYUP */
	$('#search_computer').keyup(function(e){
		process(e, "computer");
	});
	$('#search_name').keyup(function(e){
		process(e, "name");
	});
	$('#search_mr').keyup(function(e){
		process(e, "mr");
	});
	
	/* NAME PAGINATION */
	$('#pager_l_name').click(function(e){
		if ($(this).attr('disabled') != 'disabled') {
			$('#page_name').val(parseInt($('#page_name').val()) - 1);
			//$('#reset_page').val('0');
			process(e, "name");
		}
	});

	$('#pager_r_name').click(function(e){
		if ($(this).attr('disabled') != 'disabled') {
			$('#page_name').val(parseInt($('#page_name').val()) + 1);
			//$('#reset_page').val('0');
			process(e, "name");
		}
	});
	
	/* MR PAGINATION */
	$('#pager_l_mr').click(function(e){
		if ($(this).attr('disabled') != 'disabled') {
			$('#page_mr').val(parseInt($('#page_mr').val()) - 1);
			process(e, "mr");
		}
	});

	$('#pager_r_mr').click(function(e){
		if ($(this).attr('disabled') != 'disabled') {
			$('#page_mr').val(parseInt($('#page_mr').val()) + 1);
			process(e, "mr");
		}
	});
	
	/* MR Status Onchange */
	$('#search_mr_status').change(function() {
		if ($('#search_mr').val() != '') {
			$('#search_mr').trigger('keyup');
		}
	});

});

function process(e, search_tab_active) 
{
		search_keyword = $('#search_'+search_tab_active).val();
		search_length = search_keyword.length;

		idClicked = e.target.id;
		switch(search_tab_active) {
			case 'mr':
				if (search_length < 2 && search_length > 0) {
					return false;
				}
				search_now = true;
				break;
				
			case 'name':
				if (e.keyCode == 13 || e.keyCode == 8 || idClicked == 'pager_l_name' || idClicked == 'pager_r_name') {
					if (search_length < 2 && search_length > 0) {
						//alert("Please Input More Than 3 Letters and Press Enter.");
						//$('#search_'+search_tab_active).val('');
						return false;
					}
					search_now = true;
				}
				break;
	
			case 'computer':
				/*	START AUTO APPEND " - "	*/
				if ((search_length > 2 && search_length != 7) && e.keyCode != 8) {
				
					if (validateNumber(e) == false) {
						$('#search_'+search_tab_active).val(current_value);
						return false;
					} else if (search_length == 6) {
						$('#search_'+search_tab_active).val(search_keyword + '-');
					}
				} else if (search_length == 1 && e.keyCode != 8) {		/*	Auto append " - " if length 1 and if not barcode reader */
					if (e.which != 16) {
						$('#search_'+search_tab_active).val(search_keyword + '-');
					}
				} else if (search_length == 6 && e.keyCode != 8) {		/*	Auto append " - " if length 6 */
					$('#search_'+search_tab_active).val(search_keyword + '-');
				} else if ((search_length == 2 || search_length == 7) && e.keyCode != 8) {	/*	Force to replace any keyword to " - " if length 2 and 7 */
					$('#search_'+search_tab_active).val(current_value + '-');
				}
			/*	END AUTO APPEND " - "	*/
				
			if (search_length >= 9 && search_length <= 10) {
				search_now = true;
			}
			break;

			default:
		}
		
		if (search_now) {
			search_object = $.get(base_url_js()+'ajax/search_applicant/ajax_search?search_from=global', $("#form_search_"+search_tab_active).serialize(), function(data){
				var obj = jQuery.parseJSON(data);

				switch(search_tab_active) {
					case 'computer':
						$('#tableSearchResult').html(obj.html);
						break;
						
					case 'name':
						//alert(obj.html);
						$('#tableSearchResultName').html(obj.html);
						break;
						
					case 'mr':
						$('#tableSearchResultMR').html(obj.html);
						break;
				}
				
				if(obj.pageno > 0 && obj.total_found == 0){
					$('#pager_l_'+search_tab_active).trigger('click');						/* load previous page if query returns 0 */
				}

//				$('#load_img').hide();									/* hide loading image */
//				$('#dtl_cont').html(obj.html);							/* populate container with applicant list */
//				$('#html_remarks').show();								/* show remarks for button */
				
				/* show/hide pagination button */
				if(obj.total_found != 0){
					$('#pager_l_'+search_tab_active).show();
					$('#pager_r_'+search_tab_active).show();
					$('#txt_page_'+search_tab_active).show();
					$('#span_total_pages_'+search_tab_active).html(obj.record_end);		/* end count */
					$('#span_all_total_'+search_tab_active).html('<b>'+obj.all_total_found+'</b>');		/* all total count */
					$('#span_pageno_'+search_tab_active).html(obj.record_start);			/* start count */

					if($('#page_'+search_tab_active).val() == 0){
						$('#pager_l_'+search_tab_active).attr('disabled', 'disabled');	/* disable back button if page=1 */
					}else{
						$('#pager_l_'+search_tab_active).removeAttr('disabled');			/* enable back button */
					}

					if(obj.total_found < obj.page_limit){
						$('#pager_r_'+search_tab_active).attr('disabled', 'disabled');	/* disable next button if page=last */
					}else{
						$('#pager_r_'+search_tab_active).removeAttr('disabled');			/* enable next button */
					}
				} else {
					$('#pager_l_'+search_tab_active).hide();
					$('#pager_r_'+search_tab_active).hide();
					$('#txt_page_'+search_tab_active).hide();
				}
				/* end show/hide pagination button */

				//$('#reset_page').val('1');									/* allow reseting of page no */

				$('#search_'+search_tab_active).keydown(function(e){
					search_object.abort();
				});
			});
		}
}

function validateNumber(event) {
	var key = window.event ? event.keyCode : event.which;
		
	if (event.keyCode == 8 || event.keyCode == 46
	 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 13) {
		return true;
	} else if ( key >= 48 && key <= 57 ) {	/*	charCode 0-9 */
		return true;
	} else if ( key >= 96 && key <= 105 ) {	/* numpad 0-9 */
		return true;
	} else if ( key == 86 || key == 17) {	/*	charCode v and CTRL*/
		return true;
	} else {
		return false;
	}
};

