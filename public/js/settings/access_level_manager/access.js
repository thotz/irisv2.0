$(function () {
	$('.checkboxTitle').click(function(){
		if (typeof $(this).attr('parentid') !== typeof undefined && $(this).attr('parentid') !== false && $('#'+$(this).attr('parentid')).is(':not(:checked)')) {
			/*do not allow if parent checkbox is not yet checked*/
			return false;
		}else{
			if($(this).is(':checked')){
				var action = 'add';
			}else{
				var sub_menu = 0;
				$('input[parentid="'+$(this).attr('id')+'"]').each(function(){
					if($(this).is(':checked') == true){
						sub_menu += 1;
					}
				});

				if(sub_menu > 0){
					return false;
				}

				var action = 'remove';
			}
		}

		$.get(base_url_js()+'settings/access_level_manager/ajax_update_access?menu_id='+$(this).attr('menuinfo')+'&action='+action+'&what=', function(data){
//			alert(data);
			return true;
		});
	});

	$('.checkboxModify, .checkboxDelete').click(function(){
		var what = '';
		var action = '';

		if($('#'+$(this).attr('parentid')).is(':checked')){
			if($(this).is(':checked')){
				action = 'opt_add';
			}else{
				action = 'opt_remove';
			}

			if($(this).attr('class') == 'checkboxModify'){
				what = 'mod';
			}else{
				what = 'del';
			}
			
			$.get(base_url_js()+'settings/access_level_manager/ajax_update_access?menu_id='+$(this).attr('menuinfo')+'&action='+action+'&what='+what, function(data){
//				alert(data);
				return true;
			});
		}else{
			return false;
		}
	});

//	$('.checkboxModify, .checkboxDelete, .SubMenu').click(function(){
//		if($('#'+$(this).attr('parentid')).is(':checked')){
//			return true;
//		}else{
//			/*auto check parent menu if sub menu is checked*/
//			$('#'+$(this).attr('parentid')).attr('checked', true);
//			return true;
//		}
//	});
});