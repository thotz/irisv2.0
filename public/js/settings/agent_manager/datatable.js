        $(function () {
            $('#tableAgentList').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true,
                "columnDefs": [
                               { "orderable": false, "targets": [3,4,5,7,8,] },
                             ]
            });
        });