        $(function () {
            $('#tablePhotoshoot').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true,
                "columnDefs": [
                               { "orderable": false, "targets": [8,] },
                             ]
            });
        });