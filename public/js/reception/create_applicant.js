	$(function() {
		/* For RADIO and CHECKBOX design */
	    $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
	      checkboxClass: 'icheckbox_flat-blue',
	      radioClass: 'iradio_flat-blue'
	    });
	    
		$('#is_dependent').click(function(){
			if($('#is_dependent').is(':checked')){
				if(confirm('DEPENDENT BUTTON IS STRICTLY FOR DEPENDENT PROCESSING ONLY!')){
					return true;
				}else{
					$(this).attr('checked', false);
				}
			}
		});
		
		$('#mname').click(function() {
			check_duplicate_name();
		});
		
		$('body').keyup(function(e) {
			console.log('keyup called');
			var code = e.keyCode || e.which;
			idClicked = e.target.id;

			if (code == '9' && idClicked == 'mname') {
				check_duplicate_name();
			}
		 });
		
	    $('#address_city').change(function() {
	    	$.get(base_url_js()+'reception/create_applicant/ajax_nearest_branch/'+$(this).val(), function(data) {
	    		$('#branch_id1').html(data);
	    	});
	    });
	    
	    $('#cv_source').change(function() {
	    	$.get(base_url_js()+'reception/create_applicant/ajax_source_application/'+$(this).val(), function(data) {
	    		$('#source_id').html(data);
	    	});
	    	
	    	/* If Method is PRA - Show the PRA Form */
	    	chk_PRA(this);
	    });
	    
	    $('#source_id').change(function() {
	    	if($('#cv_source').val() == 'Direct' && $('#source_id').val() == 14){
	    		$('#myagent').show();
	    	}else if($('#cv_source').val() == 'PRA/Headhunting/Jobs Fair' && $('#source_id').val() == 14){
	    		$('#myagent').show();
	    	}else{
	    		$('#myagent').hide();
	    	}
	    	
	    	if($('#cv_source').val() == 'Referred' && $('#source_id').val() == 13){
	    		$('#refid').show();
	    	}
	    });
	    
	    $('#no_mname').on('ifChanged', function(event){
	    	  set_no_midle();
	    });
	        
	    $('#btnSubmit').click(function() {
	    	if ( check_required_field()) {

	    		  /* Processing */
	    		  $('#btnSubmit').hide();
	    		  $('#btnCancel').hide();
	    		  $('#on_processing_display').show();
	    		
	    		  /* Grab all form data */  
	    		  var formData = new FormData($('form#form_create_applicant')[0]);
	    		 
	    		  $.ajax({
		    		    url: base_url_js()+'reception/create_applicant/ajax_check_applicant_data',
		    		    type: 'POST',
		    		    data: formData,
		    		    async: false,
		    		    cache: false,
		    		    contentType: false,
		    		    processData: false,
		    		    success: function (res) {
		    		    	var obj = jQuery.parseJSON(res);
		    		    	
		    		    	if (obj.status == 'success') {
		    		    		$('#new_applicant_id').val(obj.applicant_id);
		    		    		
		    		    		/* Show popup message */
		    		    		jQuery.facebox({ajax:base_url_js()+"reception/create_applicant/ajax_popup_message?applicant_id="+obj.applicant_id+"&status="+obj.status+"&message="+obj.message});
		    		    	} else {
		    		    		if (obj.link) {
		    		    			alert(obj.message);
		    		    			
		    		    			/* Error Redirect */
		    		    			window.location = obj.link;
		    		    			
		    		    		} else {
									/* Processing */
									$('#btnSubmit').show();
									$('#btnCancel').show();
									$('#on_processing_display').hide();
									
									alert(obj.message); return false;
		    		    		}
		    		    	}
		    		    }
	    		  });
	    		 
	    		  return false;
	    	}
	    });
	    
	});
	
	function check_duplicate_name() {
		var lname = $('#lname').val();
		var fname = $('#fname').val();
		var searchbox = lname+','+fname;
		var pagename = 'follow_up.php';
		var param = 'applicant_id';
		var additionalparam = 'tab2=reportedtoday';
		var search_object = new Object();

		search_object = $.get('ajax_search.php?action=search_pop', {searchbox:searchbox,pagename:pagename,param:param,srch_for:'duplicate'}, function(data){
			var obj = jQuery.parseJSON(data);
			if (obj.total_found > 0) {
				jQuery.facebox({ajax:'search_applicant_duplicate.php?what=name&obj_html='+obj.html});
				//jQuery.facebox({ajax:'search_applicant_duplicate.php?pagename=<?=basename($_SERVER['PHP_SELF'])?>&param=applicant_id&additionalparam=tab2=reportedtoday&searchbox='+searchbox});
			}
		});
	}
	
	function check_availability(item) {
		var availability_val = $(item).val();
		
		$('#availability_month_number').attr({'disabled':''});
		$('#availability_month').attr({'disabled':''});
		$('#availability_year').attr({'disabled':''});
		switch(availability_val) {
			case "1":
				$('#availability_month_number').val('');
				$('#availability_month').val('');
				$('#availability_year').val('');
				
				$('#availability_month_number').attr({'disabled':'disabled'});
				$('#availability_month').attr({'disabled':'disabled'});
				$('#availability_year').attr({'disabled':'disabled'});
				break;
			case "2":
				$('#availability_month').val('');
				$('#availability_year').val('');
				
				$('#availability_month').attr({'disabled':'disabled'});
				$('#availability_year').attr({'disabled':'disabled'});
				break;
				
			case "3":
				$('#availability_month_number').val('');
				
				$('#availability_month_number').attr({'disabled':'disabled'});
				break;
				
			default:
				
		}
	}
	
	function set_no_midle() 
	{
		if ($("#no_mname").is(':checked') == true) {
			$("#mname").val('');
			$("#mname").attr('disabled','disabled');
		} else {
			$("#mname").removeAttr('disabled');
		}
	}
	
	/* compute applicant's age DoB = YYYY-MM-DD */
	function get_age(dob) {
		dob = new Date(dob);
		var today = new Date();
		var age = Math.floor((today-dob) / (365.25 * 24 * 60 * 60 * 1000));
		return age;
	}
	
	function chk_PRA(obj){
		if($(obj).val() == 'PRA/Headhunting/Jobs Fair'){
			$('#mypra, #mypra2').show();
			$('#is_walkin0').attr('checked', true);
		}else{
			$('#mypra, #mypra2').hide();
			$('#is_walkin0').attr('checked', false);
		}
	}

	function check_required_field() {
		/* required if new computer or deployed */
		if ($('#old_applicant_id').val() == '' || ($('#old_applicant_id').val() && ($('#action').val() == 'add_deployed' || $('#action').val() == 'add_deadfile'))) {
			/* start checking for Manpower Position */
				var mr_positions_array = [];
				var has_mr_position_array = [];
				$("select[name='mr_positions[]']").each(function () {
					mr_positions_array.push(this.value);
					
					/* For Required Checking : stored manpower positions has value */
					if (this.value) {
						has_mr_position_array.push(this.value);
					}
				});
			/* start checking for Manpower Position */
			
			/* start checking for Prefered Position and Job Specialization */
				var position_array = [];
				var jobspec_array = [];
				var has_position_array = [];
				$("select[name='position_ids[]']").each(function () {
					position_array.push(this.value);
					
					/* For Required Checking : stored positions has value */
					if (this.value) {
						has_position_array.push(this.value);
					}
				});
				$("select[name='jobspec_ids[]']").each(function () {
					jobspec_array.push(this.value);
				});
				
				/* for required Preferred Position - not null */
				if (has_position_array.length == 0 && has_mr_position_array.length == 0) {
					alert("Please select atleast one of Prefered Position/Company Applied Position.");
					return false;
				}
				
			/* end checking for Prefered Position and Job Specialization */
		}

		if($('#lname').val() == '') {
			$("#lname").focus();
			alert('Last Name is required.');
			return false;
		}
		if($('#fname').val() == '') {
			$("#fname").focus();
			alert('First Name is required.');
			return false;
		}
		if($('#mname').val() == '') {
			if ($("#no_mname").is(':checked') != true) {
				$("#mname").focus();
				alert('Middle Name is required.');
				return false;
			}
		}

		if ($('#source_id').val() == '13' || $('#source_id').val() == '28') {
			/* Source of Application is [ Referred by Company and Referred for Processing ]*/
			
		} else {
			/* validate age */
			var calday = $('#bday_day').val();
			var calmon = $('#bday_month').val();
			var calyear = $('#bday_year').val();

			if((calyear == "" || calmon == "" || calday == "") && $('#cv_source').val() != 'PRA/Headhunting/Jobs Fair'){
				/*disregard bday validation if source = PRA [id:4]*/
				alert("Birthdate is required.");
				return false;
			}else{
				var dob = calyear+'-'+calmon+'-'+calday;
				var age = get_age(dob);
				var min_age = "21";
				var max_age = "59";
				
				/*VALIDATE AGE IF NOT DEPENDENT*/
				if($('#is_dependent').is(':checked') == false){
					if(age < min_age){
						alert('APPLICATION NOT ACCEPTED DUE TO UNDERAGE.  INFORM THE APPLICANT AND CANCEL HIS APPLICATION.\n\nIF HE IS APPLYING FOR DEPENDENT VISA, PLEASE CHECK THE DEPENDENT BUTTON NEAR THE BIRTHDATE ENTRY.');
						return false;
					}else if(age > max_age){
						alert('APPLICATION NOT ACCEPTED DUE TO OVERAGE.  INFORM THE APPLICANT AND CANCEL HIS APPLICATION.\n\nIF HE IS APPLYING FOR DEPENDENT VISA, PLEASE CHECK THE DEPENDENT BUTTON NEAR THE BIRTHDATE ENTRY.');
						return false;
					}
				}
			}

			/* PRA */
			if($('#cv_source').val() == 'PRA/Headhunting/Jobs Fair'){
				if($('#pra_city_id').val() == '') {
					$("#pra_city_id").focus();
					alert('PRA City is required.');
					return false;
				}	
				if($('#pra_user_id').val() == '') {
					alert('PRA Leader is required.');
					return false;
				}
				
				if($('#pra_date_day').val() == "" || $('#pra_date_month').val() == "" || $('#pra_date_year').val() == ""){
					alert("PRA Date is required.");
					return false;
				}
			}
			
			//if(document.thisonly.sex[0].checked == false && document.thisonly.sex[1].checked == false){
			if ($('input[name="sex"]:checked').length == 0) {
				alert('Gender is required.');
				return false;
			}

			if($('#cellphone').val() == '') {
				$("#cellphone").focus();
				alert('Mobile Number is required.');
				return false;
			} else {
				valid_mob = new RegExp('^0[0-9]{10}');
				if(valid_mob.test($('#cellphone').val()) == false || $('#cellphone').val().length > 11 || $('#cellphone').val().length < 11){
					alert('Invalid Mobile No. Primary Number should start with 0 and should be 11 characters long.');
					$('#cellphone').focus();
					return false;
				}
			}
			
			if($('#address1').val() == '') {
				$("#address1").focus();
				alert('Present Address is required.');
				return false;
			}
			if($('#address_city').val() == '') {
				$("#address_city").focus();
				alert('Municipality is required.');
				return false;
			}
			
			if($('#cv_source').val() == '') {
				$("#cv_source").focus();
				alert('Method of Application is required.');
				return false;
			}	

			if($('#source_id').val() == '' && $('#cv_source').val() != 'PRA/Headhunting/Jobs Fair') {
				$("#source_id").focus();
				alert('Source is required.');
				return false;
			}
			
			if($('#source_id').val() == '14' && ($('#cv_source').val() == 'Direct' || $('#cv_source').val() == 'PRA/Headhunting/Jobs Fair') ) {
				if ($('#agent_id').val() == '') {
					$("#agent_id").focus();
					alert('Agent is required.');
					return false;					
				}
			}
			
			/*  start Work Availability  */
				if ( ! $('input:radio[name="availability"]').is(':checked')) {
					alert("Work Availability is Required.");
					return false;
				} else {
					var availability_val = $('input:radio[name="availability"]:checked').val();
					if (availability_val == 1) {
						$('#work_availability').val(availability_val);
					} else if (availability_val == 2) {
						if ($('#availability_month_number').val() == '') {
							alert("Please select how many months, after your notice.");
							return false;
						}
						$('#work_availability').val(availability_val + ":" + $('#availability_month_number').val());
					} else if (availability_val == 3) {
						if ($('#availability_month').val() == '' || $('#availability_year').val() == '') {
							alert("Please select month and year, for your next job.");
							return false;
						}
						$('#work_availability').val(availability_val + ":" + $('#availability_month').val() + ":" + $('#availability_year').val());
					} else {
						alert("Work Availability is Required.");
						return false;
					}
				}

			/* end Work Availability  */

			/*	Applicant CV required only if
				- not PRA
			*/
			if($('#cv_source').val() != 'PRA/Headhunting/Jobs Fair'){
				/*	Applicant CV required only if
					- not deployed from followup (reported today)
					- not deadfile from followup (reported today)
				*/
				if ($('#action').val() == '') {
					if($('#cv_applicant').val() == '' && $('#cv_applicant_pdf').val() == '' && $('#cv_source').val() != 'PRA/Headhunting/Jobs Fair') {
						/*disregard cv/word validation if source = PRA [id:4]
						OR
							is_walkin0 is checked */
						if ($('#is_walkin0').is(":checked") == false) {
							$("#cv_applicant").focus();
							alert('Applicant CV (Word/PDF) is required.');
							return false;
						}
					}
				}
			}
			/* end validate age */

			/*VALIDATE IF WALKIN OR ISSUANCE ONLY*/
			if($('#is_walkin0').is(":checked") == false && $('#is_walkin1').is(":checked") == false){
				alert('Please select whether the applicant is Walk-in or Issuance of Computer No. only');
				return false;
			}
		}
		
		if (confirm('Are you sure you want to update?')) {
			return true;
		} else {
			return false;
		}
	}