	var count_position = 0;
	var count_education = 0;
	var count_licenses = 0;
	var count_training = 0;
	var count_tradetest = 0;
	var count_beneficiaries = 0;
	var count_employment = 0;
	var count_cvlocation = 0;
	var count_general = 0;
	
	var referral = "";
	var item = "";
	var id ="";
	
	
	$(function() {
		/* For RADIO and CHECKBOX design */
	    $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
	      checkboxClass: 'icheckbox_flat-blue',
	      radioClass: 'iradio_flat-blue'
	    });
	    
		//var assessments = ["prefered","position","education","licenses","training","tradetest","beneficiaries","employment","general"];
		var assessments = ["education","employment","licenses","training","tradetest","beneficiaries","prefered","position","cvlocation","general"];
		//var assessments = ["education","employment"];
		for (var i = 0; i < assessments.length; i++) {
			load_assessments_lists(assessments[i]);
		}
		
		//disabled_item_skills();
		

	});
	
	function testfacebox()
	{
		jQuery.facebox('test');
	}


	function load_assessments_lists(item) {
		var content_id = "assessment_"+item+"_view";
		var action = "ajax_assessment_"+item+"_view";
		$("#"+content_id).load(base_url_js()+"reception/assessment/"+action+"?applicant_id="+applicant_id,function(response,status,xhr){
			if (status=="success") {
				var obj = jQuery.parseJSON(response);
				$(this).html(obj.html);
				
				/* Show Logs */
				$('#datalogs-'+item).html(obj.logs);
				
				set_count_assessment(item, obj.count);
			} else {
				$(this).html("An error occured: <br/>" + xhr.status + " " + xhr.statusText)
			}
		});
	}
	
	function popup_recruitment_assessment(item, applicant_id, id) {
		item = item;
		id = id;
		
		if (item) {
			if (my_javascript_data.assessment_from == 'walkin') {		/* For Walkin applicant it should be in the office - require to update the TRAINING AND TRADETEST */
				switch (item) {
					case 'tradetest':
						var what = "training";
						var error_msg = "Please update first TRAINING/SEMINARS and TRADETEST.";
						break;
						
					case 'prefered':
					case 'position':
					case 'cvlocation':
					case 'general':
						var what = "tradetest";
						var error_msg = "Please update first TRAINING/SEMINARS and TRADETEST.";
						break;
					default:
						var what = "";
						var error_msg = "error on popup.";
				}
				
				if (what) {
					$.post(base_url_js()+"reception/assessment/ajax_check_applicant_assessment?action=check_applicant_assessment&assessment_from="+my_javascript_data.assessment_from, {table:what, applicant_id:applicant_id}, function(data){
						if (data) {
							javascript: jQuery.facebox({ajax:base_url_js()+"reception/assessment_facebox/ajax_main?action="+item+"&applicant_id="+applicant_id+"&id="+id});
						} else {
							alert(error_msg);
						}
					});
				} else {
					javascript: jQuery.facebox({ajax:base_url_js()+"reception/assessment_facebox/ajax_main?action="+item+"&applicant_id="+applicant_id+"&id="+id});
				}
			} else {
				/*	For Online -  no restriction for now. */
				switch (item) {
					case 'prefered':
						var what = "cv_location";
						var error_msg = "Please complete the Interview Status.";
						break;
					case 'position':
						var what = "prefered_position";
						var error_msg = "Please complete the Prefered Position.";
						break;
					case 'education':
						var what = "apply_applicant";
						var error_msg = "Please complete the Position Applied.";
						break;
					case 'employment':
						var what = "education";
						var error_msg = "Please complete the education.";
						break;
					case 'licenses':
						var what = "employment";
						var error_msg = "Please complete the workhistory.";
						break;
					case 'training':
						var what = "licenses";
						var error_msg = "Please complete the licenses.";
						break;
					case 'tradetest':
						var what = "training";
						var error_msg = "Please complete the training.";
						break;
					case 'beneficiaries':
						var what = "tradetest";
						var error_msg = "Please complete the tradetest.";
						break;
					case 'general':
						var what = "beneficiaries";
						var error_msg = "Please complete the beneficiaries.";
						break;
					default:
						var what = "";
						var error_msg = "error on popup.";
				}

				javascript: jQuery.facebox({ajax:base_url_js()+"reception/assessment_facebox/ajax_main?action="+item+"&applicant_id="+applicant_id+"&id="+id});

			}
		} else {
			alert("Error.");
		}
	}
	
	function delete_recruitment_assessment(action, applicant_id, id) {
		$result = confirm('Are you sure you want to delete this?');
		if ($result) {
			switch (action) {
				case 'prefered':
					var what = "rec_prefered_position";
					break;
				case 'position':
					var what = "rec_position_applied";
					break;
				case 'education':
					var what = "rec_education";
					break;
				case 'licenses':
					var what = "rec_licenses";
					break;
				case 'training':
					var what = "rec_training";
					break;
				case 'tradetest':
					var what = "rec_tradetest";
					break;
				case 'beneficiaries':
					var what = "rec_beneficiaries";
					break;
				case 'employment':
					var what = "rec_employment";
					break;
				case 'cvlocation':
					var what = "rec_cvlocation";
					break;
				case 'general':
					var what = "rec_general";
					break;
				default:
					var what = "";
			}
			
			if (what) {
				$.post(base_url_js()+'reception/assessment_delete/ajax_'+action+'?what='+what+'&id='+id+'&applicant_id='+applicant_id, function(data) {
					var response = jQuery.parseJSON(data);
					if (response.status == 'Success') {
						load_assessments_lists(action);
					}
					alert(response.message);
					return false;
				});
			} else {
				alert("error on delete.");
			}
		}
	}
	
	function set_count_assessment(item, value) {
		switch (item) {
			case 'prefered':
			
				break;
			case 'position':
				count_position = value;
				break;
			case 'education':
				count_education = value;
				break;
			case 'licenses':
				count_licenses = value;
				break;
			case 'training':
				count_training = value;
				break;
			case 'tradetest':
				count_tradetest = value;
				break;
			case 'beneficiaries':
				count_beneficiaries = value;
				break;
			case 'employment':
				count_employment = value;
				break;
			case 'cvlocation':
				count_cvlocation = value;
				break;
			case 'general':
				count_general = value;
				break;
			default:

		}
		
	}
	
	function for_confirmation_box(applicant_id, lineup_id, set_to="") {
		if (applicant_id && lineup_id) {
			jQuery.facebox({ajax:'lineup_for_confirmation.php?applicant_ids='+applicant_id+'&lineup_ids='+lineup_id+'&set_to='+set_to+'&databank=true'});
		}
	}
	
	function for_confirmation_close(item) {
		if ($(item).attr('title')) {
			load_assessments_lists('position');
		}
		jQuery(document).trigger('close.facebox'); return false;
	}
	
	function assessment_show_logs(item)
	{
    	$('ul#datalogs-'+item+' li').show();
        $('.hide-logs-'+item).show();
        $('.show-logs-'+item).hide();
	}
	
	function assessment_hide_logs(item)
	{
    	$('ul#datalogs-'+item+' li').hide();
        $('.show-logs-'+item).show();
        $('.hide-logs-'+item).hide();
	}
	
	function toggle_Advisory(txtA_id, mode, what, btn_more, btn_less){
		if(mode==1){
			var vis = "show";
		}else{
			var vis = "hide";
		}

		if(what==1){
			/*details table*/
			if (mode==1){
				$(btn_more).hide();
				$(btn_less).show();
			}else{
				$(btn_more).show();
				$(btn_less).hide();
			}
		}else{
			/* clear all textarea*/
			$('textarea').val('');
			$('.counter_cont').html('');
		}

		$(txtA_id).animate({opacity: vis});
	}